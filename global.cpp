#include "global.h"
#include <QProcess>
#include <QtCore/qdebug.h>

int currentWriteTask = 0;

void setWrite(){
    currentWriteTask++;
    #ifdef Q_OS_LINUX
    QProcess process1;
    process1.start("rw");

    if(!process1.waitForFinished()){
       return;
    }
    #endif
}

void setReadOnly(){
    currentWriteTask--;
    if(currentWriteTask < 1){
        currentWriteTask = 0;
        #ifdef Q_OS_LINUX
        QProcess process2;
        process2.start("ro");

        if(!process2.waitForFinished()){
           return;
        }
        #endif
    }
}

QString toCamelCase(const QString& s)
{
    QStringList parts = s.split(' ', QString::SkipEmptyParts);
    for (int i = 0; i < parts.size(); ++i)
        parts[i].replace(0, 1, parts[i][0].toUpper());

    return parts.join(" ");
}

QString toTimeString(const unsigned int s)
{
    QString stime = "";
    unsigned int t = s;
    unsigned int h = t / (60*60*1000);
    if(h > 0){
        stime = QString::number(h);
        t -= h*(60*60*1000);
    }

    unsigned int m = t / (60*1000);
    if(stime != "") stime += ":";
    if(m > 0){
        if(m < 10) stime += "0";
        stime += QString::number(m);
        t -= m*(60*1000);
    }else{
        stime += "00";
    }

    unsigned int ss = t / (1000);
    if(stime != "") stime += ":";
    if(ss > 0){
        if(ss < 10) stime += "0";
        stime += QString::number(ss);
    }else{
        stime += "00";
    }

    return stime;
}

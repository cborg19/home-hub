#ifndef SYDNEYTRAINS_H
#define SYDNEYTRAINS_H

#include <QObject>
#include <QQmlListProperty>
#include <QThreadPool>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDateTime>
#include <QNetworkReply>
#include <QMutex>

class AsyncTrainResponse : public QObject, public QRunnable
{
    Q_OBJECT

    Q_PROPERTY(QVariantList list READ getList NOTIFY listChanged)

public:
    AsyncTrainResponse(QString type, QString from, QString to, QDateTime t, bool voice)
        : m_type(type), m_from(from), m_to(to), m_time(t), m_voice(voice)
    {

    }

    ~AsyncTrainResponse();

    void run() override;

    QVariantList getList() { return m_list; }
signals:
    void listChanged(QVariantList, QString);
private:
    QString m_type, m_from, m_to;
    QDateTime m_time;
    QVariantList m_list;
    bool m_voice;

    QNetworkAccessManager *manager;
    QNetworkRequest request;
    QNetworkReply *reply;

    void parseTimeTable(QJsonDocument doc, QString &);
    QString getDisassembledName(QJsonObject);
    QString getPlatform(QJsonObject);
};

class SydneyTrains : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList sydneyTrainList READ getTrainList NOTIFY trainListChanged)
public:
    explicit SydneyTrains(QObject *parent = nullptr);

    QVariantList getTrainList() { return i_list; }

    void setSettings(QString, QString);
    void NextDefaultTrains();
    void NextTrains(QString from, QString to, bool voice = false);

    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void trainListChanged();

    void pageTrigger(QString page, int index);
    void assistantVoice(QString);
private slots:
    void listChanged(QVariantList, QString);
private:
    QString m_defaultFrom, m_defaultTo;

    QThreadPool pool;
    QVariantList i_list;

    QMutex l_list;

    QNetworkAccessManager *manager;
    QNetworkRequest request;
    QNetworkReply *reply;

    QString lookUpStation(QString);
};

#endif // SYDNEYTRAINS_H

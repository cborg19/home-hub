import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle {
        color: "transparent"
    }

    Rectangle {
       x: 20
       y: 10
       width: 760
       height: 125
       visible: controller.kodiCurrentMetaData.label!==undefined?true:false

       color: Qt.hsla(0,0,0,0.4)

       RowLayout {
           spacing: 0
           Rectangle {
               x: 2
               y: 2
               width: 100
               height: 105
               color: "black"
               Image {
                   id: imgPoster
                   anchors.fill: parent
                   fillMode: Image.PreserveAspectCrop
                   source: controller.kodiCurrentMetaData.fanImage !== undefined?"image://imageprovider/"+controller.kodiCurrentMetaData.fanImage:"" //
               }
           }

           ColumnLayout {
               spacing: 0
               Label {
                   text: controller.kodiCurrentMetaData.label!==undefined?controller.kodiCurrentMetaData.label:""
                   color: "white"
                   font.pointSize: 26
                   font.family: "Helvetica"
               }
               Label {
                   text: kodiSwipeView.getShowTitle()
                   color: "white"
                   font.pointSize: 20
                   font.family: "Helvetica"
               }

               RowLayout {
                   spacing: 50
                   Rectangle {
                       width: 50
                       color: "transparent"
                   }

                   Rectangle {
                       id: btFastBackwards
                       width: 40
                       height: 40
                       radius: 20
                       color: "#ce6ddf"
                       Text {
                           color: "white"
                           anchors.centerIn: parent
                           font.pointSize: 20
                           font.family: fontAwesome.name
                           text: FontAwesome.icons.fa_fast_backward
                       }
                       MouseArea {
                           anchors.fill: parent
                           onClicked: {
                                controller.keyPress(Qt.Key_AudioRewind)
                           }
                       }
                   }
                   Rectangle {
                       id: btPause
                       width: 40
                       height: 40
                       radius: 20
                       color: "#ce6ddf"
                       Text {
                           color: "white"
                           anchors.centerIn: parent
                           font.pointSize: 20
                           font.family: fontAwesome.name
                           text: controller.kodiPaused?FontAwesome.icons.fa_play:FontAwesome.icons.fa_pause
                       }
                       MouseArea {
                           anchors.fill: parent
                           onClicked: {
                               if(controller.kodiPaused){
                                   controller.keyPress(Qt.Key_MediaPlay)
                               }else{
                                   controller.keyPress(Qt.Key_MediaPause)
                               }
                           }
                       }
                   }
                   Rectangle {
                       id: btStop
                       width: 40
                       height: 40
                       radius: 20
                       color: "#ce6ddf"
                       Text {
                           color: "white"
                           anchors.centerIn: parent
                           font.pointSize: 20
                           font.family: fontAwesome.name
                           text: FontAwesome.icons.fa_stop
                       }
                       MouseArea {
                           anchors.fill: parent
                           onClicked: {
                                controller.keyPress(Qt.Key_MediaStop)
                           }
                       }
                   }
                   Rectangle {
                       id: btFastForward
                       width: 40
                       height: 40
                       radius: 20
                       color: "#ce6ddf"
                       Text {
                           color: "white"
                           anchors.centerIn: parent
                           font.pointSize: 20
                           font.family: fontAwesome.name
                           text: FontAwesome.icons.fa_fast_forward
                       }
                       MouseArea {
                           anchors.fill: parent
                           onClicked: {
                                controller.keyPress(Qt.Key_AudioForward)
                           }
                       }
                   }
               }
           }
       }
    }

    Item {
        x: 280
        y: 150

        ColumnLayout{
            spacing: 15
            RowLayout {
                spacing: 15
                Rectangle {
                    id: btContextMenu
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_clipboard
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Menu)
                        }
                    }
                }
                Rectangle {
                    id: btUp
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_caret_up
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Up)
                        }
                    }
                }
                Rectangle {
                    id: btInfo
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_info_circle
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Info)
                        }
                    }
                }
            }
            RowLayout {
                spacing: 15
                Rectangle {
                    id: btLeft
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_caret_left
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Left)
                        }
                    }
                }
                Rectangle {
                    id: btSelect
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_th_large
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Enter)
                        }
                    }
                }
                Rectangle {
                    id: btRight
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_caret_right
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Right)
                        }
                    }
                }
            }
            RowLayout {
                spacing: 15
                Rectangle {
                    id: btBack
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_arrow_circle_left
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Back)
                        }
                    }
                }
                Rectangle {
                    id: btDown
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_caret_down
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Down)
                        }
                    }
                }
                Rectangle {
                    id: btShowOSD
                    width: 60
                    height: 60
                    radius: 30
                    color: "#ce6ddf"
                    Text {
                        color: "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_th
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.keyPress(Qt.Key_Documents)
                        }
                    }
                }
            }
        }
    }

    RowLayout {
        x: 260
        y: 400
        spacing: 10

        Rectangle {
            id: btHome
            width: 50
            height: 50
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_home
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    //controller.keyPress(Qt.Key_AudioRewind)
                }
            }
        }
        Rectangle {
            id: btFilm
            width: 50
            height: 50
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_film
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_F1)
                }
            }
        }
        Rectangle {
            id: btTv
            width: 50
            height: 50
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_desktop
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_F2)
                }
            }
        }
        Rectangle {
            id: btMusic
            width: 50
            height: 50
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_headphones
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_F3)
                }
            }
        }
        Rectangle {
            id: btImage
            width: 50
            height: 50
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_image
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                  //  controller.keyPress(Qt.Key_F4)
                }
            }
        }
    }
}

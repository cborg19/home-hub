import QtQuick 2.0
import QtQuick.Controls 2.4

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Image {
        anchors.fill: parent
        source: "files/icon/FoodBackground.jpg"
        fillMode: Image.PreserveAspectCrop
    }

    function showCategoryList(){
        listCategory.visible = true
        listRecipes.visible = false
    }

    function showRecipeList(){
        console.log("showRecipeList")
        listCategory.visible = false
        listRecipes.visible = true
    }

    Component {
        id: delegateCategory
        Item {
            width: listCategory.cellWidth
            height: listCategory.cellHeight


            Rectangle {
                anchors.centerIn: parent
                width: 110
                height: 130
                color: "white"
                radius: 10
                Column {
                    anchors.fill: parent
                    spacing: 10
                    Image {
                        width: 90
                        height: 90
                        source: controller.recipeCategoryList[index].thumbnail
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Label {
                        width: 90
                        height: 20
                        text: controller.recipeCategoryList[index].title
                        color: "black"
                        font.pointSize: 10
                        font.family: "Helvetica"
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Label.AlignHCenter
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    listCategory.visible = false
                    controller.recipeGetCategory(controller.recipeCategoryList[index].id.toString())
                    listRecipes.visible = true
                }
            }
        }
    }

    GridView {
        id: listCategory
        x: 20
        y: 20
        width: imageWidth - 40
        height: imageHeight - 40

        cellWidth: 120
        cellHeight: 140
        model: controller.recipeCategoryList
        delegate: delegateCategory

        Text {
            x: 720
            y: 0
            z: 3
            id: btBack
            color: "white"
            font.pointSize: 25
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_arrow_left
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    recipeList.visible = false
                    btMenu.visible = true;
                }
            }
        }

        Text {
            x: 720
            y: 40
            z: 3
            id: btExit
            color: "white"
            font.pointSize: 25
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_times_circle
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.switchToMenu(true)
                }
            }
        }
    }

    Component {
        id: delegateRecipes
        Item {
            width: listRecipes.cellWidth
            height: listRecipes.cellHeight


            Rectangle {
                anchors.centerIn: parent
                width: 110
                height: 130
                color: "white"
                radius: 10
                Column {
                    anchors.fill: parent
                    Image {
                        width: 90
                        height: 90
                        source: controller.recipeList[index].thumbnail
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Label {
                        width: 90
                        height: 20
                        text: controller.recipeList[index].title
                        color: "black"
                        font.pointSize: 10
                        font.family: "Helvetica"
                        wrapMode: Text.WordWrap
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    //listRecipes.visible = false
                    controller.recipeGetRecipe(index);
                    recipeList.visible = false
                    //recipeShow.visible = true

                    recipeShow.load();
                }
            }
        }
    }

    GridView {
        id: listRecipes
        x: 20
        y: 20
        width: imageWidth - 40
        height: imageHeight - 40
        visible: false;

        cellWidth: 120
        cellHeight: 140
        model: controller.recipeList
        delegate: delegateRecipes

        Text {
            x: 720
            y: 0
            z: 3
            id: btBackg
            color: "white"
            font.pointSize: 25
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_arrow_left
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    listRecipes.visible = false
                    listCategory.visible = true 
                }
            }
        }

        Text {
            x: 720
            y: 40
            z: 3
            id: btExitAgain
            color: "white"
            font.pointSize: 25
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_times_circle
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.switchToMenu(true)
                }
            }
        }
    }
}

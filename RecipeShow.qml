import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.2
import QtMultimedia 5.8

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    id: recipeDetail
    width: imageWidth
    height: imageHeight

    Item{
        id: videoFrame
        x: 0
        y: 0
        width: imageWidth
        height: imageHeight
        z: 3
        visible: false

        Rectangle {
            anchors.fill: parent
            color: "black"
        }
    }

    function pageDown(){
        var y = flick.contentY;
        y += imageHeight;
        if(y > flick.contentHeight - imageHeight)
            y = flick.contentHeight - imageHeight
        flick.contentY = y
    }

    function pageUp(){
        var y = flick.contentY;
        y -= imageHeight;
        if(y < 0)
            y = 0
        flick.contentY = y
    }


    background: Image {
        id: backImage
        z: 1
        anchors.fill: parent
        source: "files/icon/FoodBackgroundBorder.png"
        fillMode: Image.PreserveAspectCrop

        Text {
            x: 740
            y: 1
            z: 2
            id: btBackg
            color: "white"
            font.pointSize: 25
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_arrow_left
            MouseArea {
                anchors.fill: parent
                onClicked: {
//                    recipeShow.visible = false
                    recipeList.visible = true
                    recipeDetail.destroy();
                }
            }
        }

        Text {
            x: 30
            y: 1
            z: 3
            id: btExit
            color: "white"
            font.pointSize: 25
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_times_circle
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    window.switchToMenu(true)
                }
            }
        }
    }

    Rectangle {
        width: imageWidth - 60
        height: imageHeight
        x: 30
        y: 30
        color: "white"

        //ScrollView {
         //   anchors.fill: parent
         //   ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
         //   ScrollBar.vertical.policy: ScrollBar.AlwaysOn

            Flickable {
                id: flick
                contentHeight: flick.trueHeight
                contentWidth: parent.width
                anchors.fill: parent

                property int trueHeight: 360 + swipeRecipeDetail.height

                Label {
                    x: 10
                    y: 10
                    width: 350
                    height: 20
                    text: controller.recipeCurrent.length > 0? controller.recipeCurrent[0].title : ""
                    color: "black"
                    font.pointSize: 20
                    font.family: "Helvetica"
                    wrapMode: Text.WordWrap
                }

                Item {
                    id: details
                    x: 10
                    y: 60
                    width: 300
                    height: 180

                    function getPep(){
                        if(controller.recipeCurrent.length === 0) return "";
                        if(controller.recipeCurrent[0].prep === undefined) return ""
                        return controller.recipeCurrent[0].prep
                    }

                    function getCook(){
                        if(controller.recipeCurrent.length === 0) return "";
                        if(controller.recipeCurrent[0].cook === undefined) return ""
                        return controller.recipeCurrent[0].cook
                    }

                    function getCool(){
                        if(controller.recipeCurrent.length === 0) return "";
                        if(controller.recipeCurrent[0].cool === undefined) return ""
                        return controller.recipeCurrent[0].cool
                    }

                    function getPeople(){
                        if(controller.recipeCurrent.length === 0) return "";
                        if(controller.recipeCurrent[0].people === undefined) return ""
                        return controller.recipeCurrent[0].people
                    }

                    function getRating(){
                        if(controller.recipeCurrent.length === 0) return -1;
                        if(controller.recipeCurrent[0].rating === undefined) return -1
                        return controller.recipeCurrent[0].rating
                    }

                    Column{
                        spacing: 5
                        Rectangle {
                            height: 140
                            width: 330
                            radius: 20
                            border.color: "#d4d4d4"
                            border.width: 1
                            Item {
                                x: 10
                                y: 19
                                Column {
                                    Layout.topMargin: 10
                                    spacing: 20
                                    Label {
                                        text: "Prep"
                                        color: "black"
                                        font.pointSize: 10
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                    Label {
                                        text: details.getPep()
                                        color: "black"
                                        font.pointSize: 16
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                }
                            }
                            Item {
                                x: 90
                                y: 19
                                Column {
                                    Layout.topMargin: 10
                                    spacing: 20
                                    Label {
                                        text: "Cook"
                                        color: "black"
                                        font.pointSize: 10
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                    Label {
                                        text: details.getCook()
                                        color: "black"
                                        font.pointSize: 16
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                }
                            }
                            Item {
                                x: 165
                                y: 19
                                Column {
                                    Layout.topMargin: 10
                                    spacing: 20
                                    Label {
                                        text: "Cool"
                                        color: "black"
                                        font.pointSize: 10
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                    Label {
                                        text: details.getCool()
                                        color: "black"
                                        font.pointSize: 16
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                }
                            }
                            Item {
                                x: 245
                                y: 19
                                Column {
                                    Layout.topMargin: 10
                                    spacing: 20
                                    Label {
                                        text: "People"
                                        color: "black"
                                        font.pointSize: 10
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                    Label {
                                        text: details.getPeople()
                                        color: "black"
                                        font.pointSize: 16
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                }
                            }

                            Item{
                                x: 90
                                y: 105
                                Row {
                                    spacing: 5
                                    Repeater {
                                        model: 5;
                                        delegate: Text {
                                            visible: index <= details.getRating() && index !== -1? true : false
                                            color: "#e7b00c"
                                            font.pointSize: 20
                                            font.family: fontAwesome.name
                                            text: FontAwesome.icons.fa_star
                                        }
                                    }
                                }
                            }
                        }
                    }
                }       

                Image {
                    id: imgAuthor
                    x: 380
                    y: 10
                    width: 350
                    height: 200
                    Loader{
                        anchors.fill: parent
                        id: imageLoader
                        asynchronous: true
                        visible: status == Loader.Ready
                        sourceComponent: mainImage
                    }

                    Component {
                        id: mainImage
                        Image{
                            visible: imageLoader.status == Loader.Ready
                            source: imgAuthor.getImage()
                            height: imgAuthor.height
                            width: imgAuthor.width
                            anchors.horizontalCenter: imgAuthor.horizontalCenter
                            anchors.verticalCenter: imgAuthor.verticalCenter
                            Component.onCompleted: {
                                thumbImage.visible = false
                            }
                        }
                    }

                    function getThumbnail(){
                        if(controller.recipeCurrent.length === 0) return "";
                        if(controller.recipeCurrent[0].thumbnail === undefined) return ""

                        return controller.recipeCurrent[0].thumbnail
                    }

                    function getImage(){
                        if(controller.recipeCurrent.length === 0) return "";
                        if(controller.recipeCurrent[0].image === undefined) return ""
                        return controller.recipeCurrent[0].image
                    }

                    function hasVideo(){
                        if(controller.recipeCurrent.length === 0) return false;
                        if(controller.recipeCurrent[0].video === undefined) return false
                        if(controller.recipeCurrent[0].video === "") return false
                        return true
                    }

                    function getVideo(){
                        if(controller.recipeCurrent.length === 0) return "";
                        if(controller.recipeCurrent[0].video === undefined) return ""
                        return controller.recipeCurrent[0].video
                    }

                    /*Image{
                        visible: true
                        source: imgAuthor.getImage()
                        height: parent.height
                        width: parent.width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                    }*/

                    Image{
                        id: thumbImage
                        //visible: imageLoader.status !== Loader.Ready // || !imgAuthor.hasVideo()
                        source: imgAuthor.getThumbnail()
                        height: parent.height
                        width: parent.width
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    Text {
                        visible: imgAuthor.hasVideo()
                        anchors.centerIn: parent
                        color: "white"
                        font.pointSize: 40
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_video_camera
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(imgAuthor.hasVideo()){
                                videofile.visible = true
                                //imageLoader.visible = false
                                videofile.play();
                            }
                        }
                    }

                    Video {
                        id: videofile
                        anchors.fill: imgAuthor
                        autoLoad: false
                        autoPlay: false
                        volume: window.volume
                        source: imgAuthor.getVideo()
                        visible: false

                        Rectangle {
                            id: btStatus
                            x: 0
                            y: parent.height - 80
                            width: parent.width
                            height: 80
                            color: "transparent"

                            Item{
                                id: statusInfo
                                visible: false
                                anchors.fill: parent

                                Label {
                                    x: 20
                                    y: 5
                                    text: SimpleJsTools.milltoString(videofile.position)
                                    color: "white"
                                    font.pointSize: 18
                                    font.family: "Helvetica"
                                }

                                Label {
                                    x: parent.width - 60
                                    y: 5
                                    text: SimpleJsTools.milltoString(videofile.duration)
                                    color: "white"
                                    font.pointSize: 18
                                    font.family: "Helvetica"
                                }

                                Text {
                                    x: parent.width/2 - 30
                                    y: 5
                                    visible: videofile.playbackState === MediaPlayer.PlayingState
                                    color: "white"
                                    font.pointSize: 30
                                    font.family: fontAwesome.name
                                    text: FontAwesome.icons.fa_pause
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            videofile.pause()
                                        }
                                    }
                                }

                                Text {
                                    x: parent.width/2 - 30
                                    y: 5
                                    visible: videofile.playbackState !== MediaPlayer.PlayingState
                                    color: "white"
                                    font.pointSize: 30
                                    font.family: fontAwesome.name
                                    text: FontAwesome.icons.fa_play
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            videofile.play()
                                        }
                                    }
                                }

                                Text {
                                    x: parent.width/2 + 30
                                    y: 5
                                    color: "white"
                                    font.pointSize: 30
                                    font.family: fontAwesome.name
                                    text: FontAwesome.icons.fa_expand
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            if(videofile.width > 350){
                                                videoFrame.visible = false
                                                backImage.visible = true
                                                videofile.parent = imgAuthor;
                                                videofile.anchors.fill = imgAuthor;
                                            }else{
                                                videofile.parent = videoFrame;
                                                videofile.anchors.fill = videoFrame;
                                                backImage.visible = false
                                                videoFrame.visible = true
                                            }
                                        }
                                    }
                                }

                                Slider {
                                    id: seekSlider
                                    value: videofile.position
                                    to: videofile.duration

                                    x: 20
                                    y: 40
                                    //z: 2
                                    width: parent.width - 40
                                    height: 20

                                    ToolTip {
                                        parent: seekSlider.handle
                                        visible: seekSlider.pressed
                                        text: pad(Math.floor(value / 60)) + ":" + pad(Math.floor(value % 60))
                                        y: parent.height

                                        readonly property int value: seekSlider.valueAt(seekSlider.position)

                                        function pad(number) {
                                            if (number <= 9)
                                                return "0" + number;
                                            return number;
                                        }
                                    }

                                    handle: Rectangle {
                                        x: seekSlider.visualPosition * (seekSlider.width - width)
                                        y: (seekSlider.height - height) / 2
                                        width: 40
                                        height: 40

                                        radius: 20
                                        color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
                                        border.color: "#9c27b0"
                                    }

                                    background: Rectangle {
                                        y: (seekSlider.height - height) / 2
                                        height: 10
                                        radius: 5
                                        color: "#686868" //background slider

                                        Rectangle {
                                            width: seekSlider.visualPosition * parent.width
                                            height: parent.height
                                            color: "#b92ed1" //done part
                                            radius: 5
                                        }
                                    }

                                    onPressedChanged: {
                                        videofile.seek(seekSlider.value)
                                    }
                                }
                            }

                            MouseArea {
                                id: statusMouseArea
                                anchors.fill: parent
                                onClicked: {
                                    if(!statusInfo.visible){
                                        statusInfo.visible = true
                                        timeout.restart()
                                        statusMouseArea.visible = false
                                    }
                                }
                            }

                            Timer {
                                id: timeout
                                interval: 5000
                                running: false
                                repeat: false
                                onTriggered:{
                                    timeout.stop()
                                    statusInfo.visible = false
                                    statusMouseArea.visible = true
                                }
                            }
                        }
                    }
                }

                //video

                /*Rectangle {
                    x: 30
                    y: 215
                    width: 670
                    height: 1
                    color: "#d4d4d4"
                }*/

                Rectangle {
                    x: 100
                    y: 220
                    width: 600
                    height: 20

                    Row {
                        spacing: 300
                        Label {
                            text: "Ingredients"
                            color: "black"
                            font.pointSize: 18
                            font.bold: swipeRecipeDetail.currentIndex === 0?true: false
                            font.family: "Helvetica"
                            wrapMode: Text.WordWrap
                        }
                        Label {
                            text: "Method"
                            color: "black"
                            font.pointSize: 18
                            font.bold: swipeRecipeDetail.currentIndex === 1?true: false
                            font.family: "Helvetica"
                            wrapMode: Text.WordWrap
                        }
                    }
                }

                Rectangle {
                    x: 30
                    y: 250
                    width: 670
                    height: 1
                    color: "#d4d4d4"
                }

                SwipeView {
                    id: swipeRecipeDetail
                    x: 10
                    y: 260
                    width: imageWidth - 60
                    height: swipeRecipeDetail.trueHeight

                    property int trueHeight: pswipeSecond.height + swipeRecipeDetail.height

                    function getIngredients(){
                        if(controller.recipeCurrent.length === 0) return [];
                        return controller.recipeCurrent[0].ingredients;
                    }

                    function getMethod(){
                        if(controller.recipeCurrent.length === 0) return [];
                        if(controller.recipeCurrent[0].method === undefined) return [];
                        return controller.recipeCurrent[0].method;
                    }

                    function getMethodSize(){
                        if(controller.recipeCurrent.length === 0) return 0;
                        if(controller.recipeCurrent[0].method === undefined) return 0;
                        return controller.recipeCurrent[0].method.length;
                    }

                    function getTips(){
                        if(controller.recipeCurrent.length === 0) return [];
                        if(controller.recipeCurrent[0].tips === undefined) return [];
                        return controller.recipeCurrent[0].tips;
                    }

                    function hasTips(){
                        if(controller.recipeCurrent.length === 0) return false;
                        if(controller.recipeCurrent[0].tips === undefined) return false;
                        return true;
                    }

                    Item {
                        id: panelIng
                        width: imageWidth - 60
                        height: 500//panelMethod.height

                        Component {
                            id: delegateIngredients
                            Item {
                                width: 600
                                height: 50

                                Label {
                                    x: 20
                                    width: 550
                                    text: controller.recipeCurrent[0].ingredients[index]
                                    color: "black"
                                    font.pointSize: 15
                                    font.family: "Helvetica"
                                    wrapMode: Text.WordWrap
                                }

                                Image {
                                    x: 550
                                    width: 20
                                    height: 20
                                    source: "files/icon/addcart.png"
                                }
                            }
                        }

                        Column{
                            spacing: 20
                            Row{
                                spacing: 340
                                Label {
                                    //x: 20
                                    width: 200
                                    text: "Ingredients for "+(controller.recipeCurrent.length > 0? controller.recipeCurrent[0].people : "")
                                    color: "black"
                                    font.pointSize: 15
                                    font.family: "Helvetica"
                                }

                                Image {
                                    width: 20
                                    height: 20
                                    source: "files/icon/addcart.png"
                                }
                            }
                            ListView {
                                id: recipeIngredients
                                width: panelIng.width
                                height: panelIng.height - 50

                                model: swipeRecipeDetail.getIngredients()
                                delegate: delegateIngredients
                            }
                        }
                    }

                    Item {
                        id: panelMethod
                        width: imageWidth - 60
                        height: panelMethod.trueHeight

                        property int trueHeight: pswipeSecond.trueHeight + swipeRecipeDetail.trueHeight

                        /*Component {
                            id: delegateMethod
                            Item {
                                width: 600

                                Column{
                                    spacing: 10
                                    Label {
                                        text: "Step "+(index+1).toString()
                                        color: "black"
                                        font.pointSize: 15
                                        font.family: "Helvetica"
                                        font.bold: true
                                        wrapMode: Text.WordWrap
                                    }
                                    Label {
                                        width: 650
                                        text: controller.recipeCurrent[0].method[index]
                                        color: "black"
                                        font.pointSize: 15
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                }
                            }
                        }

                        Component {
                            id: delegateTips
                            Item {
                                width: 600

                                Column{
                                    spacing: 10
                                    Label {
                                        width: parent.width
                                        text: "* " + controller.recipeCurrent[0].tips[index]
                                        color: "black"
                                        font.pointSize: 15
                                        font.family: "Helvetica"
                                        wrapMode: Text.WordWrap
                                    }
                                }
                            }
                        }

                        Column{
                            ListView {
                                id: recipeMethod
                                width: parent.width
                                height: parent.height - 200

                                model: swipeRecipeDetail.getMethod()
                                delegate: delegateMethod
                            }

                            ListView {
                                id: recipeTips
                                width: parent.width
                                height: 200

                                model: swipeRecipeDetail.getTips()
                                delegate: delegateTips
                            }
                        }*/

                        Column{
                            height: pswipeSecond.height
                            ColumnLayout{
                                height: pswipeSecond.height
                                Repeater {
                                    id: pswipeSecond
                                    height: pswipeSecond.trueHeight
                                    model: swipeRecipeDetail.getMethodSize();

                                    property int trueHeight: pswipeSecond.getTotalHeight()

                                    //property int itemcount: value
                                    function getTotalHeight(){
                                        var total = 0;
                                        for(var x=0; x<pswipeSecond.count; x++){
                                           // console.log("h",pswipeSecond.itemAt(x).trueHeight);
                                            if(total < pswipeSecond.itemAt(x).trueHeight)
                                                total = pswipeSecond.itemAt(x).trueHeight;
                                            //total += pswipeSecond.itemAt(x).trueHeight;
                                        }
                                        console.log("total",total*pswipeSecond.count);
                                        return total*pswipeSecond.count
                                    }

                                    delegate: Item {
                                        id: itemTest
                                        Layout.fillWidth: true

                                        property int trueHeight: col4.height

                                        Column{
                                            id: col4
                                            spacing: 10
                                            Label {
                                                text: "Step "+(index+1).toString()
                                                color: "black"
                                                font.pointSize: 15
                                                font.family: "Helvetica"
                                                font.bold: true
                                                wrapMode: Text.WordWrap
                                                MouseArea {
                                                    anchors.fill: parent
                                                    onClicked: {
                                                        console.log("col4",col4.height);
                                                        console.log("itemTest",itemTest.height);
                                                        console.log("Repeater",pswipeSecond.height);
                                                        console.log("panelMethod",panelMethod.height);
                                                        console.log("swipeRecipeDetail",swipeRecipeDetail.height);
                                                    }
                                                }
                                            }
                                            Label {
                                                width: 650
                                                text: controller.recipeCurrent[0].method[index]
                                                color: "black"
                                                font.pointSize: 15
                                                font.family: "Helvetica"
                                                wrapMode: Text.WordWrap
                                            }
                                        }


                                    }
                                }
                            }
                            ColumnLayout{
                                width: 600
                                height: swipeRecipeDetail.height
                                Repeater {
                                    id: pswipeSecondTip
                                    model: swipeRecipeDetail.getTips().length;
                                    height: pswipeSecondTip.trueHeight

                                    property int trueHeight: pswipeSecondTip.getTotalHeight()

                                    //property int itemcount: value
                                    function getTotalHeight(){
                                        var total = 0;
                                        for(var x=0; x<pswipeSecondTip.count; x++){
                                            if(total < pswipeSecondTip.itemAt(x).trueHeight)
                                                total = pswipeSecondTip.itemAt(x).trueHeight;
                                        }
                                        return total*pswipeSecondTip.count
                                    }

                                    delegate: Item {
                                        width: 600

                                        property int trueHeight: tipLabel.height

                                        Label {
                                            id: tipLabel
                                            width: parent.width
                                            text: "* " + controller.recipeCurrent[0].tips[index]
                                            color: "black"
                                            font.pointSize: 15
                                            font.family: "Helvetica"
                                            wrapMode: Text.WordWrap
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        //}
    } 
}

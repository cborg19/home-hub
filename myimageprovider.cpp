#include "myimageprovider.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QImageReader>
#include <QThread>
#include <QDirIterator>

#include <taglib/fileref.h>
#include <taglib/tag.h>
#include <taglib/id3v2tag.h>
#include <taglib/mpegfile.h>
#include <taglib/id3v2frame.h>
#include <taglib/id3v2header.h>
#include <taglib/attachedpictureframe.h>

AsyncImageResponse::~AsyncImageResponse()
{

}

void AsyncImageResponse::run()
{
    qDebug() << "async image" << m_id;

    if(m_id.startsWith("fibaro")){
        //https://forum.fibaro.com/topic/18941-how-to-get-the-icon-path-for-devices-rest-api/?tab=comments#comment-68893

        QStringList w = m_id.split("%7C");
        QString url = w[1];
        QString param = w[2];
//qDebug()<<url<<param;
        getURLImage(url, param);
        if (m_requestedSize.isValid())
            m_image = m_image.scaled(m_requestedSize);
     }else if(m_id.contains("%7C")){
        QStringList w = m_id.split("%7C");
        QString url = w[0];
        QString p = w[1];
        QString param = w[2];

        p = p.split("#X3A").join("%3a");
        p = p.split("#X2F").join("%2f");

        p = p.split("%").join("%25");
        p = p.split("/").join("%2F");
        p = p.split(":").join("%3A");

        url += p;
//qDebug() <<"ATTACK "<<     url;
        getURLImage(url, param);
        if (m_requestedSize.isValid())
            m_image = m_image.scaled(m_requestedSize);
    }else if(m_id.startsWith("mp3")){
        //return
        QThread::msleep(100);
        QString filepath = "";
        #ifdef Q_OS_LINUX
        if(m_id.startsWith("mp3/file:/"))
            filepath = m_id.remove(0,11);
        else filepath = m_id.remove(0,4);
        #else
        if(m_id.startsWith("mp3/file:/"))
            filepath = m_id.remove(0,14);
        else filepath = m_id.remove(0,4);
        #endif

        bool exists = QFileInfo::exists(m_id) && QFileInfo(m_id).isFile();

        bool found = false;
        if(exists){
            QByteArray ba = filepath.toLocal8Bit();
            const char *c_str2 = ba.data();
            TagLib::MPEG::File source(c_str2);
            if(source.isOpen()){
                TagLib::ID3v2::Tag *tag = source.ID3v2Tag();
                if(tag){
                    TagLib::ID3v2::FrameList l = tag->frameList("APIC");

                    if(!l.isEmpty()){
                        TagLib::ID3v2::AttachedPictureFrame *f =
                            static_cast<TagLib::ID3v2::AttachedPictureFrame *>(l.front());
                        if(f){
                            m_image.loadFromData((const uchar *) f->picture().data(), f->picture().size());
                            found = true;
                        }
                    }
                }
                source.clear();
            }
        }
        if(!found)
            m_image = QImage(":/files/noCoverArt");
    }else if(m_id.startsWith("mp4")){
        /*
        TagLib::MP4::File f(file);
        TagLib::MP4::Tag* tag = f.tag();
        TagLib::MP4::ItemListMap itemsListMap = tag->itemListMap();
        TagLib::MP4::Item coverItem = itemsListMap["covr"];
        TagLib::MP4::CoverArtList coverArtList = coverItem.toCoverArtList();
        if (!coverArtList.isEmpty()) {
            TagLib::MP4::CoverArt coverArt = coverArtList.front();
            image.loadFromData((const uchar *)
            coverArt.data().data(),coverArt.data().size());
        }
        */
    }
    emit finished();
}

void AsyncImageResponse::getURLImage(QString url, QString params)
{
    QNetworkAccessManager *manager = new QNetworkAccessManager();
    QNetworkRequest request;

    request.setUrl(QUrl(url));
    if(params != "")
        request.setRawHeader("Authorization", params.toLocal8Bit());

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);

    QNetworkReply* reply = manager->get(request);

    QEventLoop eventloop;
    connect(reply,SIGNAL(finished()),&eventloop,SLOT(quit()));
    eventloop.exec();

    if (reply->error() == QNetworkReply::NoError)
    {
        m_image.loadFromData(reply->readAll());
    }else{
    }
    delete manager;
}


MyImageProvider::MyImageProvider()
{
}

QQuickImageResponse * MyImageProvider::requestImageResponse(const QString &id, const QSize &requestedSize)
{
    //QString blah = QString(QCryptographicHash::hash((id.toUtf8()),QCryptographicHash::Md5).toHex());

    AsyncImageResponse *response = new AsyncImageResponse(id, requestedSize);
    pool.start(response);
    return response;
}


CacheImageProvider::CacheImageProvider() : QQuickImageProvider(QQuickImageProvider::Image)
{

}

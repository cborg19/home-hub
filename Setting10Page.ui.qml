import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 800
    height: 480

    background: Rectangle{
        color:"transparent"
    }

    header: Label {
        text: qsTr("News")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

}

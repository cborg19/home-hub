#ifndef WATHER_H
#define WATHER_H

#include <QObject>
#include <QTimer>
#include <QQmlListProperty>
#include <QQuickImageProvider>

#include <QMutex>
#include "networkmanager.h"

extern bool gdebug;

class Weather : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ sname WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString id READ sid WRITE setId NOTIFY idChanged)

    Q_PROPERTY(QVariantMap current READ current WRITE setCurrent NOTIFY currentChanged)

    Q_PROPERTY(QVariantList daily READ dailysource WRITE setDaily NOTIFY dailyChanged)
    Q_PROPERTY(QVariantList hourly READ hourly WRITE setHourly NOTIFY hourlyChanged)

    Q_PROPERTY(bool hasRadar READ hasradar NOTIFY radarChanged)
    Q_PROPERTY(bool hasData READ hasdata NOTIFY dataChanged)

    Q_PROPERTY(bool hasRadarImages READ hasradarImages NOTIFY hasradarImagesChanged)
    Q_PROPERTY(QStringList radarBg READ radarBg NOTIFY radarbgChanged)
    Q_PROPERTY(QStringList radarImages READ getradarImages NOTIFY radarImagesChanged)
    Q_PROPERTY(int radarImagesSize READ getradarImagesSize NOTIFY radarImagesSizeChanged)

//    Q_PROPERTY(QImage radarImage READ radarImage NOTIFY radarImageChanged)
    Q_PROPERTY(qint64 lastUpdate READ lastUpdate)
public:
    explicit Weather(const QStringList settings, QObject *parent = nullptr);
    explicit Weather(const QString &id, const QString &name, const QString &latitude, const QString &longitude, QObject *parent = nullptr);

    QString sname() const;
    void setName(const QString &source);

    QString sid() const;
    void setId(const QString &source);

    qint64 lastUpdate() const;

    bool hasradar() const;
    bool hasdata() const;
    bool hasradarImages() const;
    QStringList radarBg() const;

  //  QImage radarImage() const;

    Q_INVOKABLE void downloadRadarImages();

   // Q_INVOKABLE QImage getRadarImage(int pos);

    QStringList getradarImages() const;
    int getradarImagesSize();

    QVariantMap current();
    void setCurrent(const QVariantMap &source);

    QVariantList hourly();
    void setHourly(const QVariantList &source);

    QVariantList dailysource();
    void setDaily(const QVariantList &source);

    QString getRadarBgImage(int pos);

    QString getLatitude() const;
    QString getLongitude() const;
    QString getBOM() const;

    void setLatitude(QString);
    void setLongitude(QString) ;
    void setBOM(QString);

    bool isPrimary() const;
    void setPrimary(bool v);

    bool update();
signals:
    void nameChanged();
    void idChanged();
    void currentChanged();
    void dailyChanged();
    void hourlyChanged();
    void radarChanged();
    void radarbgChanged();
    void hasradarImagesChanged();
    void radarImagesChanged();
    void radarImagesSizeChanged();
    void dataChanged();

    void primaryChanged();
 //   void radarImageChanged();
public slots:

private:
    void getWeatherRadar();
    void getWeatherRadarBg();
    bool getWeatherDarkSky();
    bool parseJSONResponse(QString *response);

    //QTimer *updateTimer;
    //int m_checkInterval;
    NetworkManager *networkManager;

    QMutex updating;
    QVariantMap m_current;
    QVariantList m_daily;
    QVariantList m_hourly;

    QString m_id;
    QString m_name;
    QString m_longitude;
    QString m_latitude;
    QString m_Bom;
    QString m_pathName;
    QStringList m_radarbg;
    QStringList m_radarImagelist;
    bool m_primary;

    qint64 m_lastUpdate;

    QString APIKey;
};


class WeatherController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString pathName READ pathName WRITE setPathName NOTIFY pathNameChanged)
    Q_PROPERTY(QQmlListProperty<QObject> list READ getList NOTIFY listChanged)
    Q_PROPERTY(int listSize READ getListSize NOTIFY listSizeChanged)
public:
    explicit WeatherController(QObject *parent = nullptr);

    QQmlListProperty<QObject> getList();
    int getListSize();

    QString pathName();
    void setPathName(const QString &pathName);

    QStringList getWeatherOption();
    QStringList getWeatherOptionAt(int x);

    void addWeatherOption(const QStringList &list);
    void updateWeatherOption(int pos, const QStringList &list);
    void removeWeatherOption(int pos);
    int getPrimary();
    void setPrimary(int pos);

    Q_INVOKABLE void setSettings(QStringList list);
    QList<QStringList> getSettings();

    QList<QObject *> *GetAllList();

    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void pathNameChanged();
    void listChanged();
    void listSizeChanged();

    void primaryUpdate();
    void pageTrigger(QString page, int index);
private:
    QString m_pathName;
    QList<QObject *> weatherList;
    QMutex l_weather;
    int primary;
public slots:
    void primaryUpdateHandler();
};

#endif // WATHER_H

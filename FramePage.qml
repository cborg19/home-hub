import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import "js/skycons.js" as WeatherIcon
import "js/simpletools.js" as SimpleTools
import "./js/fontawesome.js" as FontAwesome


Page {
    anchors.fill: parent

    width: imageWidth
    height: imageHeight

    background: Rectangle{
        color:"black"
    }

    function refreshCanvas(){
        panCanvas.requestPaint();
    }

    Connections {
        target: frameView
        onVisibleChanged:
            if(visible){
                console.log("Flick Timer started")
                flickTimer.start();
                clockTimer.start();
                clockWidget.displayTime();
            }else{
                console.log("Flick Timer stopped")
                flickTimer.stop();
                clockTimer.stop();
            }

        ignoreUnknownSignals: true
    }

    property int slideInterval: window.slideInterval
    property int slideDuration: 900
    property int slideCount: 5
    property int frames_per_second: 30

    Component.onCompleted: {

    }

    Timer {
       id: flickTimer
       interval: slideInterval
       running: false
       repeat: false
       onTriggered:{
           moveSlide();
       }
       function moveSlide(){
console.log("QML moveSlide");
           flickTimer.stop();
           //delegate.pictureOptions.visible = false;
           //delegate.albumOptions.visible = false;
           //delegate.albumOptionsTimer.stop();

           if(view.currentIndex+1 < controller.galleryDataSize){
               view.currentIndex++
               console.log("added",view.currentIndex)
           }
           else
           {
               console.log("About to ScanGallery")
               controller.scanGallery();
               console.log("back to zero")
               view.currentIndex = 0;
           }

           if(controller.galleryDataModel[view.currentIndex] === "Panorama"){
               flickTimer.interval = slideInterval * 2
           }else{
               flickTimer.interval = slideInterval
           }
           //panCanvas.requestPaint();
           flickTimer.restart();
       }
   }


    Component {
        id: delegate

        Loader {
            width: imageWidth
            height: imageHeight

            property string name: controller.galleryDataModel[index].name
            property string slideId: controller.galleryDataModel[index].slideId
            property var sourceList: controller.galleryDataModel[index].sourceList
            property string type: controller.galleryDataModel[index].type
            property var album: controller.galleryDataModel[index].album!==undefined?controller.galleryDataModel[index].album:false

            Text {
                id: btAlbum
                visible: controller.galleryDataModel[index].album!==undefined?true:false
                x: 760
                y: 10
                z: 3
                color: "white"
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_camera_retro
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        albumOptions.visible = true;
                        albumOptionsTimer.start()
                    }
                }
            }

            Rectangle{
                visible: controller.galleryDataModel[index].album === undefined?true:false
                x: 750
                y: 0
                z: 3
                width: 50
                height: 50

                color: "transparent"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        pictureOptions.visible = true;
                        albumOptionsTimer.start()
                    }
                }
            }

            Timer {
               id: albumOptionsTimer
               interval: 5000
               running: false
               repeat: false
               onTriggered:{
                   albumOptions.visible = false
                   pictureOptions.visible = false
               }
            }

            Item {
                id: albumOptions
                x: 600
                y: 10
                z: 3
                width: 200
                height: 170
                visible: false

                Rectangle{
                    anchors.fill: parent
                    color:"white"
                }

                ColumnLayout{
                    x: 10
                    y: 10
                    spacing: 10

                    Label {
                        text: "Add to Favourites"
                        color: "#BEBEBE"
                        font.pointSize: 18
                        font.family: "Helvetica"
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                controller.favouriteAlbum(controller.galleryDataModel[index].album.index)
                                albumOptions.visible = false
                                albumOptionsTimer.stop()
                            }
                        }
                    }

                    Label {
                        text: "Don't Show Album"
                        color: "#BEBEBE"
                        font.pointSize: 18
                        font.family: "Helvetica"
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                controller.blockAlbum(controller.galleryDataModel[index].album.index)
                                albumOptions.visible = false
                                albumOptionsTimer.stop()
                            }
                        }
                    }

                    Label {
                        text: "Show Album on Kodi"
                        color: "#BEBEBE"
                        font.pointSize: 18
                        font.family: "Helvetica"
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                controller.showKodiGallery(controller.galleryDataModel[index].album.path)
                                albumOptions.visible = false
                                albumOptionsTimer.stop()
                            }
                        }
                    }

                    Label {
                        text: "Show Picture on Kodi"
                        color: "#BEBEBE"
                        font.pointSize: 18
                        font.family: "Helvetica"
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                controller.showKodiPicture(controller.galleryDataModel[index].album.path)
                                albumOptions.visible = false
                                albumOptionsTimer.stop()
                            }
                        }
                    }

                    Label {
                        text: "Cancel"
                        color: "#BEBEBE"
                        font.pointSize: 18
                        font.family: "Helvetica"
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                albumOptions.visible = false
                                albumOptionsTimer.stop()
                            }
                        }
                    }
                }
            }

            Item {
                id: pictureOptions
                x: 600
                y: 10
                z: 3
                width: 200
                height: 70
                visible: false

                Rectangle{
                    anchors.fill: parent
                    color:"white"
                }

                ColumnLayout{
                    x: 10
                    y: 10
                    spacing: 10

                    Label {
                        text: "Show Picture on Kodi"
                        color: "#BEBEBE"
                        font.pointSize: 18
                        font.family: "Helvetica"
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                var s = [];
                                for(var x=0; x<controller.galleryDataModel[index].sourceList.length; x++){
                                    s.push(controller.galleryDataModel[index].sourceList[x].name);
                                }
                                controller.showKodiPicture(s)
                                pictureOptions.visible = false
                                albumOptionsTimer.stop()
                            }
                        }
                    }

                    Label {
                        text: "Cancel"
                        color: "#BEBEBE"
                        font.pointSize: 18
                        font.family: "Helvetica"
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                pictureOptions.visible = false
                                albumOptionsTimer.stop()
                            }
                        }
                    }
                }
            }

            function getComp(index)
            {
                //return "LiveImages.qml"
                //console.log("TYPE",index, name);
                //Default case
                return controller.galleryDataModel[index].type+".qml"
                //return "WebCamera.qml"
            }
            source: getComp(index)
        }
    }

    PathView {
        id: view
        anchors.fill: parent
        pathItemCount: slideCount
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        highlightRangeMode: PathView.StrictlyEnforceRange
        highlightMoveDuration: slideDuration

        property string currentId: ""

        //when manually flicked, stop the auto timer and reset when the move is finished
        onMovingChanged: {
console.log("QML onMovingChanged");
             if(this.moving)
             {
                 console.log("Stopping Timer")
                 flickTimer.stop();
                 //view.pictureOptions.visible = false;
                 //delegate.albumOptions.visible = false;
                 //delegate.albumOptionsTimer.stop();
             }
             else
             {
                 console.log("Restart Timer")
                 flickTimer.restart();
             }
        }

        onCurrentItemChanged: {
console.log("QML onCurrentItemChanged");
            if(view.currentIndex === controller.galleryDataSize){
            //if(view.currentIndex %2 == 0){
                console.log("Item Change about to Scan");
                controller.scanGallery();
            }

            if(controller.galleryDataSize){
                currentId = controller.galleryDataModel[view.currentIndex].slideId
                console.log("currentId",currentId)
            }
        }

        model: controller.galleryDataModel //imageListModel
        delegate: delegate
        path: Path {
//             startX: -imageWidth; startY: view.height/2
//             PathLine {x: 0; y: view.height/2}
//             PathLine {x: -imageWidth+imageWidth*view.pathItemCount; y: view.height/2}
            startX: -imageWidth*2; startY: view.height/2
            PathLine {relativeX: imageWidth*view.pathItemCount; y: view.height/2}
        }
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 3
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("FramePage switchToMenu");
            window.switchToMenu(true)
        }
    }

    function roundDown(value){
        return Math.floor(value).toString();
    }

    Page {
        id: clockWidget

        x: 20
        y: 320

        property string currentTime: ""
        property string currentHalf: ""

        background: Rectangle {
            color: "transparent"
        }

        Row {
            spacing: 0
            Label {
                id: clockHrMin
                text: qsTr(clockWidget.currentTime)
                font.pointSize: 60
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                id: clockDatHalf
                text: qsTr(clockWidget.currentHalf)
                font.pointSize: 30
                y: 35
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }
        function displayTime(){
            var currentDate = new Date();
            if(time24hr){
                clockWidget.currentTime = Qt.formatDateTime(currentDate, "HH:mm")
                clockWidget.currentHalf = ""
            }else{
                var time = Qt.formatDateTime(currentDate, "h:mm|A").split("|")
                clockWidget.currentTime = time[0]
                clockWidget.currentHalf = time[1]
            }
            //if(secondRow.lastUpdateWeather === null || (currentDate - secondRow.lastUpdateWeather) > 30 * 60 * 1000){
            //    panCanvas.requestPaint();
            //}
        }

        Timer {
            id: clockTimer
            interval: 1000
            running: false
            repeat: true
            onTriggered:{
                clockWidget.displayTime();
            }
       }
    }
    Page {
        id: info
        x: 20
        y: 400

        background: Rectangle {
            color: "transparent"
        }

        Item {
            id: secondRow
            property int canvasWidth: 50
            property int canvasHeight: 50
            property date lastUpdateWeather: new Date("2010-01-01")

            function getTemp(){
                //if(controller.weatherDataSize === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].current.temperature === undefined) return "";
                return SimpleTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].current.temperature);
            }
            function getTempName(){
                //console.log("dd",controller.weatherDataSize)
                //if(controller.weatherDataSize === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].current.temperature === undefined) return "";
                return "°C";
            }

            Canvas {
                id: panCanvas
                width: secondRow.canvasWidth
                height: secondRow.canvasHeight
                x:0
                y:10
                Component.onCompleted: {

                }
                onPaint: {
                    if(controller.weatherDataModel[controller.weatherPrimary] !== undefined){
                        var ctx = getContext("2d");
                        ctx.save();
                        ctx.clearRect(0, 0, secondRow.canvasWidth, secondRow.canvasHeight);

                        WeatherIcon.skycons(ctx,controller.weatherDataModel[controller.weatherPrimary].current.icon, 0);
                        ctx.restore();
                        secondRow.lastUpdateWeather = new Date();
                    }
                }
            }
            Label {
                anchors.left: panCanvas.right
                anchors.top: panCanvas.top
                id: tempText
                text:  qsTr(secondRow.getTemp())
                font.pointSize: 40
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: tempText.right
                anchors.top: tempText.top
                text:  qsTr(secondRow.getTempName())
                font.pointSize: 20
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }
    }
}

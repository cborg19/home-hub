import QtQuick 2.11
import QtQuick.Controls 2.4

Item {
     id: portraitRotate
     anchors.fill: parent;
     width: imageWidth
     height: imageHeight
     Image {
          id: img
          //anchors.fill: parent;
          x: 160
          y: -160
          width: imageHeight
          height: imageWidth
          rotation: 90
          //source: galleryPath + sourceList[0].name
          fillMode: Image.PreserveAspectCrop
          cache: true 
     }

     MouseArea {
         id: btInfo2
         x:300
         y:140
         z: 2
         height: 340
         width: 500
         onClicked: {
             console.log(mouseX,mouseY)
             if(view.currentId === slideId){
                 console.log("PortraitRotate switchToInfo");
                 window.switchToInfo(true);
             }
         }
     }

     property int test: view.currentId === slideId ? shown() : stopped()
     property bool initialised: false
     property bool sourceLoaded: false

     function loadInitial(){
         if(!portraitRotate.sourceLoaded){
             img.source = galleryPath + sourceList[0].name
             portraitRotate.sourceLoaded = true
         }

         delayCheck.start();
     }

     Component.onCompleted: {
         delayLoad.start();
     }

     Timer {
        id: delayLoad
        interval: 1500
        running: false
        repeat: false
        onTriggered:{
            loadInitial()
        }
    }

    Timer {
        id: delayCheck
        interval: 250
        running: false
        repeat: true
        onTriggered:{
            if(img.sourceSize.width !== 0){
                portraitRotate.initialised = true;
                busyIndication.running = false;
                delayCheck.stop();
            }
        }
    }

    BusyIndicator {
       id: busyIndication
       anchors.centerIn: parent
       running: false
       // 'running' defaults to 'true'
    }

    /*Connections {
        target: portraitRotate
        onVisibleChanged:
            if(visible){
                console.log("PORTRAITROTATE Shown")
                shown();
            }else{
                console.log("PORTRAITROTATE stopped")
                stopped();
            }

        ignoreUnknownSignals: true
    }*/

    function shown(){
        console.log('SHOWN ROT', sourceList[0].name);
        if(!portraitRotate.sourceLoaded){
           loadInitial();
           delayLoad.stop();
        }

        if(!portraitRotate.initialised){
            busyIndication.running = true;
        }
        return 1
    }

    function stopped(){
         return 1
    }

     //Component.onCompleted: {
     //     console.log("Yo6",imageSource.get(0).name);
     //}
}

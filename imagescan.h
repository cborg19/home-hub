#ifndef IMAGESCAN_H
#define IMAGESCAN_H

#include <QObject>
#include <QQmlListProperty>
#include <QMutex>
/*
class SlideObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString slideId READ slideId WRITE setSlideId NOTIFY slideIdChanged)
    //Q_PROPERTY(QStringList source READ source WRITE setSource NOTIFY sourceChanged)

    Q_PROPERTY(QVariantList sourceList READ source WRITE setSource NOTIFY sourceChanged)

public:
    SlideObject(QObject *parent=0);
    SlideObject(const QString &name, const QString &type, const QString &slideId, const QVariantList &source, QObject *parent=0);
    ~SlideObject();

    QString name() const;
    void setName(const QString &name);

    QString type() const;
    void setType(const QString &type);

    QString slideId() const;
    void setSlideId(const QString &slideId);

    QVariantList source() const;
    void setSource(const QVariantList &source);
signals:
    void nameChanged();
    void typeChanged();
    void slideIdChanged();
    void sourceChanged();

private:
    QString m_name;
    QString m_type;
    QString m_slideId;
    QVariantList m_source;
};
*/
class Imagescan : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString pathName READ pathName WRITE setPathName)
    Q_PROPERTY(int slideInterval READ slideInterval WRITE setslideInterval NOTIFY slideIntervalChanged)
    Q_PROPERTY(int weatherInterval READ weatherInterval WRITE setWeatherInterval NOTIFY weatherIntervalChanged)
    Q_PROPERTY(int otherSlideInterval READ otherInterval WRITE setOtherInterval NOTIFY otherIntervalChanged)
    //Q_PROPERTY(QQmlListProperty<QObject> galleryDataModel READ getList NOTIFY listChanged)
    Q_PROPERTY(QVariantList galleryDataModel READ getList NOTIFY listChanged)
    Q_PROPERTY(int gallerySize READ getListSize NOTIFY listChangedSize)
public:
    explicit Imagescan(QObject *parent = nullptr);

    QString pathName();
    void setPathName(const QString &pathName);

    int slideInterval();
    void setslideInterval(const int &pathName);

    int weatherInterval();
    void setWeatherInterval(const int &pathName);

    int otherInterval();
    void setOtherInterval(const int &pathName);

    void hasWeather(bool v){m_doWeather = v;}

    void changeTypeAlbum(int, QString);

    //QQmlListProperty<QObject> getList();
    QVariantList getList();
    Q_INVOKABLE int getListSize();

    Q_INVOKABLE void scanGallery();

    void syncGallery();
    void syncAlbum();
    int getLastScanDay();

    QVariantList getAlbumList();
signals:
    void slideIntervalChanged();
    void weatherIntervalChanged();
    void otherIntervalChanged();
    void listChanged();
    void listChangedSize();
    void albumListChanged();
private:
    QMutex updating;
    QString m_pathName;
    QStringList nameFilters;
    int m_slideInterval;
    int m_weatherInterval;
    int m_otherInterval;
    bool m_doWeather;

    //QList<QObject *> fileGalleryList;
    QVariantList fileGalleryList;

    void ShuffleList();

    void addWeatherSlides();
    void addOtherSlides();
    void getAlbumSlides();

    void scanPath(QString dir, QMap<QString, QStringList> *);
    int scan_day;

    void getVideoImages(QString filename);

    void clearAll();
public slots:
};

#endif // IMAGESCAN_H

import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 800
    height: 480
    contentHeight: 2

    background: Rectangle {
        color: "transparent"
    }

    header: Label {
        text: qsTr("Notification")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    property alias cbShowNotification: cbShowNotification
    property alias cbAlwaysOn: cbAlwaysOn
    property alias gbNotificationGroup: gbNotificationGroup
    property alias switchMusic: switchMusic
    property alias switchKodi: switchKodi
    property alias switchWeather: switchWeather
    property alias switchNews: switchNews

    CheckBox {
        id: cbShowNotification
        x: 33
        y: 7
        width: 259
        height: 40
        text: qsTr("Show Notification")
        font.pointSize: 16
        display: AbstractButton.IconOnly
        checked: window.showNotification
    }

    CheckBox {
        id: cbAlwaysOn
        x: 33
        y: 53
        width: 259
        height: 40
        text: qsTr("Always on")
        font.pointSize: 16
        checked: window.notificationAlwaysOn
    }

    GroupBox {
        id: gbNotificationGroup
        x: 41
        y: 110
        width: 712
        height: 290
        font.pointSize: 12
        title: qsTr("Components")

        Switch {
            id: switchMusic
            x: 19
            y: 8
            width: 293
            height: 40
            text: qsTr("Music")
            font.pointSize: 14
        }

        Switch {
            id: switchKodi
            x: 19
            y: 54
            width: 293
            height: 40
            text: qsTr("Kodi")
            font.pointSize: 16
        }

        Switch {
            id: switchWeather
            x: 19
            y: 103
            width: 275
            height: 40
            text: qsTr("Weather")
            font.pointSize: 14
        }

        Switch {
            id: switchNews
            x: 19
            y: 149
            width: 275
            height: 40
            text: qsTr("News")
            font.pointSize: 14
        }
    }
}

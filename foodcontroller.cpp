#include "foodcontroller.h"
#include <QtCore/qdebug.h>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QJsonObject>
#include <QJsonArray>
#include <QDirIterator>
#include "global.h"

void AsyncRecipeResponse::run()
{
    qDebug() << "AsyncRecipeResponse" << m_path << m_type << m_basic;

    QJsonDocument doc;

    QNetworkAccessManager manager;
    QNetworkRequest request;

    request.setUrl(QUrl(m_path));

    request.setRawHeader("Content-Type", "application/json");
    //request.setRawHeader("Authorization", m_basic.toUtf8());

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);

    if(m_type == "category" || m_type == "list" || m_type == "recipe"){
        QNetworkReply* reply = manager.get(request);

        QEventLoop eventloop;
        connect(reply,SIGNAL(finished()),&eventloop,SLOT(quit()));
        eventloop.exec();

        if (reply->error() == QNetworkReply::NoError)
        {
            QString data = reply->readAll();
//qDebug() << data.toUtf8();
            doc = QJsonDocument::fromJson(data.toUtf8());
            if(m_type == "category") parseCategory(doc);
            else if(m_type == "list") parseList(doc);
            else if(m_type == "recipe") parseRecipe(doc);
        }else{
            emit listChanged(m_list, "failed");
            return;
        }
    }

    emit listChanged(m_list, m_type);
}

void AsyncRecipeResponse::parseRecipe(QJsonDocument doc)
{
    if(doc.isEmpty()) return;

    QVariantMap list;
    QJsonObject resultObj = doc.object();
    foreach(const QString& key, resultObj.keys()) {
        QJsonValue value = resultObj.value(key);
        if(key == "categorie"){
            list.insert(key, value.toString());
        }else if(key == "rating"){
            if(value.isString()){
                QString v = value.toString();
                if(v == "")
                    list.insert(key, -1);
                else
                    list.insert(key, v.toInt());
            }else
                list.insert(key, value.toInt());
        }else if(key == "prep" || key == "cook" || key == "cool" || key == "people"){
            list.insert(key, value.toString());
        }else if(key == "method" || key == "tips"){
            QJsonArray ing = value.toArray();
            QStringList l;
            for(int x=0; x<ing.size(); x++){
                l.append(ing[x].toString());
            }
            list.insert(key, l);
        }
    }
    m_list.push_back(list);
}

void AsyncRecipeResponse::parseList(QJsonDocument doc)
{
    if(doc.isEmpty()) return;
    if(!doc["results"].isArray()) return;

    QJsonArray cat = doc["results"].toArray();
    for(int x=0; x<cat.size(); x++){
        QJsonObject resultObj = cat[x].toObject();

        QVariantMap list;
        list.insert("id", resultObj["id"].toInt());
        list.insert("title", resultObj["title"].toString());
        list.insert("path", resultObj["path"].toString());
        list.insert("thumbnail", resultObj["thumbnail"].toString());
        list.insert("description", resultObj["description"].toString());
        QString video = resultObj["video"].toString();
        if(video == "" || video == "0")
            list.insert("video", "");
        else
            list.insert("video", resultObj["video"].toString());
        list.insert("image", resultObj["image"].toString());
        list.insert("json", resultObj["json"].toString());
        list.insert("catergory", resultObj["video"].toArray());
        QString ing = resultObj["ingredients"].toString();
        ing.replace("[\"","");
        ing.replace("\"]","");
        QStringList l = ing.split("\",\"");
//qDebug() << "ingredients" << l;
        list.insert("ingredients", l);
        m_list.push_back(list);
    }
}

void AsyncRecipeResponse::parseCategory(QJsonDocument doc)
{
    if(doc.isEmpty()) return;
    if(!doc["results"].isArray()) return;

    QJsonArray cat = doc["results"].toArray();
    for(int x=0; x<cat.size(); x++){
        QJsonObject resultObj = cat[x].toObject();

        QVariantMap category;
        category.insert("id", resultObj["id"].toInt());
        category.insert("title", resultObj["title"].toString());
        category.insert("thumbnail", resultObj["thumbnail"].toString());
        category.insert("alais", resultObj["alais"].toString());

        m_list.push_back(category);
    }
}


FoodController::FoodController(QObject *parent) : QObject(parent),
    m_enabled(false)
{

}

void FoodController::setSettings(bool enabled, QString ip, QString user, QString password, int port)
{
    m_ip = ip;

    m_uname = user;
    m_password = password;
    m_port = port;
    m_enabled = enabled;
    QString concatenated = user + ":" + password;
    QByteArray data = concatenated.toLocal8Bit().toBase64();
    m_basic = "Basic " + data;

    currentStep = 0;
}

void FoodController::setEnabled(bool v)
{
    if(v){
       m_enabled = true;
    }else{
       m_enabled = false;
    }
}

bool FoodController::getEnabled()
{
    return m_enabled;
}

QStringList FoodController::getSettings()
{
    QStringList v;
    v << m_ip << QString::number(m_port) << m_uname << m_password;
    return v;
}

void FoodController::setSettings(QStringList v)
{
    if(v.length() >= 7){
       m_ip = v[0];
       m_port = v[1].toInt();
       m_uname = v[2];
       m_password = v[3];

       QString concatenated = m_uname + ":" + m_password;
       QByteArray data = concatenated.toLocal8Bit().toBase64();
       m_basic = "Basic " + data;
    }
}

void FoodController::login()
{
    QNetworkAccessManager manager;
    QNetworkRequest request;

    QString url = "http://"+m_ip+":"+QString::number(m_port)+"/login";

    request.setUrl(QUrl(url));

    request.setRawHeader("Content-Type", "application/json");
    //request.setRawHeader("Authorization", m_basic.toUtf8());

    QJsonDocument  json;
    QJsonObject  recordObject;

    recordObject.insert("user", QJsonValue::fromVariant(m_uname));
    recordObject.insert("password", QJsonValue::fromVariant(m_password));
    //recordObject.insert("deviceId", QJsonValue::fromVariant("PhotoFrame"));
    //recordObject.insert("notfToken", QJsonValue::fromVariant(""));

    QJsonDocument postJson(recordObject);
    QByteArray jsonData= postJson.toJson();

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);

    QNetworkReply* reply = manager.post(request, jsonData);

    QEventLoop eventloop;
    connect(reply,SIGNAL(finished()),&eventloop,SLOT(quit()));
    eventloop.exec();

    if (reply->error() == QNetworkReply::NoError)
    {
        QString data = reply->readAll();

        QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());

        if(!doc.isNull())
        {
            if(doc["status"].isString()){
                if(doc["status"].toString() == "connected")
                    m_token = doc["token"].isString();
            }
        }
    }else{

    }
}

void FoodController::getAllCategory()
{
    //login();

    QString url = "http://"+m_ip+":"+QString::number(m_port)+"/api?t=c";
    AsyncRecipeResponse *response = new AsyncRecipeResponse(url, m_token,"category");
    connect(response, &AsyncRecipeResponse::listChanged, this, &FoodController::listChanged);

    pool.start(response);
}

void FoodController::getCategory(QString id)
{
    QString url = "http://"+m_ip+":"+QString::number(m_port)+"/api?t=i&c="+id;
    AsyncRecipeResponse *response = new AsyncRecipeResponse(url, m_token,"list");
    connect(response, &AsyncRecipeResponse::listChanged, this, &FoodController::listChanged);

    pool.start(response);
}

void FoodController::getByIngredient(QString ingredient)
{
    QString url = "http://"+m_ip+":"+QString::number(m_port)+"/api?t=i&c="+ingredient;
    AsyncRecipeResponse *response = new AsyncRecipeResponse(url, m_token,"list");
    connect(response, &AsyncRecipeResponse::listChanged, this, &FoodController::listChanged);

    pool.start(response);
}

void FoodController::getRecipe(int index)
{
    qDebug() <<"getRecipe"<<index;
    QVariantMap r = m_recipes[index].toMap();
    QString url = r["json"].toString();

    currentStep = 0;
    m_recipe.clear();
    m_recipe.push_back(r);
    AsyncRecipeResponse *response = new AsyncRecipeResponse(url, m_token,"recipe");
    connect(response, &AsyncRecipeResponse::listChanged, this, &FoodController::listChanged);

    pool.start(response);
    emit currentRecipeChanged();
}

void FoodController::listChanged(QVariantList list, QString type)
{
    if(type == "category"){
        m_category = list;
        emit categoryListChanged();
    }else if(type == "list"){
        m_recipes = list;
        emit recipeListChanged();
    }else if(type == "recipe"){
        QVariantMap recipe = m_recipe[0].toMap();
        QVariantMap r = list[0].toMap();
        foreach(const QString& key, r.keys()) {
            QVariant value = r.value(key);

            recipe.insert(key, value);
        }
        m_recipe[0] = recipe;
        emit currentRecipeChanged();
    }
}

bool FoodController::wordInterger(QString word, int & value)
{
    int v = word.toInt();
    if(v != 0){
        value = v;
        return true;
    }

    QStringList wONES, wTEENS, wTENS, wSTEPS;
    wSTEPS << "first" << "second" << "third" << "fourth" << "fifth" << "sixth" << "seventh" << "eighth" << "ninth" << "tenth" << "eleventh"<< "twelfth"<< "thirteenth"<< "fourteenth"<< "fifteenth"<< "sixteenth"<< "seventeenth"<< "eighteenth"<< "nineteenth"<< "twentieth";
    wONES << "one" << "two" << "three" << "four" << "five" << "six" << "seven" << "eight" << "nine";
    wTEENS << "ten" << "eleven" << "twelve" << "thirteen" << "fourteen" << "fifteen" << "sixteen" << "seventeen" << "eighteen" << "nineteen";
    wTENS << "twenty" << "thirty" << "forty" << "fifty" << "sixty" << "seventy" << "eighty" << "ninety";

    if(wSTEPS.contains(word)){
        v = wSTEPS.indexOf(word);
        value = v+1;
        return true;
    }

    v = 0;
    int tensValue = 0; //number's tens value (i.e. 30)
    int onesValue = 0; //ones value (i.e. 3)

    QString tens;
    QString ones;
    QStringList w;
    if(word.contains("-")){
        w = word.split("-");
        tens = w[0];
        ones = w[1];
    }else if(word.contains(" ")){
        w = word.split(" ");
        tens = w[0];
        ones = w[1];
    }else{
        ones = word;
    }

    //searches for matches in String array for tens
    if (tens != "") {
        for (int u = 0; u < wTENS.length(); u++) {
            if (tens == wTENS[u]) {
                v = (u+2)*10;
            }
        }
    }

    //searches for matches in String array for ones
    for(int u = 0; u < wONES.length(); u++) {
        if (ones == wONES[u]) {
            v += u+1;
        }
    }

    // if a "teen" override what's in total
    for(int u = 0; u < wTEENS.length(); u++) {
     if (ones == wTEENS[u]) {
         v = u+10;
     }
    }

    value = v;
    return true;
}

bool FoodController::processVoiceCmd(QString cmd, QString &response)
{
    if(cmd.contains("recipe") || cmd.contains("food")){
        if(cmd.contains("go to")){
            emit pageTrigger("recipe", 0);
            return true;
        }
    }

    if(cmd.contains("show food categories") || cmd.contains("show recipe categories")){
        emit pageTrigger("recipe", 1);
        return true;
    }

    if(cmd.contains("show") && cmd.contains("category")){
        QString category = "", id = "";

        QStringList w = cmd.split(" ");
        int p = w.indexOf("show");
        if(p != -1){
            p++;
            for(int x=p; x<w.length(); x++){
                if(w[x] == "category") break;
                if(x != p) category += " ";
                category += w[x];
            }
            category = category.trimmed();
        }
        if(category != ""){
            for(int x=0; x<m_category.length(); x++){
                QVariantMap c = m_category[x].toMap();
                QString title = c["title"].toString().toLower();
                if(title == category){
                   id = c["id"].toString();
                   break;
                }
            }
        }

        if(id != ""){
            getCategory(id);

            emit pageTrigger("recipe", 10);
            return true;
        }
    }

    if(cmd.contains("show") && cmd.contains("recipe")){
        QString recipe = "";
        int id = -1;

        QStringList w = cmd.split(" ");
        int p = w.indexOf("show");
        if(p != -1){
            p++;
            for(int x=p; x<w.length(); x++){
                if(w[x] == "recipe") break;
                if(x != p) recipe += " ";
                recipe += w[x];
            }
            recipe = recipe.trimmed();
        }

        if(recipe != ""){
            QList<int> possible;
            for(int x=0; x<m_recipes.length(); x++){
                QVariantMap c = m_recipes[x].toMap();
                QString title = c["title"].toString().toLower();
                if(title == recipe){
                   id = x;
                   break;
                }else if(title.contains(recipe)){
                    possible.push_back(x);
                }
            }

            if(id == -1 && possible.length() > 0){
                id = possible.at(0);
            }
        }

        if(id > -1){
            getRecipe(id);

            emit pageTrigger("recipe", 11);
            return true;
        }
    }

    if(cmd.contains("read recipe ingredient")){
        if(m_recipe.size() > 0){
            QVariantMap r = m_recipe[0].toMap();
            QStringList i = r["ingredients"].toStringList();
            if(i.length() == 0){
                response = "Unable to find recipe ingredients";
                return true;
            }else{
                response = "Ingredients are. ";
                for(int x=0; x<i.length();x++){
                    if(x != 0) response += ". ";
                    response += i[x];
                }
                return true;
            }
        }
    }else if(cmd.contains("read recipe method")){
        if(m_recipe.size() > 0){
            if(m_recipe.size() > 0){
                QVariantMap r = m_recipe[0].toMap();
                QStringList i = r["method"].toStringList();
                if(i.length() == 0){
                    response = "Unable to find recipe methods";
                    return true;
                }else{
                    response = "Methods are. ";
                    for(int x=0; x<i.length();x++){
                        if(x != 0) response += ". ";
                        response += i[x];
                    }
                    return true;
                }
            }
        }
    }else if(cmd.contains("read recipe tip")){
        if(m_recipe.size() > 0){
            if(m_recipe.size() > 0){
                QVariantMap r = m_recipe[0].toMap();
                QStringList i = r["tips"].toStringList();
                if(i.length() == 0){
                    response = "Unable to find recipe tips";
                    return true;
                }else{
                    response = "Tips are. ";
                    for(int x=0; x<i.length();x++){
                        if(x != 0) response += ". ";
                        response += i[x];
                    }
                    return true;
                }
            }
        }
    }else if(cmd.contains("read recipe") && cmd.contains("step")){
        if(m_recipe.size() > 0){
            QString step = "";
            int id = -1;

            QStringList w = cmd.split(" ");
            int p = w.indexOf("recipe");
            if(p != -1){
                p++;
                for(int x=p; x<w.length(); x++){
                    if(w[x] == "step") break;
                    if(x != p) step += " ";
                    step += w[x];
                }
                step = step.trimmed();
            }


            if(wordInterger(step, id)){
                QVariantMap r = m_recipe[0].toMap();
                QStringList i = r["method"].toStringList();
                if(i.length() == 0 || id > i.length()){
                    response = "Unable to find recipe methods";
                    return true;
                }else {
                    response = i[id-1];
                    currentStep = id-1;
                    return true;
                }
            }
        }
    }else if(cmd.contains("read recipe next step")){
        if(m_recipe.size() > 0){
            currentStep++;
            QVariantMap r = m_recipe[0].toMap();
            QStringList i = r["method"].toStringList();
            if(i.length() == 0 || currentStep > i.length()){
                response = "Unable to find recipe methods step";
                currentStep = 0;
                return true;
            }else {
                response = i[currentStep];
                return true;
            }
        }
    }else if(cmd.contains("read recipe previous step")){
        if(m_recipe.size() > 0){
            currentStep--;
            QVariantMap r = m_recipe[0].toMap();
            QStringList i = r["method"].toStringList();
            if(i.length() == 0 || currentStep > i.length() || currentStep < 0){
                response = "Unable to find recipe methods step";
                currentStep = 0;
                return true;
            }else {
                response = i[currentStep];
                return true;
            }
        }
    }else if(cmd.contains("recipe page down") || cmd.contains("recipe page up")){
        if(m_recipe.size() > 0){
            if(cmd.contains("recipe page down"))
                emit pageTrigger("recipe", 12);
            else emit pageTrigger("recipe", 13);
        }
    }

    return false;
}

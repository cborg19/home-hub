import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.VirtualKeyboard 2.3
import QtQuick.Layouts 1.0
import QtMultimedia 5.11

import "./js/fontawesome.js" as FontAwesome
//import "./js/simpleTools.js" as SimpleJsTools
import photoframe.controller 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 800
    height: 480
    title: qsTr("Tabs")

    FontLoader{ id: fontAwesome; source: "./fonts/fontawesome-webfont.ttf"}

    property int imageWidth: 800
    property int imageHeight: 480
    //property string settingsDir: "/Users/Chris/Documents/Development/reactjs/hubhomeqt/PhotoFrame/"
    //property string settingsDir: "/home/pi/Frame/"
    property string galleryDir: "/Users/Chris/Documents/Development/reactjs/hubhomeqt/PhotoFrame/"
    //property string galleryDir: "/home/pi/Frame/"
    property string galleryPath: "file://" + galleryDir

    property bool time24hr: false
    property string nightBgColour: "black"
    property string nightFontColour: "white"
    property int slideInterval: 30000
    property int weatherInterval: 30
    property real volume: 0.5
    property string aplhabet: "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    property bool showNotification: false
    property bool notificationAlwaysOn: false

    PhotoFrameController {
        id: controller
        Component.onCompleted: {
            console.log("Loaded PhotoFrameController");
            window.galleryDir = controller.pathGallery;
            window.galleryPath = "file://" + galleryDir;
            window.slideInterval = controller.slideInterval;
            window.weatherInterval = controller.weatherInterval;
            window.nightBgColour = controller.nightBgColour
            window.nightFontColour = controller.nightFontColour

            window.volume = (controller.volume/100)

            window.showNotification = controller.showNotification
            window.notificationAlwaysOn = controller.notificationAlwaysOn
            settingNotification.load()

            //controller.loadWeather();
            controller.scanGallery();

            settingWeather.load()
            settingKodi.load()
            settingFibaro.load()
            settingRecipe.load()
            settingGallery.load()
            settingNight.load()
            //menuPage1.load()
            //menuPage2.load()

            backgroundTimer.start();
        }

        onPrimaryWeather: {
            clockView.refreshCanvas();
            sleepView.refreshCanvas();
            frameView.refreshCanvas();
            triggerView.refreshCanvas();
        }

        onOperateScreen: {
            console.log("HEY",screen,frameView.visible);
            switch(screen){
            case "sleep":
                if(!sleepView.visible)
                    switchToSleep()
                break;
            case "night":
                if(!clockView.visible)
                    switchToClock()
                break;
            case "standard":
                if(!frameView.visible)
                    switchToPhoto();
                break;
            }
        }

        onSwitchToPage: {
            switch(page){
            case "alarm":
                if(!alarmView.visible) switchToAlarm()
                break;
            case "fibaro":
                if(!fibaroView.visible) switchToFibaro()
                break;
            case "kodi":
                if(!kodiView.visible) switchToKodi()
                if(index !== -1){
                    kodiView.setPageIndex(index)
                }
                break;
            case "menu":
                if(!swipeView.visible) switchToMenu()
                break;
            case "mirror":
                if(!mirrorView.visible) switchToMirror()
                break;
            case "music":
                if(!musicView.visible) switchToMusic()
                if(index !== -1){
                    musicView.setPageIndex(index)
                }
                break;
            case "night":
                if(!clockView.visible) switchToClock()
                break;
            case "news":
                if(!newsView.visible) switchToNews()
                break;
            case "recipe":
                if(!recipeView.visible) switchToRecipe()
                if(index !== -1){
                    recipeView.setPageIndex(index)
                }
                break;
            case "settings":
                if(!settingPageView.visible) switchToSettings()
                break;
            case "sleep":
                if(!sleepView.visible) switchToSleep()
                break;
            case "standard":
                if(!frameView.visible) switchToPhoto();
                break;
            case "trains":
                if(!trainsDisplay.visible) switchToTrains();
                break;
            case "timer":
                if(!timerDisplay.visible) switchToTimer();
                break;
            case "trigger":
                if(!triggerView.visible) switchToTrigger();
                break;
            case "youtube":
                if(!youtubeView.visible) switchToYouTube()
                break;
            case "weather":
                if(!weatherView.visible) switchToWeather()
                if(index !== -1){
                    weatherView.setPageIndex(index)
                }

                break;
            case "video":
                if(!videoView.visible) switchToVideo()
                break;
            }
        }

        onVolume: {
            console.log("volume",vol);
            window.volume = vol/100
            showVolume()
        }

        onKodiConnectionChanged: {
            kodiView.onInitialLoad();
        }

        onAlertIcons: {
            if(iconId === "garagedoor" && iconValue){
                alertsFrame.visible = true;
                alertGarageDoor.visible = true;
            }else{
                alertsFrame.visible = false;
                alertGarageDoor.visible = false;
            }
        }

        onMediaImageChanged: {
            ProvedorImagem.carregaImagem(controller.getMediaImage());
        }
    }

    Component.onCompleted: {
        console.log("Loaded main.qml");
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent

        property int stimeoutInterval: 60000

        background: Image {
            id: imgBackground
            //anchors.fill: parent
            fillMode: Image.PreserveAspectCrop
            source: "/files/icon/menu.jpg"
            width: 1000
            height: 600

            Behavior on x {
                NumberAnimation {
                    //This specifies how long the animation takes
                    duration: 600
                    //This selects an easing curve to interpolate with, the default is Easing.Linear
                    easing.type: Easing.InOutQuad
                }
            }
        }

        onCurrentIndexChanged:{
            console.log("index changed:"+swipeView.currentIndex)
            imgBackground.x = swipeView.currentIndex * -50;
        }

        Timer {
           id: backgroundTimer
           interval: swipeView.stimeoutInterval
           running: false
           repeat: false
           onTriggered:{
               window.switchToPhoto();
           }
        }

        Page1Form {
            id: menuPage1
        }

        Page2Form {
            id: menuPage2

        }
    }

    Page {
        id: menuSwipeViewDots
        z: 2
        x: 400 - (swipeView.count/2)*(5+5)
        y: 448
        width: swipeView.count*(5+5)
        height: 10
        visible: swipeView.visible

        background: Rectangle {
            color: "transparent"
        }

        RowLayout {
            spacing: 5
            Repeater {
                model: swipeView.count

                delegate: Rectangle {
                    color: swipeView.currentIndex==index?"black":"white"
                    height: 5
                    width: 5
                    radius: 2
                }
            }
        }
    }

    Page {
        id: settingPageView
        anchors.fill: parent
        visible: false

        background: Rectangle {
            color: "#f2f2f2"
        }

        header: ToolBar{
            height: 20
            background: Rectangle {
                color: "transparent"
            }

            Text {
                color: "black"
                x: 770
                y: 10
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_times
            }

            Button {
                id: btExitSetting
                x: 765
                y: 5
                width: 35
                height: 35
                focusPolicy: Qt.NoFocus
                display: AbstractButton.IconOnly
                background: Rectangle {
                    color: "transparent"
                }
            }

            Connections {
                target: btExitSetting
                onClicked: {
                    swipeView.visible = true;
                    settingPageView.visible = false;
                    backgroundTimer.start();
                }
            }

        }

        SwipeView {
            id: settingSwipeView
            anchors.fill: parent
            Setting1Page {
                id: settingGallery

                property variant intervalList: ["5", "10", "20", "30", "40", "50", "60","Off"]
                property variant slideList: ["30", "45", "60"]

                btAlbumlist.onClicked: {
                    albumlist.load();
                }

                cbImageSlide {
                    onActivated: {
                        var v = parseInt(cbImageSlide.currentText) * 1000
                        window.slideInterval = v
                        controller.slideInterval = v
                    }
                }

                cbOtherSlide {
                    onActivated: {
                        if(cbOtherSlide.currentText === "Off"){
                            controller.otherInterval = 0;
                        }else{
                            controller.otherInterval = parseInt(cbOtherSlide.currentText);
                        }
                    }
                }

                function load(){
                    var interval = (controller.slideInterval/1000).toString()
                    for(var x=0; x<settingGallery.slideList.length; x++){
                        if(settingGallery.slideList[x] === interval){
                            cbImageSlide.currentIndex = x;
                            break;
                        }
                    }

                    var OtherInterval = controller.otherInterval.toString();
                    if(OtherInterval === "0") interval = "Off";
                    for(var x=0; x<settingWeather.intervalList.length; x++){
                        if(settingGallery.intervalList[x] === OtherInterval){
                            cbOtherSlide.currentIndex = x;
                            break;
                        }
                    }
                }
            }

            Setting2Page {
                id: settingWeather

                property variant intervalList: ["10", "20", "30", "40", "50", "60","Off"]

                Component.onCompleted: {
                    settingWeather.load()
                }

                bmComboWeather {
                    onActivated: {
                        if(bmComboWeather.currentText === "Off")
                            window.weatherInterval = 0
                        else
                            window.weatherInterval = parseInt(bmComboWeather.currentText)
                    }
                }

                btPrimaryWeather.onClicked: {
                    var pos = bmComboLocation.currentIndex-1;
                    controller.makeWeatherPrimary(pos)
                }

                btAddWeather.onClicked: {
                    var list = [bmComboLocation.count.toString(), locName.text, locLatitude.text, locLongitude.text, locBOM.text]

                    controller.addWeatherOption(list)

                    settingWeather.load()

                    bmComboLocation.currentIndex = bmComboLocation.count-1;
                }

                btRemoveWeather.onClicked: {
                    var pos = bmComboLocation.currentIndex-1;
                    controller.removeWeatherOption(pos)
                }

                btUpdateWeather.onClicked: {
                    var pos = bmComboLocation.currentIndex-1;
                    var list = [locName.text, locLatitude.text, locLongitude.text, locBOM.text]
                    controller.updateWeatherOption(pos, list)
                }

                bmComboLocation {
                    onActivated: {
                        if(bmComboLocation.currentIndex === 0){
                            locName.text = "";
                            locLatitude.text = "";
                            locLongitude.text = "";
                            locBOM.text = "";

                            btAddWeather.visible = true;
                            btUpdateWeather.visible = false;
                            btPrimaryWeather.visible = false;
                            btRemoveWeather.visible = false;
                        }else{
                            var wl = controller.getWeatherOptionAt(bmComboLocation.currentIndex-1);

                            locName.text = wl[0];
                            locLatitude.text = wl[1];
                            locLongitude.text = wl[2];
                            locBOM.text = wl[3];

                            btAddWeather.visible = false;
                            btUpdateWeather.visible = true;

                            if(wl[4] === "false"){
                                btPrimaryWeather.visible = true;
                                btRemoveWeather.visible = false;
                            }else{
                                btPrimaryWeather.visible = false;
                                btRemoveWeather.visible = true;
                            }
                        }
                    }
                }

                function load(){
                    var list = ["New"];
                    list = list.concat(controller.weatherOption);
                    bmComboLocation.model = list;

                    btAddWeather.visible = true;
                    btUpdateWeather.visible = false;
                    btPrimaryWeather.visible = false;
                    btRemoveWeather.visible = false;
//                    var wl = controller.getWeatherOptionAt(0);
//                    console.log("WL",wl);
//                    locName.text = wl[0];
//                    locLatitude.text = wl[1];
//                    locLongitude.text = wl[2];
//                    locBOM.text = wl[3];

                    var interval = window.weatherInterval.toString()
                    if(interval === "0") interval = "Off";
                    for(var x=0; x<settingWeather.intervalList.length; x++){
                        if(settingWeather.intervalList[x] === interval){
                            bmComboWeather.currentIndex = x;
                            break;
                        }
                    }
                }
            }

            Setting3Page {
                id: settingNotification
                cbShowNotification {
                    onCheckedChanged: {
                        window.showNotification = cbShowNotification.checked
                        controller.showNotification = cbShowNotification.checked
                        gbNotificationGroup.enabled = cbShowNotification.checked
                    }
                }

                cbAlwaysOn {
                    onCheckedChanged: {
                        window.notificationAlwaysOn = cbAlwaysOn.checked
                        controller.notificationAlwaysOn = cbAlwaysOn.checked
                    }
                }

                switchMusic {
                    onCheckedChanged: {
                        if(switchMusic.checked){
                            if(controller.allowNotification.indexOf("music") === -1){
                                controller.allowNotification.push("music");
                            }
                        }else{
                            var p = controller.allowNotification.indexOf("music")
                            if(p !== -1)
                                controller.allowNotification.slice(p,1);
                        }
                    }
                }

                switchKodi {
                    onCheckedChanged: {
                        if(switchMusic.checked){
                            if(controller.allowNotification.indexOf("kodi") === -1){
                                controller.allowNotification.push("kodi");
                            }
                        }else{
                            var p = controller.allowNotification.indexOf("kodi")
                            if(p !== -1)
                                controller.allowNotification.slice(p,1);
                        }
                    }
                }

                switchWeather {
                    onCheckedChanged: {
                        if(switchMusic.checked){
                            if(controller.allowNotification.indexOf("weather") === -1){
                                controller.allowNotification.push("weather");
                            }
                        }else{
                            var p = controller.allowNotification.indexOf("weather")
                            if(p !== -1)
                                controller.allowNotification.slice(p,1);
                        }
                    }
                }

                switchNews {
                    onCheckedChanged: {
                        if(switchMusic.checked){
                            if(controller.allowNotification.indexOf("news") === -1){
                                controller.allowNotification.push("news");
                            }
                        }else{
                            var p = controller.allowNotification.indexOf("news")
                            if(p !== -1)
                                controller.allowNotification.slice(p,1);
                        }
                    }
                }

                function load(){
                    gbNotificationGroup.enabled = window.showNotification

                    switchKodi.checked = false;
                    switchMusic.checked = false;
                    switchWeather.checked = false;
                    switchNews.checked = false;
                    for(var x=0; x<controller.allowNotification.length; x++){
                        var item = controller.allowNotification[x];
                        if(item === "kodi")
                            switchKodi.checked = true;
                        else if(item === "music")
                            switchMusic.checked = true;
                        else if(item === "weather")
                            switchWeather.checked = true;
                        else if(item === "news")
                            switchNews.checked = true;
                    }
                }
            }

            Setting4Page {
                id: settingKodi

                cbKodiEnable {
                    onCheckedChanged: {
                        if(cbKodiEnable.checked){
                            gbKodiSettings.enabled = true
                            controller.setKodiEnabled(true)
                        }else{
                            gbKodiSettings.enabled = false
                            controller.setKodiEnabled(false)
                        }
                    }
                }

                btUpdate.onClicked: {
                    var list = [tfIpAddress.text,tfPort.text,tfHttpPort.text,tfUserName.text,tfPassword.text];
                    if(swTcp.checked){
                        list.push("1")
                    }else list.push("0")
                    if(swEventServer.checked){
                        list.push("1")
                    }else list.push("0")
                    controller.setKodiSettings(list);
                }

                function load(){
                    if(controller.kodiEnabled){
                        gbKodiSettings.enabled = true
                        cbKodiEnable.checked = true
                    }else{
                        gbKodiSettings.enabled = false
                        cbKodiEnable.checked = false
                    }

                    var settings = controller.getKodiSettings();
                    if(settings.length >= 7){
                        tfIpAddress.text = settings[0]
                        tfPort.text = settings[1]
                        tfHttpPort.text = settings[2]
                        tfUserName.text = settings[3]
                        tfPassword.text = settings[4]
                        if(settings[5] === "1") swTcp.checked = true; else swTcp.checked = false;
                        if(settings[6] === "1") swEventServer.checked = true; else swEventServer.checked = false;
                    }
                }
            }

            Setting5Page {
                id: settingNight
                //property string nightBgColour: "black"
                //property string nightFontColour: "white"

                tBGColour {
                    onTextChanged: {
                        console.log(tBGColour.text);
                        window.nightBgColour = tBGColour.text
                        controller.nightBgColour = tBGColour.text
                    }
                }

                tFontColour {
                    onTextChanged: {
                        window.nightFontColour = tFontColour.text
                        controller.nightFontColour = tFontColour.text
                    }
                }

                function load(){
                    tBGColour.text = window.nightBgColour
                    tFontColour.text = window.nightFontColour

                    colorDialog.color = window.nightBgColour
                    colorFontDialog.color = window.nightFontColour
                }
            }

            Setting6Page {

            }

            Setting7Page {
                id: settingFibaro

                cbFibaroEnable {
                    onCheckedChanged: {
                        if(cbFibaroEnable.checked){
                            gbFibaroSettings.enabled = true
                            controller.setFibaroEnabled(true)
                        }else{
                            gbFibaroSettings.enabled = false
                            controller.setFibaroEnabled(false)
                        }
                    }
                }

                btUpdate.onClicked: {
                    var list = [tfIpAddress.text,tfPort.text,tfUserName.text,tfPassword.text];
                    controller.setFibaroSettings(list);
                }

                function load(){
                    if(controller.fibaroEnabled){
                        gbFibaroSettings.enabled = true
                        cbFibaroEnable.checked = true
                    }else{
                        gbFibaroSettings.enabled = false
                        cbFibaroEnable.checked = false
                    }

                    var settings = controller.getFibaroSettings();
                    if(settings.length >= 4){
                        tfIpAddress.text = settings[0]
                        tfPort.text = settings[1]
                        tfUserName.text = settings[2]
                        tfPassword.text = settings[3]
                    }
                }
            }
            
            Setting8Page {
                id: settingRecipe

                cbRecipeEnable {
                    onCheckedChanged: {
                        if(cbRecipeEnable.checked){
                            gbRecipeSettings.enabled = true
                            controller.setRecipeEnabled(true)
                        }else{
                            gbRecipeSettings.enabled = false
                            controller.setRecipeEnabled(false)
                        }
                    }
                }

                btUpdate.onClicked: {
                    var list = [tfIpAddress.text,tfPort.text,tfUserName.text,tfPassword.text];
                    controller.setRecipeSettings(list);
                }

                function load(){
                    if(controller.recipeEnabled){
                        gbRecipeSettings.enabled = true
                        cbRecipeEnable.checked = true
                    }else{
                        gbRecipeSettings.enabled = false
                        cbRecipeEnable.checked = false
                    }

                    var settings = controller.getRecipeSettings();
                    if(settings.length >= 4){
                        tfIpAddress.text = settings[0]
                        tfPort.text = settings[1]
                        tfUserName.text = settings[2]
                        tfPassword.text = settings[3]
                    }
                }
            }

            Setting9Page {
                id: settingAssistant
            }
            
            Setting10Page {
                id: settingNews
            }

            Setting11Page {
                id: settingSystem
            }
        }

        Item {
            id: albumlist
            x: 0
            width: imageWidth
            height: imageHeight
            z: 3

            property variant modelData: ({})
            function load(){
                var component = Qt.createComponent("AlbumList.qml");
                if( component.status !== Component.Ready )
                {
                    if( component.status === Component.Error )
                        console.debug("Error:"+ component.errorString() );
                    return; // or maybe throw
                }
                albumlist.modelData = controller.getAlbumList()
                component.createObject(albumlist);
            }
        }

        Page {
            id: settingsSwipeViewDots
            z: 2
            x: 400 - (settingSwipeView.count/2)*(5+5)
            y: 448
            width: settingSwipeView.count*(5+5)
            height: 10
            visible: settingPageView.visible

            background: Rectangle {
                color: "transparent"
            }

            RowLayout {
                spacing: 5
                Repeater {
                    model: settingSwipeView.count

                    delegate: Rectangle {
                        color: settingSwipeView.currentIndex==index?"black":"grey"
                        height: 5
                        width: 5
                        radius: 2
                    }
                }
            }
        }
    }

    WeatherPage {
        id: weatherView
        anchors.fill: parent
        visible: false
    }

    FramePage {
        id: frameView
        anchors.fill: parent
        visible: false
    }

    ClockPage {
        id: clockView
        anchors.fill: parent
        visible: false
    }

    SleepPage {
        id: sleepView
        anchors.fill: parent
        visible: false
    }

    KodiPage {
        id: kodiView
        anchors.fill: parent
        visible: false
    }

    FibaroPage {
        id: fibaroView
        anchors.fill: parent
        visible: false
    }

    RecipePage {
        id: recipeView
        anchors.fill: parent
        visible: false
    }

    MusicPage {
        id: musicView
        anchors.fill: parent
        visible: false
    }

    MirrorPage {
        id: mirrorView
        anchors.fill: parent
        visible: false
    }

    NewsPage {
        id: newsView
        anchors.fill: parent
        visible: false
    }

    VideoPage {
        id: videoView
        anchors.fill: parent
        visible: false
    }

    YouTubePage {
        id: youtubeView
        anchors.fill: parent
        visible: false
    }

    WidgetMusic {
        id: widgetMusic
        visible: false

    }

    TrainsDisplay {
        id: trainsDisplay
        visible: false

    }

    TimerPage {
        id: timerDisplay
        visible: false

    }

    AlarmPage {
        id: alarmView
        visible: false

    }

    TriggerPage {
        id: triggerView
        visible: false

    }

    function showMusicNoification(){
        if(frameView.visible){
            widgetMusic.visible = true
            if(!window.notificationAlwaysOn){
                widgetTimer.start();
            }
        }else if(swipeView.visible || weatherView.visible || kodiView.visible || videoView.visible){
            widgetMusic.visible = true
            widgetTimer.start();
        }

    }

    function doNotification(process){
        widgetMusic.visible = false
        if(!window.showNotification || !window.notificationAlwaysOn) return;

        if(process === true){
            if(musicplayer.playbackState === MediaPlayer.PlayingState || musicplayer.playbackState === MediaPlayer.PausedState){
                widgetMusic.visible = true
            }
        }
    }

    Timer {
        id: widgetTimer
        interval: 10000
        running: false
        repeat: false
        onTriggered:{
            widgetMusic.visible = false
        }
    }

    function alive(){
        controller.alive();
    }

    function closeAll(){
        swipeView.visible = false;
        frameView.visible = false;
        sleepView.visible = false;
        clockView.visible = false;
        weatherView.visible = false;
        musicView.visible = false;
        kodiView.visible = false;
        fibaroView.visible = false;
        videoView.visible = false;
        youtubeView.visible = false;
        trainsDisplay.visible = false;
        timerDisplay.visible = false;
        recipeView.visible = false;
        mirrorView.visible = false;
        alarmView.visible = false;
        triggerView.visible = false;
        newsView.visible = false;
    }

    function switchToPhoto(wakeup){
        console.log("switchToPhoto",wakeup)
        closeAll();
        frameView.visible = true;

        doNotification(true);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToMenu(wakeup){
        console.log("switchToMenu",wakeup)
        closeAll();
        swipeView.visible = true;

        doNotification(false);
        backgroundTimer.start();
        if (wakeup === true) window.alive();
    }

    function switchToInfo(wakeup){
        console.log("switchToInfo",wakeup)
        closeAll();
        swipeView.visible = true;

        doNotification(false);
        backgroundTimer.start();
        if (wakeup === true) window.alive();
    }

    function switchToSleep(wakeup){
        console.log("switchToSleep",wakeup)
        closeAll();
        sleepView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToClock(wakeup){
        console.log("switchToClock",wakeup)
        closeAll();
        clockView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToWeather(wakeup){
        console.log("switchToWeather",wakeup)
        closeAll();
        weatherView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToMusic(wakeup){
        console.log("switchToMusic",wakeup)
        closeAll();
        musicView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToKodi(wakeup){
        console.log("switchToKodi",wakeup)
        closeAll();
        kodiView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToFibaro(wakeup){
        console.log("switchToFibaro",wakeup)
        closeAll();
        fibaroView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToRecipe(wakeup){
        console.log("switchToRecipe",wakeup)
        closeAll();
        recipeView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToVideo(wakeup){
        console.log("switchToVideo",wakeup)
        videoView.visible = true;
        closeAll();

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToYouTube(wakeup){
        console.log("switchToYouTube",wakeup)
        closeAll();
        youtubeView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToTrains(wakeup){
        console.log("switchToTrains",wakeup)
        closeAll();
        trainsDisplay.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToTimer(wakeup){
        console.log("switchToTimer",wakeup)
        closeAll();
        timerDisplay.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToTrigger(wakeup){
        console.log("switchToTimer",wakeup)
        closeAll();
        triggerView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToAlarm(wakeup){
        console.log("switchToTimer",wakeup)
        closeAll();
        alarmView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToMirror(wakeup){
        console.log("switchToMirror",wakeup)
        closeAll();
        mirrorView.start();
        mirrorView.visible = true;

        doNotification(false);
        backgroundTimer.stop();
        if (wakeup === true) window.alive();
    }

    function switchToNews(wakeup){
        console.log("switchToNews",wakeup)
        closeAll();
        newsView.visible = true;

        doNotification(false);
        backgroundTimer.start();
        if (wakeup === true) window.alive();
    }

    function switchToSettings(){
        swipeView.visible = false;
        settingPageView.visible = true;
        backgroundTimer.stop();
    }

    /*Timer {
        id: volumeTimer2
        interval: 10000
        running: true
        repeat: true
        onTriggered:{
            showVolume(window.volume + 8);
        }
   }*/

    function showVolume(){
        volumeFrame.visible = true
        volumeTimer.restart()
    }

    Item {
        id: alertsFrame
        x: 0
        y: 0
        height: 80
        width: imageWidth - 100
        visible: false
        RowLayout {
            spacing: 10
            x: 10
            y: 10
            Rectangle{
                id: alertGarageDoor
                height: 60
                width: 60
                color: "white"
                radius: 10
                visible: false
                Image {
                    anchors.fill: parent
                    source: "files/icon/garageOpen.png"
                    fillMode: Image.PreserveAspectFit
                }
            }
        }
    }

    Item {
        id: assistantFrame
        x: 0
        y: 0
        height: 80
        width: imageWidth
        z: 5
        visible: controller.assistantStatus
        Rectangle {
            anchors.fill: parent
            color: "white"
            opacity: 1
        }

        RowLayout {
            spacing: 10
            x: 10
            y: 10
            Rectangle {
                width: 60; height: 60

                AnimatedImage {
                    anchors.fill: parent
                    id: animation
                    source: "files/icon/assistant.gif"
                }

                Rectangle {
                    property int frames: animation.frameCount

                    width: 4; height: 8
                    x: (animation.width - width) * animation.currentFrame / frames
                    y: animation.height
                    //color: "red"
                }
            }
            Rectangle {
                width: imageWidth - 100
                height: 60
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: controller.assistantCommand
                    font.pointSize: 20
                    font.family: "Helvetica"
                    font.bold: true
                    wrapMode: Text.WordWrap
                }
            }
        }
    }

    Item {
        id: volumeFrame
        x: 0
        y: 400
        height: 80
        width: imageWidth
        z: 5
        visible: false

        function getOpacity(i){
            var v = window.volume * 100
            if(i*10 < v){
                return 1.0;
            }else if(i*10 < (v+10)){
                var o = v % 10;
                console.log(v,o,o/10*0.8);
                return (o/10*0.8)+0.2;
            }

            return 0.2;
        }

        Rectangle {
            anchors.fill: parent
            color: "black"
            opacity: 0.4
        }

        Timer {
            id: volumeTimer
            interval: 3000
            running: false
            repeat: false
            onTriggered:{
                volumeFrame.visible = false;
            }
       }

        RowLayout {
            spacing: 45
            x: 20
            y: 20
            Repeater {
                model: 10;
                Rectangle {
                    height: 30
                    width: 30
                    radius: 20
                    opacity: volumeFrame.getOpacity(index)
                    color: "white"
                }
            }
        }
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: window.height
        width: window.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: window.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }

//    MouseArea {
//        x:0
//        y:0
//        height: imageWidth
//        width: imageHeight
//        onClicked: {
//            console.log(mouseX,mouseY)
//        }
//    }
}

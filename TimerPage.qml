import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.0

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle{
        color: "white"
    }


    Rectangle {
        id : setup
        x: 0
        y: 40
        width: imageWidth
        height: imageHeight - 60
        visible: controller.timerList.length > 0? false: true

        property int startminutes: 5

        Column {
            anchors.centerIn: parent
            height: 300
            width: 400
            spacing: 20
            Text {
                text: "New Timer"
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 30
                color: "black"
            }

            Row {
                spacing: 20

                Text {
                    text: "Minutes"
                    minimumPointSize: 10
                    fontSizeMode: Text.HorizontalFit
                    font.pointSize: 16
                    color: "black"
                }

                TextField {
                    id: tfTime
                    width: 90
                    height: 30
                    text: setup.startminutes
                    placeholderText: "Minutes"
                    inputMethodHints: Qt.ImhDigitsOnly
                    font.pointSize: 16
                }
                Text {
                    color: "grey"
                    font.pointSize: 30
                    font.family: fontAwesome.name
                    text: FontAwesome.icons.fa_plus

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            setup.startminutes++
                        }
                    }
                }
                Text {
                    color: "grey"
                    font.pointSize: 30
                    font.family: fontAwesome.name
                    text: FontAwesome.icons.fa_minus

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            setup.startminutes--;
                            if(setup.startminutes < 1)
                                setup.startminutes = 1
                        }
                    }
                }
            }

            Button {
                id: btStart
                anchors.horizontalCenter: parent.horizontalCenter
                height: 42
                text: qsTr("Start")
                font.pointSize: 16
            }

            Connections {
                target: btStart
                onClicked: {
                    controller.createTimer("Timer",setup.startminutes*60*1000);
                }
            }
        }
    }

    GridLayout {
        id : grid
        x: 0
        y: 40
        width: imageWidth
        height: imageHeight - 60
        columns : grid.getColums()
        columnSpacing: 0
        rowSpacing: 0
        visible: controller.timerList.length > 0? true: false

        function getColums(){
            var c = controller.timerList.length;
            if(c <= 1) return 1;
            else if(c <= 4) return 2;
            return 3;
        }

        function getColumsSpan(index){
            var c = controller.timerList.length;

            if(index === (c-1) && grid.columns === 2){
                return 2;
            }

            return 1;
        }

        Repeater {
            model: controller.timerList.length
            Rectangle {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.columnSpan: grid.getColumsSpan(index)

                Column {
                    anchors.fill: parent

                    Rectangle {
                        width: parent.width
                        height: 20

                        Text {
                            text: controller.timerList[index].name
                            anchors.centerIn: parent
                            minimumPointSize: 10
                            fontSizeMode: Text.HorizontalFit
                            font.pointSize: 20
                            color: "black"
                        }
                    }

                    Rectangle {
                        width: parent.width
                        height: parent.height - 20

                        CircularProgressBar {
                            id: circleProgress
                            lineWidth: 10
                            value: controller.timerList[index].percentage
                            size: parent.width < parent.height? parent.width : parent.height
                            secondaryColor: "#e0e0e0"
                            primaryColor: "#29b6f6"
                            anchors.centerIn: parent

                            Column {
                                anchors.centerIn: parent
                                Text {
                                    text: controller.timerList[index].currentStr
                                    //width: parent.width
                                    //height: parent.height
                                    minimumPointSize: 10
                                    fontSizeMode: Text.Fit
                                    font.pointSize: 60
                                    color: controller.timerList[index].enable?circleProgress.primaryColor:circleProgress.secondaryColor
                                }

                                Rectangle {
                                    //width: parent.width
                                    height: 10
                                }

                                Row {
                                    spacing: 20

                                    anchors.horizontalCenter: parent.horizontalCenter

                                    Text {
                                        color: "grey"
                                        font.pointSize: 20
                                        font.family: fontAwesome.name
                                        text: controller.timerList[index].enable?FontAwesome.icons.fa_pause:FontAwesome.icons.fa_play
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                if(controller.timerList[index].enable)
                                                    controller.pauseTimer(index)
                                                else
                                                    controller.resumeTimer(index)
                                            }
                                        }
                                    }

                                    Text {
                                        color: "grey"
                                        font.pointSize: 20
                                        font.family: fontAwesome.name
                                        text: FontAwesome.icons.fa_stop
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                controller.stopTimer(index)
                                            }
                                        }
                                    }

                                    Text {
                                        color: "grey"
                                        font.pointSize: 20
                                        font.family: fontAwesome.name
                                        text: FontAwesome.icons.fa_plus
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                controller.addMinuteToTimer(index)
                                            }
                                        }
                                    }
                                 }
                            }
                        }
                    }
                }
            }
        }
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("VideoPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

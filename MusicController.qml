import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
//import QtGraphicalEffects 1.0
import QtMultimedia 5.11

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle{
        color:"black"
    }

//    MediaPlayer{
//        id: player
//        playlist: Playlist {
//            id: playlist
//            Component.onCompleted: {
//                playlist.load(galleryPath+"/music/allfiles.m3u")
//            }
//        }
//    }

    Image {
        id: imgBG
        x: 0
        y: 0
        width: imageWidth
        height: imageHeight
        fillMode: Image.PreserveAspectCrop
        smooth: true
        source: controller.musicMediaData.songArt

        Rectangle {
            anchors.fill: parent;
            color: Qt.hsla(0,0,0,0.9)
        }
    }

//    FastBlur {
//        anchors.fill: imgBG
//        source: imgBG
//        radius: 32
//    }

    Item {
        x: 20
        y: 40
        width: 380
        height: 360

        Rectangle{
            color:"black"
        }

        Image {
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: controller.musicMediaData.songArt
        }
    }

    Label {
        x: 20
        y: 400
        text: SimpleJsTools.milltoString(controller.musicPosition)
        color: "white"
        font.pointSize: 18
        font.family: "Helvetica"
    }

    Label {
        x: imageWidth - 60
        y: 400
        text: SimpleJsTools.milltoString(controller.musicDuration)
        color: "white"
        font.pointSize: 18
        font.family: "Helvetica"
    }

    Slider {
        id: seekSlider
        value: controller.musicPosition
        to: controller.musicDuration

        x: 20
        y: 430
        width: imageWidth - 40
        height: 20

        ToolTip {
            parent: seekSlider.handle
            visible: seekSlider.pressed
            text: pad(Math.floor(value / 60)) + ":" + pad(Math.floor(value % 60))
            y: parent.height

            readonly property int value: seekSlider.valueAt(seekSlider.position)

            function pad(number) {
                if (number <= 9)
                    return "0" + number;
                return number;
            }
        }

        handle: Rectangle {
            x: seekSlider.visualPosition * (seekSlider.width - width)
            y: (seekSlider.height - height) / 2
            width: 40
            height: 40

            radius: 20
            color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
            border.color: "#9c27b0"
        }

        background: Rectangle {
            y: (seekSlider.height - height) / 2
            height: 10
            radius: 5
            color: "#686868" //background slider

            Rectangle {
                width: seekSlider.visualPosition * parent.width
                height: parent.height
                color: "#b92ed1" //done part
                radius: 5
            }
        }

        onPressedChanged: {
            controller.MusicController.seek(seekSlider.value)
        }
    }

    /*TODO Label {
        anchors.horizontalCenter: parent.horizontalCenter
        y: 10
        text: musicplayer.currentSongIndex===-1?"":"Now Playing - "+ (musicplayer.currentSongIndex+1) +" of "+musicplaylist.itemCount
        color: "white"
        font.pointSize: 16
        font.family: "Helvetica"
        font.bold: true
    }*/

    Item {
        id: songLabelContainer
        x: 420
        y: 100
        width: imageWidth - 420
        height: 35

        clip: true

        Layout.fillWidth: true
        Layout.preferredHeight: songNameLabel.implicitHeight

        SequentialAnimation {
            running: true
            loops: Animation.Infinite

            PauseAnimation {
                duration: 10000
            }
            ParallelAnimation {
                XAnimator {
                    target: songNameLabel
                    from: 0
                    to: -(songLabelContainer.width - songNameLabel.implicitWidth)
                    duration: 10000
                }
            }
            ParallelAnimation {
                XAnimator {
                    target: songNameLabel
                    from: 0
                    to: 0
                    duration: 100
                }
            }
            PauseAnimation {
                duration: 10000
            }
        }

        Rectangle {
            id: leftGradient
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#0d0d0e57"
                }
                GradientStop {
                    position: 1
                    color: "#000000ba"
                }
            }

            width: height
            height: parent.height
            anchors.left: parent.left
            z: 1
            rotation: -90
            opacity: 0
        }

        Label {
            id: songNameLabel
            text: controller.musicMediaData.Title
            color: "white"
            font.pointSize: 20
            font.family: "Helvetica"
            font.bold: true
        }

        Rectangle {
            id: rightGradient
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#000000ba"
                }
                GradientStop {
                    position: 1
                    color: "#0d0d0e57"
                }
            }

            width: height
            height: parent.height
            anchors.right: parent.right
            rotation: -90
        }
    }

    Item {
        x: 420
        y: 130
        ColumnLayout {
            spacing: 5
            Label {
                text: controller.musicMediaData.Artist
                color: "white"
                font.pointSize: 16
                font.family: "Helvetica"
            }
            Label {
                text: controller.musicMediaData.Album
                color: "white"
                font.pointSize: 16
                font.family: "Helvetica"
            }
        }
    }

    RowLayout {
        x: 420
        y: 270
        spacing: 16
        Layout.alignment: Qt.AlignHCenter

        Rectangle {
            id: btPrevious
            width: 50
            height: 50
            radius: 25
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_step_backward
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.MusicController.musicPrevious()
                }
            }
        }
        Rectangle {
            id: btStop
            width: 50
            height: 50
            radius: 25
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_stop
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.MusicController.musicStop();
                }
            }
        }
        Rectangle {
            id: btPause
            width: 70
            height: 70
            radius: 35
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: controller.isMusicPlaying?FontAwesome.icons.fa_pause:FontAwesome.icons.fa_play
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {               
                    console.log("clicked");
                    if(controller.MusicController.isPlaying){
                        console.log("pause");
                        controller.MusicController.pause()
                    }else{
                        console.log("play");
                        controller.MusicController.play()
                    }
                }
            }
        }
        Rectangle {
            id: btRandom
            width: 50
            height: 50
            radius: 25
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_random
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.MusicController.musicShuffle()
                }
            }
        }  

        Rectangle {
            id: btForward
            width: 50
            height: 50
            radius: 25
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_step_forward
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.MusicController.musicNext()
                }
            }
        }
    }

    Timer {
        id: volumeTimer
        interval: 3000
        running: false
        repeat: false
        onTriggered:{
            volumeSlider.visible = false;
        }
   }

    Slider {
        id: volumeSlider
        x: 490
        y: 365
        width: 200
        height: 20

        value: window.volume * 100
        to: 100

        visible: false

        //orientation: Qt.Vertical

        ToolTip {
            parent: volumeSlider.handle
            visible: volumeSlider.pressed
            text: pad(Math.floor(value / 60)) + ":" + pad(Math.floor(value % 60))
            y: parent.height

            readonly property int value: volumeSlider.valueAt(volumeSlider.position)

            function pad(number) {
                if (number <= 9)
                    return "0" + number;
                return number;
            }
        }

        handle: Rectangle {
            x: volumeSlider.visualPosition * (volumeSlider.width - width)
            y: (volumeSlider.height - height) / 2
            width: 40
            height: 40

            radius: 20
            color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
            border.color: "#9c27b0"
        }

        background: Rectangle {
            y: (volumeSlider.height - height) / 2
            height: 10
            radius: 5
            color: "#686868" //background slider

            Rectangle {
                width: volumeSlider.visualPosition * parent.width
                height: parent.height
                color: "#b92ed1" //done part
                radius: 5
            }
        }

        onPressedChanged: {
            controller.setVolume(volumeSlider.value)
            volumeTimer.restart()
        }
    }


    Rectangle {
        id: btVolume
        x: 420
        y: 350
        width: 50
        height: 50
        radius: 25
        color: "#ce6ddf"
        Text {
            color: "white"
            anchors.centerIn: parent
            font.pointSize: 25
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_volume_up
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                volumeSlider.visible = !volumeSlider.visible
                volumeTimer.start()
            }
        }
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("MusicPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

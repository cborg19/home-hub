#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QObject>
#include <QTime>
#include <QMutex>
#include <QSharedMemory>

#include "imagescan.h"
#include "weather.h"
#include "gpio.h"
#include "lightsensor.h"
#include "lcdcontroller.h"
#include "earthdata.h"
#include "musiccontroller.h"
#include "volumecontroller.h"
#include "fibarocontroller.h"
#include "foodcontroller.h"
#include "kodicontroller.h"
#include "sydneytrains.h"
#include "timercontroller.h"
#include "voiceassistant.h"
#include "alarmcontroller.h"
#include "newscontroller.h"

extern bool gdebug;

struct VoiceCommand {
    bool enabled;
    QString command;
};

struct Controllers {
    Imagescan *m_imagescan;
    WeatherController *m_weather;
    Gpio *m_gpio;
    LightSensor *m_lightSensor;
    LCDController *m_LCD;
    EarthData *m_earth;
    VolumeController *m_volume;
    MusicController *m_musicplayer;
    KodiController *m_kodi;
    SydneyTrains *m_trains;
    FibaroController *m_fibaro;
    FoodController *m_recipe;
    VoiceAssistant *m_assistant;
    TimerController *m_timers;
    AlarmController *m_alarms;
    NewsController *m_news;
};

class WorkerThread : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int lightDayTime READ getDayTimeLight WRITE setDayTimeLight)
    Q_PROPERTY(int lightNightTime READ getNightTimeLight WRITE setNightTimeLight)
    Q_PROPERTY(int lightClockTime READ getClockTimeLight WRITE setClockTimeLight)
public:
    explicit WorkerThread(QObject *parent = nullptr);

    void Initialise(Controllers c);

    void stop() { QMutexLocker M(&l_stop); running = false; }
    void stayalive();
    void awayMode();
    void backHome();

    int getDayTimeLight(){ QMutexLocker M(&l_settings); return lightDay; }
    void setDayTimeLight(int v){ QMutexLocker M(&l_settings); lightDay = v; }
    int getNightTimeLight(){ QMutexLocker M(&l_settings); return lightNight; }
    void setNightTimeLight(int v){ QMutexLocker M(&l_settings); lightNight = v; }
    int getClockTimeLight(){ QMutexLocker M(&l_settings); return lightClock; }
    void setClockTimeLight(int v){ QMutexLocker M(&l_settings); lightClock = v; }

    void setDayTime(int v, int on, int off){
        QMutexLocker M(&l_settings);
        lightDay = v;
        lightDayOn = on;
        lightDayOff = off;
    }
    void setClockTime(int v, int on, int off){
        QMutexLocker M(&l_settings);
        lightClock = v;
        lightClockOn = on;
        lightClockOff = off;
    }
    void setNightTime(int v, int on, int off){
        QMutexLocker M(&l_settings);
        lightNight = v;
        lightNightOn = on;
        lightNightOff = off;
    }
signals:
    void switchScreen(const QString &result);
    void resultReady(const QString &result);
    void assistantChange(bool, QString);
    void assistantVoice(QString);
public slots:
    void run();
private:
    QMutex l_stop, l_alive, l_settings;

    bool running;
    bool m_timeSyncCompleted;
    bool m_screenEnable;

    qint64 pLight;

    enum ScreenType {Standard, Night, Clock};
    int screenMode;

    bool doAlive, doAway;
    qint64 pAlive;
    bool pAway;

    Controllers control;

    QTime nightModeStart;
    QTime nightModeFinish;
    QTime sunrise;
    QTime sunset;

    int lightNight, lightClock, lightDay;
    int lightNightOn, lightClockOn, lightDayOn;
    int lightNightOff, lightClockOff, lightDayOff;

    void SyncTime();

    void timeStarted(QTime time, QTime r, QTime s, int light = -1);
    void timeState(QTime time,  QTime r, QTime s);

    void setSleepMode(int light = -1);
    void setSunRise(int light = -1);
    void setSunSet(int light = -1);
    void setStandard(int light = -1);

    void alive(bool results);
    void away();
    void voiceAlive();

    QSharedMemory sharedMemory;

    QString autoCorrect(QString);
};


class BackgroundThread : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString pathName READ pathName WRITE setPathName)
public:
    explicit BackgroundThread(QObject *parent = nullptr);

    void Initialise(Controllers c);

    void stop() { QMutexLocker M(&l_stop); running = false; }

    void processMusic();

    QString pathName();
    void setPathName(const QString &pathName);
signals:
public slots:
    void run();
private slots:
    void showModifiedDir(const QString& str);
private:
    QMutex l_stop, l_music;
    bool running;

    QString m_pathName;

    qint64 pLastWeather;
    qint64 pLastGalleryScan;

    qint64 m_weatherUpdate;
    qint64 m_galleryUpdate;

    bool scanMusic;
    void scanMusicDirectory();
    //QFileSystemWatcher watcher;

    Controllers control;
};


#endif // WORKERTHREAD_H

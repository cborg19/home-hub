import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Page {
    width: 800
    height: 480

    background: Rectangle {
        color: "transparent"
    }

    function onInitialLoad(){
       kodiSwipeView.currentIndex = 1
    }

    function setPageIndex(index){
        kodiSwipeView.currentIndex = index;
    }

    SwipeView {
        id: kodiSwipeView
        anchors.fill: parent
        visible: controller.kodiConnected

        function getShowTitle(){
            if(controller.kodiCurrentMetaData.type === "movie")
                if(controller.kodiCurrentMetaData.tagline !== undefined)
                    return controller.kodiCurrentMetaData.tagline;
            if(controller.kodiCurrentMetaData.type === "episode")
                if(controller.kodiCurrentMetaData.showtitle !== undefined)
                    return controller.kodiCurrentMetaData.showtitle;
            return ""
        }

        /*function base64Encode (input) {
            var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
            var str = String(input);
            for (
                // initialize result and counter
                var block, charCode, idx = 0, map = chars, output = '';
                str.charAt(idx | 0) || (map = '=', idx % 1);
                output += map.charAt(63 & block >> 8 - idx % 1 * 8)
                ) {
                charCode = str.charCodeAt(idx += 3/4);
                if (charCode > 0xFF) {
                    throw new Error("Base64 encoding failed: The string to be encoded contains characters outside of the Latin1 range.");
                }
                block = block << 8 | charCode;
            }
            return output;
        }*/

        background: Image {
            id: kodiBackground
            //anchors.fill: parent
            fillMode: Image.PreserveAspectCrop
            source: controller.kodiCurrentMetaData.bgImage !== undefined?"image://imageprovider/"+controller.kodiCurrentMetaData.bgImage:"" //
            width: 1000
            height: 600
            x: -100

            Behavior on x {
                NumberAnimation {
                    //This specifies how long the animation takes
                    duration: 600
                    //This selects an easing curve to interpolate with, the default is Easing.Linear
                    easing.type: Easing.InOutQuad
                }
            }

            Rectangle {
                anchors.fill: parent;
                color: Qt.hsla(0,0,0,0.6)
            }
        }

        onCurrentIndexChanged:{
            console.log("index changed:"+kodiSwipeView.currentIndex)
            var p = -100;
            if(kodiSwipeView.currentIndex === 1) p = -50;
            else if(kodiSwipeView.currentIndex === 2) p = 0;

            kodiBackground.x = p;
        }

        KodiDetail {
        }

        KodiControls{
            id: kodiControls
        }

        KodiList {
        }
    }

    Page {
        visible: !controller.kodiConnected
        width: imageWidth
        height: imageHeight

        background: Image {
            anchors.fill: parent
            fillMode: Image.PreserveAspectCrop
            source: "/files/icon/kodib.jpg"
        }

        Label {
            x: 320
            y: 100
            text: "Kodi not connected"
            color: "white"
            font.pointSize: 20
            font.family: "Helvetica"
        }
    }

    Page {
        id: kodiSwipeViewDots
        z: 1
        x: 375
        y: 470
        width: 50
        height: 10
        visible: controller.kodiConnected

        background: Rectangle {
            color: "transparent"
        }

        RowLayout {
            spacing: 5
            Rectangle {
                color: kodiSwipeView.currentIndex==0?"black":"white"
                height: 5
                width: 5
                radius: 2
            }
            Rectangle {
                color: kodiSwipeView.currentIndex==1?"black":"white"
                height: 5
                width: 5
                radius: 2
            }
            Rectangle {
                color: kodiSwipeView.currentIndex==2?"black":"white"
                height: 5
                width: 5
                radius: 2
            }
        }
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("KodiPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

import QtQuick 2.0
import QtQuick.Controls 2.4

Item {
    id: radarItem
    width: 480
    height: 480

    property int imageWidth: 480
    property int imageHeight: 480
    property int currentPos: 0

    z: 2
    Rectangle {
         color:"black"
         anchors.fill: parent;
    }

    Repeater {
        model: 4; // just define the number you want, can be a variable too
        Label {
            x:0
            y:0
            z: 4
            id: location
            anchors.horizontalCenter: itemView.horizontalCenter
            text: qsTr(radardata.modelData.name)//
            color: "white"
            font.pointSize: 40
            font.family: "Helvetica"
            font.bold: true
        }

        Image {
            id: img
            anchors.fill: parent;
            source: "file://" + radardata.modelData.radarBg[index]
            //fillMode: Image.PreserveAspectFit
            cache: true
            //Using SourceSize will store the scaled down image in memory instead of the whole original image
            sourceSize.width: imageWidth
            sourceSize.height: imageHeight
        }

    }

    Repeater {
        id: imgDisplay
        model: radardata.modelData.radarImagesSize;
        Image {
            z: 3
            anchors.fill: parent;
            source: radardata.modelData.radarImages[index]
            visible: false
            //source: radardata.modelData.m_radarImagelist[]
            //source: radardata.modelData.radarImages[0]
            //fillMode: Image.PreserveAspectFit
            cache: true
            //Using SourceSize will store the scaled down image in memory instead of the whole original image
            sourceSize.width: imageWidth
            sourceSize.height: imageHeight
        }
    }

    Timer {
        id: cycleImages
        interval: 500
        running: false
        repeat: true
        onTriggered:{
            var pointer = imgDisplay.itemAt(radarItem.currentPos)
            if(pointer !== null) pointer.visible = false;
            radarItem.currentPos++;
            if(radarItem.currentPos > radardata.modelData.radarImagesSize) radarItem.currentPos = 0;
            pointer = imgDisplay.itemAt(radarItem.currentPos)
            if(pointer !== null) pointer.visible = true;
        }
    }

    Component.onCompleted: {
        radarItem.currentPos = 0;
        var pointer = imgDisplay.itemAt(radarItem.currentPos)
        if(pointer !== null) pointer.visible = true;
        cycleImages.start();
    }

    Button {
        id: btMenu
        x: 480-50
        y: 0
        height: 50
        width: 50
        z: 4
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
        Image {
            id: btFrameImage
            anchors.fill: parent
            source: "files/icon/cancel.png"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            cycleImages.stop();
            delayClose.start();
        }
    }

    Timer {
        id: delayClose
        interval: 500
        running: false
        repeat: false
        onTriggered:{
            delayClose.stop();
            radarItem.destroy();
        }
    }
}

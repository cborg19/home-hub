import QtQuick 2.11
import QtQuick.Controls 2.4

import "js/skycons.js" as WeatherIcon
import "js/simpletools.js" as SimpleTools

Page {
    id: sleepClock
    anchors.fill: parent

    width: imageWidth
    height: imageHeight

    function refreshCanvas(){
        panCanvas.requestPaint();
        panCanvasNight.requestPaint();
        panCanvasTop.requestPaint();
        panCanvasTomorrow.requestPaint();
    }

    function setWeather(currentDate){
        var hr = parseInt(Qt.formatDateTime(currentDate, "HH"));
        if(hr < 12){
            nightTemp.visible = false
            tomrrowTemp.visible = false
            topTemp.visible = true
        }else{
            topTemp.visible = false
            nightTemp.visible = true
            tomrrowTemp.visible = true
        }
    }

    Connections {
        target: sleepView
        onVisibleChanged:
            if(visible){
                clockTimer.start();
                timeView.displayTime();
            }else{
                clockTimer.stop();
            }

        ignoreUnknownSignals: true
    }

    background: Rectangle{
        color: nightBgColour
    }

    Page {
        id: timeView
        anchors.fill: parent
        width: imageWidth
        height: imageHeight

        property string currentTime: ""
        property string currentHalf: ""
        property string currentDate: ""

        background: Rectangle{
            color:"transparent"
        }

        Row {
            id: topRow
            //anchors.centerIn: parent
            anchors.horizontalCenter: parent.horizontalCenter
            y:100
            spacing: 0
            Label {
                id: clockHrMin
                text: qsTr(timeView.currentTime)
                font.pointSize: 80
                font.family: "Helvetica"
                font.bold: true
                color: nightFontColour
            }
            Label {
                id: clockDatHalf
                text: qsTr(timeView.currentHalf)
                font.pointSize: 40
                y: 40
                font.family: "Helvetica"
                font.bold: true
                color: nightFontColour
            }
        }

        Label {
            id: dateRow
            anchors.horizontalCenter: topRow.horizontalCenter
            anchors.top: topRow.bottom
            anchors.topMargin: 10
            text: qsTr(timeView.currentDate)
            font.pointSize: 30
            color: nightFontColour
        }

        Button {
            id: btFrame
            anchors.fill: parent

            x:0
            y:0
            width: imageWidth
            height: imageHeight
            z: 2
            padding: 10
            spacing: 10
            display: AbstractButton.IconOnly
            focusPolicy: Qt.NoFocus
            background: Rectangle {
                color: "transparent"
            }
        }

        Connections {
            target: btFrame
            onClicked: {
//                controller.screenPhoto()
                window.switchToPhoto(true)
            }
        }

        function displayTime(){
            var currentDate = new Date();
            if(time24hr){
                timeView.currentTime = Qt.formatDateTime(currentDate, "HH:mm")
                timeView.currentHalf = ""
            }else{
                var time = Qt.formatDateTime(currentDate, "h:mm|A").split("|")
                timeView.currentTime = time[0]
                timeView.currentHalf = time[1]
            }

            timeView.currentDate = Qt.formatDateTime(currentDate, "dddd, d MMMM yyyy")
            sleepClock.setWeather(currentDate)
        }

        Timer {
            id: clockTimer
            interval: 1000
            running: false
            repeat: true
            onTriggered:{
                timeView.displayTime();
            }
        }
    }

    Page {
        id: info
        x: 20
        y: 400
        property int canvasWidth: 50
        property int canvasHeight: 50

        background: Rectangle {
            color: "transparent"
        }

        Item {
            id: secondRow
            property date lastUpdateWeather: new Date("2010-01-01")

            function getTemp(){
                //if(controller.weatherDataSize === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].current.temperature === undefined) return "";
                return SimpleTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].current.temperature);
            }
            function getTempName(){
                //console.log("dd",controller.weatherDataSize)
                //if(controller.weatherDataSize === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].current.temperature === undefined) return "";
                return "°C";
            }

            Canvas {
                id: panCanvas
                width: info.canvasWidth
                height: info.canvasHeight
                x:0
                y:10
                Component.onCompleted: {

                }
                onPaint: {
                    if(controller.weatherDataModel[controller.weatherPrimary] !== undefined){
                        var ctx = getContext("2d");
                        ctx.save();
                        ctx.clearRect(0, 0, info.canvasWidth, info.canvasHeight);

                        WeatherIcon.skycons(ctx,controller.weatherDataModel[controller.weatherPrimary].current.icon, 0);
                        ctx.restore();
                        secondRow.lastUpdateWeather = new Date();
                    }
                }
            }
            Label {
                anchors.left: panCanvas.left
                anchors.bottom: panCanvas.top
                text:  qsTr("Current")
                font.pointSize: 10
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: panCanvas.right
                anchors.top: panCanvas.top
                id: tempText
                text:  qsTr(secondRow.getTemp())
                font.pointSize: 40
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: tempText.right
                anchors.top: tempText.top
                text:  qsTr(secondRow.getTempName())
                font.pointSize: 20
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }
    }
    Page {
        id: details

        background: Rectangle {
            color: "transparent"
        }
        Item {
            id: nightTemp
            visible: false
            x: 450
            y: 400

            function getTemp(){
                if(controller.weatherDataModel[controller.weatherPrimary].daily.length === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].daily[1].temperatureLow === undefined) return "";
                return SimpleTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].daily[1].temperatureLow);
            }
            function getTempName(){
                if(controller.weatherDataModel[controller.weatherPrimary].daily.length === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].daily[1].temperatureLow === undefined) return "";
                return "°C";
            }

            Canvas {
                id: panCanvasNight
                width: info.canvasWidth
                height: info.canvasHeight
                x:0
                y:10
                Component.onCompleted: {

                }
                onPaint: {
                    if(controller.weatherDataModel[controller.weatherPrimary] !== undefined){
                        var ctx = getContext("2d");
                        ctx.save();
                        ctx.clearRect(0, 0, info.canvasWidth, info.canvasHeight);

                        WeatherIcon.skycons(ctx,"clear-night", 0);
                        ctx.restore();
                        secondRow.lastUpdateWeather = new Date();
                    }
                }
            }
            Label {
                anchors.left: panCanvasNight.left
                anchors.bottom: panCanvasNight.top
                text:  qsTr("Night")
                font.pointSize: 10
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: panCanvasNight.right
                anchors.top: panCanvasNight.top
                id: tempNight
                text:  qsTr(nightTemp.getTemp())
                font.pointSize: 40
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                id: tempNightDegree
                anchors.left: tempNight.right
                anchors.top: tempNight.top
                text:  qsTr(nightTemp.getTempName())
                font.pointSize: 20
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }
        Item {
            id: tomrrowTemp
            visible: false
            x: 600
            y: 400

            function getTemp(){
                if(controller.weatherDataModel[controller.weatherPrimary].daily.length === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].daily[1].temperatureHigh === undefined) return "";
                return SimpleTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].daily[1].temperatureHigh);
            }
            function getTempName(){
                if(controller.weatherDataModel[controller.weatherPrimary].daily.length === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].daily[1].temperatureHigh === undefined) return "";
                return "°C";
            }

            Canvas {
                id: panCanvasTomorrow
                width: info.canvasWidth
                height: info.canvasHeight
                x:0
                y:10
                Component.onCompleted: {

                }
                onPaint: {
                    if(controller.weatherDataModel[controller.weatherPrimary].daily.length > 0){
                        var ctx = getContext("2d");
                        ctx.save();
                        ctx.clearRect(0, 0, info.canvasWidth, info.canvasHeight);

                        WeatherIcon.skycons(ctx,controller.weatherDataModel[controller.weatherPrimary].daily[1].icon, 0);
                        ctx.restore();
                        secondRow.lastUpdateWeather = new Date();
                    }
                }
            }
            Label {
                anchors.left: panCanvasTomorrow.left
                anchors.bottom: panCanvasTomorrow.top
                text:  qsTr("Tomorrow")
                font.pointSize: 10
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: panCanvasTomorrow.right
                anchors.top: panCanvasTomorrow.top
                id: tempTomorrow
                text:  qsTr(tomrrowTemp.getTemp())
                font.pointSize: 40
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: tempTomorrow.right
                anchors.top: tempTomorrow.top
                text:  qsTr(tomrrowTemp.getTempName())
                font.pointSize: 20
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }
        Item {
            id: topTemp
            visible: false
            x: 450
            y: 400

            function getTemp(){
                if(controller.weatherDataModel[controller.weatherPrimary].daily.length === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].daily[0].temperatureHigh === undefined) return "";
                return SimpleTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].daily[0].temperatureHigh);
            }
            function getTempName(){
                if(controller.weatherDataModel[controller.weatherPrimary].daily.length === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].daily[0].temperatureHigh === undefined) return "";
                return "°C";
            }

            Canvas {
                id: panCanvasTop
                width: info.canvasWidth
                height: info.canvasHeight
                x:0
                y:10
                Component.onCompleted: {

                }
                onPaint: {
                    if(controller.weatherDataModel[controller.weatherPrimary].daily.length > 0){
                        var ctx = getContext("2d");
                        ctx.save();
                        ctx.clearRect(0, 0, info.canvasWidth, info.canvasHeight);

                        WeatherIcon.skycons(ctx,controller.weatherDataModel[controller.weatherPrimary].daily[0].icon, 0);
                        ctx.restore();
                        secondRow.lastUpdateWeather = new Date();
                    }
                }
            }
            Label {
                anchors.left: panCanvasTop.left
                anchors.bottom: panCanvasTop.top
                text:  qsTr("Top")
                font.pointSize: 10
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: panCanvasTop.right
                anchors.top: panCanvasTop.top
                id: tempTop
                text:  qsTr(topTemp.getTemp())
                font.pointSize: 40
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: tempTop.right
                anchors.top: tempTop.top
                text:  qsTr(topTemp.getTempName())
                font.pointSize: 20
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }
    }
}

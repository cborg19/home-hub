#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QStandardPaths>
#include <QQmlContext>
#include <QCursor>
//#include <QtWebEngine>
#include <QIcon>

#include "photoframecontroller.h"
#include "myimageprovider.h"
#include "camera.h"

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
	
//    QtWebEngine::initialize(); // init the QT WebEngine

    //assign classes
    // add single instance of your object to the QML context as a property
    // the object will be available in QML with name "myObject"

    qmlRegisterType<PhotoFrameController>("photoframe.controller", 1, 0, "PhotoFrameController");
    qmlRegisterType<Camera>("Camera", 1, 0, "CameraWidget");

//    qmlRegisterType<Imagescan>("photoframe.imagescan", 1, 0, "Imagescan");
//    qmlRegisterType<Settings>("photoframe.settings", 1, 0, "Settings");
//    qmlRegisterType<WeatherController>("photoframe.weather", 1, 0, "WeatherController");

    #ifdef Q_OS_LINUX
    QGuiApplication::setOverrideCursor(QCursor(Qt::BlankCursor));
    #endif

    QQmlApplicationEngine engine;

    //Image Provider
    CacheImageProvider *ImageProvider = new CacheImageProvider;
    engine.rootContext()->setContextProperty("ProvedorImagem", ImageProvider);
    engine.addImageProvider(QLatin1String("provedor"), ImageProvider);

    engine.addImageProvider(QLatin1String("imageprovider"), new MyImageProvider);

    //Modification
    QUrl appPath(QString("%1").arg(app.applicationDirPath()));
    engine.rootContext()->setContextProperty("appPath", appPath);

    QUrl homePath;
    const QStringList homesLocation = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    if (homesLocation.isEmpty())
      homePath = appPath.resolved(QUrl("/"));
    else
      homePath = QString("%1").arg(homesLocation.first());
    engine.rootContext()->setContextProperty("homePath", homePath);

    QUrl desktopPath;
    const QStringList desktopsLocation = QStandardPaths::standardLocations(QStandardPaths::DesktopLocation);
    if (desktopsLocation.isEmpty())
      desktopPath = appPath.resolved(QUrl("/"));
    else
      desktopPath = QString("%1").arg(desktopsLocation.first());
    engine.rootContext()->setContextProperty("desktopPath", desktopPath);


    //orginal
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QObject::connect(&engine, &QQmlApplicationEngine::quit, &QGuiApplication::quit);
//    imagescan scan;
//    engine.rootContext()->setContextProperty("imagescan", &scan);

    return app.exec();
}

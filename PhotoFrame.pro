QT += multimedia qml quick websockets webengine
#QT += webengine multimedia qml quick websockets serialport
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS TAGLIB_STATIC

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    imagescan.cpp \
    settings.cpp \
    weather.cpp \
    networkmanager.cpp \
    photoframecontroller.cpp \
    gpio.cpp \
    lightsensor.cpp \
    lcdcontroller.cpp \
    workerthread.cpp \
    ktimezoned.cpp \
    earthdata.cpp \
    global.cpp \
    musiccontroller.cpp \
    volumecontroller.cpp \
    kodicontroller.cpp \
    myimageprovider.cpp \
    sydneytrains.cpp \
    fibarocontroller.cpp \
    foodcontroller.cpp \
    camera.cpp \
    voiceassistant.cpp \
    timercontroller.cpp \
    alarmcontroller.cpp \
    newscontroller.cpp

RESOURCES += qml.qrc 

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    imagescan.h \
    settings.h \
    weather.h \
    networkmanager.h \
    photoframecontroller.h \
    gpio.h \
    lightsensor.h \
    lcdcontroller.h \
    workerthread.h \
    ktimezoned.h \
    earthdata.h \
    global.h \
    musiccontroller.h \
    volumecontroller.h \
    kodicontroller.h \
    myimageprovider.h \
    sydneytrains.h \
    fibarocontroller.h \
    foodcontroller.h \
    camera.h \
    voiceassistant.h \
    timercontroller.h \
    alarmcontroller.h \
    newscontroller.h

#INCLUDEPATH += /usr/include/
#LIBS += -lpigpio -lrt -lpthread

#rampicam
###mmal, mmal_core, mmal_util, vcos, mmal_vc_client, mmal_components
# Requirements for raspicam
#INCLUDEPATH += $$PWD/lib/raspicam/include
#LIBS += -L/opt/vc/lib -lmmal -lmmal_core -lmmal_util
#LIBS += -L$$PWD/lib/raspicam/lib -lraspicam

#INCLUDEPATH += \
#    $$PWD/lib/taglib/include/taglib

#LIBS += \
#    -L$$PWD/lib/taglib/lib \
#    -ltag

#unix|win32: LIBS += -L$$PWD/lib/taglib/lib/ -ltag

LIBS += -L$$PWD/lib/taglib/lib/ -ltag
win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/taglib/lib/tag.lib
else:unix|win32-g++: PRE_TARGETDEPS += $$PWD/lib/taglib/lib/libtag.a
#PRE_TARGETDEPS += $$PWD/lib/taglib/lib/tag.lib
INCLUDEPATH += $$PWD/lib/taglib/include
DEPENDPATH += $$PWD/lib/taglib/include


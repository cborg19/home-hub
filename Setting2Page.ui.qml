import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    id: settingWeather
    width: imageWidth
    height: imageHeight

    background: Rectangle {
        color: "transparent"
    }

    header: Label {
        text: qsTr("Weather")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    property alias btPrimaryWeather: btPrimaryWeather
    property alias btAddWeather: btAddWeather
    property alias btUpdateWeather: btUpdateWeather
    property alias btRemoveWeather: btRemoveWeather
    property alias bmComboWeather: bmComboWeather
    property alias bmComboLocation: bmComboLocation
    property alias locName: locName
    property alias locLatitude: locLatitude
    property alias locLongitude: locLongitude
    property alias locBOM: locBOM

    ComboBox {
        id: bmComboWeather
        x: 239
        y: 10
        width: 114
        height: 32
        editable: false
        font.pointSize: 12
        model: settingWeather.intervalList
    }

    Label {
        id: label
        x: 43
        y: 13
        width: 190
        height: 25
        text: qsTr("Show weather slide every")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        font.pointSize: 16
    }

    GroupBox {
        id: groupBox
        x: 43
        y: 55
        width: 436
        height: 267
        font.pointSize: 10
        title: qsTr("Locations")

        ComboBox {
            id: bmComboLocation
            x: 0
            y: 6
            width: 305
            height: 24
        }

        TextField {
            id: locName
            x: 97
            y: 55
            width: 208
            height: 25
            text: qsTr("")
        }

        Label {
            id: label1
            x: 0
            y: 173
            width: 91
            height: 25
            text: qsTr("BOM Radar Id")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pointSize: 12
        }

        TextField {
            id: locLatitude
            x: 97
            y: 92
            width: 208
            height: 25
            text: qsTr("")
        }

        TextField {
            id: locLongitude
            x: 97
            y: 129
            width: 208
            height: 25
            text: qsTr("")
        }

        TextField {
            id: locBOM
            x: 97
            y: 173
            width: 208
            height: 25
            text: qsTr("")
        }

        Label {
            id: label2
            x: 30
            y: 55
            width: 61
            height: 25
            text: qsTr("Name")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pointSize: 12
        }

        Label {
            id: label3
            x: 15
            y: 92
            width: 76
            height: 25
            text: qsTr("Latitude")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pointSize: 12
        }

        Label {
            id: label4
            x: 23
            y: 129
            width: 68
            height: 25
            text: qsTr("Longitude")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pointSize: 12
        }

        Button {
            id: btAddWeather
            x: 337
            y: 0
            width: 65
            height: 35
            text: qsTr("Add")
        }

        Button {
            id: btUpdateWeather
            x: 337
            y: 0
            width: 65
            height: 35
            text: qsTr("Add")
            visible: false
        }

        Button {
            id: btRemoveWeather
            x: 337
            y: 63
            width: 65
            height: 35
            text: qsTr("Delete")
            font.capitalization: Font.MixedCase
        }

        Button {
            id: btPrimaryWeather
            x: 337
            y: 126
            width: 65
            height: 35
            text: qsTr("Primary")
            font.capitalization: Font.MixedCase
            visible: false
        }
    }

    Label {
        id: label5
        x: 359
        y: 13
        height: 25
        text: qsTr("minutes")
        font.pointSize: 16
        verticalAlignment: Text.AlignVCenter
    }


    /* Label {
        anchors.centerIn: parent
        text: "This is the applications path: " + appPath + "\nThis is the users home directory: "
              + homePath + "\nThis is the Desktop path: " + aString
    }*/
}

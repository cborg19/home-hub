import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.0

import "./js/fontawesome.js" as FontAwesome

Page {
    id: timeView
    width: imageWidth
    height: imageHeight

    background: Rectangle{
        color: "#f2f2f2"
    }

    property string dayOfWeek: "SMTWTFS"
    property var nextDate: null
    property string nextDateLeft: ""
    property string nextDateTime: ""

    Connections {
        target: alarmView
        onVisibleChanged:
            if(visible){
                nextDate = getNextAlarm()
            }else{
                nextDate = null
            }

        ignoreUnknownSignals: true
    }

    function getNextAlarm(){
        var date = null;
        for(var x=0; x<controller.alarmList.length; x++){
            if(!controller.alarmList[x].enabled) continue;
            if(controller.alarmList[x].type === "day"){

            }else if(controller.alarmList[x].type === "date"){
            }else continue;

        }

        //"Alarm in 20 hours 36 minutes"
        //"Friday, 20 Aug, 04:44"
        nextDateLeft = ""
        nextDateTime = ""
        return null;
    }

    function getTime(t){
        var date = new Date(t);
        var hours = date.getHours();

        var ampm = hours >= 12 ? 'pm' : 'am';

        hours = hours % 12;
        hours = hours ? hours : 12;

        var minutes = date.getMinutes();
        minutes = minutes < 10 ? '0'+minutes : minutes;

        if(time24hr){
            return hours+":"+minutes;
        }

        return hours+":"+minutes+" "+ampm;
    }

    AlarmNew {
        id: alarmNewData

        width: imageWidth
        height: imageHeight

        visible: false

        property int current: -1

        function loadEdit(){
            mainList.visible = false
            btMenu.visible = false
            addNew.visible = false

            alarmNewData.visible = true
        }

        function close(){
            addNew.visible = true
            mainList.visible = true;
            btMenu.visible = true;

            alarmNewData.visible = false
        }
    }

    /*Item {
        id: alarmNewData
        width: imageWidth
        height: imageHeight

        property int current: -1

        function loadEdit(){
            mainList.visible = false
            btMenu.visible = false
            addNew.visible = false
            var component = Qt.createComponent("AlarmNew.qml");
            if( component.status !== Component.Ready )
            {
                if( component.status === Component.Error )
                    console.debug("Error:"+ component.errorString() );
                return; // or maybe throw
            }
            component.createObject(alarmNewData);
        }

        function close(){
            addNew.visible = true
            mainList.visible = true;
            btMenu.visible = true;

        }
    }*/


    Label {
        x: 20
        y: 20
        id: nextAlarm
        text: nextDateLeft
        font.pointSize: 20
        color: "#333333"
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        x: 20
        y: 40
        id: nextDate
        text: nextDateTime
        font.pointSize: 20
        color: "#333333"
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: addNew
        x: imageWidth - 40
        y: 20
        color: "#333333"
        font.pointSize: 35
        font.family: fontAwesome.name
        text: FontAwesome.icons.fa_plus

        MouseArea {
            anchors.fill: parent
            onClicked: {
                alarmNewData.loadEdit()
            }
        }
    }

    Component {
        id: delegateAlarm
        ItemDelegate {
            width: parent.width
            height: 60
            id: rowItem
            property int mainIndex: index

            Rectangle{
                radius: 10
                anchors.fill: parent
                color: "white"
                Label {
                    x: 5
                    y: 5
                    text: controller.alarmList[index].messsage
                    color: "black"
                    font.pointSize: 8
                    font.family: "Helvetica"
                    font.bold: true
                }
                Label {
                    x:5
                    y:15
                    text: getTime(controller.alarmList[index].Time)
                    color: "black"
                    font.pointSize: 30
                    font.family: "Helvetica"
                    font.bold: true
                }

                Row {
                    x: 300
                    y: 20
                    visible: controller.alarmList[index].type === "day"? true:false
                    spacing: 10
                    Repeater {
                        model: 7
                        Label {
                            text: dayOfWeek[index]
                            color: controller.alarmList[rowItem.mainIndex].day[index] === 1? "blue": "black"
                            font.pointSize: 18
                            font.family: "Helvetica"
                            font.bold: true
                        }
                    }
                }

                Switch {
                    x: 600
                    y: 15
                    font.pointSize: 16
                    checked: controller.alarmList[index].enabled
                    onClicked: {
                        controller.setAlarmEnable(index, !controller.alarmList[index].enabled);
                    }
                }

                Text {
                    x: 700
                    y: 15
                    color: "#333333"
                    font.pointSize: 35
                    font.family: fontAwesome.name
                    text: FontAwesome.icons.fa_ellipsis_v

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            alarmNewData.current = index;
                            alarmNewData.loadEdit()
                        }
                    }
                }
            }
        }
    }

    ListView {
        id: mainList
        x: 20
        y: 100
        width: imageWidth-40
        height: imageHeight-100
        spacing: 10
        model: controller.alarmList
        delegate: delegateAlarm
    }


    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("AlarmPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

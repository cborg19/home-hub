#include "voiceassistant.h"
#include <QFileInfo>
#include <QtCore/qdebug.h>
#include <QProcess>

void AsyncVoiceResponse::run()
{
    qDebug() <<"AsyncVoiceResponse" << m_response;
    QFile check_file("/tmp/output.mp3");
    if(check_file.exists()){
        qDebug() << "FILE EXISTS AND REMOVE";
        check_file.remove();
    }
    #ifdef Q_OS_LINUX
    QString path("/home/pi/snowboy/");
    QString command("/usr/bin/python3");
    #else
    QString path("/Users/Chris/Documents/Development/reactjs/hubhomeqt/google/python3/");
    QString  command("/Users/Chris/AppData/Local/Programs/Python/Python37-32/python.exe");
    #endif
    qDebug() << "VOICE PATH - "<<command << path;
    qDebug() << "VOICE RESPONSE - "<<m_response;
    QStringList params = QStringList() << "tts.py" << m_response;

    QProcess process;
    process.startDetached(command, params, path);
    process.waitForFinished();
    QString p_stdout = process.readAll();
    process.close();

    emit voiceChanged();
}


VoiceAssistant::VoiceAssistant(QObject *parent) : QObject(parent)
{

}

bool VoiceAssistant::processVoiceCmd(QString cmd, QString &response)
{   
    if(cmd.contains("going away") || cmd.contains("home lockdown") || cmd.contains("house lockdown") || cmd.contains("home in lockdown") || cmd.contains("house in lockdown")){
        response = "Switching house into lockdown mode";
        pageTrigger("away",0);
        return true;
    }

    if(cmd.contains("good night")){
        response = "Good Night and sweet dreams";
        pageTrigger("away",0);
        return true;
    }

    if(cmd.contains("switch off all lights")){
        response = "Ok switching off all lights";
        return true;
    }

    if(cmd.contains("we are back") || cmd.contains("we home") || cmd.contains("we are home") || cmd.contains("rihanna")){
        response = "Welcome home";
        pageTrigger("alive",0);
        return true;
    }

    if(cmd.contains("exploded") || cmd.contains("self destruct") || cmd.contains("self-destruct")){
        response = "Self destructing in 5... 4... 3... 2... 1... bang";
        return true;
    }

    if(cmd.contains("who better")){
        QString key = "";
        if(cmd.contains("verse")) key = "verse";
        else if(cmd.contains("or")) key = "or";

        if(key != ""){
            cmd = cmd.remove("who better").trimmed();

            QStringList l = cmd.split(key);

            response = "";
            for(int x=0; x<l.length(); x++){
                response += "some say "+l[x].trimmed()+", ";
            }

            response += "while everyone say I am the best.";

            return true;
        }

        return false;
    }

    if(cmd.contains("tell")){ //tell mick to piss off
        QStringList w = cmd.split(" ");
        QString name = "";
        QString rep = "";
        int p = w.indexOf("to");
        if(p != -1){
            for(int x=1; x<p; x++){
                if(x != 1) name += " ";
                name += w[x];
            }

            for(int x=p+1; x<w.length(); x++){
                if(x != 1) rep += " ";
                rep += w[x];
            }
        }

        if(name != "" && rep != "")
            response = name+" "+rep;

        return true;
    }


    return false;
}

void VoiceAssistant::processAssistant(QString response)
{
    qDebug() <<"processAssistant" <<response;
    emit assistantVoice("reset");
    AsyncVoiceResponse *res = new AsyncVoiceResponse(response);
    connect(res, &AsyncVoiceResponse::voiceChanged, this, &VoiceAssistant::voiceChanged);

    pool.start(res);
}

void VoiceAssistant::voiceChanged()
{
    qDebug() <<"voiceChanged";
    QFile check_file("/tmp/output.mp3");

    int c = 0;
    while(!check_file.exists() && c < 100){
        QThread::msleep(100);
        c++;
    }
    if(check_file.exists()){
        //play audio file
        emit assistantVoice("start");
    }
}

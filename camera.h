#ifndef CAMERA_H
#define CAMERA_H

#include <QtQuick/QQuickPaintedItem>
#include <QThread>
#include <QImage>
#include <QMutex>
#ifdef Q_OS_LINUX
#include <raspicam.h>
using namespace raspicam;
#endif

class CameraWorker : public QObject
{
    Q_OBJECT
public:
    explicit CameraWorker();
    ~CameraWorker();

    void Stop();
private:
#ifdef Q_OS_LINUX
    RaspiCam camera;
#endif
    bool cameraRunning, m_running;
    unsigned char *data;
    QMutex l_running;

signals:
    void handleImage(QImage &image);
    void finished();

public slots:
    void doWork();
    void stopWork();
};


class Camera : public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(bool running WRITE setRunning)
    Q_PROPERTY(bool connected READ getConnected NOTIFY connectedChange)
public:
    explicit Camera(QQuickItem *parent = nullptr);

    void setRunning(bool);
    bool getConnected(){ return cameraRunning; }

    void paint(QPainter *painter);
signals:
    void connectedChange();
public slots:
private slots:
    void handleImage(QImage &image);
    void cameraFinished();
private:
    bool cameraRunning;
    QThread workerThread;
    CameraWorker *worker;
    QImage m_lastImage;

    void initialise();
};

#endif // CAMERA_H

#include <QtCore/qdebug.h>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QEventLoop>
#include <QJsonObject>
#include <QJsonArray>
#include <QtMath>
#include "sydneytrains.h"

AsyncTrainResponse::~AsyncTrainResponse()
{

}

void AsyncTrainResponse::run()
{

    QString url = "";
    //qDebug() << "AsyncTrainResponse" << m_type << m_from << m_to;
    if(m_type == "next"){
        QString d = m_time.toString("yyyyMMdd");
        QString t = m_time.toString("hhmm");
        url = "https://api.transport.nsw.gov.au/v1/tp/trip?outputFormat=rapidJSON&coordOutputFormat=EPSG%3A4326&depArrMacro=dep&itdDate="+d+"&itdTime="+t+"&type_origin=any&name_origin="+m_from+"&type_destination=any&name_destination="+m_to+"&calcNumberOfTrips=6&excludedMeans=checkbox&exclMOT_5=1&exclMOT_7=1&exclMOT_11=1&TfNSWTR=true&version=10.2.1.42";
    }


qDebug() << "AsyncTrainResponse" << url;
    manager = new QNetworkAccessManager();
    QNetworkRequest request;

    request.setUrl(QUrl(url));

    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "apikey ldwGRLmuuLgXgRtN74MXCKC6vWh1cEr2pWfg");

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);

    QNetworkReply* reply = manager->get(request);

    QEventLoop eventloop;
    connect(reply,SIGNAL(finished()),&eventloop,SLOT(quit()));
    eventloop.exec();

    if (reply->error() == QNetworkReply::NoError)
    {
        qDebug() << "httpReadyRead2";
        QString data = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
        QString reponse;
        if(!doc.isNull())
        {
            if(doc.isObject())
            {
                parseTimeTable(doc, reponse);
            }
        }
qDebug() <<"AsyncTrainResponse"<<reponse << m_voice;
        emit listChanged(m_list, reponse);
    }else{
    }
    delete manager;
}

void AsyncTrainResponse::parseTimeTable(QJsonDocument doc, QString &reponse)
{
    QString depstation, depTime;
    if(doc.isObject())
    {
        if(doc["journeys"].isArray()){
            QJsonArray journeysArray = doc["journeys"].toArray();
            if(journeysArray.size() > 0){
                for(int x=0; x<journeysArray.size(); x++){
                    QVariantMap trip;
                    QJsonObject tripObj = journeysArray[x].toObject();

                    trip.insert("interchanges",tripObj["interchanges"].toInt(0));

                    if(tripObj["legs"].isArray()){
                        QJsonArray legArray = tripObj["legs"].toArray();
                        QVariantList legs;
                        QVariantMap leg;
                        if(legArray.size() > 0){
                            for(int y=0; y<legArray.size(); y++){
                                QJsonObject legObj = legArray[y].toObject();

                                leg.insert("duration",legObj["duration"].toInt(0));

                                QJsonObject orgObj = legObj["origin"].toObject();
                                if(!orgObj.isEmpty()){
                                    QVariantMap origin;
                                    origin.insert("name",orgObj["name"].toString());
                                    origin.insert("id",orgObj["id"].toString());
                                    origin.insert("type",orgObj["type"].toString());

                                    qint64 p = QDateTime::fromString(orgObj["departureTimePlanned"].toString(),Qt::ISODate).toSecsSinceEpoch();
                                    origin.insert("departureTimePlanned",p);
                                    QString f = orgObj["departureTimeEstimated"].toString();
                                    if(f != ""){
                                        p = QDateTime::fromString(orgObj["departureTimePlanned"].toString(),Qt::ISODate).toSecsSinceEpoch();
                                        origin.insert("departureTime",p);
                                        origin.insert("departureTimeEstimated",QDateTime::fromString(orgObj["departureTimeEstimated"].toString(),Qt::ISODate).toSecsSinceEpoch());
                                    }else{
                                        origin.insert("departureTime",p);
                                        origin.insert("departureTimeEstimated",0);
                                    }

                                    QString n = getDisassembledName(orgObj["parent"].toObject());
                                    origin.insert("disassembledName",n);
                                    origin.insert("platform",getPlatform(orgObj["parent"].toObject()));
                                    if(depstation == ""){
                                        depstation = n;
                                        QDateTime now = QDateTime::currentDateTime();
                                        qint64 diff = p - now.toSecsSinceEpoch();
                                        if(diff < 60) depTime = "Now";
                                        else{
                                            int h = qFloor(diff / 3600);
                                            int m = qFloor(diff % 3600 / 60);
                                            if(h > 0) depTime = "in "+QString::number(h)+" hour";
                                            if(m == 0 && h == 0)
                                                depTime = "Now";
                                            else{
                                                if(depTime != "") depTime += " and ";
                                                else depTime += "in ";
                                                depTime += QString::number(m)+" minutes";
                                            }
                                        }
                                    }

                                    leg.insert("origin",origin);
                                }

                                QJsonObject destObj = legObj["destination"].toObject();
                                if(!destObj.isEmpty()){
                                    QVariantMap dest;
                                    dest.insert("name",destObj["name"].toString());

                                    dest.insert("id",destObj["id"].toString());
                                    dest.insert("type",destObj["type"].toString());
                                    qint64 p = QDateTime::fromString(destObj["arrivalTimePlanned"].toString(),Qt::ISODate).toSecsSinceEpoch();
                                    dest.insert("arrivalTimePlanned",p);
                                    QString f = destObj["arrivalTimeEstimated"].toString();
                                    if(f != ""){
                                        dest.insert("arrivalTime",QDateTime::fromString(destObj["arrivalTimeEstimated"].toString(),Qt::ISODate).toSecsSinceEpoch());
                                        dest.insert("arrivalTimeEstimated",QDateTime::fromString(destObj["arrivalTimeEstimated"].toString(),Qt::ISODate).toSecsSinceEpoch());
                                    }else{
                                        dest.insert("arrivalTime",p);
                                        dest.insert("arrivalTimeEstimated",0);
                                    }
                                    dest.insert("disassembledName",getDisassembledName(destObj["parent"].toObject()));
//qDebug() << "destObj" << getDisassembledName(destObj["parent"].toObject());
                                    leg.insert("destination",dest);
                                }

                                QJsonObject transObj = legObj["transportation"].toObject();
                                if(!transObj.isEmpty()){
                                    QVariantMap trans;

                                    trans.insert("name",transObj["name"].toString());
                                    trans.insert("id",transObj["id"].toString());
                                    QString tline = transObj["disassembledName"].toString();
                                    trans.insert("disassembledName",tline);
                                    if(tline == "T1")
                                        trans.insert("lineColour","#f89c1c");
                                    else if(tline == "T2")
                                        trans.insert("lineColour","#0097cd");
                                    else if(tline == "T3")
                                        trans.insert("lineColour","#f36421");
                                    else if(tline == "T4")
                                        trans.insert("lineColour","#015aa5");
                                    else if(tline == "T5")
                                        trans.insert("lineColour","#c32190");
                                    else if(tline == "T6")
                                        trans.insert("lineColour","#436cab");
                                    else if(tline == "T7")
                                        trans.insert("lineColour","#6e818e");
                                    else if(tline == "T8")
                                        trans.insert("lineColour","#008a45");
                                    else if(tline == "T9")
                                        trans.insert("lineColour","#d21f2f");
                                    else if(tline == "BMT")
                                        trans.insert("lineColour","#F79210");
                                    else if(tline == "CCN")
                                        trans.insert("lineColour","#D31B2E");
                                    else if(tline == "SCO")
                                        trans.insert("lineColour","#1F57A9");
                                    else if(tline == "SHL")
                                        trans.insert("lineColour","#0B974A");
                                    else if(tline == "HUN")
                                        trans.insert("lineColour","#802630");

                                    trans.insert("number",transObj["number"].toString());
                                    trans.insert("description",transObj["description"].toString());

                                    QJsonObject lineDest = transObj["destination"].toObject();
                                    if(!lineDest.isEmpty()){
                                        QString l = lineDest["name"].toString();
                                        QStringList w = l.split("via");

                                        trans.insert("lineName",w[0].trimmed());
                                        if(w.length() > 1)
                                            trans.insert("lineVia","via "+w[1].trimmed());
                                        else trans.insert("lineVia","");
                                    }

                                    leg.insert("transportation",trans);
                                }

                                bool limited = false;
                                QJsonArray stopsArray = legObj["stopSequence"].toArray();
                                if(stopsArray.size() > 0){
                                    QVariantList allstops;
                                    QVariantList stops;
                                    for(int z=0; z<stopsArray.size(); z++){
                                        QJsonObject stopsObj = stopsArray[z].toObject();

                                        QVariantMap station;
                                        station.insert("name",stopsObj["name"].toString().split(",")[1].split("Station")[0]);
                                        station.insert("id",stopsObj["id"].toString());
                                        station.insert("type",stopsObj["type"].toString());
                                        station.insert("disassembledName",getDisassembledName(stopsObj["parent"].toObject()));

                                        QJsonObject dep = stopsObj["departureTimePlanned"].toObject();
                                        if(dep.isEmpty()){
                                            station.insert("isLeaving",false);
                                        }else{
                                            station.insert("isLeaving",true);
                                            qint64 p = QDateTime::fromString(stopsObj["departureTimePlanned"].toString(),Qt::ISODate).toSecsSinceEpoch();
                                            station.insert("departureTimePlanned",p);
                                            QString f = stopsObj["departureTimeEstimated"].toString();
                                            if(f != ""){
                                                station.insert("departureTime",QDateTime::fromString(stopsObj["departureTimePlanned"].toString(),Qt::ISODate).toSecsSinceEpoch());
                                                station.insert("departureTimeEstimated",QDateTime::fromString(stopsObj["departureTimeEstimated"].toString(),Qt::ISODate).toSecsSinceEpoch());
                                            }else{
                                                station.insert("departureTime",p);
                                                station.insert("departureTimeEstimated",0);
                                            }
                                        }

                                        QString arr = stopsObj["arrivalTimePlanned"].toString();
                                        if(arr == ""){
                                            station.insert("isStopping",false);
                                        }else{
                                            station.insert("isStopping",true);
                                            qint64 p = QDateTime::fromString(stopsObj["arrivalTimePlanned"].toString(),Qt::ISODate).toSecsSinceEpoch();
                                            station.insert("arrivalTimePlanned",p);
                                            QString f = stopsObj["arrivalTimeEstimated"].toString();
                                            if(f != ""){
                                                station.insert("arrivalTime",QDateTime::fromString(stopsObj["arrivalTimeEstimated"].toString(),Qt::ISODate).toSecsSinceEpoch());
                                                station.insert("arrivalTimeEstimated",QDateTime::fromString(stopsObj["arrivalTimeEstimated"].toString(),Qt::ISODate).toSecsSinceEpoch());
                                            }else{
                                                station.insert("arrivalTime",p);
                                                station.insert("arrivalTimeEstimated",0);
                                            }
                                            stops.append(station);
                                        }

                                        if(dep.isEmpty() && arr.isEmpty())
                                            limited = true;

                                        allstops.append(station);
                                    }

                                    leg.insert("allstops",allstops);
                                    leg.insert("stops",stops);
                                    leg.insert("limited",limited);
                                }

                                QJsonArray infosArray = legObj["infos"].toArray();
                                if(infosArray.size() > 0){
                                    QVariantList infros;
                                    for(int z=0; z<infosArray.size(); z++){
                                        QJsonObject infoObj = infosArray[z].toObject();

                                        QVariantMap info;
                                        info.insert("id",infoObj["id"].toString());
                                        info.insert("version",infoObj["version"].toString());
                                        info.insert("subtitle",infoObj["subtitle"].toString());
                                        info.insert("url",infoObj["url"].toString());

                                        QJsonObject timeStamp = infoObj["timestamps"].toObject();
                                        if(!timeStamp.isEmpty()){
                                            info.insert("creation",timeStamp["creation"].toString());
                                            info.insert("lastModification",timeStamp["lastModification"].toString());
                                        }

                                        infros.append(info);
                                    }

                                    leg.insert("info",infros);
                                }
                                legs.append(leg);
                            }
                        }

                        trip.insert("legs",legs);
                    }

                    m_list.append(trip);
                }
            }
        }
        if(m_voice){
            reponse = "Next train departs from "+depstation+" "+depTime;
        }
    }
}

QString AsyncTrainResponse::getDisassembledName(QJsonObject p)
{
    if(!p.isEmpty()){
       QString value = p["disassembledName"].toString();
       QStringList w = value.split(",");
       if(w.length() > 1)
           return w[0];
    }

    return "";
}

QString AsyncTrainResponse::getPlatform(QJsonObject p)
{
    if(!p.isEmpty()){
        QString value = p["disassembledName"].toString();
        QStringList w = value.split("Platform");
       if(w.length() > 1)
           return w[1].trimmed();
    }

    return "";
}

//-------------------------------------------------------------------
SydneyTrains::SydneyTrains(QObject *parent) : QObject(parent)
{

}

void SydneyTrains::setSettings(QString from, QString to)
{
    m_defaultFrom = from;
    m_defaultTo = to;
}

void SydneyTrains::NextDefaultTrains()
{
    if(l_list.tryLock()){
        i_list.clear();
        l_list.unlock();
    }
    AsyncTrainResponse *response = new AsyncTrainResponse("next",m_defaultFrom,m_defaultTo,QDateTime::currentDateTime(), false);
    connect(response, &AsyncTrainResponse::listChanged, this, &SydneyTrains::listChanged);
    pool.start(response);
}

void SydneyTrains::NextTrains(QString from, QString to, bool voice)
{
    if(l_list.tryLock()){
        i_list.clear();
        l_list.unlock();
    }
    AsyncTrainResponse *response = new AsyncTrainResponse("next",from,to,QDateTime::currentDateTime(),voice);
    connect(response, &AsyncTrainResponse::listChanged, this, &SydneyTrains::listChanged);
    pool.start(response);
}

void SydneyTrains::listChanged(QVariantList v, QString voice)
{
    l_list.lock();
    i_list = v;
    l_list.unlock();
qDebug() <<"listChanged"<<voice;
    emit trainListChanged();
    if(voice != "") emit assistantVoice(voice);
}

bool SydneyTrains::processVoiceCmd(QString cmd, QString &response)
{
    /* When the next train to city
     * When the next train
     * When the next train from revesby to city
    */
    if(cmd.contains("train")){
        qDebug() << "SydneyTrains processVoiceCmd";
        QString from = m_defaultFrom;
        QString to = m_defaultTo;

        QStringList l = cmd.split(" ");
        if(cmd.contains("from")){
            int p = l.indexOf("from");
            p++;
            if(p < l.length()){
                QString s = l[p].trimmed();
                if(s == "the"){
                    p++;
                    if(p < l.length()){
                        s = l[p].trimmed();
                    }
                }
                from = lookUpStation(s);
            }
        }

        if(cmd.contains("to")){
            int p = l.indexOf("to");
            p++;
            if(p < l.length()){
                QString s = l[p].trimmed();
                if(s == "the"){
                    p++;
                    if(p < l.length()){
                        s = l[p].trimmed();
                    }
                }
                to = lookUpStation(s);
            }
        }

        if(from == "" || to == ""){
qDebug() << "SydneyTrains processVoiceCmd failed" << from << to;
            //Unable to process query, unable to find station
            return true;
        }
//qDebug() << "next train to"<<from<<to;
        NextTrains(from, to, true);
        emit pageTrigger("trains", 0);
qDebug() << "SydneyTrains processVoiceCmd finish";
        return true;
    }

    return false;
}

QString SydneyTrains::lookUpStation(QString name)
{
    if(name == "central") return "10101100";
    else if(name == "circular") return "10101103";
    else if(name == "city") return "10101103";
    else if(name == "revesby") return "10101417";
    else if(name == "sutherland") return "10101345";
    else if(name == "wollongong") return "10101372";
    else if(name == "wolli creek") return "10101328";
    else if(name == "aberdeen") return "10101192";
    else if(name == "adamstown") return "10101159";
    else if(name == "albion park") return "10101381";
    else if(name == "albury") return "10155026";
    else if(name == "allawah") return "10101338";
    else if(name == "armidale") return "10155025";
    else if(name == "arncliffe") return "10101333";
    else if(name == "artarmon") return "10101116";
    else if(name == "ashfield") return "10101209";
    else if(name == "asquith") return "10101128";
    else if(name == "auburn") return "10101219";
    else if(name == "austinmer") return "10101363";
    else if(name == "awaba") return "10101150";
    else if(name == "banksia") return "10101334";
    else if(name == "bankstown") return "10101401";
    else if(name == "bardwell park") return "10101410";
    else if(name == "bargo") return "10101303";
    else if(name == "bathurst") return "10101281";
    else if(name == "beecroft") return "10101197";
    else if(name == "bell") return "10101272";
    else if(name == "bellambi") return "10101367";
    else if(name == "bellata") return "10155045";
    else if(name == "belmore") return "10101397";
    else if(name == "berala") return "10101390";
    else if(name == "beresfield") return "10101172";
    else if(name == "berowra") return "10101131";
    else if(name == "berry") return "10101388";
    else if(name == "beverly hills") return "10101413";
    else if(name == "bexley north") return "10101411";
    else if(name == "birrong") return "10101403";
    else if(name == "blackheath") return "10101270";
    else if(name == "blacktown") return "10101235";
    else if(name == "blaxland") return "10101256";
    else if(name == "blayney") return "10155031";
    else if(name == "boggabri") return "10155054";
    else if(name == "bomaderry") return "10101389";
    else if(name == "bombo") return "10101385";
    else if(name == "bondi junction") return "10101109";
    else if(name == "booragul") return "10101152";
    else if(name == "bowral") return "10101306";
    else if(name == "branxton") return "10101189";
    else if(name == "brisbane (roma street)") return "10155061";
    else if(name == "broadmeadow") return "10101160";
    else if(name == "broken hill") return "10155000";
    else if(name == "bullaburra") return "10101265";
    else if(name == "bulli") return "10101365";
    else if(name == "bundanoon") return "10101310";
    else if(name == "bungendore") return "10155034";
    else if(name == "burradoo") return "10101307";
    else if(name == "burwood") return "10101207";
    else if(name == "cabramatta") return "10101287";
    else if(name == "camellia") return "10101222";
    else if(name == "campbelltown") return "10101296";
    else if(name == "campsie") return "10101396";
    else if(name == "canberra") return "10155022";
    else if(name == "canley vale") return "10101286";
    else if(name == "canterbury") return "10101395";
    else if(name == "cardiff") return "10101157";
    else if(name == "caringbah") return "10101349";
    else if(name == "carlingford") return "10101226";
    else if(name == "carlton") return "10101337";
    else if(name == "carramar") return "10101408";
    else if(name == "casino") return "10155062";
    else if(name == "casula") return "10101290";
    else if(name == "central") return "10101100";
    else if(name == "chatswood") return "10101117";
    else if(name == "cheltenham") return "10101198";
    else if(name == "chester hill") return "10101405";
    else if(name == "circular quay") return "10101103";
    else if(name == "clarendon") return "10101243";
    else if(name == "clyde") return "10101220";
    else if(name == "coalcliff") return "10101359";
    else if(name == "cockle creek") return "10101155";
    else if(name == "coffs harbour") return "10155060";
    else if(name == "coledale") return "10101423";
    else if(name == "como") return "10101343";
    else if(name == "concord west") return "10101204";
    else if(name == "condobolin") return "10155039";
    else if(name == "coniston") return "10101373";
    else if(name == "coolamon") return "10155041";
    else if(name == "cootamundra") return "10155050";
    else if(name == "corrimal") return "10101368";
    else if(name == "cowan") return "10101132";
    else if(name == "cringila") return "10101375";
    else if(name == "cronulla") return "10101351";
    else if(name == "croydon") return "10101208";
    else if(name == "culcairn") return "10155032";
    else if(name == "dapto") return "10101425";
    else if(name == "darnick") return "10155004";
    else if(name == "denistone") return "10101200";
    else if(name == "domestic airport") return "10101331";
    else if(name == "doonside") return "10101246";
    else if(name == "dora creek") return "10101149";
    else if(name == "douglas park") return "10101300";
    else if(name == "dubbo") return "10155006";
    else if(name == "dulwich hill light rail") return "10101462";
    else if(name == "dulwich hill") return "10101393";
    else if(name == "dundas") return "10101224";
    else if(name == "dungog") return "10101186";
    else if(name == "east hills") return "10101419";
    else if(name == "east maitland") return "10101176";
    else if(name == "east richmond") return "10101244";
    else if(name == "eastwood") return "10101199";
    else if(name == "edgecliff") return "10101108";
    else if(name == "edmondson park") return "10101447";
    else if(name == "emu plains") return "10101253";
    else if(name == "engadine") return "10101353";
    else if(name == "epping") return "10101429";
    else if(name == "erskineville") return "10101324";
    else if(name == "euabalong west") return "10155018";
    else if(name == "eungai") return "10155049";
    else if(name == "exeter") return "10101309";
    else if(name == "fairfield") return "10101285";
    else if(name == "fairy meadow") return "10101370";
    else if(name == "fassifern") return "10101151";
    else if(name == "faulconbridge") return "10101260";
    if(name == "flemington") return "10101217";
    else if(name == "gerringong") return "10101387";
    else if(name == "geurie") return "10155013";
    else if(name == "glenbrook") return "10101255";
    else if(name == "glenfield") return "10101291";
    else if(name == "gloucester") return "10155028";
    else if(name == "gordon") return "10101121";
    else if(name == "gosford") return "10101139";
    else if(name == "goulburn") return "10101315";
    else if(name == "grafton") return "10155063";
    else if(name == "granville") return "10101227";
    else if(name == "green square") return "10101329";
    else if(name == "greta") return "10101188";
    else if(name == "griffith") return "10155011";
    else if(name == "guildford") return "10101283";
    else if(name == "gunnedah") return "10155055";
    else if(name == "gunning") return "10155030";
    else if(name == "gymea") return "10101347";
    else if(name == "hamilton") return "10101161";
    else if(name == "harden") return "10155003";
    else if(name == "harris park") return "10101228";
    else if(name == "hawkesbury river") return "10101133";
    else if(name == "hazelbrook") return "10101263";
    else if(name == "heathcote") return "10101354";
    else if(name == "helensburgh") return "10101356";
    else if(name == "henty") return "10155029";
    else if(name == "hexham") return "10101170";
    else if(name == "high street") return "10101177";
    else if(name == "hilldale") return "10101183";
    else if(name == "holsworthy") return "10101420";
    else if(name == "homebush") return "10101216";
    else if(name == "hornsby") return "10101127";
    else if(name == "hurlstone park") return "10101394";
    else if(name == "hurstville") return "10101339";
    else if(name == "ingleburn") return "10101293";
    else if(name == "international airport") return "10101332";
    else if(name == "ivanhoe") return "10155007";
    else if(name == "jannali") return "10101344";
    else if(name == "junee") return "10155047";
    else if(name == "katoomba") return "10101268";
    else if(name == "kembla grange") return "10101379";
    else if(name == "kempsey") return "10155046";
    else if(name == "kendall") return "10155040";
    else if(name == "kiama") return "10101386";
    else if(name == "killara") return "10101120";
    else if(name == "kings cross") return "10101107";
    else if(name == "kingsgrove") return "10101412";
    else if(name == "kingswood") return "10101251";
    else if(name == "kirrawee") return "10101346";
    else if(name == "kogarah") return "10101336";
    else if(name == "koolewong") return "10101136";
    else if(name == "kootingal") return "10155012";
    else if(name == "kotara") return "10101158";
    else if(name == "kyogle") return "10155066";
    else if(name == "lakemba") return "10101398";
    else if(name == "lapstone") return "10101254";
    else if(name == "lawson") return "10101264";
    else if(name == "leeton") return "10155020";
    else if(name == "leightonfield") return "10101406";
    else if(name == "leppington") return "10101448";
    else if(name == "leumeah") return "10101295";
    else if(name == "leura") return "10101267";
    else if(name == "lewisham") return "10101211";
    else if(name == "lidcombe") return "10101218";
    else if(name == "linden") return "10101261";
    else if(name == "lindfield") return "10101119";
    else if(name == "lisarow") return "10101142";
    else if(name == "lithgow") return "10101274";
    else if(name == "liverpool") return "10101289";
    else if(name == "lochinvar") return "10101187";
    else if(name == "loftus") return "10101352";
    else if(name == "lysaghts") return "10101374";
    else if(name == "macarthur") return "10101297";
    else if(name == "macdonaldtown") return "10101215";
    else if(name == "macksville") return "10155053";
    else if(name == "macquarie fields") return "10101292";
    else if(name == "macquarie park") return "10101427";
    else if(name == "macquarie university") return "10101426";
    else if(name == "maitland") return "10101178";
    else if(name == "marayong") return "10101236";
    else if(name == "marrickville") return "10101392";
    else if(name == "martin place") return "10101106";
    else if(name == "martins creek") return "10101182";
    else if(name == "marulan") return "10101314";
    else if(name == "mascot") return "10101330";
    else if(name == "meadowbank") return "10101202";
    else if(name == "meadowbank wharf") return "10102028";
    else if(name == "medlow bath") return "10101269";
    else if(name == "menangle park") return "10101298";
    else if(name == "menangle") return "10101299";
    else if(name == "menindee") return "10155001";
    else if(name == "merrylands") return "10101282";
    else if(name == "metford") return "10101174";
    else if(name == "milsons point") return "10101111";
    else if(name == "milsons point wharf") return "10102017";
    else if(name == "mindaribba") return "10101180";
    else if(name == "minnamurra") return "10101384";
    else if(name == "minto") return "10101294";
    else if(name == "miranda") return "10101348";
    else if(name == "mittagong") return "10101305";
    else if(name == "moree") return "10155051";
    else if(name == "morisset") return "10101148";
    else if(name == "mortdale") return "10101341";
    else if(name == "moss vale") return "10101308";
    else if(name == "mount colah") return "10101129";
    else if(name == "mount druitt") return "10101248";
    else if(name == "mount kuring-gai") return "10101130";
    else if(name == "mount victoria") return "10101271";
    else if(name == "mulgrave") return "10101241";
    else if(name == "murrurundi") return "10155005";
    else if(name == "museum") return "10101104";
    else if(name == "muswellbrook") return "10101191";
    else if(name == "nambucca heads") return "10155058";
    else if(name == "narara") return "10101140";
    else if(name == "narrabri") return "10155044";
    else if(name == "narrandera") return "10155024";
    else if(name == "narwee") return "10101414";
    else if(name == "newcastle interchange") return "10101165";
    else if(name == "newtown") return "10101214";
    else if(name == "niagara park") return "10101141";
    else if(name == "normanhurst") return "10101194";
    else if(name == "north ryde") return "10101428";
    else if(name == "north strathfield") return "10101205";
    if(name == "north sydney") return "10101112";
    else if(name == "north wollongong") return "10101371";
    else if(name == "oak flats") return "10101382";
    else if(name == "oatley") return "10101342";
    else if(name == "olympic park") return "10101424";
    else if(name == "orange") return "10155023";
    else if(name == "otford") return "10101357";
    else if(name == "ourimbah") return "10101143";
    else if(name == "padstow") return "10101416";
    else if(name == "panania") return "10101418";
    else if(name == "parkes") return "10155056";
    else if(name == "parramatta") return "10101229";
    else if(name == "paterson") return "10101181";
    else if(name == "pendle hill") return "10101232";
    else if(name == "pennant hills") return "10101196";
    else if(name == "penrith") return "10101252";
    else if(name == "penrose") return "10101311";
    else if(name == "penshurst") return "10101340";
    else if(name == "petersham") return "10101212";
    else if(name == "picton") return "10101301";
    else if(name == "point clare") return "10101138";
    else if(name == "port kembla north") return "10101376";
    else if(name == "port kembla") return "10101377";
    else if(name == "punchbowl") return "10101400";
    else if(name == "pymble") return "10101122";
    else if(name == "quakers hill") return "10101237";
    else if(name == "queanbeyan") return "10155027";
    else if(name == "quirindi") return "10155059";
    else if(name == "railway square bus interchange") return "10101110";
    else if(name == "redfern") return "10101421";
    else if(name == "regents park") return "10101391";
    else if(name == "revesby") return "10101417";
    else if(name == "rhodes") return "10101203";
    else if(name == "richmond") return "10101245";
    else if(name == "riverstone") return "10101239";
    else if(name == "riverwood") return "10101415";
    else if(name == "rockdale") return "10101335";
    else if(name == "rooty hill") return "10101247";
    else if(name == "rosehill") return "10101221";
    else if(name == "roseville") return "10101118";
    else if(name == "rydal") return "10155052";
    else if(name == "rydalmere") return "10101223";
    else if(name == "sandgate") return "10101169";
    else if(name == "sawtell") return "10155065";
    else if(name == "scarborough") return "10101360";
    else if(name == "schofields") return "10101238";
    else if(name == "scone") return "10101193";
    else if(name == "sefton") return "10101404";
    else if(name == "seven hills") return "10101234";
    else if(name == "shellharbour junction") return "10101446";
    else if(name == "singleton") return "10101190";
    else if(name == "springwood") return "10101259";
    else if(name == "james") return "10101105";
    else if(name == "leonards") return "10101115";
    else if(name == "marys") return "10101249";
    else if(name == "peters") return "10101443";
    else if(name == "stanmore") return "10101213";
    else if(name == "stanwell") return "10101358";
    else if(name == "strathfield") return "10101206";
    else if(name == "stuart town") return "10155019";
    else if(name == "summer hill") return "10101210";
    else if(name == "sutherland") return "10101345";
    else if(name == "sydenham") return "10101326";
    else if(name == "tahmoor") return "10101302";
    else if(name == "tallong") return "10101313";
    else if(name == "tamworth") return "10155009";
    else if(name == "tarago") return "10155038";
    else if(name == "tarana") return "10155048";
    else if(name == "taree") return "10155037";
    else if(name == "tarro") return "10101171";
    else if(name == "tascott") return "10101137";
    else if(name == "telarah") return "10101179";
    else if(name == "telopea") return "10101225";
    else if(name == "tempe") return "10101327";
    else if(name == "teralba") return "10101153";
    else if(name == "rock") return "10155035";
    else if(name == "thirroul") return "10101364";
    else if(name == "thornleigh") return "10101195";
    else if(name == "thornton") return "10101173";
    else if(name == "toongabbie") return "10101233";
    else if(name == "town hall") return "10101101";
    else if(name == "towradgi") return "10101369";
    else if(name == "tuggerah") return "10101144";
    else if(name == "turramurra") return "10101123";
    else if(name == "turrella") return "10101409";
    else if(name == "unanderra") return "10101378";
    else if(name == "uralla") return "10155021";
    else if(name == "urunga") return "10155064";
    else if(name == "valley heights") return "10101258";
    else if(name == "victoria street") return "10101175";
    else if(name == "villawood") return "10101407";
    else if(name == "vineyard") return "10101240";
    else if(name == "wagga wagga") return "10155042";
    else if(name == "wahroonga") return "10101125";
    else if(name == "waitara") return "10101126";
    else if(name == "walcha road") return "10155016";
    else if(name == "wallarobba") return "10101184";
    else if(name == "warabrook") return "10101168";
    else if(name == "waratah") return "10101167";
    else if(name == "warnervale") return "10101146";
    else if(name == "warrawee") return "10101124";
    else if(name == "warrimoo") return "10101257";
    else if(name == "warwick farm") return "10101288";
    else if(name == "waterfall") return "10101355";
    else if(name == "wauchope") return "10155043";
    else if(name == "waverton") return "10101113";
    else if(name == "wellington") return "10155017";
    else if(name == "wentworth falls") return "10101266";
    else if(name == "wentworthville") return "10101231";
    else if(name == "werrington") return "10101250";
    else if(name == "werris creek") return "10155057";
    else if(name == "west ryde") return "10101201";
    else if(name == "westmead") return "10101230";
    else if(name == "wiley park") return "10101399";
    else if(name == "willow tree") return "10155002";
    else if(name == "windsor") return "10101242";
    else if(name == "wingello") return "10101312";
    else if(name == "wingham") return "10155033";
    else if(name == "wirragulla") return "10101185";
    else if(name == "wollstonecraft") return "10101114";
    else if(name == "wombarra") return "10101361";
    else if(name == "wondabyne") return "10101134";
    else if(name == "woodford") return "10101262";
    if(name == "woolooware") return "10101350";
    else if(name == "woonona") return "10101366";
    else if(name == "woy woy") return "10101135";
    else if(name == "wyee") return "10101147";
    else if(name == "wynyard") return "10101102";
    else if(name == "wyong") return "10101145";
    else if(name == "yagoona") return "10101402";
    else if(name == "yass junction") return "10155014";
    else if(name == "yennora") return "10101284";
    else if(name == "yerrinbool") return "10101304";
    else if(name == "zig zag") return "10101273";

    return "";
}

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtMultimedia 5.8

//import QtWebEngine 1.4

Page {
     id: video
     width: imageWidth
     height: imageHeight

     background: Rectangle{
         color:"black"
     }

     Video {
         id: videofile
         anchors.fill: parent
         autoLoad: false
         autoPlay: false
         muted: true
         volume: window.volume
         z: 1

         onStopped: {
             console.log("VIDEO STOPPED");
             if(videofile.position >= videofile.duration){
                 console.log("VIDEO 1");
                 if(videofile.muted){
                     console.log("VIDEO 2");
                     //hold the position
                 }else{
                    videofile.muted = true;
                    flickTimer.moveSlide();
                     console.log("VIDEO 3");
                 }
             }
         }
         //onPlaying: {
         //    videofile.isPlaying = true
         //}
         //ond: {
         //    videofile.isPlaying = false
         //}
     }

     property int test: view.currentId === slideId ? shown() : stopped()
     property bool initialised: false
     property bool sourceLoaded: false

     function loadInitial(){
         if(!video.sourceLoaded){
             console.log("video loaded");
             videofile.source = galleryPath + sourceList[0].name
             video.sourceLoaded = true
             video.initialised = true;
             videofile.play();
             delayCheck.start();
         }
    }

     Component.onCompleted: {
         delayLoad.start();
     }

     Timer {
        id: delayCheck
        interval: 500
        running: false
        repeat: true
        onTriggered:{
            console.log("video delycheck",videofile.duration);
            if(videofile.duration > 0){
                console.log("video set Initial posisition");
                videofile.play();
//                videofile.seek(videofile.duration * 0.1);
//                stopPause.start();
                delayCheck.stop();
            }

            /*if(videofile.status === MediaPlayer.NoMedia)
                console.log("NoMedia");
            if(videofile.status === MediaPlayer.Loading)
                console.log("Loading");
            if(videofile.status === MediaPlayer.Loaded)
                console.log("Loaded");
            if(videofile.status === MediaPlayer.Buffering)
                console.log("Buffering");
            if(videofile.status === MediaPlayer.Buffered)
                console.log("Buffered");
            if(videofile.status === MediaPlayer.UnknownStatus)
                console.log("UnknownStatus");*/
//            videofile.seek(videofile.duration * 0.1);
        }
    }

     Timer {
        id: stopPause
        interval: 1000
        running: false
        repeat: false
        onTriggered:{
            videofile.pause();
        }
    }


     Timer {
        id: delayLoad
        interval: 1500
        running: false
        repeat: false
        onTriggered:{
            console.log("video delay load")
            loadInitial()
        }
    }

    Timer {
        id: delayStart
        interval: 2000
        running: false
        repeat: false
        onTriggered:{
console.log("delayStart",videofile.playbackState,MediaPlayer.PlayingState,MediaPlayer.PausedState,MediaPlayer.StoppedState);
            if(videofile.playbackState === MediaPlayer.StoppedState || videofile.playbackState === MediaPlayer.PausedState){
                console.log("video start file");
                videofile.seek(0);
                videofile.play();
            }
        }
    }

    BusyIndicator {
       id: busyIndication
       anchors.centerIn: parent
       running: false
       // 'running' defaults to 'true'
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 3
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("Video switchToMenu");
            videofile.muted = true;
            videofile.stop();
            window.switchToMenu(true)
        }
    }

    Button {
        id: btCentre
        x:100
        y:0
        height: imageHeight
        width: 700
        z: 3
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btCentre
        onClicked: {
            if(videofile.position >= videofile.duration){
                if(videofile.seekable){
                    videofile.seek(0);
                    videofile.play();
                    console.log("video play",videofile.position, videofile.duration);
                }
            }else{
                flickTimer.stop();
                videofile.muted = false;
                console.log("video Mute off",videofile.position, videofile.duration);
            }
        }
    }

    /*Connections {
        target: video
        onVisibleChanged:
            if(visible){
                console.log("VIDEO Shown")
                shown();
            }else{
                console.log("VIDEO stopped")
                stopped();
            }

        ignoreUnknownSignals: true
    }*/

    function shown(){
        console.log("shown VIDEO",sourceList[0].name);
        if(!video.sourceLoaded){
            delayLoad.stop();
            video.loadInitial();
            console.log("Video A");
        }

        if(!video.initialised){
            busyIndication.running = true;
            console.log("Video B");
        }else{
            delayStart.start();
            console.log("Video C");
        }

        return 1
    }

    function stopped(){
        videofile.muted = true;
        console.log("stopped");
        videofile.stop();
        return 1
    }
}

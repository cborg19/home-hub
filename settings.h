#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString pathName READ pathName WRITE setPathName NOTIFY pathNameChanged)
public:
    explicit Settings(QObject *parent = nullptr);

    QString pathName();
    void setPathName(const QString &pathName);

    Q_INVOKABLE QString getString(QString group, QString key, QString initial);
    Q_INVOKABLE void setString(QString group, QString key, QString value);

    Q_INVOKABLE bool getBool(QString group, QString key, bool initial);
    Q_INVOKABLE void setBool(QString group, QString key, bool value);

    Q_INVOKABLE int getInt(QString group, QString key, int initial);
    Q_INVOKABLE void setInt(QString group, QString key, int value);

    Q_INVOKABLE QList<QStringList> getStringArray(QString group, QStringList initial);
    Q_INVOKABLE void setStringArray(QString group, QStringList keys, QList<QStringList> value);

    Q_INVOKABLE QStringList getStringList(QString group, QStringList initial);
    Q_INVOKABLE void setStringList(QString group, QStringList value);
signals:
    void pathNameChanged();

public slots:

private:
    QString m_pathName;
    QSettings *m_settings;
};

#endif // SETTINGS_H

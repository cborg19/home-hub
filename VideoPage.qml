import QtQuick 2.0
import QtQuick.Controls 2.4
import QtMultimedia 5.8

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle{
        color:"black"
    }

    Video {
        id: videofile
        anchors.fill: parent
        autoLoad: true
        autoPlay: false
        source: galleryPath+"/assets/Indiana Jones Theme.mp4"
        volume: window.volume
        z: 1

        //onStopped: {
        //}
        //onPlaying: {
        //    videofile.isPlaying = true
        //}
        //ond: {
        //    videofile.isPlaying = false
        //}
        
        Rectangle {
            id: btPause
            x: imageWidth/2 - 75
            y: imageHeight/2 - 75
            width: 150
            height: 150
            color: "transparent"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 50
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_pause
                visible: videofile.playbackState === MediaPlayer.PausedState?true:false
            }

            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 50
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_play
                visible: videofile.playbackState === MediaPlayer.StoppedState?true:false
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(videofile.hasVideo && videofile.playbackState !== MediaPlayer.PlayingState){
                        videofile.play();
                    }else{
                        console.log("C");
                        videofile.pause();
                    }
                }
            }
        }

        Rectangle {
            id: btStatus
            x: 0
            y: 400
            width: imageWidth
            height: 80
            color: "transparent"

            Item{
                id: statusInfo
                visible: false
                anchors.fill: parent

                Label {
                    x: 20
                    y: 5
                    text: SimpleJsTools.milltoString(videofile.position)
                    color: "white"
                    font.pointSize: 18
                    font.family: "Helvetica"
                }

                Label {
                    x: imageWidth - 60
                    y: 5
                    text: SimpleJsTools.milltoString(videofile.duration)
                    color: "white"
                    font.pointSize: 18
                    font.family: "Helvetica"
                }

                Slider {
                    id: seekSlider
                    value: videofile.position
                    to: videofile.duration

                    x: 20
                    y: 40
                    //z: 2
                    width: imageWidth - 40
                    height: 20

                    ToolTip {
                        parent: seekSlider.handle
                        visible: seekSlider.pressed
                        text: pad(Math.floor(value / 60)) + ":" + pad(Math.floor(value % 60))
                        y: parent.height

                        readonly property int value: seekSlider.valueAt(seekSlider.position)

                        function pad(number) {
                            if (number <= 9)
                                return "0" + number;
                            return number;
                        }
                    }

                    handle: Rectangle {
                        x: seekSlider.visualPosition * (seekSlider.width - width)
                        y: (seekSlider.height - height) / 2
                        width: 40
                        height: 40

                        radius: 20
                        color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
                        border.color: "#9c27b0"
                    }

                    background: Rectangle {
                        y: (seekSlider.height - height) / 2
                        height: 10
                        radius: 5
                        color: "#686868" //background slider

                        Rectangle {
                            width: seekSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#b92ed1" //done part
                            radius: 5
                        }
                    }

                    onPressedChanged: {
                        videofile.seek(seekSlider.value)
                    }
                }
            }

            MouseArea {
                id: statusMouseArea
                anchors.fill: parent
                onClicked: {
                    if(!statusInfo.visible){
                        statusInfo.visible = true
                        timeout.restart()
                        statusMouseArea.visible = false
                    }
                }
            }

            Timer {
                id: timeout
                interval: 5000
                running: false
                repeat: false
                onTriggered:{
                    timeout.stop()
                    statusInfo.visible = false
                    statusMouseArea.visible = true
                }
            }
        }
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("VideoPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

import QtQuick 2.0
import QtQuick.Controls 2.4

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Image {
        anchors.fill: parent
        source: "files/icon/fibaroback.jpg"
        fillMode: Image.PreserveAspectCrop
    }

    Component {
        id: delegateRooms
        Item {
            width: listRoom.cellWidth
            height: listRoom.cellHeight


            Rectangle {
                anchors.centerIn: parent
                width: 110
                height: 110
                color: "white"
                radius: 10
                Column {
                    anchors.fill: parent
                    Image {
                        width: 90
                        height: 90
                        source: listRoom.getRoomIcon(controller.FibaroRoomList[index]);
                        anchors.horizontalCenter: parent.horizontalCenter

                        Rectangle {
                            color: controller.FibaroRoomList[index].devicesOn? "red" : "grey"
                            x: 80
                            y: 5
                            width: 8
                            height: 8
                            radius: 4
                        }

                        Label {
                            text: controller.FibaroRoomList[index].temp !== ""? controller.FibaroRoomList[index].temp + "°C" : ""
                            x: 0
                            y: 5
                            color: "blue"
                            font.pointSize: 10
                            font.bold: true
                            font.family: "Helvetica"
                        }
                    }
                    Label {
                        text: controller.FibaroRoomList[index].name
                        color: "black"
                        font.pointSize: 10
                        font.family: "Helvetica"
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    listRoom.visible = false
                    controller.fibaroGetRoomDevices(controller.FibaroRoomList[index].id);
                    listDevice.visible = true
                }
            }
        }
    }

    GridView {
        id: listRoom
        x: 20
        y: 50
        width: imageWidth - 40
        height: imageHeight - 40

        cellWidth: 120
        cellHeight: 120
        model: controller.FibaroRoomList
        delegate: delegateRooms

        function getRoomIcon(item){
            if(item.icon !== ""){
                return "image://imageprovider/"+item.iconurl;
            }
/*
            var picture = "fibaroBedRoom.png";
            if(id === 6){
                picture = "fibaroGarage.png";
            }else if(id === 8){
                picture = "fibaroKidRoom.png";
            }else if(id === 9){
                picture = "fibaroDinning.png";
            }else if(id === 10){
                picture = "fibaroStudy.png";
            }else if(id === 11){
                picture = "fibaroKitchen.png";
            }else if(id === 12){
                picture = "fibaroLiving.png";
            }else if(id === 26){
                picture = "fibaroLaundry.png";
            }else if(id === 41){
                picture = "fibaroHallWay.png";
            }else if(id === 42){
                picture = "fibaroPatio.png";
            }else if(id === 43){
                picture = "fibaroBathroom.png";
            }
            return "files/icon/"+picture;*/
            return "";
        }
    }

    Component {
        id: delegateDevice
        Item {
            width: listDevice.cellWidth
            height: listDevice.cellHeight


            Rectangle {
                anchors.centerIn: parent
                width: 110
                height: 110
                color: "white"
                radius: 10
                Column {
                    anchors.fill: parent
                    Image {
                        width: 90
                        height: 90
                        source: listDevice.getDeviceIcon(controller.FibaroDeviceList[controller.FibaroCustomList[index].id], index);
                        anchors.horizontalCenter: parent.horizontalCenter

                        Label {
                            text: controller.FibaroDeviceList[controller.FibaroCustomList[index].id].properties.power !== "0.00"? controller.FibaroDeviceList[controller.FibaroCustomList[index].id].properties.power + "W" : ""
                            x: 0
                            y: 5
                            color: "blue"
                            font.pointSize: 10
                            font.bold: true
                            font.family: "Helvetica"
                        }
                    }
                    Label {
                        text: controller.FibaroDeviceList[controller.FibaroCustomList[index].id].name
                        color: "black"
                        font.pointSize: 10
                        font.family: "Helvetica"
                        anchors.horizontalCenter: parent.horizontalCenter
                    }

                    //properties
                    // batteryLevel
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.fibaroDeviceToggleIndex(index);
                }
            }
        }
    }

    GridView {
        id: listDevice
        x: 20
        y: 60
        width: imageWidth - 40
        height: imageHeight - 40

        visible: false

        cellWidth: 120
        cellHeight: 120
        model: controller.FibaroCustomList
        delegate: delegateDevice

        function getDeviceIcon(item, index){
            if(item.icon !== ""){
                return "image://imageprovider/"+controller.fibaroGetIconURL("device", item.properties.deviceIcon, index);
            }


            return "";
        }

        Text {
            x: 720
            y: 0
            z: 3
            id: btBack
            color: "white"
            font.pointSize: 25
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_arrow_left
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    listDevice.visible = false
                    listRoom.visible = true
                }
            }
        }
    }
}

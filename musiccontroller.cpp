#include <QMediaPlaylist>
#include <QDirIterator>
#include <QtCore/qdebug.h>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QJsonObject>
#include <QJsonArray>
#include <QImage>

#include <QMediaMetaData>

#include "musiccontroller.h"
#include "global.h"

#include <taglib/fileref.h>
#include <taglib/tag.h>
#include <taglib/id3v2tag.h>
#include <taglib/mpegfile.h>
#include <taglib/id3v2frame.h>
#include <taglib/id3v2header.h>
#include <taglib/attachedpictureframe.h>

void AsyncMusicFilesResponse::run()
{
    for(int x=0; x<26; x++)
        m_index.append(-1);

    if(m_type == "all"){
        QFile file(m_path+"music/allfiles.list");
        if (!file.open(QFile::ReadOnly | QFile::Text)) return;

        QMap<QString, QVariantMap> list;
        QStringList order;
        QTextStream input(&file);
        while(!input.atEnd()){
            QString l = input.readLine();
            QStringList w = l.split("|");
            QVariantMap row;

            if(w.length() == 5){
                row.insert("source",w[0]);
                row.insert("title",w[1]);
                row.insert("artist",w[2]);
                row.insert("album",w[3]);
                row.insert("cover",w[4]);
                order.push_back(w[1]);
            }else{
                row.insert("source",l);
                row.insert("title","");
                row.insert("album","");
                row.insert("artist","");
                row.insert("cover","");
                order.push_back(l);
            }

            list[w[1]] = row;
        }

        file.close();

        order.sort();

        for(int x=0; x<order.length(); x++){
            QVariantMap r = list[order[x]];
            QString n = r["title"].toString().toUpper();
            if(n.length() > 0){
                int c = n.at(0).toLatin1();
                if(c >= 65 && c <= 90){
                    if(m_index[c-65] == -1){
                        m_index[c-65] = x;
                    }
                }
            }
            m_list.push_back(r);
        }
    }else if(m_type == "albums"){
        QFile file(m_path+"music/allfiles.list");
        if (!file.open(QFile::ReadOnly | QFile::Text)) return;

        QMap<QString, QVariantList> list;
        QStringList order;
        QTextStream input(&file);
        while(!input.atEnd()){
            QString l = input.readLine();
            QStringList w = l.split("|");
            QVariantMap row;

            if(w.length() == 5){
                row.insert("source",w[0]);
                row.insert("title",w[1]);
                row.insert("artist",w[2]);
                row.insert("album",w[3]);
                row.insert("cover",w[4]);

                QString key = w[3]+"|"+w[2];
                if(list.contains(key)){
                    list[key].push_back(row);
                }else{
                    QVariantList l;
                    l.push_back(row);
                    list[key] = l;
                    order.push_back(key);
                }
            }
        }

        file.close();

        order.sort();

        for(int x=0; x<order.length(); x++){
            QString key = order[x];
            QString n = key.toUpper();
            if(n.length() > 0){
                int c = n.at(0).toLatin1();
                if(c >= 65 && c <= 90){
                    if(m_index[c-65] == -1){
                        m_index[c-65] = x;
                    }
                }
            }

            QVariantMap r;
            QStringList w = key.split("|");
            QVariantList l = list[key];

            r.insert("album",w[0]);
            r.insert("list",l);
            r.insert("size",l.length());

            m_list.push_back(r);
        }
    }else if(m_type == "artist"){
        QFile file(m_path+"music/allfiles.list");
        if (!file.open(QFile::ReadOnly | QFile::Text)) return;

        QMap<QString, QVariantList> list;
        QStringList order;
        QTextStream input(&file);
        while(!input.atEnd()){
            QString l = input.readLine();
            QStringList w = l.split("|");
            QVariantMap row;

            if(w.length() == 5){
                row.insert("source",w[0]);
                row.insert("title",w[1]);
                row.insert("artist",w[2]);
                row.insert("album",w[3]);
                row.insert("cover",w[4]);

                QString key = w[2];
                if(list.contains(key)){
                    list[key].push_back(row);
                }else{
                    QVariantList l;
                    l.push_back(row);
                    list[key] = l;
                    order.push_back(key);
                }
            }else{
                row.insert("source",l);
                row.insert("title","");
                row.insert("album","");
                row.insert("artist","");
                row.insert("cover","");

                QString key = "Unknown";
                if(list.contains(key)){
                    list[key].push_back(row);
                }else{
                    QVariantList l;
                    l.push_back(row);
                    list[key] = l;
                    order.push_back(key);
                }
            }
        }

        file.close();

        order.sort();

        for(int x=0; x<order.length(); x++){
            QString key = order[x];
            QString n = key.toUpper();
            if(n.length() > 0){
                int c = n.at(0).toLatin1();
                if(c >= 65 && c <= 90){
                    if(m_index[c-65] == -1){
                        m_index[c-65] = x;
                    }
                }
            }

            QVariantMap r;
            QVariantList l = list[key];

            r.insert("artist",key);
            r.insert("list",l);
            r.insert("size",l.length());

            m_list.push_back(r);
        }
    }

    emit listChanged(m_list, m_index);
}

//----------------------------------------------------------------------------------------
void AsyncInternetRadioResponse::run()
{
    //qDebug() << "AsyncInternetRadioResponse" << m_url << m_type;

    //https://www.last.fm/api/radio
    //http://wiki.shoutcast.com/wiki/SHOUTcast_Radio_Directory_API

    QString url, filePath;
    QJsonDocument doc;
    bool downloadFiles = true;
    if(m_type == "world"){
        url = "http://www.radio-browser.info/webservice/json/countries";
        filePath = m_path +"playlist/"+m_type+".ir";
    }else if(m_type == "country"){
        url = "http://www.radio-browser.info/webservice/json/stations/bycountry/"+m_url;
        filePath = m_path +"playlist/"+m_type+"-"+m_url+".ir";
    }else if(m_type == "language"){
        url = "http://www.radio-browser.info/webservice/json/languages";
        filePath = m_path +"playlist/"+m_type+".ir";
    }else if(m_type == "tag"){
        url = "http://www.radio-browser.info/webservice/json/tags";
        filePath = m_path +"playlist/"+m_type+".ir";
    }else if(m_type == "lang"){
        url = "http://www.radio-browser.info/webservice/json/stations/bylanguage/"+m_url;
        filePath = m_path +"playlist/"+m_type+"-"+m_url+".ir";
    }else if(m_type == "group"){
        url = "http://www.radio-browser.info/webservice/json/stations/bytag/"+m_url;
        filePath = m_path +"playlist/"+m_type+"-"+m_url+".ir";
    }else if(m_type == "favourites"){
        filePath = m_path +"playlist/"+m_type+".ir";
        downloadFiles = false;
    }

    bool exists = QFileInfo::exists(filePath) && QFileInfo(filePath).isFile();
    if(exists){
        QFile jsonFile(filePath);
        jsonFile.open(QFile::ReadOnly);
        QJsonDocument obj = QJsonDocument().fromJson(jsonFile.readAll());
        if(!obj.isNull())
        {
            if(obj.isObject()){
                qint64 l = obj["lastupdate"].toVariant().toLongLong();
                qint64 n = QDateTime::currentMSecsSinceEpoch();

                if((n - l) < 2678400000){
                    downloadFiles = false;
                    if(obj["response"].isArray()){
                        QJsonDocument newDoc(obj["response"].toArray());
                        doc = newDoc;
                    }
                }
            }else if(obj.isArray()){
                QJsonDocument newDoc(obj);
                doc = newDoc;
            }
        }
    }

    for(int x=0; x<26; x++)
        m_index.append(-1);

    if(downloadFiles){
        QNetworkAccessManager manager;
        QNetworkRequest request;

        request.setUrl(QUrl(url));

        QSslConfiguration conf = request.sslConfiguration();
        conf.setPeerVerifyMode(QSslSocket::VerifyNone);
        request.setSslConfiguration(conf);

        QNetworkReply* reply = manager.get(request);

        QEventLoop eventloop;
        connect(reply,SIGNAL(finished()),&eventloop,SLOT(quit()));
        eventloop.exec();

        if (reply->error() == QNetworkReply::NoError)
        {
            //qDebug() << "AsyncInternetRadioResponse GOT DATA";
            QString data = reply->readAll();

            doc = QJsonDocument::fromJson(data.toUtf8());
            if(!doc.isNull())
            {
                QJsonObject obj;
                obj.insert("lastupdate", QJsonValue::fromVariant(QDateTime::currentMSecsSinceEpoch()));
                if(doc.isArray()){
                    QJsonArray resultArray = doc.array();
                    obj.insert("response", resultArray);
                }

                QJsonDocument newDoc(obj);
                setWrite();

                QFile jsonFile(filePath);
                jsonFile.open(QFile::WriteOnly);
                jsonFile.write(newDoc.toJson());
            }
        }else{
            //qDebug() << "AsyncInternetRadioResponse FAILED";
        }
    }

    if(!doc.isNull()){
        if(m_type == "world")
            parseWorld(doc);
        else if(m_type == "country")
            parseListStation(doc,"world");
        else if(m_type == "language")
            parseLanguage(doc);
        else if(m_type == "tag")
            parseTag(doc);
        else if(m_type == "lang")
            parseListStation(doc,"language");
        else if(m_type == "group")
            parseListStation(doc,"tag");
        else if(m_type == "favourites")
            parseFav(doc);
    }
}

void AsyncInternetRadioResponse::parseFav(QJsonDocument doc)
{
    if(!doc.isNull())
    {
        QVariantMap back;
        back.insert("name","...");
        back.insert("icon","");
        back.insert("link","");
        back.insert("params","");
        m_list.append(back);

        if(doc.isArray()){
            QJsonArray resultArray = doc.array();
            for(int x=0; x<resultArray.size(); x++){
                QJsonObject resultObj = resultArray[x].toObject();

                QVariantMap b;
                b.insert("name",resultObj["name"].toString());
                b.insert("icon",resultObj["icon"].toString());
                b.insert("link","station");
                b.insert("tags",resultObj["tag"].toString());
                b.insert("params",resultObj["params"].toString());
                m_list.append(b);
            }
        }

        emit listChanged(m_list, m_index);
    }
}

void AsyncInternetRadioResponse::parseListStation(QJsonDocument doc, QString backString)
{
    if(!doc.isNull())
    {
        QVariantMap back;
        back.insert("name","...");
        back.insert("icon","");
        back.insert("link",backString);
        back.insert("params","");
        m_list.append(back);

        if(doc.isArray()){
            QJsonArray resultArray = doc.array();
            for(int x=0; x<resultArray.size(); x++){
                QJsonObject resultObj = resultArray[x].toObject();

                if(resultObj["codec"].toString().toLower() != "mp3") continue;

                QVariantMap b;
                QString n = resultObj["name"].toString().toUpper();
                int c = n.at(0).toLatin1();
                if(c >= 65 && c <= 90){
                    if(m_index[c-65] == -1)
                        m_index[c-65] = x;
                }
                b.insert("name",resultObj["name"].toString());
                b.insert("icon",resultObj["favicon"].toString());
                b.insert("link","station");
                b.insert("tags",resultObj["tags"].toString());
                b.insert("params",resultObj["url"].toString());
                m_list.append(b);
            }
        }
    }

    emit listChanged(m_list, m_index);
}

void AsyncInternetRadioResponse::parseWorld(QJsonDocument doc)
{
    if(!doc.isNull())
    {
        QVariantMap back;
        back.insert("name","...");
        back.insert("icon","");
        back.insert("link","");
        back.insert("params","");
        m_list.append(back);

        if(doc.isArray()){
            QJsonArray resultArray = doc.array();
            for(int x=0; x<resultArray.size(); x++){
                QJsonObject resultObj = resultArray[x].toObject();

                QString s = resultObj["name"].toString() + " (stations: "+resultObj["stationcount"].toString()+")";

                QString n = resultObj["name"].toString().toUpper();
                int c = n.at(0).toLatin1();
                if(c >= 65 && c <= 90){
                    if(m_index[c-65] == -1)
                        m_index[c-65] = x;
                }

                QVariantMap b;
                b.insert("name",s);
                b.insert("icon","");
                b.insert("link","country");
                b.insert("params",resultObj["value"].toString().toLower());
                m_list.append(b);
            }
        }
    }

    emit listChanged(m_list, m_index);
}

void AsyncInternetRadioResponse::parseLanguage(QJsonDocument doc)
{
    if(!doc.isNull())
    {
        QVariantMap back;
        back.insert("name","...");
        back.insert("icon","");
        back.insert("link","");
        back.insert("params","");
        m_list.append(back);

        if(doc.isArray()){
            QJsonArray resultArray = doc.array();
            for(int x=0; x<resultArray.size(); x++){
                QJsonObject resultObj = resultArray[x].toObject();

                QString s = resultObj["name"].toString() + " (stations: "+resultObj["stationcount"].toString()+")";

                QString n = resultObj["name"].toString().toUpper();
                int c = n.at(0).toLatin1();
                if(c >= 65 && c <= 90){
                    if(m_index[c-65] == -1)
                        m_index[c-65] = x;
                }

                QVariantMap b;
                b.insert("name",s);
                b.insert("icon","");
                b.insert("link","lang");
                b.insert("params",resultObj["value"].toString().toLower());
                m_list.append(b);
            }
        }
    }

    emit listChanged(m_list, m_index);
}

void AsyncInternetRadioResponse::parseTag(QJsonDocument doc)
{
    if(!doc.isNull())
    {
        QVariantMap back;
        back.insert("name","...");
        back.insert("icon","");
        back.insert("link","");
        back.insert("params","");
        m_list.append(back);

        if(doc.isArray()){
            QJsonArray resultArray = doc.array();
            for(int x=0; x<resultArray.size(); x++){
                QJsonObject resultObj = resultArray[x].toObject();

                QString s = resultObj["value"].toString() + " (stations: "+resultObj["stationcount"].toString()+")";

                QString n = resultObj["name"].toString().toUpper();
                int c = n.at(0).toLatin1();
                if(c >= 65 && c <= 90){
                    if(m_index[c-65] == -1){
                        m_index[c-65] = x;
                    }
                }

                QVariantMap b;
                b.insert("name",s);
                b.insert("icon","");
                b.insert("link","group");
                b.insert("params",resultObj["value"].toString().toLower());
                m_list.append(b);
            }
        }
    }

    emit listChanged(m_list, m_index);
}
//----------------------------------------------------------------------------------------

MusicController::MusicController(QMediaPlayer *parent) : QMediaPlayer(parent)
{
    i_position = "";

    m_playing = false;
    m_metadata["AlbumArtist"] = "";
    m_metadata["Title"] = "";
    m_metadata["Artist"] = "";
    m_metadata["Album"] = "";
    m_metadata["songArt"] = "files/noCoverArt";
    m_metadata["cover"] = "";

    m_repeat = false;

    m_voice = false;
    m_playingVoice = false;

    pauseData = false;
    pausePosition = 0;
    pausePlaylist = NULL;
    pausePlayListPos = -1;

    this->setPlaylist(&m_playlist);

    getIRMainPage();

    connect(this, &QMediaPlayer::mediaStatusChanged, this, &MusicController::mediaStatusCheckChanged);
    connect(this, &QMediaPlayer::stateChanged, this, &MusicController::stateChangedCheckChanged);
    connect(this, QOverload<>::of(&QMediaPlayer::metaDataChanged), this, &MusicController::metaDataCheckChanged);
   // connect(this, &QMediaPlayer::positionChanged, this, &MusicController::positionChanged);


/*
    TagLib::MPEG::File source("/Users/Chris/Documents/Development/reactjs/hubhomeqt/PhotoFrame/music/3/00 - Learn To Fly.mp3");

    TagLib::ID3v2::Tag *tag = source.ID3v2Tag();
    TagLib::ID3v2::FrameList l = tag->frameList("APIC");

    QImage image;

    if(l.isEmpty()){
    //    return image;
    }

    TagLib::ID3v2::AttachedPictureFrame *f =
        static_cast<TagLib::ID3v2::AttachedPictureFrame *>(l.front());

    image.loadFromData((const uchar *) f->picture().data(), f->picture().size());

    /*
TagLib::MP4::File f(file);
TagLib::MP4::Tag* tag = f.tag();
TagLib::MP4::ItemListMap itemsListMap = tag->itemListMap();
TagLib::MP4::Item coverItem = itemsListMap["covr"];
TagLib::MP4::CoverArtList coverArtList = coverItem.toCoverArtList();
if (!coverArtList.isEmpty()) {
    TagLib::MP4::CoverArt coverArt = coverArtList.front();
    image.loadFromData((const uchar *)
    coverArt.data().data(),coverArt.data().size());
}
     * */

   //TagLib::MPEG::File source("/Users/Chris/Documents/Development/reactjs/hubhomeqt/PhotoFrame/music/3/00 - Learn To Fly.mp3",true);

   // TagLib::ID3v2::Tag *id3v2tag = source.ID3v2Tag();
   // qDebug()<< "Song Artist: " << source.tag()->artist().to8Bit(true).data();

 //   connect(player, &QMediaPlayer::mediaStatusChanged, this, &MainWindow::myErrorHandler);
}

QVariantList MusicController::getInternetRadioList()
{
    return i_list;
}

QString MusicController::getInternetRadioPosition()
{
    return i_position;
}

QList<int> MusicController::getInternetRadioListIndex()
{
    return i_index;
}

void MusicController::getIRMainPage()
{
    i_list.clear();
    i_position = "";

    QVariantMap b;
    b.insert("name","Country");
    b.insert("icon","");
    b.insert("params","");
    b.insert("link","world");
    i_list.append(b);

    QVariantMap t;
    t.insert("name","Tag");
    t.insert("icon","");
    t.insert("params","");
    t.insert("link","tag");
    i_list.append(t);

    QVariantMap l;
    l.insert("name","Language");
    l.insert("icon","");
    l.insert("params","");
    l.insert("link","language");
    i_list.append(l);

    QVariantMap f;
    f.insert("name","Favourites");
    f.insert("icon","");
    f.insert("params","");
    f.insert("link","favourites");
    i_list.append(f);

    emit internetRadioListChanged();
    emit internetRadioPositionChanged();
}

void MusicController::changeInternetRadioList(QString v, QString p)
{
    if(v == "world"){
        AsyncInternetRadioResponse *response = new AsyncInternetRadioResponse(m_pathName, "","world");
        connect(response, &AsyncInternetRadioResponse::listChanged, this, &MusicController::listChanged);
        pool.start(response);
        i_position = "world";
    }else if(v == "country"){
        AsyncInternetRadioResponse *response = new AsyncInternetRadioResponse(m_pathName, p,"country");
        connect(response, &AsyncInternetRadioResponse::listChanged, this, &MusicController::listChanged);

        pool.start(response);
        i_position = "country";
    }else if(v == "tag"){
        AsyncInternetRadioResponse *response = new AsyncInternetRadioResponse(m_pathName, "","tag");
        connect(response, &AsyncInternetRadioResponse::listChanged, this, &MusicController::listChanged);

        pool.start(response);
        i_position = "tag";
    }else if(v == "language"){
        AsyncInternetRadioResponse *response = new AsyncInternetRadioResponse(m_pathName, "","language");
        connect(response, &AsyncInternetRadioResponse::listChanged, this, &MusicController::listChanged);

        pool.start(response);
        i_position = "language";
    }else if(v == "lang"){
        AsyncInternetRadioResponse *response = new AsyncInternetRadioResponse(m_pathName, p,"lang");
        connect(response, &AsyncInternetRadioResponse::listChanged, this, &MusicController::listChanged);

        pool.start(response);
        i_position = "lang";
    }else if(v == "group"){
        AsyncInternetRadioResponse *response = new AsyncInternetRadioResponse(m_pathName, p,"group");
        connect(response, &AsyncInternetRadioResponse::listChanged, this, &MusicController::listChanged);

        pool.start(response);
        i_position = "group";
    }else if(v == "favourites"){
        AsyncInternetRadioResponse *response = new AsyncInternetRadioResponse(m_pathName, "","favourites");
        connect(response, &AsyncInternetRadioResponse::listChanged, this, &MusicController::listChanged);

        pool.start(response);
        i_position = "favourites";
    }else{
        getIRMainPage();
    }
    emit internetRadioPositionChanged();
}

void MusicController::listChanged(QVariantList v, QList<int> i)
{
    i_list = v;
    i_index = i;
    emit internetRadioListChanged();
    emit internetRadioListIndexChanged();
}

void MusicController::FileslistChanged(QVariantList v, QList<int> i)
{
    i_musicList = v;
    i_musicindex = i;
    emit musicListChanged();
    emit musicListIndexChanged();
}

QString MusicController::pathName()
{
    return m_pathName;
}

void MusicController::setPathName(const QString &pathName)
{
    if (pathName == m_pathName)
        return;

    m_pathName = pathName;
}

void MusicController::addInternetRadioFav(QString name, QString icon, QString tag, QString params)
{
    QString filePath = m_pathName +"playlist/favourites.ir";
    bool exists = QFileInfo::exists(filePath) && QFileInfo(filePath).isFile();
    QJsonDocument obj;
    QJsonArray resultArray;
    if(exists){
        QFile jsonFile(filePath);
        jsonFile.open(QFile::ReadOnly);
        obj = QJsonDocument().fromJson(jsonFile.readAll());
        if(obj.isArray()){
            resultArray = obj.array();
        }
    }else{
        QJsonDocument newDoc(resultArray);
        obj = newDoc;
    }

    bool found = false;
    for(int x=0; x<resultArray.size(); x++){
        QJsonObject resultObj = resultArray[x].toObject();
        if(resultObj["params"].toString() == params){
           found = true;
           break;
        }
    }

    if(!found){
        QJsonObject obj;
        obj.insert("name", name);
        obj.insert("icon", icon);
        obj.insert("tag", tag);
        obj.insert("params", params);

        resultArray.push_back(obj);
    }

    QJsonDocument doc(resultArray);

    QFile jsonWrite(filePath);
    jsonWrite.open(QFile::WriteOnly);
    jsonWrite.write(doc.toJson());
}

QVariantList MusicController::getMusicList()
{
    return i_musicList;
}

QList<int> MusicController::getMusicListIndex()
{
    return i_musicindex;
}

void MusicController::setMusicList(QString type)
{
    AsyncMusicFilesResponse *response = new AsyncMusicFilesResponse(m_pathName,type);
    connect(response, &AsyncMusicFilesResponse::listChanged, this, &MusicController::FileslistChanged);
    pool.start(response);
}

void MusicController::scanMusicDirectory()
{
    QMutexLocker M(&l_scanning);
    qDebug() << "scanMusicDirectory started";
    qint64 start = QDateTime::currentMSecsSinceEpoch();
    //QMediaPlaylist musicFiles;
    QStringList musicfilter;
    musicfilter << "*.mp3" << "*.wma";

    QDirIterator it(m_pathName+"music/", musicfilter, QDir::Files, QDirIterator::Subdirectories);

    QFile file(m_pathName+"music/allfiles.list");
    if (!file.open(QFile::WriteOnly | QFile::Text)) return;
    QTextStream out(&file);

    while (it.hasNext()) {
        QString filepath = it.next();
        //qDebug() << "File" << filepath;

        QByteArray ba = filepath.toLocal8Bit();
        const char *c_str2 = ba.data();

        TagLib::MPEG::File source(c_str2,true);

        QString artist, name, album, cover;
        if(source.isOpen()){
            TagLib::ID3v2::Tag *id3v2tag = source.ID3v2Tag();
            name = source.tag()->title().to8Bit(true).data();
            artist = source.tag()->artist().to8Bit(true).data();
            album = source.tag()->album().to8Bit(true).data();

            if(id3v2tag){
                TagLib::ID3v2::FrameList l = id3v2tag->frameList("APIC");

                if(!l.isEmpty()){
                    TagLib::ID3v2::AttachedPictureFrame *f =
                        static_cast<TagLib::ID3v2::AttachedPictureFrame *>(l.front());
                    if(f){
                        QImage image;
                        image.loadFromData((const uchar *) f->picture().data(), f->picture().size());

                        QFileInfo fi(filepath);
                        //QFileInfo fi2(fi.fileName());
                        cover = fi.baseName()+".png";
                        image.save(m_pathName+"music/cache/"+cover);
                    }
                }
            }
        }

        if(name == ""){
            QString f = filepath;
            int p = f.lastIndexOf("/");
            f = f.remove(0,p+1);
            f = f.replace("_"," ");
            int e = f.lastIndexOf(".");
            f.chop(f.length() - e);
            name = f;
        }

        QStringList list;
        list << filepath << name << artist << album << cover;

        out << list.join("|") << endl;
    }
    //musicFiles.save(QUrl::fromLocalFile(m_pathName+"music/allfiles.m3u"),"m3u");
    file.close();
    qint64 end = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "scanMusicDirectory finished" << end-start;
}

void MusicController::triggerSound(QString type)
{
    if(type == "standard"){
       this->stop();
       this->setMedia(QUrl::fromLocalFile(m_pathName+"media/beep.mp3"));
       //mediaPlayer.setVolume(50);
       m_repeat = true;
       this->play();
    }
}

void MusicController::dismissSounds()
{
    this->stop();
    m_repeat = false;
}

//Media Player
Q_INVOKABLE void MusicController::musicShuffle()
{
    this->playlist()->shuffle();
}

Q_INVOKABLE void MusicController::musicNext()
{
    this->playlist()->next();
}

Q_INVOKABLE void MusicController::musicPrevious()
{
    this->playlist()->previous();
}

Q_INVOKABLE void MusicController::playSource(QString file)
{
    this->setMedia(QUrl::fromLocalFile(file));
    this->play();
}

Q_INVOKABLE void MusicController::playlistSource(QString file)
{
    this->playlist()->load(QUrl::fromLocalFile(file));
}

Q_INVOKABLE void MusicController::playlistIndex(int pos)
{
    this->playlist()->setCurrentIndex(pos);
}

int MusicController::getVolumeAlarm(){
    return v_alarm;
}

void MusicController::setVolumeAlarm(int v){
    v_alarm = v;
}

void MusicController::stateChangedCheckChanged(QMediaPlayer::State state)
{
    m_playing = false;
    if(state >= QMediaPlayer::PlayingState){
        m_playing = true;
    }
    emit isPlayingChanged(m_playing);
}

void MusicController::mediaStatusCheckChanged(QMediaPlayer::MediaStatus status)
{
    if(status == QMediaPlayer::EndOfMedia){
        if(m_repeat){
            this->play();
        }

        if(m_playingVoice){
            ProcessAssistant("resume");
        }
    }
}

bool MusicController::getRepeat(){
    return m_repeat;
}

void MusicController::setRepeat(bool r){
    m_repeat = r;
}

bool MusicController::getIsPlaying()
{
    return m_playing;
}

Q_INVOKABLE void MusicController::musicStop()
{
    this->stop();
    m_metadata.clear();
    m_metadata["Title"] = "";
    m_metadata["Artist"] = "";
    m_metadata["Album"] = "";
    m_metadata["songArt"] = "files/noCoverArt";
    m_image = QImage();
    emit mediaImageChanged();
    emit mediaDataChanged(m_metadata);
}

void MusicController::metaDataCheckChanged()
{
    //qDebug() <<"metaDataCheckChanged";
    if (this->isMetaDataAvailable()) {
        //qDebug() <<"A" << this->currentMedia().canonicalUrl().toString();
        m_metadata.clear();
        QString title = this->metaData(QMediaMetaData::Title).toString();
        if(this->metaData(QMediaMetaData::Title).toString() == ""){
            QFileInfo fileInfo(this->currentMedia().canonicalUrl().toString());
            title = fileInfo.baseName();
        }
        m_metadata["Title"] = title;
        m_metadata["Artist"] = this->metaData(QMediaMetaData::AlbumArtist).toString();
        m_metadata["Album"] = "";
        m_metadata["songArt"] = "files/noCoverArt";

        m_image = QImage();

        QUrl url = this->metaData(QMediaMetaData::CoverArtUrlLarge).value<QUrl>();
        if(!url.isEmpty()){
            qDebug() <<"a"<<url;
            m_metadata["songArt"] = url.toString();
        }
        url = this->metaData(QMediaMetaData::CoverArtUrlSmall).value<QUrl>();
        if(!url.isEmpty()){
            qDebug() <<"b"<<url;
            m_metadata["songArt"] = url.toString();
        }
        url = this->metaData(QMediaMetaData::PosterUrl).value<QUrl>();
        if(!url.isEmpty()){
            qDebug() <<"c"<<url;
            m_metadata["songArt"] = url.toString();
            //if (m_coverLabel) {
//                QUrl url = m_player->metaData(QMediaMetaData::CoverArtUrlLarge).value<QUrl>();

//                m_coverLabel->setPixmap(!url.isEmpty()
//                        ? QPixmap(url.toString())
//                        : QPixmap());
//            }//
        }

        QImage image = this->metaData(QMediaMetaData::ThumbnailImage).value<QImage>();
        if(!image.isNull()){
            //qDebug() << "d" << image;
            m_metadata["songArt"] = "image://provedor/"+title;//image;
            m_image = image;
        }
        image = this->metaData(QMediaMetaData::CoverArtImage).value<QImage>();
        if(!image.isNull()){
            //qDebug() <<"e" << image;
            m_metadata["songArt"] = "image://provedor/"+title;//image;
            m_image = image;
        }
        image = this->metaData(QMediaMetaData::PosterImage).value<QImage>();
        if(!image.isNull()){
            //qDebug() <<"f" << image;
            m_metadata["songArt"] = "image://provedor/"+title;//image;
            m_image = image;
        }

        emit mediaImageChanged();
        emit mediaDataChanged(m_metadata);
    }else{
        //qDebug() <<"B" << this->currentMedia().canonicalUrl().toString();
        QFileInfo fileInfo(this->currentMedia().canonicalUrl().toString());
        m_metadata.clear();
        m_metadata["Title"] = fileInfo.baseName();
        m_metadata["Artist"] = "";
        m_metadata["Album"] = "";
        m_metadata["songArt"] = "files/noCoverArt";
        emit mediaDataChanged(m_metadata);
    }
}

Q_INVOKABLE QImage MusicController::getMediaImage()
{
    return m_image;
}

QVariantMap MusicController::getMediaData()
{
    return m_metadata;
}

//Assistant
void MusicController::ProcessAssistant(QString type)
{
    if(type == "reset"){
        this->stop();
    }else if(type == "start"){
        m_repeat = false;
        m_playingVoice = true;
        this->setMedia(QUrl::fromLocalFile(QFileInfo("/tmp/output.mp3").absoluteFilePath()));
        //mediaPlayer.setVolume(50);
        this->play();
    }else if(type == "pause"){
        if(m_playing && m_voice){
            pauseData = true;
            pauseMedia = this->currentMedia();
            pausePosition = this->position();
            pausePlaylist = this->playlist();
            pausePlayListPos = -1;
            if(pausePlaylist != NULL){
                pausePlayListPos = pausePlaylist->currentIndex();
            }
        }
    }else if(type == "resume"){
        m_playingVoice = false;
        if(pauseData && m_voice){
            pauseData = false;
            if(pausePlaylist != NULL){
                this->setPlaylist(pausePlaylist);
                this->playlist()->setCurrentIndex(pausePlayListPos);
            }else{
                this->setMedia(pauseMedia);
            }
            this->setPosition(pausePosition);
            this->play();
        }
    }
}

bool MusicController::processVoiceCmd(QString cmd, QString &response)
{
    if(cmd.contains("music")){
        if(cmd.contains("go to")){
            emit pageTrigger("music", 0);
            return true;
        }
    }


    return false;
}

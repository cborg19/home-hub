#ifndef GLOBAL_H
#define GLOBAL_H
#include <QString>

void setWrite();
void setReadOnly();
QString toCamelCase(const QString& s);
QString toTimeString(const unsigned int s);

#endif // GLOBAL_H

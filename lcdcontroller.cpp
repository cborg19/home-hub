#include "lcdcontroller.h"
#include <QProcess>
#include <QtCore/qdebug.h>

LCDController::LCDController(QObject *parent) : QObject(parent),
    m_brightness(-1)
{

}

void LCDController::setBrightness(int value)
{
    #ifdef Q_OS_LINUX
    QString cmd = "bash -c \"echo "+QString::number(value)+" | sudo tee /sys/class/backlight/rpi_backlight/brightness";
    QProcess process;
    process.start(cmd);
    process.waitForFinished();
    m_brightness = value;
    #else
    qDebug() << "LCDController setBrightness" << value;
    #endif
}

void LCDController::lcdOff()
{
    #ifdef Q_OS_LINUX
    QString cmd = "bash -c \"echo 1 | sudo tee /sys/class/backlight/rpi_backlight/bl_power";
    QProcess process;
    process.start(cmd);
    process.waitForFinished();
    #else
    qDebug() << "LCDController lcdOff";
    #endif
}

void LCDController::lcdOn()
{
    #ifdef Q_OS_LINUX
    QString cmd = "bash -c \"echo 0 | sudo tee /sys/class/backlight/rpi_backlight/bl_power";
    QProcess process;
    process.start(cmd);
    process.waitForFinished();
    #else
    qDebug() << "LCDController lcdOn";
    #endif
}

int LCDController::currentBrightness()
{
    return m_brightness;
}


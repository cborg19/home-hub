#ifndef EARTHDATA_H
#define EARTHDATA_H

#include <QObject>
#include <QTime>

class EarthData
{
public:
    EarthData();
    EarthData(double latitude, double longitude, double gmt);

    void setLocation(double latitude, double longitude, double gmt);

    QTime getSunRise();
    QTime getSunSet();
    QTime getSunRise(QDateTime c);
    QTime getSunSet(QDateTime c);
private:
    double latitude;
    double longitude;
    double gmt;
};

#endif // EARTHDATA_H

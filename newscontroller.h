#ifndef NEWSCONTROLLER_H
#define NEWSCONTROLLER_H

#include <QObject>
#include <QThreadPool>

class NewsController : public QObject
{
    Q_OBJECT
public:
    explicit NewsController(QObject *parent = nullptr);

    void checkUpdate();

    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void pageTrigger(QString page, int index);
};

#endif // NEWSCONTROLLER_H

#ifndef KODICONTROLLER_H
#define KODICONTROLLER_H

#include <QObject>
#include <QtWebSockets/QWebSocket>
#include <QTimer>
#include <QImage>

#include "networkmanager.h"

#ifdef _MSC_VER
#define default_range {0,25}
#else
    static constexpr QPair<int, int> default_range = {0,25};
#endif

extern bool gdebug;

class KodiController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantMap currentMetaData READ getCurrentMetaData NOTIFY currentMetaDataChanged)
    Q_PROPERTY(double currentPercentage READ getPercentage NOTIFY currentPercentageChanged)
    Q_PROPERTY(QVariantMap currentTime READ getTime NOTIFY currentTimeChanged)
    Q_PROPERTY(QVariantMap currentTotalTime READ getTotalTime NOTIFY currentTotalTimeChanged)

    Q_PROPERTY(QVariantList list READ getList NOTIFY listChanged)
    Q_PROPERTY(QList<int> listIndex READ getListIndex NOTIFY listIndexChanged)
    Q_PROPERTY(QString menu READ getMenu NOTIFY menuChanged)

    Q_PROPERTY(QVariantList playlist READ getplayLists NOTIFY playlistChanged)
public:
    explicit KodiController(QObject *parent = nullptr);

    void setSettings(bool enabled, QString host, int port, int httpport, QString uname, QString password, bool tcp, bool eventserver);

    void setEnabled(bool);
    bool getEnabled();

    QStringList getSettings();
    void setSettings(QStringList);

    QVariantMap getCurrentMetaData();
    double getPercentage();
    QVariantMap getTime();
    QVariantMap getTotalTime();

    QVariantList getList();
    QList<int> getListIndex() { return m_index; }
    QString getMenu();

    QVariantList getplayLists();

    // input
    void keypress(int keycode);

    bool isConnected() { return m_connected; }
    bool isPaused() { return m_paused; }
    bool isPlaying() { return m_playing; }
    bool isMute() { return m_muted; }
    double getVolume() { return m_volume; }
    void setVolume(double);
    void setPlayer(QString);
    void addToPlayList(QString);
    void showGallery(QString);
    void showPicture(QStringList);

    void getTvShows(int, int);
    void getTvShow(int);
    void getMovies(int, int);
    void getMusic(int);
    void getAlbums(int);
    void getArtists(int);
    void getPlaylist(int);
    void getSeason(int, int);
    void getVideo(int);
    void getShowDetails(int);
    void getPlayList();

    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void connection(bool);
    void isplaying(bool);
    void ispaused(bool);

    void currentMetaDataChanged();
    void currentPercentageChanged();
    void currentTimeChanged();
    void currentTotalTimeChanged();

    void listChanged();
    void playlistChanged();
    void listIndexChanged();
    void menuChanged();

    void volume(bool, double);
    void pageTrigger(QString page, int index);
public slots:
private slots:
    void connectionEstablished();
    void connectionClosed();
    void tryConnect();
    void record_response(QString response);
    void duration();
private:
    QWebSocket socket;

    bool m_enabled;
    QString m_host;
    int m_port, m_httpport;
    QString m_uname;
    QString m_password;
    QString m_basic;
    bool m_tcp ;
    bool m_eventserver;
    bool m_connected, m_paused, m_playing;
    bool m_muted;
    double m_volume;
    double m_percentage;
    QVariantMap m_time;
    QVariantMap m_totaltime;
    int m_timeCount;

    int m_total;
    QVariantList m_list, m_playlist;
    QList<int> m_index;
    QString m_menu;

    QTimer timerConenct, timerDuration;

    QString RPC_prolog;
    QString RPC_epilog;
    QString response;
    void call_method(QString method, QList<QPair<QString, QString>> parameters = {}, QString id = "");

    QVariantMap m_currentMetaData;

    QString getImage(QString url);
};

#endif // KODICONTROLLER_H

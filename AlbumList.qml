import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Page {
    id: albump
    anchors.fill: parent

    width: imageWidth
    height: imageHeight
    z: 3

    background: Rectangle {
        color: "#f2f2f2"
    }

    Button {
        id: btClose
        x: 10
        y: 0
        width: 65
        height: 35
        text: qsTr("Close")
    }

    Connections {
        target: btClose
        onClicked: {
            delayClose.start();
        }
    }

    Timer {
        id: delayClose
        interval: 500
        running: false
        repeat: false
        onTriggered:{
            delayClose.stop();
            albumlist.destroy();
        }
    }

    Component {
        id: delegateAlbumPath
        Item {
            id: rowAlbum

            width: parent.width
            height: 45

            property string state: albumlist.modelData[index].state
            RowLayout {
                spacing: 10
                Label {
                    id: btAddtoFav
                    width: 30
                    text: rowAlbum.state.toUpperCase()
                    color: "black"
                    font.pointSize: 18
                    font.family: "Helvetica"
                }

                Label {
                    text: albumlist.modelData[index].path
                    color: "black"
                    font.pointSize: 18
                    font.family: "Helvetica"
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(rowAlbum.state === "n"){
                        rowAlbum.state = "f"
                        controller.favouriteAlbum(index+1)
                    }else if(rowAlbum.state === "f"){
                        rowAlbum.state = "b"
                        controller.blockAlbum(index+1)
                    }else{
                        rowAlbum.state = "n"
                        controller.neturalAlbum(index+1)
                    }
                }
            }
        }
    }

    Item {
        id: mainList
        x: 10
        y: 40
        height: imageHeight - 50
        width: 780

        Rectangle {
            anchors.fill: parent;
            color: "white"
        }

        ListView {
            anchors.fill: parent;

            model: albumlist.modelData
            delegate: delegateAlbumPath
        }
    }
}

#include "fibarocontroller.h"
#include <QtCore/qdebug.h>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QJsonObject>
#include <QJsonArray>
#include <QDirIterator>
#include "global.h"

void AsyncFibaroResponse::run()
{
    //qDebug() << "AsyncFibaroResponse" << m_path << m_type << m_basic;

    QString filePath = "/Users/Chris/Documents/Development/reactjs/hubhomeqt/PhotoFrame/fibaro/"+ m_type + ".txt";
    bool exists = QFileInfo::exists(filePath) && QFileInfo(filePath).isFile();
    bool downloadFiles = true;
    QJsonDocument doc;

    /*if(exists){
        QFile jsonFile(filePath);
        jsonFile.open(QFile::ReadOnly);
        QJsonDocument obj = QJsonDocument().fromJson(jsonFile.readAll());
        if(!obj.isNull())
        {
            if(obj.isObject()){
                qint64 l = obj["lastupdate"].toVariant().toLongLong();
                qint64 n = QDateTime::currentMSecsSinceEpoch();

                if((n - l) < 2678400000){
                    downloadFiles = false;
                    if(obj["response"].isArray()){
                        QJsonDocument newDoc(obj["response"].toArray());
                        doc = newDoc;
                    }
                }
            }else if(obj.isArray()){
                QJsonDocument newDoc(obj);
                doc = newDoc;
            }
        }
    }*/

    if(downloadFiles){
        QNetworkAccessManager manager;
        QNetworkRequest request;

        request.setUrl(QUrl(m_path));

        request.setRawHeader("Content-Type", "application/json");
        request.setRawHeader("Authorization", m_basic.toUtf8());

        QSslConfiguration conf = request.sslConfiguration();
        conf.setPeerVerifyMode(QSslSocket::VerifyNone);
        request.setSslConfiguration(conf);

        QNetworkReply* reply = manager.get(request);

        QEventLoop eventloop;
        connect(reply,SIGNAL(finished()),&eventloop,SLOT(quit()));
        eventloop.exec();

        if (reply->error() == QNetworkReply::NoError)
        {
            //qDebug() << "AsyncFibaroResponse GOT DATA";
            QString data = reply->readAll();

            doc = QJsonDocument::fromJson(data.toUtf8());
//qDebug() <<data.toUtf8();
            /*if(!doc.isNull())
            {
                QJsonObject obj;
                obj.insert("lastupdate", QJsonValue::fromVariant(QDateTime::currentMSecsSinceEpoch()));
                if(doc.isArray()){
                    QJsonArray resultArray = doc.array();
                    obj.insert("response", resultArray);
                }

                QJsonDocument newDoc(obj);
                setWrite();

                QFile jsonFile(filePath);
                jsonFile.open(QFile::WriteOnly);
                jsonFile.write(newDoc.toJson());
            }*/
        }else{
            emit listChanged(m_list, "failed");
            //qDebug() << "AsyncFibaroResponse FAILED";
        }
    }

    if(!doc.isNull()){
        if(m_type == "room"){
            if(doc.isArray()){
                QJsonArray resultArray = doc.array();
                for(int x=0; x<resultArray.size(); x++){
                    QJsonObject resultObj = resultArray[x].toObject();

                    QVariantMap room;
                    room.insert("id", resultObj["id"].toInt());
                    room.insert("name", resultObj["name"].toString());
                    room.insert("sectionID", resultObj["sectionID"].toString());
                    room.insert("icon", resultObj["icon"].toString());
                    if(resultObj["icon"].toString() != ""){
                       room.insert("iconurl", "fibaro|http://192.168.178.10/fibaro/icons/rooms/"+resultObj["icon"].toString()+".png|"+m_basic);
                    }
                    room.insert("category", resultObj["category"].toString());

                    QJsonObject sensorObj = resultObj["defauktSensors"].toObject();
                    int temp = sensorObj["temperature"].toInt(0);
                    int light = sensorObj["light"].toInt(0);

                    room.insert("temperature", sensorObj["temperature"].toInt());

                    room.insert("humidity", sensorObj["humidity"].toInt());
                    room.insert("light", sensorObj["light"].toInt());

                    if(light != 0 || temp != 0){
                        room.insert("sensor", true);
                    }else{
                        room.insert("sensor", false);
                    }

                    room.insert("devicesOn", false);
                    QVariantList dlist;
                    room.insert("devices", dlist);
                    room.insert("temp", "");

                    m_list.push_back(room);
                }
            }

            emit listChanged(m_list, m_type);
        }else if(m_type == "section"){
            if(doc.isArray()){
                QJsonArray resultArray = doc.array();
                for(int x=0; x<resultArray.size(); x++){
                    QJsonObject resultObj = resultArray[x].toObject();

                    QVariantMap room;
                    room.insert("id", resultObj["id"].toString());
                    room.insert("name", resultObj["name"].toString());

                    m_list.push_back(room);
                }
            }

            emit listChanged(m_list, m_type);
        }else if(m_type == "weather"){
            QJsonObject resultObj = doc.object();
            QVariantMap room;
            room.insert("Temperature", resultObj["Temperature"].toString());
            room.insert("TemperatureUnit", resultObj["TemperatureUnit"].toString());
            room.insert("Humidity", resultObj["Humidity"].toString());
            room.insert("Wind", resultObj["Wind"].toString());
            room.insert("WindUnit", resultObj["WindUnit"].toString());
            room.insert("WeatherCondition", resultObj["WeatherCondition"].toString());
            room.insert("ConditionCode", resultObj["ConditionCode"].toString());

            m_list.push_back(room);
            emit listChanged(m_list, m_type);
        }else if(m_type == "device"){
            if(doc.isArray()){
                QJsonArray resultArray = doc.array();
                for(int x=0; x<resultArray.size(); x++){
                    QJsonObject resultObj = resultArray[x].toObject();

                    QVariantMap room;
                    room.insert("id", resultObj["id"].toInt());
                    room.insert("name", resultObj["name"].toString());
                    room.insert("roomID", resultObj["roomID"].toInt());
                    room.insert("type", resultObj["type"].toString());
                    room.insert("baseType", resultObj["baseType"].toString());
                    room.insert("enabled", resultObj["enabled"].toBool());
                    room.insert("visible", resultObj["visible"].toBool());
                    room.insert("isPlugin", resultObj["isPlugin"].toBool());
                    room.insert("parentId", resultObj["parentId"].toInt());
                    room.insert("remoteGatewayId", resultObj["remoteGatewayId"].toInt());
                    room.insert("viewXml", resultObj["viewXml"].toBool());
                    room.insert("configXml", resultObj["configXml"].toBool());

                    QJsonArray interArray = resultObj["interfaces"].toArray();
                    QStringList interface;
                    if(!interArray.isEmpty()){
                        for(int y=0; y<interArray.size(); y++){
                            interface.append(interArray[y].toString());
                        }
                    }
                    room.insert("interfaces", interface);

                    QJsonObject actionObj = resultObj["actions"].toObject();
                    QVariantMap actions;
                    if(!actionObj.isEmpty()){
                        foreach(const QString& key, actionObj.keys()) {
                            QJsonValue value = actionObj.value(key);

                            if(value.isString())
                               actions.insert(key,value.toString());
                            else if(value.isDouble())
                               actions.insert(key,value.toDouble());
                            else if(value.isBool())
                                actions.insert(key,value.toBool());
                        }
                    }
                    room.insert("actions", actions);

                    QJsonObject propertiesObj = resultObj["properties"].toObject();
                    QVariantMap properties;
                    if(!propertiesObj.isEmpty()){
                        foreach(const QString& key, propertiesObj.keys()) {
                            QJsonValue value = propertiesObj.value(key);

                            if(value.isString())
                               properties.insert(key,value.toString());
                            else if(value.isDouble())
                               properties.insert(key,value.toDouble());
                            else if(value.isBool())
                                properties.insert(key,value.toBool());
                        }
                    }
                    room.insert("properties", properties);

                    m_list.push_back(room);
                }
            }

            emit listChanged(m_list, m_type);
        }else if(m_type == "scene"){
            if(doc.isArray()){
                QJsonArray resultArray = doc.array();
                for(int x=0; x<resultArray.size(); x++){
                    QJsonObject resultObj = resultArray[x].toObject();

                    QVariantMap room;
                    room.insert("id", resultObj["id"].toString());
                    room.insert("name", resultObj["name"].toString());
                    room.insert("type", resultObj["type"].toString());
                    room.insert("roomID", resultObj["roomID"].toInt());
                    room.insert("iconID", resultObj["iconID"].toInt());
                    room.insert("runConfig", resultObj["runConfig"].toString());
                    room.insert("autostart", resultObj["autostart"].toBool());
                    room.insert("protectedByPIN", resultObj["protectedByPIN"].toBool());
                    room.insert("killable", resultObj["killable"].toBool());
                    room.insert("killOtherInstances", resultObj["killOtherInstances"].toBool());
                    room.insert("maxRunningInstances", resultObj["maxRunningInstances"].toInt());
                    room.insert("runningInstances", resultObj["runningInstances"].toInt());

                    m_list.push_back(room);
                    /*
{
    "id": 4,
    "name": "Switch Off PS",
    "type": "com.fibaro.blockScene",
    "categories": [
      "other"
    ],
    "roomID": 12,
    "iconID": 5,
    "runConfig": "TRIGGER_AND_MANUAL",
    "alexaProhibited": false,
    "autostart": true,
    "protectedByPIN": false,
    "killable": true,
    "killOtherInstances": false,
    "maxRunningInstances": 2,
    "runningInstances": 1,
    "instances": [
      "autostart"
    ],
    "runningManualInstances": 0,
    "visible": true,
    "isLua": false,
    "properties": "",
    "triggers": {
      "properties": [],
      "globals": [],
      "events": [],
      "weather": []
    },
    "actions": {
      "devices": [
        13
      ],
      "scenes": [],
      "groups": []
    },
    "sortOrder": 47
  },
  {
                    */
                }
            }
            emit listChanged(m_list, m_type);
        }else if(m_type == "icon"){
            QJsonArray resultArray = doc["device"].toArray();
            for(int x=0; x<resultArray.size(); x++){
                QJsonObject resultObj = resultArray[x].toObject();

                QVariantMap room;
                room.insert("id", QString::number(resultObj["id"].toInt()));
                room.insert("deviceType", resultObj["deviceType"].toString());
                room.insert("iconSetName", resultObj["iconSetName"].toString());

                m_list.push_back(room);
            }

            emit listChanged(m_list, m_type);
        }else if(m_type == "refresh"){
            QVariantMap room;
            room.insert("last", doc["last"].toInt());

            QVariantList events;
            QJsonArray eventsArray = doc["events"].toArray();
            for(int x=0; x<eventsArray.size(); x++){
                QJsonObject eventsObj = eventsArray[x].toObject();

                QVariantMap event;
                foreach(const QString& key, eventsObj.keys()) {
                    QJsonValue value = eventsObj.value(key);

                    if(key == "data"){
                       QJsonObject dataObj = eventsObj["data"].toObject();
                       if(!eventsObj.isEmpty()){
                           foreach(const QString& keyData, dataObj.keys()) {
                               QJsonValue value = dataObj.value(keyData);

                               if(value.isString())
                                  event.insert(keyData,value.toString());
                               else if(value.isDouble())
                                  event.insert(keyData,value.toDouble());
                               else if(value.isBool())
                                  event.insert(keyData,value.toBool());
                               else {
                                   event.insert(keyData,value.toInt());
                               }
                           }
                       }
                    }else if(value.isString())
                       event.insert(key,value.toString());
                    else if(value.isDouble())
                       event.insert(key,value.toDouble());
                    else if(value.isBool())
                       event.insert(key,value.toBool());
                }

                events.push_back(event);
            }

            room.insert("events", events);

            m_list.push_back(room);
            emit listChanged(m_list, m_type);
        }
    }
}

FibaroController::FibaroController(QObject *parent) : QObject(parent),
    m_enabled(false), m_lastTime(0), m_connected(false)
{
    connect(&timerConenct, SIGNAL(timeout()), this, SLOT(tryConnect()));
    connect(&timerDuration, SIGNAL(timeout()), this, SLOT(tryRefresh()));
}

void FibaroController::setSettings(bool enabled, QString ip, QString user, QString password, int port)
{
    m_ip = ip;

    m_uname = user;
    m_password = password;
    m_port = port;
    m_enabled = enabled;
    QString concatenated = user + ":" + password;
    QByteArray data = concatenated.toLocal8Bit().toBase64();
    m_basic = "Basic " + data;

    if(m_enabled)
        timerConenct.start(5000);
}

void FibaroController::setEnabled(bool v)
{
    if(v){
       m_enabled = true;
       timerConenct.start(5000);
    }else{
       m_enabled = false;
       timerConenct.stop();
       timerDuration.stop();
    }
}

bool FibaroController::getEnabled()
{
    //qDebug() <<"FibaroController::getEnabled" << m_enabled;
    return m_enabled;
}

QStringList FibaroController::getSettings()
{
    QStringList v;
    v << m_ip << QString::number(m_port) << m_uname << m_password;
    return v;
}

void FibaroController::setSettings(QStringList v)
{
    if(v.length() >= 7){
       m_ip = v[0];
       m_port = v[1].toInt();
       m_uname = v[2];
       m_password = v[3];

       QString concatenated = m_uname + ":" + m_password;
       QByteArray data = concatenated.toLocal8Bit().toBase64();
       m_basic = "Basic " + data;
    }
}

void FibaroController::tryConnect()
{
    timerConenct.stop();
    populate();
}

void FibaroController::tryRefresh()
{
if(gdebug) if(gdebug) qDebug() <<"CRASH - FibaroController tryRefresh";
    QString url = "http://"+m_ip+":"+QString::number(m_port)+"/api/refreshStates?last="+QString::number(m_lastTime);
    AsyncFibaroResponse *response = new AsyncFibaroResponse(url, m_basic,"refresh");
    connect(response, &AsyncFibaroResponse::listChanged, this, &FibaroController::listChanged);
    pool.start(response);
if(gdebug) if(gdebug) qDebug() <<"CRASH - FibaroController tryRefresh finished";
}

void FibaroController::populate()
{
if(gdebug) if(gdebug) qDebug() <<"CRASH - FibaroController populate";
    QString url = "http://"+m_ip+":"+QString::number(m_port)+"/api/icons";
    AsyncFibaroResponse *response5 = new AsyncFibaroResponse(url, m_basic,"icon");
    connect(response5, &AsyncFibaroResponse::listChanged, this, &FibaroController::listChanged);

    pool.start(response5);

    url = "http://"+m_ip+":"+QString::number(m_port)+"/api/rooms";
    AsyncFibaroResponse *response = new AsyncFibaroResponse(url, m_basic,"room");
    connect(response, &AsyncFibaroResponse::listChanged, this, &FibaroController::listChanged);

    pool.start(response);

    url = "http://"+m_ip+":"+QString::number(m_port)+"/api/sections";
    AsyncFibaroResponse *response2 = new AsyncFibaroResponse(url, m_basic,"section");
    connect(response2, &AsyncFibaroResponse::listChanged, this, &FibaroController::listChanged);

    pool.start(response2);

    url = "http://"+m_ip+":"+QString::number(m_port)+"/api/devices";
    AsyncFibaroResponse *response3 = new AsyncFibaroResponse(url, m_basic,"device");
    connect(response3, &AsyncFibaroResponse::listChanged, this, &FibaroController::listChanged);

    pool.start(response3);

    url = "http://"+m_ip+":"+QString::number(m_port)+"/api/scenes";
    AsyncFibaroResponse *response4 = new AsyncFibaroResponse(url, m_basic,"scene");
    connect(response4, &AsyncFibaroResponse::listChanged, this, &FibaroController::listChanged);

    pool.start(response4);

    /*
    url = "http://"+m_ip+":80/api/weather";
    AsyncFibaroResponse *response2 = new AsyncFibaroResponse(url, m_basic,"weather");
    connect(response2, &AsyncFibaroResponse::listChanged, this, &FibaroController::listChanged);

    pool.start(response2);
    */
if(gdebug) if(gdebug) qDebug() <<"CRASH - FibaroController populate finished";
}

void FibaroController::listChanged(QVariantList list, QString type)
{
if(gdebug) if(gdebug) qDebug() <<"CRASH - FibaroController listChanged";
    m_connected = true;
    if(type == "room"){
        QMutexLocker M(&l_devices);
        m_rooms = list;

        if(m_devices.length() > 1){
            indexRooms();
            parseRoomDevices();

            timerDuration.stop();
            timerDuration.start(5000);
        }

        emit roomListChanged();
    }else if(type == "section"){
        m_sections = list;
        emit sectionListChanged();
    }else if(type == "device"){
        QMutexLocker M(&l_devices);
        m_devices = list;

        mapDevices.clear();
        for(int x=0; x<m_devices.length(); x++){
            QVariantMap d = m_devices[x].toMap();
            if(d["id"].isNull()) continue;
            int id = d["id"].toInt();
            mapDevices.insert(id, x);
        }

        if(m_rooms.length() > 1){
            indexRooms();
            parseRoomDevices();

            timerDuration.stop();
            timerDuration.start(3000);

            emit roomListChanged();
        }
        emit deviceListChanged();
    }else if(type == "scene"){
        m_scenes = list;
        emit sceneListChanged();
    }else if(type == "icon"){
        m_icons = list;
        emit iconListChanged();
    }else if(type == "refresh"){
        populateRefresh(list);
    }else if(type == "failed"){
        m_connected = false;
        timerConenct.start(10000);
    }
if(gdebug) qDebug() <<"CRASH - FibaroController listChanged finished";
}

void FibaroController::deviceAction(int id, QString action)
{
if(gdebug) qDebug() <<"CRASH - FibaroController deviceAction";
    QString url = "http://"+m_ip+":"+QString::number(m_port)+"/api/devices/"+QString::number(id)+"/action/"+action;
    sendCommand(url, "Post");
if(gdebug) qDebug() <<"CRASH - FibaroController deviceAction finished";
}

void FibaroController::deviceToggleIndex(int index)
{
if(gdebug) qDebug() <<"CRASH - FibaroController deviceToggleIndex";
    if(index != -1 && index < m_custom.length()){
  //TODO      QMutexLocker M(&l_devices);
        if(!m_devices[m_custom[index]].isNull()){
            QVariantMap device = m_devices[m_custom[index]].toMap();
            if(!device["type"].isNull()){
                if(device["type"].toString() == "com.fibaro.binarySwitch"){
                    if(!device["id"].isNull() && !device["properties"].isNull()){
                        int id = device["id"].toInt();
                        QString action = "turnOff";
                        //QVariantMap a = device["actions"].toMap();
                        QVariantMap p = device["properties"].toMap();
                        if(!p["value"].isNull())
                            if(p["value"].toString() == "false")
                                action = "turnOn";
                        QString url = "http://"+m_ip+":"+QString::number(m_port)+"/api/devices/"+QString::number(id)+"/action/"+action;
                        sendCommand(url, "Post");
                    }
                }
            }
        }
    }
if(gdebug) qDebug() <<"CRASH - FibaroController deviceToggleIndex finished";
}

bool FibaroController::sendCommand(QString url, QString Method, QString body)
{
if(gdebug) qDebug() <<"CRASH - FibaroController sendCommand";
    QNetworkAccessManager manager;
    QNetworkRequest request;
//qDebug() << "URL "<<url << m_basic;
    request.setUrl(QUrl(url));

    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("Authorization", m_basic.toUtf8());

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);

    QNetworkReply* reply = NULL;
    if(Method.toLower() == "post"){
        //qDebug() << "POST";
        reply = manager.post(request, body.toUtf8());
    }else if(Method.toLower() == "put")
        reply = manager.put(request, body.toUtf8());
    else
        reply = manager.get(request);

    QEventLoop eventloop;
    connect(reply,SIGNAL(finished()),&eventloop,SLOT(quit()));
    eventloop.exec();

    if (reply->error() == QNetworkReply::NoError)
    {
if(gdebug) qDebug() <<"CRASH - FibaroController sendCommand finished";
     //   qDebug() << "FAILED";
        return false;
    }else{
        QVariant statusCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
        int status = statusCode.toInt();
    //    qDebug() << "PASS " << reply->readAll() << status;
if(gdebug) qDebug() <<"CRASH - FibaroController sendCommand finished";
        return true;
    }
}

QString FibaroController::getIconURL(QString type, QString id, int index)
{
if(gdebug) qDebug() <<"CRASH - FibaroController getIconURL";
    if(type == "device"){
        QString file = "";
        if(index != -1 && index < m_custom.length()){
            QMutexLocker M(&l_devices);
            if(!m_devices[m_custom[index]].isNull()){
                QVariantMap device = m_devices[m_custom[index]].toMap();
                //qDebug() << device["type"].toString();
                if(!device["type"].isNull()){
                    if(device["type"].toString() == "com.fibaro.seismometer"){
                        if(device["value"].toString() == "0.00")
                            file = "0";
                        else
                            file = "100";
                    }else if(device["type"].toString() == "com.fibaro.binarySwitch" ||
                             device["type"].toString() == "com.fibaro.FGMS001v2" ||
                             device["type"].toString() == "com.fibaro.doorSensor" ||
                             device["type"].toString() == "com.fibaro.heatDetector"
                           ){
                        if(!device["properties"].isNull()){
                            QVariantMap p = device["properties"].toMap();
                            if(p["value"].toString() == "false")
                                file = "0";
                            else
                                file = "100";
                        }
                    }else if(device["type"].toString() == "com.fibaro.FGD212"){
                        if(!device["properties"].isNull()){
                            QVariantMap p = device["properties"].toMap();
                            if(!p["value"].isNull()){
                                int v = p["value"].toString().toInt();
                                v = (v / 10) * 10;
                                file = QString::number(v);
                            }
                        }
                    }
                }
            }
        }

        for(int x=0; x<m_icons.length(); x++){
            QVariantMap item = m_icons[x].toMap();
            if(item["id"].isNull()) continue;
            if(item["id"].toString() == id){
if(gdebug) qDebug() <<"CRASH - FibaroController getIconURL finished1";
                return "fibaro|http://192.168.178.10:"+QString::number(m_port)+"/fibaro/icons/"+item["iconSetName"].toString()+"/"+item["iconSetName"].toString()+file+".png|"+m_basic;
            }
        }
    }
if(gdebug) qDebug() <<"CRASH - FibaroController getIconURL finished";
    return "";
}

void FibaroController::getRoomDevices(int id)
{
if(gdebug) qDebug() <<"CRASH - FibaroController getRoomDevices";
    QMutexLocker M(&l_devices);
    m_custom.clear();
    m_cusIndex.clear();
    for(int x=0; x<m_devices.length(); x++){
        QVariantMap item = m_devices[x].toMap();
        if(item["roomID"].isNull()) continue;
        if(item["roomID"] == id){
            m_custom.push_back(x);
            QVariantMap d;
            d.insert("id",x);
            m_cusIndex.push_back(d);
        }
    }

    emit customListChanged();
if(gdebug) qDebug() <<"CRASH - FibaroController getRoomDevices finished";
}

void FibaroController::populateRefresh(QVariantList list)
{
if(gdebug) qDebug() <<"CRASH - FibaroController populateRefresh";
    if(list.length() < 1) return;

    QVariantMap item = list[0].toMap();

    if(item["last"].isNull()) return;
    m_lastTime = item["last"].toInt();

    bool roomDevice = false;
    //Update Devices
    if(item["events"].isNull()) return;
    QVariantList events = item["events"].toList();

    for(int x=0; x<events.length(); x++){
        QVariantMap event = events[x].toMap();

        if(event["type"].isNull()) continue;
        if(event["type"].toString() == "DevicePropertyUpdatedEvent"){
            if(event["id"].isNull()) continue;
            if(event["property"].isNull()) continue;
            int id = event["id"].toInt();
            //qDebug() << "ID"<<id;

            QString element = event["property"].toString();
            QMutexLocker M(&l_devices);
            if(mapDevices.find(id) != mapDevices.end()){
                int pos = mapDevices[id];
                QVariantMap d = m_devices[pos].toMap();
                if(!d["properties"].isNull() && !event["newValue"].isNull()){
                    QVariantMap p = d["properties"].toMap();

                    p[element] = event["newValue"];

                    d["properties"] = p;
                    m_devices[pos] = d;

                    if(!d["name"].isNull() && !p["value"].isNull()){
                        if(d["name"].toString().toLower() == "garage door sensor"){
                            if(p["value"].toString().toLower() == "true"){
                                emit alertChange("garagedoor", true);
                            }else{
                                emit alertChange("garagedoor", false);
                            }
                        }
                    }

                    roomDevice = true;
                }
            }
        }
    }

    if(roomDevice){
        QMutexLocker M(&l_devices);
        parseRoomDevices();
        emit roomListChanged();
    }

    emit deviceListChanged();
if(gdebug) qDebug() <<"CRASH - FibaroController populateRefresh finished";
}

void FibaroController::indexRooms()
{
if(gdebug) qDebug() <<"CRASH - FibaroController indexRooms";
    for(int x=0; x<m_rooms.length(); x++){
        QVariantMap item = m_rooms[x].toMap();

        if(item["id"].isNull()) continue;

        int id = item["id"].toInt();

        QVariantList devices;
        for(int x=0; x<m_devices.length(); x++){
            QVariantMap d = m_devices[x].toMap();
            if(!d["roomID"].isNull())
                if(d["roomID"] == id)
                    devices.push_back(d["id"].toInt());
        }

        item["devices"] = devices;
        m_rooms[x] = item;
    }
if(gdebug) qDebug() <<"CRASH - FibaroController indexRooms finished";
}

void FibaroController::parseRoomDevices()
{
if(gdebug) qDebug() <<"CRASH - FibaroController parseRoomDevices";
    for(int x=0; x<m_rooms.length(); x++){
        QVariantMap room = m_rooms[x].toMap();
        QVariantList devices = room["devices"].toList();
        bool deviceOn = false;
        QString temp;

        for(int y=0; y<devices.length(); y++){
            int dID = devices[y].toInt();

            if(mapDevices.find(dID) != mapDevices.end()){
                int pos = mapDevices[dID];
                if(pos < m_devices.length()){
                    QVariantMap d = m_devices[mapDevices[dID]].toMap();
                    QString type = d["type"].toString();
                    if(type == "com.fibaro.binarySwitch"){
                        QVariantMap p = d["properties"].toMap();
                        if(p["value"].toBool())
                            deviceOn = true;
                    }else if(type == "com.fibaro.temperatureSensor"){
                        QVariantMap p = d["properties"].toMap();
                        QString t = p["value"].toString();
                        if(t != "")
                            temp = t;
                    }
                }
            }
        }
        room["devicesOn"] = deviceOn;
        room["temp"] = temp;
        m_rooms[x] = room;
    }
if(gdebug) qDebug() <<"CRASH - FibaroController parseRoomDevices finished";
}

QVariantList FibaroController::getCustomList()
{
    return m_cusIndex;
}

QVariantList FibaroController::getRoomList()
{
    return m_rooms;
}

QVariantList FibaroController::getSectionList()
{
    return m_sections;
}

QVariantList FibaroController::getDeviceList()
{
if(gdebug) qDebug() <<"CRASH - FibaroController getDeviceList";
    QVariantList devices;
    if(l_devices.tryLock()){
        devices = m_devices;
        l_devices.unlock();
    }
if(gdebug) qDebug() <<"CRASH - FibaroController getDeviceList finished";
    return devices;
}

QVariantList FibaroController::getSceneList()
{
    return m_scenes;
}

QVariantList FibaroController::getIconList()
{
    return m_icons;
}

bool FibaroController::processVoiceCmd(QString cmd, QString &response)
{
if(gdebug) qDebug() <<"CRASH - FibaroController processVoiceCmd";
    //QStringList words = cmd.split(" ");
    if(cmd.contains("go to") && cmd.contains("fibaro")){
        emit pageTrigger("fibaro", 0);
        return true;
    }

    QVariantList devices;
    if(l_devices.tryLock()){
        devices = m_devices;
        l_devices.unlock();
    }

    if(cmd.contains("going away")){
        QString door;
        for(int x=0; x<devices.length(); x++){
            QVariantMap d = devices[x].toMap();
            if(!d["enabled"].toBool()) continue;
            if(!d["visible"].toBool()) continue;
            QString type = d["type"].toString();
            if(type == "com.fibaro.binarySwitch"){
                QString name = d["name"].toString().toLower();
                if(name.contains("light")){
                    if(!d["id"].isNull()){
                        int id = d["id"].toInt();

                        QVariantMap p = d["properties"].toMap();
                        if(p["dead"].toBool()) continue;
                        if(!p["isLight"].toBool()) continue;
                        if(!p["value"].toBool()) continue;

                        deviceAction(id, "turnOff");
                    }
                }
            }else if(type == "com.fibaro.doorSensor"){
                if(!d["id"].isNull()){
                    QVariantMap p = d["properties"].toMap();
                    if(p["dead"].toBool()) continue;
                    if(!p["value"].toBool()) continue;

                    QString name = d["name"].toString().toLower();
                    if(name.contains("garage")) continue;

                    name = name.replace(" sensor","");

                    if(door != "") door += ", ";
                    door += name;
                }
            }
        }

        if(door.length() > 0){
            response = door + " has been left opened.";

            return true;
        }

        return false;
    }

    if(cmd.contains("home lockdown") || cmd.contains("house lockdown") || cmd.contains("home in lockdown") || cmd.contains("house in lockdown")){
        QString door, action;
        for(int x=0; x<devices.length(); x++){
            QVariantMap d = devices[x].toMap();
            if(!d["enabled"].toBool()) continue;
            if(!d["visible"].toBool()) continue;
            QString type = d["type"].toString();
            if(type == "com.fibaro.binarySwitch"){
                QString name = d["name"].toString().toLower();
                if(name.contains("light")){
                    if(!d["id"].isNull()){
                        int id = d["id"].toInt();

                        QVariantMap p = d["properties"].toMap();
                        if(p["dead"].toBool()) continue;
                        if(!p["isLight"].toBool()) continue;
                        if(!p["value"].toBool()) continue;

                        deviceAction(id, "turnOff");
                    }
                }
            }else if(type == "com.fibaro.doorSensor"){
                if(!d["id"].isNull()){
                    QVariantMap p = d["properties"].toMap();
                    if(p["dead"].toBool()) continue;
                    if(!p["value"].toBool()) continue;

                    QString name = d["name"].toString().toLower();
                    name = name.replace(" sensor","");

                    if(name.contains("garage door")){
                        int id = d["id"].toInt();
                        deviceAction(id, "turnOn");

                        action = ", now closing garage door";
                    }

                    if(door != "") door += ", ";
                    door += name;
                }
            }
        }

        if(door.length() > 0){
            response = door + " has been left opened"+action;

            return true;
        }
        return false;
    }

    if(cmd.contains("good night")){
        QString door;
        for(int x=0; x<devices.length(); x++){
            QVariantMap d = devices[x].toMap();
            if(!d["enabled"].toBool()) continue;
            if(!d["visible"].toBool()) continue;
            QString type = d["type"].toString();
            if(type == "com.fibaro.binarySwitch"){
                QString name = d["name"].toString().toLower();
                if(name.contains("light")){
                    if(!d["id"].isNull()){
                        int id = d["id"].toInt();

                        QVariantMap p = d["properties"].toMap();
                        if(p["dead"].toBool()) continue;
                        if(!p["isLight"].toBool()) continue;
                        if(!p["value"].toBool()) continue;

                        deviceAction(id, "turnOff");
                    }
                }
            }else if(type == "com.fibaro.doorSensor"){
                if(!d["id"].isNull()){
                    int id = d["id"].toInt();

                    QVariantMap p = d["properties"].toMap();
                    if(p["dead"].toBool()) continue;
                    if(!p["value"].toBool()) continue;

                    QString name = d["name"].toString().toLower();
                    name = name.replace(" sensor","");

                    if(door != "") door += ", ";
                    door += name;
                }
            }
        }

        if(door.length() > 0){
            response = door + " has been left opened.";

            return true;
        }

        return false;
    }

    if(cmd.contains("switch off all lights")){
        for(int x=0; x<devices.length(); x++){
            QVariantMap d = devices[x].toMap();
            if(!d["enabled"].toBool()) continue;
            if(!d["visible"].toBool()) continue;
            QString name = d["name"].toString().toLower();
            if(name.contains("light")){
                if(!d["id"].isNull()){
                    int id = d["id"].toInt();
                    QString type = d["type"].toString();
                    if(type == "com.fibaro.binarySwitch"){
                        QVariantMap p = d["properties"].toMap();
                        if(p["dead"].toBool()) continue;
                        if(!p["isLight"].toBool()) continue;
                        if(!p["value"].toBool()) continue;

                        deviceAction(id, "turnOff");
                    }
                }
            }
        }

        return false;
    }


    for(int x=0; x<devices.length(); x++){
        QVariantMap d = devices[x].toMap();
        QString name = d["name"].toString().toLower();
        if(cmd.contains(name)){
            if(!d["id"].isNull()){
                int id = d["id"].toInt();
                QString action;
                if(name.contains("garage")){
                    bool garageOpen = false;
                    for(int y=0; y<devices.length(); y++){
                        QVariantMap d2 = devices[y].toMap();
                        if(!d2["name"].isNull()){
                            QString name = d2["name"].toString().toLower();
                            if(name == "garage door sensor"){
                                QVariantMap property = d2["properties"].toMap();
                                if(!property["value"].isNull())
                                    if(property["value"].toString().toLower() == "true")
                                        garageOpen = true;
                            }
                        }
                    }
                    //garage door sensor //arm //47  //value = false
                    if(cmd.contains("close") && garageOpen)
                        action = "turnOn";
                }else{
                    if(cmd.contains("on"))
                        action = "turnOn";
                    else
                        action = "turnOff";
                }
                if(action != "")
                    deviceAction(id, action);
if(gdebug) qDebug() <<"CRASH - FibaroController processVoiceCmd finished 2";
                return true;
            }
        }
    }
if(gdebug) qDebug() <<"CRASH - FibaroController processVoiceCmd finished 1";
    return false;
}

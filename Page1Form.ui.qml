import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 800
    height: 480

    background: Rectangle {
        color: "transparent"
    }

    property alias btKodi: btKodi

    Grid {
        id: grid
        x: 25
        y: 29
        width: 738
        height: 416
        spacing: 20
        transformOrigin: Item.TopLeft
        rows: 3
        columns: 6

        Button {
            id: btFrame
            height: 100
            width: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            display: AbstractButton.IconOnly
            focusPolicy: Qt.NoFocus
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btFrameImage
                anchors.fill: parent
                source: "files/icon/frame.png"
            }
        }

        Connections {
            target: btFrame
            onClicked: {
                window.switchToPhoto(true)
            }
        }

        Button {
            id: btKodi
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            visible: controller.kodiEnabled
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btKodiImage
                width: 100
                height: 100
                source: "files/icon/kodi.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btKodi
            onClicked: {
                window.switchToKodi(true)
 //               controller.testMusic()
            }
        }

        Button {
            id: btWeather
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btWeatherImage
                width: 100
                height: 100
                source: "files/icon/weather.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btWeather
            onClicked: {
                window.switchToWeather(true)
            }
        }

        Button {
            id: btMusic
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btMusicImage
                width: 100
                height: 100
                source: "files/icon/music.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btMusic
            onClicked: {
                window.switchToMusic(true)
            }
        }

        Button {
            id: btRecipe
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            visible: controller.recipeEnabled
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btRecipeImage
                width: 100
                height: 100
                source: "files/icon/recipe.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btRecipe
            onClicked: {
                window.switchToRecipe(true)
            }
        }

        Button {
            id: btFibaro
            width: 100
            height: 100
            padding: 10
            spacing: 10
            visible: controller.fibaroEnabled
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btFibaroImage
                width: 100
                height: 100
                source: "files/icon/fibaro.png"
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
            }
        }

        Connections {
            target: btFibaro
            onClicked: {
                window.switchToFibaro(true)
            }
        }

        Button {
            id: btTrains
            width: 100
            height: 100
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btTrainsImage
                width: 100
                height: 100
                source: "files/icon/trains.png"
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
            }
        }

        Connections {
            target: btTrains
            onClicked: {
                controller.nextDefaultTrains();
                window.switchToTrains(true)
            }
        }

        Button {
            id: btBrowser
            width: 100
            height: 100
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btBrowserImage
                anchors.fill: parent
                source: "files/icon/browser.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Button {
            id: btVideo
            width: 100
            height: 100
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btVideoImage
                anchors.fill: parent
                source: "files/icon/video.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btVideo
            onClicked: {
                window.switchToVideo(true)
            }
        }

        Button {
            id: btSleep
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btSleepImage
                width: 100
                height: 100
                source: "files/icon/sleep.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btSleep
            onClicked: {
                controller.swittchAwayMode()
                //window.switchToSleep(true)
            }
        }

        Button {
            id: btSetting
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btSettingImage
                width: 100
                height: 100
                source: "files/icon/settings.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btSetting
            onClicked: {
                window.switchToSettings()
            }
        }
    }
}

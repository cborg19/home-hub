#include "ktimezoned.h"
#include <QDir>
#include <QTextStream>
#include <QStringList>
#include <QDateTime>
#include <QDataStream>

ktimezoned::ktimezoned()
{
    populate();
}

bool ktimezoned::populate()
{
    const QString ZONE_TAB_FILE = QLatin1String("/zone.tab");
    const QString ZONE_INFO_DIR = QLatin1String("/usr/share/zoneinfo");

    // Find and open zone.tab - it's all easy except knowing where to look.
    // Try the LSB location first.
    QDir dir;
    QString zoneinfoDir = ZONE_INFO_DIR;
    // make a note if the dir exists; whether it contains zone.tab or not
    if (!dir.exists(zoneinfoDir))
    {
        return false;
    }

    QFile f;
    if(!QFile::exists(ZONE_INFO_DIR + ZONE_TAB_FILE)){
        return false;
    }

    f.setFileName(zoneinfoDir + ZONE_TAB_FILE);
    if(!f.open(QIODevice::ReadOnly)) return false;

    // Parse the already open real or fake zone.tab.
    QRegExp lineSeparator("[ \t]");

    //mZones.clear();
    QTextStream str(&f);
    while (!str.atEnd())
    {
        QString line = str.readLine();
        if (line.isEmpty() || line[0] == '#')
            continue;
        QStringList tokens = perlSplit(lineSeparator, line, 4);
        int n = tokens.count();
        if (n < 3)
        {
            continue;
        }

        // Add entry to list.
        if (tokens[0] == "??")
            tokens[0] = "";
        //else if (!tokens[0].isEmpty())
        //    mHaveCountryCodes = true;
        ZONE zone;
        zone.Code = tokens[0];

        Region.insert(tokens[2],zone);
    }
    f.close();
}

QStringList ktimezoned::perlSplit(const QRegExp & sep, const QString & s, int max)
{
    bool ignoreMax = 0 == max;

    QStringList l;

    int searchStart = 0;
    int tokenStart = sep.indexIn(s, searchStart);
    int len = sep.matchedLength();

    while (-1 != tokenStart && (ignoreMax || l.count() < max - 1))
    {
        if (!s.mid(searchStart, tokenStart - searchStart).isEmpty())
          l << s.mid(searchStart, tokenStart - searchStart);

        searchStart = tokenStart + len;
        tokenStart = sep.indexIn(s, searchStart);
        len = sep.matchedLength();
    }

    if (!s.mid(searchStart, s.length() - searchStart).isEmpty())
        l << s.mid(searchStart, s.length() - searchStart);

    return l;
}

//------------------------------------------------------------------------------------
ktimezone::ktimezone(const QString name, bool All) :
        zone(name)
{
    populate(All);
}

bool ktimezone::populate(bool All)
{
    quint32 abbrCharCount;     // the number of characters of time zone abbreviation strings
    quint32 ttisgmtcnt;
    quint8  is;
    quint8  T_, Z_, i_, f_;    // tzfile identifier prefix

    const QString ZONE_INFO_DIR = QLatin1String("/usr/share/zoneinfo");
    QFile f(ZONE_INFO_DIR + '/' + zone);
    if (!f.open(QIODevice::ReadOnly))
    {
        return false;
    }
    QDataStream str(&f);

    // Read the file type identifier
    str >> T_ >> Z_ >> i_ >> f_;
    if (T_ != 'T' || Z_ != 'Z' || i_ != 'i' || f_ != 'f')
    {
        return false;
    }
    // Discard 16 bytes reserved for future use
    unsigned i;
    for (i = 0; i < 4; i++)
        str >> ttisgmtcnt;

    // Read the sizes of arrays held in the file
    quint32 nTransitionTimes;
    quint32 nLocalTimeTypes;
    quint32 nLeapSecondAdjusts;
    quint32 nIsStandard;
    quint32 nIsUtc;
    str >> nIsUtc
        >> nIsStandard
        >> nLeapSecondAdjusts
        >> nTransitionTimes
        >> nLocalTimeTypes
        >> abbrCharCount;
    // Read the transition times, at which the rules for computing local time change
    struct TransitionTime
    {
        qint32 time;            // time (as returned by time(2)) at which the rules for computing local time change
        quint8 localTimeIndex;  // index into the LocalTimeType array
    };

    TransitionTime *transitionTimes = new TransitionTime[nTransitionTimes];
    for (i = 0;  i < nTransitionTimes;  ++i)
    {
        str >> transitionTimes[i].time;
    }
    for (i = 0;  i < nTransitionTimes;  ++i)
    {
        str >> transitionTimes[i].localTimeIndex;
    }

    // Read the local time types
    struct LocalTimeType
    {
        qint32 gmtoff;     // number of seconds to be added to UTC
        bool   isdst;      // whether tm_isdst should be set by localtime(3)
        quint8 abbrIndex;  // index into the list of time zone abbreviations
        bool   isutc;      // transition times are in UTC. If UTC, isstd is ignored.
        bool   isstd;      // if true, transition times are in standard time;
                           // if false, transition times are in wall clock time,
                           // i.e. standard time or daylight savings time
                           // whichever is current before the transition
    };
    LocalTimeType *localTimeTypes = new LocalTimeType[nLocalTimeTypes];
    LocalTimeType *ltt = localTimeTypes;
    for (i = 0;  i < nLocalTimeTypes;  ++ltt, ++i)
    {
        str >> ltt->gmtoff;
        str >> is;
        ltt->isdst = (is != 0);
        str >> ltt->abbrIndex;
        ltt->isstd = false;   // default if no data
        ltt->isutc = false;   // default if no data
    }

    // Read the timezone abbreviations. They are stored as null terminated strings in
    // a character array.
    // Make sure we don't fall foul of maliciously coded time zone abbreviations.
    if (abbrCharCount > 64)
    {
        //kError() << "excessive length for timezone abbreviations: " << abbrCharCount << endl;
        //delete data;
        delete[] transitionTimes;
        delete[] localTimeTypes;
        return false;
    }
    QByteArray array(abbrCharCount, 0);
    str.readRawData(array.data(), array.size());
    char *abbrs = array.data();
    if (abbrs[abbrCharCount - 1] != 0)
    {
        // These abbreviations are corrupt!
        //kError() << "timezone abbreviations not null terminated: " << abbrs[abbrCharCount - 1] << endl;
        //delete data;
        delete[] transitionTimes;
        delete[] localTimeTypes;
        return false;
    }
    quint8 n = 0;
    QList<QByteArray> abbreviations;
    for (i = 0;  i < abbrCharCount;  ++n, i += strlen(abbrs + i) + 1)
    {
        abbreviations += QByteArray(abbrs + i);
        // Convert the LocalTimeTypes pointer to a sequential index
        ltt = localTimeTypes;
        for (unsigned j = 0;  j < nLocalTimeTypes;  ++ltt, ++j)
        {
            if (ltt->abbrIndex == i)
                ltt->abbrIndex = n;
        }
    }

    // Read the leap second adjustments
    qint32  t;
    quint32 s;
    //QList<KTimeZone::LeapSeconds> leapChanges;
    for (i = 0;  i < nLeapSecondAdjusts;  ++i)
    {
        str >> t >> s;
        // kDebug() << "leap entry: " << t << ", " << s << endl;
        // Don't use QDateTime::setTime_t() because it takes an unsigned argument
//        localTimeTypes[i].s = static_cast<int>(s);
//        localTimeTypes[i].t = t;
    }

    // Read the standard/wall time indicators.
    // These are true if the transition times associated with local time types
    // are specified as standard time, false if wall clock time.
    for (i = 0;  i < nIsStandard;  ++i)
    {
        str >> is;
        localTimeTypes[i].isstd = (is != 0);
        // kDebug() << "standard: " << is << endl;
    }

    // Read the UTC/local time indicators.
    // These are true if the transition times associated with local time types
    // are specified as UTC, false if local time.
    for (i = 0;  i < nIsUtc;  ++i)
    {
        str >> is;
        localTimeTypes[i].isutc = (is != 0);
        // kDebug() << "UTC: " << is << endl;
    }


    // Find the starting offset from UTC to use before the first transition time.
    // This is first non-daylight savings local time type, or if there is none,
    // the first local time type.
    int firstoffset = (nLocalTimeTypes > 0) ? localTimeTypes[0].gmtoff : 0;
    ltt = localTimeTypes;
    for (i = 0;  i < nLocalTimeTypes;  ++ltt, ++i)
    {
        if (!ltt->isdst)
        {
            firstoffset = ltt->gmtoff;
            break;
        }
    }

    TransitionTime *tt = transitionTimes;
    TIMECHANGE time;
    QDate date = QDate::currentDate();
    for (i = 0;  i < nTransitionTimes;  ++tt, ++i)
    {
        if (tt->localTimeIndex >= nLocalTimeTypes)
        {
            continue;
        }

        // Convert local transition times to UTC
        ltt = &localTimeTypes[tt->localTimeIndex];

        time.DateTime = fromTime_t(tt->time);
        time.DateTime = time.DateTime.addSecs(ltt->gmtoff);
        if(!All)
            if(time.DateTime.date() < date) continue;
        time.DayLightSavings = ltt->isdst;
        time.GMT = ltt->gmtoff;

//        if(IsSandBox())
//            qDebug() << "Num " << i<< ": " << time.DateTime.toString("h:mm dd/MM/yyyy");

        Dates.append(time);
    }

    delete[] localTimeTypes;
    delete[] transitionTimes;

    return true;
}

QDateTime ktimezone::fromTime_t(time_t t)
{
    static QDate epochDate(1970,1,1);
    static QTime epochTime(0,0,0);
    int days = t / 86400;
    int secs;
    if (t >= 0)
        secs = t % 86400;
    else
    {
        secs = 86400 - (-t % 86400);
        --days;
    }
    return QDateTime(epochDate.addDays(days), epochTime.addSecs(secs), Qt::UTC);
}

void ktimezone::GetNextChange(bool &daylight, QDateTime &datetime)
{
    QDate date = QDate::currentDate();
    for(int x=0; x<Dates.count(); x++){
        TIMECHANGE time = Dates.at(x);
        if(time.DateTime.date() < date) continue;

        daylight = time.DayLightSavings;
        datetime = time.DateTime;
        break;
    }
}

//----------------------------------------------------------------------------
/*void WordTime::GetList(QMap<QString, QString> &List)
{
    QString Config = CONFIG_PATH;
    QString filename = "weather.data";

    QFile file( Config + filename );
    if( file.exists()){
        if ( file.open(QIODevice::ReadOnly) ) {
        }else return;
    }

    while (!file.atEnd()) {
        QString line = file.readLine();
        QStringList list = line.split("|");
        if(list.count() < 3) continue;

        List.insert(list.at(2).trimmed(),list.at(0).trimmed());
    }
}*/

//----------------------------------------------------------------------------
CityTime::CityTime(const QString p):
        Place(p), daylight(false)
{

}

QDateTime CityTime::CurrentTime()
{
    ktimezone zone(Place, true);
    QList<ktimezone::TIMECHANGE> list = zone.GetTimes();

    QDateTime utctime = QDateTime::currentDateTime();
    utctime = utctime.toUTC();

    int addExtra = 0;
    for(int x=0; x<list.count(); x++){
        ktimezone::TIMECHANGE ctime = list.at(x);

        if(ctime.DateTime.date() < utctime.date()){
            addExtra = ctime.GMT;
            daylight = ctime.DayLightSavings;
            continue;
        }
        break;
    }

    utctime = utctime.addSecs(addExtra);

    return utctime;
}

bool CityTime::DayLightSavings()
{
    return daylight;
}

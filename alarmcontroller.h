#ifndef ALARMCONTROLLER_H
#define ALARMCONTROLLER_H

#include <QObject>
#include <QDateTime>
#include <QMutex>
#include <QVariant>

/*struct alarm {
    bool enabled;
    QString messsage;
    QDateTime startTime;
    QDateTime endTime;
    QString repeat;
    //QList<QVariant> source;
    alarm(){
        enabled = false;
    }
};*/

class AlarmController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantMap currentAlarm READ getCurrentAlarm NOTIFY currentAlarmChanged)
    Q_PROPERTY(QVariantList alarmList READ getAlarmList NOTIFY alarmListChanged)
public:
    explicit AlarmController(QObject *parent = nullptr);

    QVariantMap getCurrentAlarm();
    QVariantList getAlarmList();

    void setAlarm(int index, QString title, int timeHr, int timeMin, QString date, QString day, QString type, QString sound, bool snooze, bool enabled = true);
    void deleteAlarm(int index);
    void setAlarmEnable(int index, bool results);

    void dismissAlarm();

    bool processVoiceCmd(QString cmd, QString &response);
    void checkAlarms();
signals:
    void currentAlarmChanged();
    void alarmListChanged();
    void triggerSound(QString);

    void pageTrigger(QString page, int index);
public slots:
    void triggerAlarm(QVariantMap);

private:
    QVariantMap m_current, m_next;
    QVariantList m_alarms;
    QMutex l_alarms;
};

#endif // ALARMCONTROLLER_H

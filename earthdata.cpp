#include "earthdata.h"
#include <math.h>
#include "ktimezoned.h"

//--------------------------------------------------------
const double pi = 3.14159;

double MoonRise()
{
    return sin(+8.0 / 60.0);
}

// Convert degree angle to radians
double degToRad(double angleDeg)
{
        return (pi * angleDeg / 180.0);
}
// Convert radian angle to degrees
double radToDeg(double angleRad)
{
    return (180.0 * angleRad / pi);
}

double julday(int Day, int Month, int Year)
{
    double year = static_cast<double>(Year);
    double mon = static_cast<double>(Month) + 1;

    if (Month <= 2) {
        year--;
        mon += 12;
    }

    long temp = static_cast<long>(Day) + 31*(static_cast<long>(Month) +12*static_cast<long>(Year));

    double jul = floor(365.25 *year) +
                 floor(30.6001 * mon) + static_cast<double>(Day) + 1720995;

    if ( temp >= (15+31*(10+12*1582))) {
            double ja = floor(0.01 * year);
            jul = jul + 2 - ja + floor(0.25 * ja);
    }
    return jul;
}

double calcJD(int year, int month, int day)
{
    if (month <= 2) {
            year -= 1;
            month += 12;
    }
    int A = year/100;
    int B = 2 - A + (A/4);

    double JD = floor(365.25*(year + 4716)) + floor(30.6001*(month+1)) + day + B - 1524.5;
    return JD;
}

unsigned char MoonPhase(int Day, int Month, int Year)
{
    double n = floor(12.37 * (static_cast<double>(Year) -1900 + ((1.0 * static_cast<double>(Month) - 0.5)/12.0)));
    double Rad = 3.14159265/180.0;
    double t = n / 1236.85;
    double t2 = t * t;
    double as = 359.2242 + 29.105356 * n;
    double am = 306.0253 + 385.816918 * n + 0.010730 * t2;
    double xtra = 0.75933 + 1.53058868 * n + ((1.178e-4) - (1.55e-7) * t) * t2;
    xtra += (0.1734 - 3.93e-4 * t) *
            sin(Rad * as) - 0.4068 * sin(Rad * am);
    double i = (xtra > 0.0 ? floor(xtra) :  ceil(xtra - 1.0));

    long j1 = static_cast<long>(julday(Day,Month,Year));
    long jd = (2415020 + 28 * static_cast<long>(n)) + static_cast<long>(i);
    i = j1-jd + 30;

    return static_cast<unsigned char>(i)%30;
}


//***********************************************************************/
//* Name:    calcTimeJulianCent							*/
//* Type:    Function									*/
//* Purpose: convert Julian Day to centuries since J2000.0.			*/
//* Arguments:										*/
//*   jd : the Julian Day to convert						*/
//* Return value:										*/
//*   the T value corresponding to the Julian Day				*/
//***********************************************************************/
double calcTimeJulianCent(double jd)
{
    double T = (jd - 2451545.0)/36525.0;
    return T;
}

//***********************************************************************/
//* Name:    calcJDFromJulianCent							*/
//* Type:    Function									*/
//* Purpose: convert centuries since J2000.0 to Julian Day.			*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   the Julian Day corresponding to the t value				*/
//***********************************************************************/
double calcJDFromJulianCent(double t)
{
    double JD = t * 36525.0 + 2451545.0;
    return JD;
}

//***********************************************************************/
//* Name:    calcSunRtAscension							*/
//* Type:    Function									*/
//* Purpose: calculate the right ascension of the sun				*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   sun's right ascension in degrees						*/
//***********************************************************************/
/*double calcSunRtAscension(double t)
{
    double e = calcObliquityCorrection(t);
    double lambda = calcSunApparentLong(t);

    double tananum = (cos(degToRad(e)) * sin(degToRad(lambda)));
    double tanadenom = (cos(degToRad(lambda)));
    double alpha = radToDeg(atan2(tananum, tanadenom));
    return alpha;		// in degrees
}

//***********************************************************************/
//* Name:    calcMeanObliquityOfEcliptic						*/
//* Type:    Function									*/
//* Purpose: calculate the mean obliquity of the ecliptic			*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   mean obliquity in degrees							*/
//***********************************************************************/
double calcMeanObliquityOfEcliptic(double t)
{
    double seconds = 21.448 - t*(46.8150 + t*(0.00059 - t*(0.001813)));
    double e0 = 23.0 + (26.0 + (seconds/60.0))/60.0;
    return e0;		// in degrees
}

//***********************************************************************/
//* Name:    calcObliquityCorrection						*/
//* Type:    Function									*/
//* Purpose: calculate the corrected obliquity of the ecliptic		*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   corrected obliquity in degrees						*/
//***********************************************************************/
double calcObliquityCorrection(double t)
{
    double e0 = calcMeanObliquityOfEcliptic(t);

    double omega = 125.04 - 1934.136 * t;
    double e = e0 + 0.00256 * cos(degToRad(omega));
    return e;		// in degrees
}

//***********************************************************************/
//* Name:    calGeomMeanLongSun							*/
//* Type:    Function									*/
//* Purpose: calculate the Geometric Mean Longitude of the Sun		*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   the Geometric Mean Longitude of the Sun in degrees			*/
//***********************************************************************/
double calcGeomMeanLongSun(double t)
{
    double L = 280.46646 + t * (36000.76983 + 0.0003032 * t);
    while( static_cast<int>(L) >  360 )
    {
        L -= 360.0;
    }
    while(  L <  0)
    {
        L += 360.0;
    }
    return L;              // in degrees
}

//***********************************************************************/
//* Name:    calGeomAnomalySun							*/
//* Type:    Function									*/
//* Purpose: calculate the Geometric Mean Anomaly of the Sun		*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   the Geometric Mean Anomaly of the Sun in degrees			*/
//***********************************************************************/
double calcGeomMeanAnomalySun(double t)
{
    double M = 357.52911 + t * (35999.05029 - 0.0001537 * t);
    return M;		// in degrees
}

//***********************************************************************/
//* Name:    calcSunEqOfCenter							*/
//* Type:    Function									*/
//* Purpose: calculate the equation of center for the sun			*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   in degrees										*/
//***********************************************************************/
double calcSunEqOfCenter(double t)
{
    double m = calcGeomMeanAnomalySun(t);

    double mrad = degToRad(m);
    double sinm = sin(mrad);
    double sin2m = sin(mrad+mrad);
    double sin3m = sin(mrad+mrad+mrad);

    double C = sinm * (1.914602 - t * (0.004817 + 0.000014 * t)) + sin2m * (0.019993 - 0.000101 * t) + sin3m * 0.000289;
    return C;		// in degrees
}

//***********************************************************************/
//* Name:    calcSunTrueLong								*/
//* Type:    Function									*/
//* Purpose: calculate the true longitude of the sun				*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   sun's true longitude in degrees						*/
//***********************************************************************/
double calcSunTrueLong(double t)
{
    double a = calcGeomMeanLongSun(t);
    double c = calcSunEqOfCenter(t);

    double O = a + c;
    return O;		// in degrees
}

//***********************************************************************/
//* Name:    calcSunApparentLong							*/
//* Type:    Function									*/
//* Purpose: calculate the apparent longitude of the sun			*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   sun's apparent longitude in degrees						*/
//***********************************************************************/
double calcSunApparentLong(double t)
{
    double o = calcSunTrueLong(t);

    double omega = 125.04 - 1934.136 * t;
    double lambda = o - 0.00569 - 0.00478 * sin(degToRad(omega));
    return lambda;		// in degrees
}

//***********************************************************************/
//* Name:    calcSunDeclination							*/
//* Type:    Function									*/
//* Purpose: calculate the declination of the sun				*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   sun's declination in degrees							*/
//***********************************************************************/
double calcSunDeclination(double t)
{
    double e = calcObliquityCorrection(t);
    double lambda = calcSunApparentLong(t);

    double sint = sin(degToRad(e)) * sin(degToRad(lambda));
    double theta = radToDeg(asin(sint));
    return theta;           // in degrees
}

//***********************************************************************/
//* Name:    calcEccentricityEarthOrbit						*/
//* Type:    Function									*/
//* Purpose: calculate the eccentricity of earth's orbit			*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   the unitless eccentricity							*/
//***********************************************************************/
double calcEccentricityEarthOrbit(double t)
{
    double e = 0.016708634 - t * (0.000042037 + 0.0000001267 * t);
    return e;		// unitless
}

//***********************************************************************/
//* Name:    calcEquationOfTime							*/
//* Type:    Function									*/
//* Purpose: calculate the difference between true solar time and mean	*/
//*		solar time									*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//* Return value:										*/
//*   equation of time in minutes of time						*/
//***********************************************************************/
double calcEquationOfTime(double t)
{
    double epsilon = calcObliquityCorrection(t);
    double l0 = calcGeomMeanLongSun(t);
    double e = calcEccentricityEarthOrbit(t);
    double m = calcGeomMeanAnomalySun(t);

    double y = tan(degToRad(epsilon)/2.0);
    y *= y;

    double sin2l0 = sin(2.0 * degToRad(l0));
    double sinm   = sin(degToRad(m));
    double cos2l0 = cos(2.0 * degToRad(l0));
    double sin4l0 = sin(4.0 * degToRad(l0));
    double sin2m  = sin(2.0 * degToRad(m));

    double Etime = y * sin2l0 - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2l0
                    - 0.5 * y * y * sin4l0 - 1.25 * e * e * sin2m;

    return radToDeg(Etime)*4.0;	// in minutes of time
}

//***********************************************************************/
//* Name:    calcSolNoonUTC								*/
//* Type:    Function									*/
//* Purpose: calculate the Universal Coordinated Time (UTC) of solar	*/
//*		noon for the given day at the given location on earth		*/
//* Arguments:										*/
//*   t : number of Julian centuries since J2000.0				*/
//*   longitude : longitude of observer in degrees				*/
//* Return value:										*/
//*   time in minutes from zero Z							*/
//***********************************************************************/
double calcSolNoonUTC(double t, double longitude)
{
    // First pass uses approximate solar noon to calculate eqtime
    double tnoon = calcTimeJulianCent(calcJDFromJulianCent(t) + longitude/360.0);
    double eqTime = calcEquationOfTime(tnoon);
    double solNoonUTC = 720 + (longitude * 4) - eqTime; // min

    double newt = calcTimeJulianCent(calcJDFromJulianCent(t) -0.5 + solNoonUTC/1440.0);

    eqTime = calcEquationOfTime(newt);
    // var solarNoonDec = calcSunDeclination(newt);
    solNoonUTC = 720 + (longitude * 4) - eqTime; // min

    return solNoonUTC;
}

//***********************************************************************/
//* Name:    calcHourAngleSunrise							*/
//* Type:    Function									*/
//* Purpose: calculate the hour angle of the sun at sunrise for the	*/
//*			latitude								*/
//* Arguments:										*/
//*   lat : latitude of observer in degrees					*/
//*	solarDec : declination angle of sun in degrees				*/
//* Return value:										*/
//*   hour angle of sunrise in radians						*/
//***********************************************************************/
double calcHourAngleSunrise(double lat, double solarDec)
{
    double latRad = degToRad(lat);
    double sdRad  = degToRad(solarDec);

    double HA = (acos(cos(degToRad(90.833))/(cos(latRad)*cos(sdRad))-tan(latRad) * tan(sdRad)));

    return HA;		// in radians
}

//***********************************************************************/
//* Name:    calcSunriseUTC								*/
//* Type:    Function									*/
//* Purpose: calculate the Universal Coordinated Time (UTC) of sunrise	*/
//*			for the given day at the given location on earth	*/
//* Arguments:										*/
//*   JD  : julian day									*/
//*   latitude : latitude of observer in degrees				*/
//*   longitude : longitude of observer in degrees				*/
//* Return value:										*/
//*   time in minutes from zero Z							*/
//***********************************************************************/
double calcSunriseUTC(double JD, double latitude, double longitude)
{
    double t = calcTimeJulianCent(JD);
    // *** First pass to approximate sunset
    double  eqTime = calcEquationOfTime(t);
    double  solarDec = calcSunDeclination(t);
    double  hourAngle = calcHourAngleSunrise(latitude, solarDec);
    double  delta = longitude - radToDeg(hourAngle);
    double  timeDiff = 4 * delta;	// in minutes of time
    double  timeUTC = 720 + timeDiff - eqTime;	// in minutes
    double  newt = calcTimeJulianCent(calcJDFromJulianCent(t) + timeUTC/1440.0);
    eqTime = calcEquationOfTime(newt);
    solarDec = calcSunDeclination(newt);


    hourAngle = calcHourAngleSunrise(latitude, solarDec);
    delta = longitude - radToDeg(hourAngle);
    timeDiff = 4 * delta;
    timeUTC = 720 + timeDiff - eqTime; // in minutes

    return timeUTC;
}

double calcHourAngleSunset(double lat, double solarDec)
{
    double latRad = degToRad(lat);
    double sdRad  = degToRad(solarDec);


    double HA = (acos(cos(degToRad(90.833))/(cos(latRad)*cos(sdRad))-tan(latRad) * tan(sdRad)));

    return -HA;              // in radians
}

double calcSunsetUTC(double JD, double latitude, double longitude)
{
    double t = calcTimeJulianCent(JD);
    // *** First pass to approximate sunset
    double  eqTime = calcEquationOfTime(t);
    double  solarDec = calcSunDeclination(t);
    double  hourAngle = calcHourAngleSunset(latitude, solarDec);
    double  delta = longitude - radToDeg(hourAngle);
    double  timeDiff = 4 * delta;	// in minutes of time
    double  timeUTC = 720 + timeDiff - eqTime;	// in minutes
    double  newt = calcTimeJulianCent(calcJDFromJulianCent(t) + timeUTC/1440.0);

    eqTime = calcEquationOfTime(newt);
    solarDec = calcSunDeclination(newt);

    hourAngle = calcHourAngleSunset(latitude, solarDec);
    delta = longitude - radToDeg(hourAngle);
    timeDiff = 4 * delta;
    timeUTC = 720 + timeDiff - eqTime; // in minutes
    return timeUTC;
}

QTime GetTimeFromDouble(double t)
{
    double floatHour = t / 60.0;
    int hour = static_cast<int>(floor(floatHour));
    double floatMinute = 60.0 * (floatHour - floor(floatHour));
    int minute = static_cast<int>(floor(floatMinute));
    double floatSec = 60.0 * (floatMinute - floor(floatMinute));
    int second = static_cast<int>(floor(floatSec + 0.5));

    return QTime(hour, minute, second, 0);
}

QString MoonTitle(int day)
{
    if(day == 29 || day == 0 || day == 1)
        return "New Moon";
    else if(2 <= day && day <= 5)
        return "Waxing Crescent";
    else if(6 <= day && day <= 9)
        return "First Quarter";
    else if(10 <= day && day <= 12)
        return "Waxing Gibbous";
    else if(13 <= day && day <= 16)
        return "Full Moon";
    else if(17 <= day && day <= 20)
        return "Waning Gibbous";
    else if(21 <= day && day <= 24)
        return "Last Quarter";

    return "Waning Crescent";
}

EarthData::EarthData():
    latitude(-33.8667), longitude(-151.2167)
{

}

EarthData::EarthData(double lat, double log, double g):
    latitude(lat), longitude(log), gmt(g)
{

}

void EarthData::setLocation(double lat, double log, double g)
{
    latitude = lat;
    longitude = log;
    gmt = g;
}

QTime EarthData::getSunRise()
{
    QDateTime Now = QDateTime::currentDateTime();
    QDate Today = Now.date();

    //It is used to access DST and other timezone related information
    //and sets all timezone global variables
    ktimezone zone("Australia/Sydney");
    bool res = false;
    QDateTime datetime;
    zone.GetNextChange(res, datetime);

    unsigned char daySavings = 0;
    if(!res){
        daySavings = 60;
    }

    /**** Today ****/
    // Calculate the time of sunrise
    double JD = calcJD(Today.year(),
                       Today.month(),
                       Today.day());

    // Calculate sunrise for this date
    // if no sunrise is found, set flag nosunrise
    double riseTimeGMT = calcSunriseUTC(JD, latitude, longitude);

    // Sunrise was found
    double riseTimeLST = riseTimeGMT - (60 * static_cast<double>(gmt)) + daySavings;

    QDateTime SunRise = QDateTime (Today, GetTimeFromDouble(riseTimeLST));
    return SunRise.time();
}

QTime EarthData::getSunSet()
{
    QDateTime Now = QDateTime::currentDateTime();
    QDate Today = Now.date();

    //It is used to access DST and other timezone related information
    //and sets all timezone global variables
    ktimezone zone("Australia/Sydney");
    bool res = false;
    QDateTime datetime;
    zone.GetNextChange(res, datetime);

    unsigned char daySavings = 0;
    if(!res){
        daySavings = 60;
    }

    /**** Today ****/
    // Calculate the time of sunrise
    double JD = calcJD(Today.year(),
                       Today.month(),
                       Today.day());

    // SunSet was found
    double setTimeGMT = calcSunsetUTC(JD, latitude, longitude);
    double setTimeLST = setTimeGMT - (60 * static_cast<double>(gmt)) + daySavings;

    QDateTime SunSet = QDateTime (Today, GetTimeFromDouble(setTimeLST));
    return SunSet.time();
}

QTime EarthData::getSunRise(QDateTime c)
{
    QDate Today = c.date();

    //It is used to access DST and other timezone related information
    //and sets all timezone global variables
    ktimezone zone("Australia/Sydney");
    bool res = false;
    QDateTime datetime;
    zone.GetNextChange(res, datetime);

    unsigned char daySavings = 0;
    if(!res){
        daySavings = 60;
    }

    /**** Today ****/
    // Calculate the time of sunrise
    double JD = calcJD(Today.year(),
                       Today.month(),
                       Today.day());

    // Calculate sunrise for this date
    // if no sunrise is found, set flag nosunrise
    double riseTimeGMT = calcSunriseUTC(JD, latitude, longitude);

    // Sunrise was found
    double riseTimeLST = riseTimeGMT - (60 * static_cast<double>(gmt)) + daySavings;

    QDateTime SunRise = QDateTime (Today, GetTimeFromDouble(riseTimeLST));
    return SunRise.time();
}

QTime EarthData::getSunSet(QDateTime c)
{
    QDate Today = c.date();

    //It is used to access DST and other timezone related information
    //and sets all timezone global variables
    ktimezone zone("Australia/Sydney");
    bool res = false;
    QDateTime datetime;
    zone.GetNextChange(res, datetime);

    unsigned char daySavings = 0;
    if(!res){
        daySavings = 60;
    }

    /**** Today ****/
    // Calculate the time of sunrise
    double JD = calcJD(Today.year(),
                       Today.month(),
                       Today.day());

    // SunSet was found
    double setTimeGMT = calcSunsetUTC(JD, latitude, longitude);
    double setTimeLST = setTimeGMT - (60 * static_cast<double>(gmt)) + daySavings;

    QDateTime SunSet = QDateTime (Today, GetTimeFromDouble(setTimeLST));
    return SunSet.time();
}

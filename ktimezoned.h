#ifndef KTIMEZONED_H
#define KTIMEZONED_H

#include <QString>
#include <QLatin1String>
#include <QMap>
#include <ctime>
#include <QDateTime>

class ktimezoned
{
public:
    ktimezoned();

    struct ZONE {
        QString Code;
        float lat;
        float lon;
    };

    QMap<QString,ZONE> & GetRegions(){ return Region; };
private:
    QMap<QString,ZONE> Region;

    bool populate();

    QStringList perlSplit(const QRegExp & sep, const QString & s, int max);
};

class ktimezone
{
public:
    ktimezone(const QString name, bool All = false);

    struct TIMECHANGE
    {
        QDateTime DateTime;
        bool DayLightSavings;
        int GMT;
        //int AddSeconds;
        TIMECHANGE(){
            DayLightSavings = false;
        }
    };

    QList<TIMECHANGE> &GetTimes(){ return Dates; };

    void GetNextChange(bool &, QDateTime &);
private:
    QString zone;
    QList<TIMECHANGE> Dates;

    bool populate(bool All);

    QDateTime fromTime_t(time_t t);
//    time_t toTime_t(const QDateTime &utcDateTime);
};

/*class WordTime
{
public:
    void GetList(QMap<QString, QString> &list);
};*/

class CityTime
{
public:
    CityTime(const QString place);

    QDateTime CurrentTime();
    bool DayLightSavings();

private:
    QString Place;
    bool daylight;
};

#endif // KTIMEZONED_H

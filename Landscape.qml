import QtQuick 2.11
import QtQuick.Controls 2.4

Item {
     id: landscape
     width: imageWidth
     height: imageHeight

     Image {
         id: img
         anchors.fill: parent;
         //source: galleryPath + imageSource.get(0).name
         //fillMode: Image.PreserveAspectFit
         cache: true
         //Using SourceSize will store the scaled down image in memory instead of the whole original image
         sourceSize.width: imageWidth
         sourceSize.height: imageHeight
     }

     MouseArea {
         id: btInfo2
         x:300
         y:140
         z: 2
         height: 340
         width: 500
         onClicked: {
             console.log(mouseX,mouseY)
             if(view.currentId === slideId){
                 console.log("landscapePage switchToInfo");
                 window.switchToInfo(true);
             }
         }
     }

     property int test: view.currentId === slideId ? shown() : stopped()
     property bool initialised: false
     property bool sourceLoaded: false

     function loadInitial(){
         if(!landscape.sourceLoaded){
             img.source = galleryPath + sourceList[0].name
             landscape.sourceLoaded = true
         }

         delayCheck.start();
     }

     Component.onCompleted: {
         delayLoad.start();
     }

     Timer {
        id: delayLoad
        interval: 1500
        running: false
        repeat: false
        onTriggered:{
            loadInitial()
        }
    }

    Timer {
        id: delayCheck
        interval: 250
        running: false
        repeat: true
        onTriggered:{
            if(img.sourceSize.width !== 0){
                landscape.initialised = true;
                busyIndication.running = false;
                delayCheck.stop();
            }
        }
    }

    BusyIndicator {
       id: busyIndication
       anchors.centerIn: parent
       running: false
       // 'running' defaults to 'true'
    }

    /*Connections {
        target: landscape
        onVisibleChanged:
            if(visible){
                console.log("LANDSCAPE Shown")
                shown();
            }else{
                console.log("LANDSCAPE stopped")
                stopped();
            }

        ignoreUnknownSignals: true
    }*/

    function shown(){
        console.log('SHOWN LAND', sourceList[0].name);
        if(!landscape.sourceLoaded){
           landscape.loadInitial();
           delayLoad.stop();
        }

        if(!landscape.initialised){
            busyIndication.running = true;
        }
        return 1
    }

    function stopped(){
         return 1
    }
}

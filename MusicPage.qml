import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.0

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle {
        color: "transparent"
    }

    function setPageIndex(index){
        musicSwipeView.currentIndex = index;
    }


    SwipeView {

        id: musicSwipeView
        anchors.fill: parent
        MusicController {

        }

        MusicList {

        }               
    }

    Page {
        id: musicSwipeViewDots
        z: 2
        x: 400 - (musicSwipeView.count/2)*(5+5)
        y: 448
        width: musicSwipeView.count*(5+5)
        height: 10
        visible: musicSwipeView.visible

        background: Rectangle {
            color: "transparent"
        }

        RowLayout {
            spacing: 5
            Repeater {
                model: musicSwipeView.count

                delegate: Rectangle {
                    color: musicSwipeView.currentIndex==index?"black":"white"
                    height: 5
                    width: 5
                    radius: 2
                }
            }
        }
    }
}

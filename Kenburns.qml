import QtQuick 2.11
import QtQuick.Controls 2.4


Item {
     id: kenburns
     width: imageWidth
     height: imageHeight

     MouseArea {
         id: btInfo2
         x:300
         y:140
         z: 2
         height: 340
         width: 500
         onClicked: {
             console.log(mouseX,mouseY)
             if(view.currentId === slideId){
                 console.log("KenPage pressed switchToInfo");
                 window.switchToInfo(true);
             }
         }
     }

     Canvas {
         id: panCanvas
         width: imageWidth
         height: imageHeight

         property int test: view.currentId === slideId ? shown() : stopped()

         property string imagefile: galleryPath + sourceList[0].name
         property int panWidth: 0
         property int panHeight: 0
         property bool initialised: false
         property bool sourceLoaded: false
         property variant r1: []
         property variant r2: []

         property int direction: 0
         property int display_time: 15000
         property int fade_time: Math.min(display_time / 2, 1000);
         property int solid_time: display_time - (fade_time * 2);
         property int fade_ratio: fade_time - display_time;
         property int frame_time: (1 / frames_per_second) * 1000;
         property double zoom_level: 1/2;
         property date start_time: new Date()
         property double anim: 0

         property Image panorama: Image {
             id: img_file
             //source: galleryPath + imageSource.get(0).name
             Component.onCompleted: {
          //       console.log("kenburns",sourceList[0].width,sourceList[0].height);
                 panCanvas.panWidth = sourceList[0].width
                 panCanvas.panHeight = sourceList[0].height
                 var r1 = panCanvas.fit(panCanvas.panWidth, panCanvas.panHeight, imageWidth, imageHeight);
                 var r2 = panCanvas.scale_rect(r1, panCanvas.zoom_level);

                 var align_x = panCanvas.randChoice(3) - 1;
                 var align_y = panCanvas.randChoice(3) - 1;
                 align_x /= 2;
                 align_y /= 2;

                 var x = r2[0];
                 r2[0] += x * align_x;
                 r2[2] += x * align_x;

                 var y = r2[1];
                 r2[1] += y * align_y;
                 r2[3] += y * align_y;

                 if (panCanvas.randChoice(2) === 1) {
                   var t = r1;
                   r1 = r2;
                   r1 = t;

                 }

                 panCanvas.r1 = r1
                 panCanvas.r2 = r2
                 panCanvas.direction = panCanvas.randChoice(2)
             }
         }

         function randChoice(n){return Math.round(Math.random()*(n-1));}

         Component.onCompleted:{
            delayLoad.start();
         }

         onPaint: {
             if (panCanvas.initialised) {
                var r = panCanvas.interpolate_rect(panCanvas.r1, panCanvas.r2, panCanvas.anim);
                var ctx = getContext("2d");
                ctx.save();
                ctx.drawImage(panCanvas.imagefile, r[0], r[1], r[2] - r[0], r[3] - r[1], 0, 0, imageWidth, imageHeight);
                ctx.restore();
             }
         }

         function loadInitial(){
         //    console.log('KENBURN loadInitial');
            if(!panCanvas.sourceLoaded){
                panCanvas.panorama.source = galleryPath + sourceList[0].name
                panCanvas.sourceLoaded = true
            }
            delayCheck.start();
         }

         function imageLoaded(){
        //    console.log('KENBURN imageLoaded');
            panCanvas.panWidth = img_file.sourceSize.width
            panCanvas.panHeight = img_file.sourceSize.height
            var r1 = panCanvas.fit(panCanvas.panWidth, panCanvas.panHeight, imageWidth, imageHeight);
            var r2 = panCanvas.scale_rect(r1, panCanvas.zoom_level);

            var align_x = panCanvas.randChoice(3) - 1;
            var align_y = panCanvas.randChoice(3) - 1;
            align_x /= 2;
            align_y /= 2;

            var x = r2[0];
            r2[0] += x * align_x;
            r2[2] += x * align_x;

            var y = r2[1];
            r2[1] += y * align_y;
            r2[3] += y * align_y;

            if (panCanvas.randChoice(2) === 1) {
               var t = r1;
               r1 = r2;
               r1 = t;

            }

            panCanvas.r1 = r1
            panCanvas.r2 = r2
            panCanvas.direction = panCanvas.randChoice(2)

            panCanvas.initialised = true
            if(panCanvas.direction === 0){
            //    console.log("KENBURN  paint", sourceList[0].name,panCanvas.display_time-100,panCanvas.direction,panCanvas.initialised);
                panCanvas.render_image(1);
            }else{
            //    console.log("KENBURN  paint", sourceList[0].name,0,panCanvas.direction);
                panCanvas.render_image(0);
            }
            busyIndication.running = false;
         }

         Timer {
            id: delayLoad
            interval: 1500
            running: false
            repeat: false
            onTriggered:{
                panCanvas.loadInitial()
            }
         }

         Timer {
             id: delayCheck
             interval: 250
             running: false
             repeat: true
             onTriggered:{
                 if(panCanvas.panorama.sourceSize.width !== 0){
                     panCanvas.imageLoaded();
                     delayCheck.stop();
                 }
             }
         }

        BusyIndicator {
            id: busyIndication
            z: 1
            anchors.centerIn: parent
            running: false
            // 'running' defaults to 'true'
        }

        function render_image(pos) {
             // Renders a frame of the effect
             if (pos > 1 || pos === undefined) {
                 return;
             }

             panCanvas.anim = pos;

             panCanvas.requestPaint();
        }

        function fit(src_w, src_h, dst_w, dst_h) {
             // Finds the best-fit rect so that the destination can be covered
             var src_a = src_w / src_h;
             var dst_a = dst_w / dst_h;
             var w = src_h * dst_a;
             var h = src_h;
             if (w > src_w)
             {
                 w = src_w;
                 h = src_w / dst_a;
             }
             var x = (src_w - w) / 2;
             var y = (src_h - h) / 2;
             return [x, y, x+w, y+h];
        }

        function interpolate_point(x1, y1, x2, y2, i) {
             // Finds a point between two other points
             return  {x: x1 + (x2 - x1) * i,
                      y: y1 + (y2 - y1) * i}
        }

        function interpolate_rect(r1, r2, i) {
             // Blend one rect in to another
             var p1 = this.interpolate_point(r1[0], r1[1], r2[0], r2[1], i);
             var p2 = this.interpolate_point(r1[2], r1[3], r2[2], r2[3], i);
             return [p1.x, p1.y, p2.x, p2.y];
        }

        function scale_rect(r, scale) {
             // Scale a rect around its center
             var w = r[2] - r[0];
             var h = r[3] - r[1];
             var cx = (r[2] + r[0]) / 2;
             var cy = (r[3] + r[1]) / 2;
             var scalew = w * scale;
             var scaleh = h * scale;
             return [cx - scalew/2,
                     cy - scaleh/2,
                     cx + scalew/2,
                     cy + scaleh/2];
        }

        function get_time() {
             var d = new Date();
             return d.getTime() - panCanvas.start_time.getTime();
        }

        Timer {
            id: moveTimer
            interval: panCanvas.frame_time
            running: false
            repeat: true
            onTriggered:{
                var time_passed = panCanvas.get_time();

                var stop = false;
                if(panCanvas.direction === 0){
                  time_passed = panCanvas.display_time - time_passed;
                  if (time_passed <= (panCanvas.fade_time+100)){
                    stop = true;
                  }
                }else{
                  if (time_passed >= panCanvas.fade_time)
                    stop = true;
                }
                if (time_passed < panCanvas.fade_time && time_passed >= 0)
                {
                    if (stop) {
                      moveTimer.stop();
                      return;
                    }
                }
                panCanvas.render_image(time_passed / panCanvas.display_time);
            }
        }

        Timer {
            id: delayStart
            interval: 3000
            running: false
            repeat: false
            onTriggered:{
                panCanvas.start_time = new Date()
                moveTimer.start();
            }
        }

       /* Connections {
            target: kenburns
            onVisibleChanged:
                if(visible){
                    console.log("KENBURNS Shown")
                    kenburns.shown();
                }else{
                    console.log("KENBURNS stopped")
                    kenburns.stopped();
                }

            ignoreUnknownSignals: true
        }*/

        function shown(){
            console.log('SHOWN KEN', sourceList[0].name);
            if(!panCanvas.sourceLoaded){
               //console.log('shown A');
               panCanvas.loadInitial();
               delayLoad.stop();
            }

            if(!panCanvas.initialised){
                //console.log('shown B');
                busyIndication.running = true;
            }else{
                //console.log('shown C');
                delayStart.start();
            }

            return 1
        }

        function stopped(){
            moveTimer.stop();
            delayStart.stop();
            return 1
        }
     }
}

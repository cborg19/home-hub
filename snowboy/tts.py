import argparse
import os

def female_voices(inputtext):
    #google female voice
    from gtts import gTTS
    tts = gTTS(inputtext)
    tts.save('/tmp/output.mp3')
    print('Audio content done')

def googleClould_voices(inputtext, lang, voice):
    #google cloud version
    from google.cloud import texttospeech
    # Instantiates a client
    client = texttospeech.TextToSpeechClient()
    # Set the text input to be synthesized
    synthesis_input = texttospeech.types.SynthesisInput(text=inputtext)
    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.types.VoiceSelectionParams(
        language_code=lang,
        name=voice)
        #ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)
    #Select the type of audio file you want returned
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)
    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(synthesis_input, voice, audio_config)
    # The response's audio_content is binary.
    with open('/tmp/output.mp3', 'wb') as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        print('Audio content done')

def list_voices():
    """Lists the available voices."""
    from google.cloud import texttospeech
    from google.cloud.texttospeech import enums
    client = texttospeech.TextToSpeechClient()

    # Performs the list voices request
    voices = client.list_voices()

    for voice in voices.voices:
        # Display the voice's name. Example: tpc-vocoded
        print('Name: {}'.format(voice.name))

        # Display the supported language codes for this voice. Example: "en-US"
        for language_code in voice.language_codes:
            print('Supported language: {}'.format(language_code))

        ssml_gender = enums.SsmlVoiceGender(voice.ssml_gender)

        # Display the SSML Voice Gender
        print('SSML Voice Gender: {}'.format(ssml_gender.name))

        # Display the natural sample rate hertz for this voice. Example: 24000
        print('Natural Sample Rate Hertz: {}\n'.format(
            voice.natural_sample_rate_hertz))

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument("input", help="input text to TTS", type=str)
parser.add_argument("-v", help="voice type", dest='voice', type=str, default="en-GB-Wavenet-D")
parser.add_argument("-l", help="language code", dest='lang', type=str, default="en-GB")
parser.add_argument("-c", help="user cloud option", dest='cloud', default=True)
parser.parse_args()

results = parser.parse_args()
#print('simple_value     =', results.input)
#print('voice            =', results.voice)
#print('lang             =', results.lang)
#print('cloud            =', results.cloud)

if(results.cloud):
    #os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "/Users/Chris/Documents/Development/reactjs/hubhomeqt/HomeFrame-c13f8f3756ae.json"
    googleClould_voices(results.input, results.lang, results.voice.strip())
else:
    female_voices(results.input)

from PyQt5.QtCore import QBuffer, QDataStream, QSharedMemory

sharedMemory = QSharedMemory('QVoiceControl')

def sendCommand(cmd):
    if sharedMemory.attach():
        print("sendCommand attached")
    # Load into shared memory.
    buf = QBuffer()
    buf.open(QBuffer.ReadWrite)
    out = QDataStream(buf)
    out.writeQString(cmd)
    size = buf.size()

    if not sharedMemory.create(size):
        return

    size = min(sharedMemory.size(), size)
    sharedMemory.lock()

    # Copy image data from buf into shared memory area.
    sharedMemory.data()[:size] = buf.data()[:size]
    sharedMemory.unlock()
    print("command {}".format(cmd))

def checkBuffer():
    #if not sharedMemory.attach():
    #    return
    print("checkBuffer")
 
    buf = QBuffer()
    ins = QDataStream(buf)
    cmd = ""
    
    sharedMemory.lock()
    buf.setData(sharedMemory.constData())
    buf.open(QBuffer.ReadOnly)
    cmd = ins.readQString()
    sharedMemory.unlock()
    sharedMemory.detach()
    print("Buffer {}".format(cmd))


def clearBuffer():
    sharedMemory.lock();
    sharedMemory.unlock();
    sharedMemory.detach();

if __name__ == '__main__':
    sendCommand("what is the weather for sunday")
    #checkBuffer()
    

#! /usr/bin/env python3
import snowboydecoder
import sys
import signal
import speech_recognition as sr
import os
from PyQt5.QtCore import QBuffer, QDataStream, QSharedMemory

sharedMemory = QSharedMemory('QVoiceControl')

"""
This demo file shows you how to use the new_message_callback to interact with
the recorded audio after a keyword is spoken. It uses the speech recognition
library in order to convert the recorded audio into text.

Information on installing the speech recognition library can be found at:
https://pypi.python.org/pypi/SpeechRecognition/
"""


interrupted = False

def sendCommand(cmd):
    if sharedMemory.attach():
        print("sendCommand attached")
    # Load into shared memory.
    buf = QBuffer()
    buf.open(QBuffer.ReadWrite)
    out = QDataStream(buf)
    out.writeQString(cmd)
    size = buf.size()

    if not sharedMemory.create(size):
        print("already created")
        sharedMemory.lock();
        sharedMemory.unlock();
        sharedMemory.detach();
        if not sharedMemory.create(size):
            print("fail twice")
    size = min(sharedMemory.size(), size)
    sharedMemory.lock()

    # Copy image data from buf into shared memory area.
    sharedMemory.data()[:size] = buf.data()[:size]
    sharedMemory.unlock()
    #sharedMemory.detach()
    print("command {}".format(cmd))

def checkBuffer():
    #if not sharedMemory.attach():
    #    return
    print("checkBuffer")
 
    buf = QBuffer()
    ins = QDataStream(buf)
    cmd = ""
    
    sharedMemory.lock()
    buf.setData(sharedMemory.constData())
    buf.open(QBuffer.ReadOnly)
    cmd = ins.readQString()
    sharedMemory.unlock()
    sharedMemory.detach()
    print("Buffer {}".format(cmd))


def clearBuffer():
    sharedMemory.lock();
    sharedMemory.unlock();
    sharedMemory.detach();

def audioRecorderCallback(fname):
    print("converting audio to text")
    r = sr.Recognizer()
    r.energy_threshold = 4000
    with sr.AudioFile(fname) as source:
        audio = r.record(source)  # read the entire audio file
    # recognize speech using Google Speech Recognition
    try:
        # for testing purposes, we're just using the default API key
        # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
        # instead of `r.recognize_google(audio)`
        text = r.recognize_google(audio)
        print(text)
        sendCommand(text)
    except sr.UnknownValueError:
        sendCommand("")
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        sendCommand("")
        print("Could not request results from Google Speech Recognition service; {0}".format(e))

    os.remove(fname)



def detectedCallback():
  sendCommand("1")
  print('recording audio...', end='', flush=True)

def signal_handler(signal, frame):
    global interrupted
    interrupted = True


def interrupt_callback():
    global interrupted
    return interrupted

#if len(sys.argv) == 1:
#    print("Error: need to specify model name")
#    print("Usage: python demo.py your.model")
#    sys.exit(-1)

model = "/home/pi/snowboy/jarvis.umdl" #sys.argv[1]

# capture SIGINT signal, e.g., Ctrl+C
signal.signal(signal.SIGINT, signal_handler)

detector = snowboydecoder.HotwordDetector(model, sensitivity=[0.8,0.8], audio_gain=5)
print('Listening... Press Ctrl+C to exit')

# main loop
detector.start(detected_callback=detectedCallback,
               audio_recorder_callback=audioRecorderCallback,
               interrupt_check=interrupt_callback,
               sleep_time=0.01)

detector.terminate()





#include "alarmcontroller.h"
#include <QtCore/qdebug.h>

AlarmController::AlarmController(QObject *parent) : QObject(parent)
{
    m_current["enabled"] = false;
    m_current["messsage"] = "";
    m_current["snooze"] = false;
    m_current["clock"] = false;
    m_current["video"] = "";
    m_current["sound"] = "";

  /*  QVariantMap a;
    a["enabled"] = true;
    a["messsage"] = "Indiana Theme";
    a["snooze"] = false;
    a["clock"] = false;
    a["video"] = "/media/Indiana Jones Theme.mp4";
    a["sound"] = "";
    a["Time"] = QTime(7,31);
    a["type"] = "day";
    QVariantList d;
    for(int x=0; x<7; x++)
        d.push_back(1);
    a["day"] = d;
    a["date"] = "";
    a["repeat"] = false;


    m_alarms.push_back(a);*/
}

void AlarmController::triggerAlarm(QVariantMap a)
{
    qDebug() << "triggerAlarm";
    l_alarms.lock();
    m_current = a;
    l_alarms.unlock();

    if(m_current.find("sound") != m_current.end()){
        emit triggerSound(m_current["sound"].toString());
    }

    emit currentAlarmChanged();
    emit pageTrigger("trigger", 0);
}

void AlarmController::dismissAlarm()
{
    l_alarms.lock();
    m_current["enabled"] = false;
    m_current["messsage"] = "";
    m_current["snooze"] = false;
    m_current["clock"] = false;
    m_current["video"] = "";
    m_current["sound"] = "";
    l_alarms.unlock();

    emit currentAlarmChanged();
}

QVariantMap AlarmController::getCurrentAlarm()
{
    QVariantMap a;
    l_alarms.lock();
    a = m_current;
    l_alarms.unlock();
    return a;
}

QVariantList AlarmController::getAlarmList()
{
    QVariantList a;
    l_alarms.lock();
    a = m_alarms;
    l_alarms.unlock();
    return a;
}

void AlarmController::setAlarm(int index, QString title, int timeHr, int timeMin, QString date, QString day, QString type, QString sound, bool snooze, bool enabled)
{
    QVariantMap a;
    a["enabled"] = enabled;
    a["clock"] = false;
    a["repeat"] = false;

    if(index != -1){
        l_alarms.lock();
        if(index < m_alarms.length())
            a = m_alarms[index].toMap();
        l_alarms.unlock();
    }

    a["messsage"] = title;
    a["snooze"] = snooze;

    a["video"] = "";
    a["sound"] = "";
    if(sound.contains(".mp4"))
        a["video"] = "/media/"+sound;
    else if(sound.length() > 0) a["sound"] = "/media/"+sound;
    a["Time"] = QTime(timeHr,timeMin);
    a["type"] = type;

    QStringList l = day.split("|");
    QVariantList d;
    for(int x=0; x<7; x++){
        if(l[x] == "1")
            d.push_back(1);
        else d.push_back(0);
    }
    a["day"] = d;
    a["date"] = date;

    l_alarms.lock();
    if(index != -1){
        m_alarms[index] = a;
    }else{
        m_alarms.push_back(a);
    }
    l_alarms.unlock();

    emit alarmListChanged();
}

void AlarmController::deleteAlarm(int index)
{
    l_alarms.lock();
    if(index > -1 && index < m_alarms.length())
        m_alarms.removeAt(index);
    l_alarms.unlock();

    emit alarmListChanged();

}

void AlarmController::setAlarmEnable(int index, bool results)
{
    l_alarms.lock();
    if(index > -1 && index < m_alarms.length()){
        QVariantMap a = m_alarms[index].toMap();
        a["enabled"] = results;
        m_alarms[index] = a;
    }
    l_alarms.unlock();

    emit alarmListChanged();
}

void AlarmController::checkAlarms()
{
    bool trig = false;
    QDateTime current = QDateTime::currentDateTime();
    l_alarms.lock();
    for(int x=0; x<m_alarms.length(); x++){
        QVariantMap a = m_alarms[x].toMap();
        if(!a["enabled"].toBool()) continue;

        if(a["type"].toString() == "day"){
            QVariantList d = a["day"].toList();
            int day = current.date().dayOfWeek();
            if(d.length() < 7) continue;
            if(d[day-1] == 0) continue;
        }else if(a["type"].toString() == "date"){
            QDate d = a["date"].toDate();
            if(d != current.date()) continue;
        }else{
            continue;
        }

        if(a["Time"].toTime().hour() == current.time().hour() && a["Time"].toTime().minute() == current.time().minute()){
            //alarm triggered
            m_current["enabled"] = true;
            m_current["messsage"] = a["messsage"];
            m_current["snooze"] = a["snooze"];
            m_current["clock"] = a["clock"];
            m_current["video"] = a["video"];
            m_current["sound"] = a["sound"];
            trig = true;
            break;
        }
    }
    l_alarms.unlock();

    if(trig){
        emit currentAlarmChanged();
        emit pageTrigger("trigger", 0);
    }
}

bool AlarmController::processVoiceCmd(QString cmd, QString &response)
{
    //set alarm for wednesday thursday friday at 8am called hello
    //set alarm for 29th March at 8am
    //delete alarm called hello
    if(cmd.contains("alarm")){
        QStringList l = cmd.split(" ");
        if(l[0] == 'set'){
            QString title = "";
            int p = l.indexOf("called");
            p++;
            for(int x=p; x < l.length(); x++){
                if(x != p) title += " ";
                title = l[x];
            }

            QString dates = "";
            p = l.indexOf("for");
            p++;
            for(int x=p; x < l.length(); x++){
                if(l[x] == "at" || l[x] == "called") break;
                if(x != p) dates += " ";
                dates = l[x];
            }

            QString time = "";
            p = l.indexOf("at");
            p++;
            for(int x=p; x < l.length(); x++){
                if(l[x] == "called") break;
                if(x != p) time += " ";
                time = l[x];

            }

            QVariantMap a;
            a["enabled"] = true;
            a["clock"] = false;
            a["repeat"] = false;
            a["video"] = "";
            a["sound"] = "media/beep.mp3";
            a["messsage"] = title;
            a["snooze"] = false;

            if(QDate::fromString(dates, "d MMMM").isValid()){
              QDate d = QDate::fromString(dates, "d MMMM");
              a["date"] = d;
              a["type"] = "date";
            }else{
                QStringList days = dates.split(" ");

                QVariantList d;
                for(int x=0; x<7; x++) d.push_back(0);
                for(int x=0; x<days.length(); x++){
                    if(days[x] == "sunday"){
                        d[0] = 1;
                    }else if(days[x] == "monday"){
                        d[1] = 1;
                    }else if(days[x] == "tuesday"){
                        d[2] = 1;
                    }else if(days[x] == "wednesday"){
                        d[3] = 1;
                    }else if(days[x] == "thursday"){
                        d[4] = 1;
                    }else if(days[x] == "friday"){
                        d[5] = 1;
                    }else if(days[x] == "saturday"){
                        d[6] = 1;
                    }else if(days[x] == "weekend"){
                        d[6] = 1;
                        d[0] = 1;
                    }else if(days[x] == "weekday"){
                        d[1] = 1;
                        d[2] = 1;
                        d[3] = 1;
                        d[4] = 1;
                        d[5] = 1;
                    }
                }
                a["day"] = d;
                a["type"] = "day";
            }

            if(time.length() > 0){
               // a["Time"] = QTime(timeHr,timeMin);

                response = "alarm created for "+time+" on "+dates;
                if(title.length() > 0)
                    response += " called "+title;
            }

            l_alarms.lock();
            m_alarms.push_back(a);
            l_alarms.unlock();

            emit alarmListChanged();

        }else if(l[0] == "delete"){
            QString title = "";
            int p = l.indexOf("called");
            p++;
            for(int x=p; x < l.length(); x++){
                if(x != p) title += " ";
                title += l[x];
            }

            int index = -1;
            l_alarms.lock();
            for(int x=0; x<m_alarms.length(); x++){
                QVariantMap a = m_alarms[x].toMap();
                if(a["messsage"].toString() == title){
                   index = x;
                   break;
                }
            }
            if(index != -1){
                m_alarms.removeAt(index);
            }
            l_alarms.unlock();
            if(index != -1){
                response = "alarm "+title+" has been deleted";
            }
        }
    }
    return false;
}

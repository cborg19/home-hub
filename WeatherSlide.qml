import QtQuick 2.11
import QtQuick.Controls 2.4

import "js/skycons.js" as WeatherIcon
import "js/simpletools.js" as SimpleTools

Item {
     id: weatherslide
     width: imageWidth
     height: imageHeight
     Image {
         id: img
         anchors.fill: parent;
         //source: galleryPath + imageSource.get(0).name
         //fillMode: Image.PreserveAspectFit
         cache: true
         //Using SourceSize will store the scaled down image in memory instead of the whole original image
         sourceSize.width: imageWidth
         sourceSize.height: imageHeight

         Rectangle {
             anchors.fill: parent;
             color: Qt.hsla(0,0,0,0.3)
         }
     }

     Button {
         id: btMenu
         x:300
         y:140
         height: 340
         width: 500
         focusPolicy: Qt.NoFocus
         background: Rectangle {
             color: "transparent"
         }
     }

     Connections {
         target: btMenu
         onClicked: {
             window.switchToInfo(true);
         }
     }

     Component {
         id: loadingDisplay
         Item {
             Rectangle {
                 anchors.fill: parent;
                 color: "transparent"
             }

             Label {
                 id: location
                 x: 20
                 y: 20
                 text: qsTr("Loading weather information")
                 color: "white"
                 font.pointSize: 20
                 font.family: "Helvetica"
                 font.bold: true
             }
         }
     }

     Component {
         id: weatherDisplay
         Item {
             Rectangle {
                 anchors.fill: parent;
                 color: "transparent"
             }

             Label {
                 id: location
                 x: 20
                 y: 10
                 text: qsTr(controller.weatherDataModel[controller.weatherPrimary].name)
                 color: "white"
                 font.pointSize: 40
                 font.family: "Helvetica"
                 font.bold: true
             }

             Component {
                 id: nodata
                 Label {
                     anchors.centerIn: parent;
                     text: qsTr("No Internet or Weather Data")
                 }
             }

             Loader {
                 id: wloader
                 // Explicitly set the size of the
                 // Loader to the parent item's size
                 anchors.fill: parent
                 sourceComponent: controller.weatherDataModel[controller.weatherPrimary].hasData?delegateWeather:nodata
             }

             Component {
                 id: delegateWeather
                 Column{
                     x: 100
                     y: 80
                     Row {
                         id: dailyRow
                         spacing: 80

                         property int dailyWidth: 50
                         property int dailyHeight: 50
                         property date startTime: new Date()

                         Repeater {
                             model: 3; // just define the number you want, can be a variable too
                             delegate: Column{
                                 Label {
                                     id: dailyTimeText
                                     text: index==0?qsTr("Today"):qsTr(SimpleTools.dayofWeek(controller.weatherDataModel[controller.weatherPrimary].daily[index].time))
                                     color: "white"
                                     font.pointSize: 20
                                     font.family: "Helvetica"
                                     font.bold: true
                                 }
                                 Canvas {
                                     id: daily1Canvas
                                     width: dailyRow.dailyWidth
                                     height: dailyRow.dailyHeight
                                     x:0
                                     y:0
                                     anchors.horizontalCenter: dailyTimeText.horizontalCenter

                                     onPaint: {
                                         var ctx = getContext("2d");
                                         ctx.save();
                                         ctx.clearRect(0, 0, dailyRow.dailyWidth, dailyRow.dailyHeight);
                                         var diff = new Date() - dailyRow.startTime
                                         WeatherIcon.skycons(ctx,controller.weatherDataModel[controller.weatherPrimary].daily[index].icon, diff);
                                         ctx.restore();
                                     }
                                     Timer {
                                        id: weatherAnimation
                                        interval: 1000 / 60
                                        running: false
                                        repeat: true
                                        onTriggered:{
                                            daily1Canvas.requestPaint();
                                        }
                                     }

                                     Component.onCompleted: {
                                        weatherAnimation.start()
                                     }

                                     Component.onDestruction: {
                                        weatherAnimation.stop()
                                     }
                                 }
                                 Label {
                                     id: rainLabel
                                     anchors.horizontalCenter: dailyTimeText.horizontalCenter
                                     text: qsTr("Rain: "+SimpleTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].daily[index].precipProbability*100)+"%")
                                     color: "white"
                                     font.pointSize: 16
                                     font.family: "Helvetica"
                                 }
                                 Item {
                                     id: itemrow
                                     anchors.top: rainLabel.bottom
                                     anchors.horizontalCenter: dailyTimeText.horizontalCenter
                                     Label {
                                         id: tempHigh1
                                         anchors.horizontalCenter: itemrow.horizontalCenter
                                         anchors.top: itemrow.top
                                         text: qsTr("High: "+(SimpleTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].daily[index].temperatureHigh)))
                                         color: "white"
                                         font.pointSize: 26
                                         font.family: "Helvetica"
                                     }
                                     Label {
                                          anchors.left: tempHigh1.right
                                          anchors.top: tempHigh1.top
                                          text: qsTr("°C")
                                          color: "white"
                                          font.pointSize: 26
                                          font.family: "Helvetica"
                                      }
                                 }

                                 Item {
                                     id: itemrow2
                                     anchors.top: itemrow.bottom
                                     anchors.horizontalCenter: dailyTimeText.horizontalCenter
                                     anchors.topMargin: 30
                                     Label {
                                         anchors.horizontalCenter: itemrow2.horizontalCenter
                                         anchors.top: itemrow2.top
                                         id: tempLow1
                                         text: qsTr("Low: "+SimpleTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].daily[index].temperatureLow))
                                         color: "white"
                                         font.pointSize: 26
                                         font.family: "Helvetica"
                                     }
                                     Label {
                                         anchors.left: tempLow1.right
                                         anchors.top: tempLow1.top
                                         text: qsTr("°C")
                                         color: "white"
                                         font.pointSize: 26
                                         font.family: "Helvetica"
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
         }
     }

     Loader {
         id: wloader
         // Explicitly set the size of the
         // Loader to the parent item's size
         anchors.fill: parent
         sourceComponent: loadingDisplay
     }

     property bool initialised: false
     property bool sourceLoaded: false

     function loadInitial(){
         if(!weatherslide.sourceLoaded){
             img.source = galleryPath + sourceList[0].name
             weatherslide.sourceLoaded = true
         }

         delayCheck.start();
     }

     Component.onCompleted: {
         delayLoad.start();
     }

     Timer {
        id: delayLoad
        interval: 1500
        running: false
        repeat: false
        onTriggered:{
            loadInitial()
        }
    }

    Timer {
        id: delayCheck
        interval: 250
        running: false
        repeat: true
        onTriggered:{
            if(img.sourceSize.width !== 0){
                weatherslide.initialised = true;
                busyIndication.running = false;
                delayCheck.stop();
                delayWeather.start();
            }
        }
    }

    Timer {
        id: delayWeather
        interval: 1000
        running: false
        repeat: true
        onTriggered:{
            if(controller.weatherDataSize > 0){
                wloader.sourceComponent = weatherDisplay;
                delayWeather.stop();
            }
        }
    }

    BusyIndicator {
       id: busyIndication
       anchors.centerIn: parent
       running: false
       // 'running' defaults to 'true'
    }

    property int test: view.currentId === slideId ? shown() : stopped()

    /*Connections {
        target: weatherslide
        onVisibleChanged:
            if(visible){
                console.log("WEAHTERSLIDE Shown")
                shown();
            }else{
                console.log("WEAHTERSLIDE stopped")
                stopped();
            }

        ignoreUnknownSignals: true
    }*/

    function shown(){
        console.log('SHOWN WEATHERSLIDE');
        if(!weatherslide.sourceLoaded){
           weatherslide.loadInitial();
           delayLoad.stop();
        }

        if(!weatherslide.initialised){
            busyIndication.running = true;
        }
        return 1
    }

    function stopped(){
         return 1
    }
}

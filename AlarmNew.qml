import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.0
import Qt.labs.folderlistmodel 2.11

import "./js/fontawesome.js" as FontAwesome

Page {
    id: alarmNewView
    width: imageWidth
    height: imageHeight
    z:4

    property string dayOfWeek: "SMTWTFS"

    property bool newEnable: true
    property string newTitle: ""
    property string newTime: "1:00:AM"
    property string newType: "day"
    property var newDay: [0,0,0,0,0,0,0]
    property date newDate: new Date()
    property bool newSnooze: false
    property string newSound: "beep.mp3"
    property string newSoundType: "sound"

    property string displayDate: ""

    Component.onCompleted: {
        if(alarmNewData.current !== -1){
            alarmNewView.newType = controller.alarmList[alarmNewData.current].type
            alarmNewView.newTitle = controller.alarmList[alarmNewData.current].messsage

            var date = new Date(controller.alarmList[alarmNewData.current].Time);
            if(date.getHours() === 0){
                hoursTumbler.currentIndex = 0;
                amPmTumbler.currentIndex = 0;
            }else if(date.getHours() < 12){
                hoursTumbler.currentIndex = date.getHours() -1;
                amPmTumbler.currentIndex = 0;
            }else{
                hoursTumbler.currentIndex = date.getHours() - 12;
                amPmTumbler.currentIndex = 1;
            }

            alarmNewView.newTime = alarmNewData.getTime(controller.alarmList[x].Time)
            for(var x=0; x<7; x++){
                alarmNewView.newDay[x] = controller.alarmList[alarmNewData.current].day[x]
            }

            alarmNewView.newDate = controller.alarmList[alarmNewData.current].date
            alarmNewView.newSound = controller.alarmList[alarmNewData.current].sound
            if(controller.alarmList[alarmNewData.current].video !== "") alarmNewView.newSound = controller.alarmList[alarmNewData.current].video
            alarmNewView.snooze = controller.alarmList[alarmNewData.current].snooze
        }
    }
    Rectangle {
        anchors.fill: parent
        color: "#f2f2f2"
    }

    function formatText(count, modelData) {
        var data = count === 12 ? modelData + 1 : modelData;
        return data.toString().length < 2 ? "0" + data : data;
    }

    function getDisplayDate(){
        if(alarmNewView.newType === "date"){
            return alarmNewView.newDate.toLocaleString(Qt.locale("en_AU"),"dddd, d MMMM yyyy");
        }

        var str = "";
        if(alarmNewView.newDay[0] === 1) str += "Sun";
        if(alarmNewView.newDay[1] === 1){
            if(str !== "") str += ", ";
            str += "Mon";
        }
        if(alarmNewView.newDay[2] === 1){
            if(str !== "") str += ", ";
            str += "Tue";
        }
        if(alarmNewView.newDay[3] === 1){
            if(str !== "") str += ", ";
            str += "Wed";
        }
        if(alarmNewView.newDay[4] === 1){
            if(str !== "") str += ", ";
            str += "Thu";
        }
        if(alarmNewView.newDay[5] === 1){
            if(str !== "") str += ", ";
            str += "Fri";
        }
        if(alarmNewView.newDay[6] === 1){
            if(str !== "") str += ", ";
            str += "Sat";
        }

        if(str === "Sun, Sat") str = "Weekends";
        if(str === "Mon, Tue, Wed, Thu, Fri") str = "Weekdays";
        if(str === "Sun, Mon, Tue, Wed, Thu, Fri, Sat") str = "Everyday";
        return str
    }


    Rectangle {
        x: 20
        y: 70
        width: imageWidth/2 - 40
        height: imageHeight - 70

        color: "transparent"

        FontMetrics {
            id: fontMetrics

        }

        Component {
            id: delegateComponent
            Label {
                width: 120
               // height: 60
                text: formatText(Tumbler.tumbler.count, modelData)
                opacity: 1.0 - Math.abs(Tumbler.displacement) / (Tumbler.tumbler.visibleItemCount / 2)
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                //font.pixelSize: fontMetrics.font.pixelSize * 1.25
                font.pointSize: 80
            }
        }

        Frame {
            id: frame
            padding: 0
            anchors.fill: parent

            Row {
                id: row

                Tumbler {
                    width: 120
                    height: imageHeight - 70
                    id: hoursTumbler
                    model: 12
                    delegate: delegateComponent
                    onCurrentIndexChanged: {
                    //    console.log(hoursTumbler.currentIndex);
                    }
                }

                Tumbler {
                    width: 120
                    height: imageHeight - 70
                    id: minutesTumbler
                    model: 60
                    delegate: delegateComponent
                }

                Tumbler {
                    width: 120
                    height: imageHeight - 70
                    id: amPmTumbler
                    model: ["AM", "PM"]
                    delegate: delegateComponent
                }
            }
        }
    }

    TextField {
        id: name
        x: 20
        y: 20
        width: 600
        text: alarmNewView.newTitle
        placeholderText: "Alarm Name"
        onEditingFinished: {
            alarmNewView.newTitle = text;
        }
    }

    CalendarItem {
        id: calendar
        visible: false
        x: 400
        y: 70
        width: imageWidth/2 - 20
        height: imageHeight - 70

        onComplete: {
            //console.log("DONE",select)
            alarmNewView.newDate = new Date(select);
            alarmNewView.newType = "date"
            alarmNewView.displayDate = alarmNewView.getDisplayDate()

            calendar.visible = false
            option.visible = true
        }
    }

    Item {
        id: option
        Label {
            id: nextDateOption
            x: 400
            y: 70
            text: alarmNewView.displayDate
            font.pointSize: 16
            color: "#333333"
        }

        Text {
            x: 700
            y: 70
            color: "#333333"
            font.pointSize: 35
            font.family: fontAwesome.name
            text: FontAwesome.icons.fa_calendar

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    calendar.visible = true
                    option.visible = false
                }
            }
        }

        Row {
            x: 400
            y: 100
            spacing: 20
            Repeater {
                model: 7
                Label {
                    text: dayOfWeek[index]
                    color: alarmNewView.newDay[index]===1?"blue":"black"
                    font.pointSize: 22
                    font.family: "Helvetica"
                    font.bold: true

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(alarmNewView.newDay[index] === 0)
                                alarmNewView.newDay[index] = 1;
                            else alarmNewView.newDay[index] = 0;

                            alarmNewView.newType = "day"
                            alarmNewView.displayDate = alarmNewView.getDisplayDate()
                         }
                    }
                }
            }
        }

        Rectangle {
            x: 400
            y: 150
            width: 400
            height: 100
            color: "transparent"
            Label {
                x: 0
                y: 0
                text: "Alarm Sound or Clip"
                font.pointSize: 16
                color: "#333333"
            }

            Label {
                x: 0
                y: 50
                text: alarmNewView.newSound
                font.pointSize: 16
                color: "#333333"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    option.visible = false;
                    soundlist.visible = true;
                }
            }
        }

        Label {
            x: 400
            y: 300
            text: "Snooze"
            font.pointSize: 16
            color: "#333333"
        }

        Switch {
            x: 600
            y: 300
            font.pointSize: 16
            checked: alarmNewView.newSnooze
            onCheckableChanged: {
                alarmNewView.newSnooze = !alarmNewView.newSnooze
            }
        }
    }

    ListView {
        id: soundlist
        x: 400
        y: 70
        width: 200;
        height: imageHeight - 70
        visible: false

        FolderListModel {
            id: folderModel
            folder: galleryPath+"/media/"
            nameFilters: ["*.mp3","*.mp4"]
        }

        Component {
            id: fileDelegate
            Row {
                CheckBox {
                    text: fileName
                    checked: alarmNewView.newSound === fileName?true: false
                    onClicked: {
                        alarmNewView.newSound = fileName;
                        option.visible = true;
                        soundlist.visible = false;
                    }
                }
            }
        }

        model: folderModel
        delegate: fileDelegate
    }

    Text {
        id: deleteItem
        x: 670
        y: 13
        z: 5
        color: "#4dff4d"
        font.pointSize: 35
        font.family: fontAwesome.name
        text: FontAwesome.icons.fa_trash_o


        MouseArea {
            anchors.fill: parent
            onClicked: {
                controller.deleteAlarm(alarmNewData.current)
            }
        }
    }

    Text {
        id: save
        x: 710
        y: 12
        z: 5
        color: "#4dff4d"
        font.pointSize: 35
        font.family: fontAwesome.name
        text: FontAwesome.icons.fa_check

        MouseArea {
            anchors.fill: parent
            onClicked: {
                var s = "";
                for(var x=0; x<7; x++){
                   if(x !== 0) s += "|";
                   s += alarmNewView.newDay[x].toString();
                }

                var hr = 0, min = 0;
                alarmNewView.newTime = (hoursTumbler.currentIndex+1).toString()+":";
                hr = hoursTumbler.currentIndex+1
                if(minutesTumbler.currentIndex < 10){
                   alarmNewView.newTime+="0";
                }
                alarmNewView.newTime+=minutesTumbler.currentIndex.toString();
                min = minutesTumbler.currentIndex;
                if(amPmTumbler.currentIndex === 0){
                    alarmNewView.newTime+=" AM"
                    if(hr === 12) hr = 0;
                }else{
                    alarmNewView.newTime+=" PM"
                    if(hr !== 12) hr += 12
                }

                controller.setAlarm(alarmNewData.current, alarmNewView.newTitle, hr, min, alarmNewView.newDate, s, alarmNewView.newType, alarmNewView.newSound, alarmNewView.newSnooze);

                delayClose.start();
            }
        }
    }

    Button {
        id: btMenu
        x: imageWidth-50
        y: 10
        height: 50
        width: 50
        z: 5
        //focusPolicy: Qt.NoFocus
        //background: Rectangle {
        //    color: "transparent"
        //}
        Image {
            id: btFrameImage
            anchors.fill: parent
            source: "files/icon/cancel.png"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            delayClose.start();
        }
    }

    Timer {
        id: delayClose
        interval: 10
        running: false
        repeat: false
        onTriggered:{
            delayClose.stop();
            alarmNewData.close();
           // alarmNewView.destroy();
        }
    }
}

#include <QtCore/qdebug.h>
#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>
#include <QDir>
#include <QProcess>
//#include <QFile>
#include <QFileInfo>
#include <QUrl>
//#include <QDirIterator>
#include <QImage>
#include <QDateTime>
#include <QMutexLocker>

#include "weather.h"

Weather::Weather(const QStringList settings, QObject *parent) : QObject(parent), m_lastUpdate(0), m_primary(false)
{
    m_id = settings[0];
    m_name = settings[1];
    m_latitude = settings[2];
    m_longitude = settings[3];
    if(settings.length() > 4)
        m_Bom = settings[4];
    if(settings.length() > 5)
        if(settings[5] == "true")
            m_primary = true;
    if(settings.length() > 6)
        m_pathName = settings[6];
    APIKey = "897b4c4b5f86a6e1574cf038fceebef5";

    networkManager = new NetworkManager();

    if(m_Bom != ""){
        getWeatherRadarBg();
        m_radarbg << m_Bom+".background.png" << m_Bom+".topography.png" << m_Bom+".locations.png" << m_Bom+".range.png";
    }
}

Weather::Weather(const QString &pid, const QString &pname, const QString &platitude, const QString &plongitude, QObject *parent) : QObject(parent),
    m_id(pid), m_name(pname), m_longitude(plongitude), m_latitude(platitude), m_Bom(""), m_pathName(""), m_lastUpdate(0), m_primary(false)
{
    APIKey = "897b4c4b5f86a6e1574cf038fceebef5";
    //updateTimer = new QTimer();
    //connect(updateTimer, SIGNAL(timeout()), this, SLOT(updateTimerTriggered()) );
    //updateTimer->setSingleShot(false);
    //updateTimer->start(m_checkInterval);
    //m_checkInterval(60 * 60 * 1000),

    networkManager = new NetworkManager();

    getWeatherDarkSky();
}

bool Weather::update()
{
    return getWeatherDarkSky();
}

qint64 Weather::lastUpdate() const
{
    return m_lastUpdate;
}
/*
QImage Weather::radarImage() const
{
    QFile fileIn ("/Users/Chris/Documents/Development/reactjs/hubhomeqt/PhotoFrame/icon/sleep.png");
    fileIn.open(QFile::ReadOnly);
    QByteArray data = fileIn.readAll();
    QImage *img = new QImage(480,480,QImage::Format_Indexed8);

    //QImage img;
    *img = QImage::fromData(data,"PNG");
    return *img;
}*/

void Weather::getWeatherRadar()
{
 /*   m_radarImagelist.clear();

    QFile fileIn ("/Users/Chris/Documents/Development/reactjs/hubhomeqt/PhotoFrame/icon/sleep.png");
    fileIn.open(QFile::ReadOnly);
    QByteArray data = fileIn.readAll();
    QImage *img = new QImage(480,480,QImage::Format_Indexed8);

    //QImage img;
    *img = QImage::fromData(data,"PNG");
    m_radarImagelist.append(img);
    emit radarbgChanged();
    return;
*/
    qDebug() << "radar download started";
    QString url = "ftp://ftp.bom.gov.au/anon/gen/radar/";
    //QString win = "/Users/Chris/Documents/Development/reactjs/hubhomeqt/build-PhotoFrame-Desktop_Qt_5_11_3_MinGW_32bit-Debug/debug/";
    QString cmd = "bash -c \"/usr/bin/curl -s "+url+" | /usr/bin/awk '{print $9}' | /bin/grep "+m_Bom+"\" | /bin/grep png";
    QProcess process1;
    process1.start(cmd);
    qDebug() << "input "<< cmd;
    process1.waitForFinished();
    QString olist = process1.readAllStandardOutput();

    QStringList rows = olist.split("\n");
    //for(int x=0; x<m_radarImagelist.size(); x++){
    //    delete m_radarImagelist.at(x);
    //}
    m_radarImagelist.clear();
    for(int x=0; x<rows.length(); x++){
        if(rows[x] == "") continue;
        QFileInfo fi(rows[x]);
        QString ext = fi.suffix().toLower();
        if(ext != "png") continue;
        QString bgURL = "ftp://ftp.bom.gov.au/anon/gen/radar/"+rows[x];

        m_radarImagelist.append(bgURL);
//qDebug() << bgURL;
  /*/      NetworkManagerResult result = networkManager->sendHTTPGetRequestSynchronous(bgURL);
        if(result.errorResponse == 0 ){
            //QImage *img = new QImage();
            QImage img;
           img = QImage::fromData(result.binary,"PNG");
//            img->loadFromData(result.binary);

            m_radarImagelist.append(img);
        }else if(result.errorResponse == -1 ){
            qDebug() << "Error network" << result.response;
            //Network timeout
        } else {
            qDebug() << "Error bad" << result.response;
        }*/
    }

    if(m_radarImagelist.count() > 0)
        emit radarbgChanged();
    qDebug() << "radar download completed " << m_radarImagelist.count();
}

Q_INVOKABLE void Weather::downloadRadarImages()
{
    getWeatherRadar();
}

void Weather::getWeatherRadarBg()
{
    bool writeOn = false;
    QString inPath = m_pathName + "weather/" + m_Bom;
    qDebug() << "getWeatherRadar" << inPath;
    if(!QDir(inPath).exists()){
        #ifdef Q_OS_LINUX
        writeOn = true;
        QProcess process1;
        process1.start("rw");

        if(!process1.waitForFinished()){
           return;
        }
        #endif
        QDir().mkdir(inPath);
    }

    inPath += "/";

    for(int x=0; x<m_radarbg.length(); x++){
        bool exists = QFileInfo::exists(inPath+m_radarbg[0]) && QFileInfo(inPath+m_radarbg[x]).isFile();
        qDebug() << "check" << exists;
        if(!exists){
            //retrive file
            QString bgURL = "ftp://ftp.bom.gov.au/anon/gen/radar_transparencies/"+m_radarbg[x];

            NetworkManagerResult result = networkManager->sendHTTPGetRequestSynchronous(bgURL);
            if(result.errorResponse == 0 ){
                if(!writeOn){
                    #ifdef Q_OS_LINUX
                    writeOn = true;
                    QProcess process1;
                    process1.start("rw");

                    if(!process1.waitForFinished()){
                       return;
                    }
                    #endif
                }
                //result.response
                QFile outFile(inPath+m_radarbg[x]);
                if (!outFile.open(QIODevice::WriteOnly)) {
                    qDebug() << "can't open outFile";
                    continue;
                }

                outFile.write(result.binary);
                outFile.close();
            }else if(result.errorResponse == -1 ){
                qDebug() << "Error network" << result.response;
                //Network timeout
            } else {
                qDebug() << "Error bad" << result.response;
            }
        }
    }
    if(writeOn){
        #ifdef Q_OS_LINUX
        QProcess process2;
        process2.start("ro");

        if(!process2.waitForFinished()){
           return;
        }
        #endif
    }
}

QString Weather::getRadarBgImage(int pos)
{
    if(pos >= m_radarbg.length()) return "";

    QString inPath = m_pathName + "weather/" + m_Bom+"/"+m_radarbg[pos];
    return  inPath;
}

QStringList Weather::radarBg() const
{
    QStringList l = m_radarbg;
    for(int x=0; x<l.length(); x++){
        l[x] = m_pathName + "weather/" + m_Bom+"/"+l[x];
    }
    return l;
}

bool Weather::hasradarImages() const
{
    return m_radarImagelist.count() > 0;
}

QStringList Weather::getradarImages() const
{
    //qDebug() << "QML asked for weatherlist";
    //return QQmlListProperty<QObject>(this, weatherList);
    return m_radarImagelist;
}

int Weather::getradarImagesSize()
{
    return m_radarImagelist.count();
}

bool Weather::getWeatherDarkSky()
{
    qDebug() << "getWeatherDarkSky";
    //https://api.darksky.net/forecast/897b4c4b5f86a6e1574cf038fceebef5/-33.9480639,150.9942686?units=ca
    QString url = "https://api.darksky.net/forecast/"+APIKey+"/"+m_latitude+","+m_longitude+"?units=ca";

    NetworkManagerResult result = networkManager->sendHTTPGetRequestSynchronous(url/*, timeout*/);

    qDebug() << "HTTP Result: " << result.response;

    if(result.errorResponse == 0 ){
        QString data = result.binary;
        //we are expecting JSON response
        if(parseJSONResponse(&data)){
            m_lastUpdate = QDateTime::currentMSecsSinceEpoch();
            return true;
        } else {
          qDebug() << "Error json";
          //  errMsg = "Unable to parse JSON Response";
        }
    } else if(result.errorResponse == -1 ){
        qDebug() << "Error network" << result.response;
        //Network timeout
    } else {
        qDebug() << "Error bad" << result.response;
    }
    return false;
}

bool Weather::parseJSONResponse(QString *response)
{
    QJsonDocument json_doc = QJsonDocument::fromJson(response->toUtf8());

    if(json_doc.isNull()){

        qDebug() << "Failed to parse JSON";
        return false;
    }

    if(!json_doc.isObject()){
        qDebug()<<"JSON is not an object.";
        return false;;
    }


    QJsonObject json_obj = json_doc.object();

    if(json_obj.isEmpty()){
        qDebug()<<"JSON object is empty.";
        return false;
    }

    QStringList key_list = json_obj.keys();

    double top = 0.0, low = 30.0;
    double htop = 0.0, hlow = 30.0;

    QVariantMap t_current;
    QVariantList t_daily;
    QVariantList t_hourly;

    if(key_list.contains("daily")){
        QJsonObject dailyObj = json_obj["daily"].toObject();
        QJsonArray dailyArray = dailyObj["data"].toArray();

        for(int x=0; x<dailyArray.count(); x++){
            QJsonObject coords_obj = dailyArray[x].toObject();

            QVariantMap row;
            row.insert("time",coords_obj["time"].toInt());
            row.insert("summary",coords_obj["summary"].toString());
            row.insert("icon",coords_obj["icon"].toString());
            row.insert("sunriseTime",coords_obj["sunriseTime"].toInt());
            row.insert("sunsetTime",coords_obj["sunsetTime"].toInt());
            row.insert("moonPhase",coords_obj["moonPhase"].toDouble());
            row.insert("precipIntensity",coords_obj["precipIntensity"].toDouble());
            row.insert("precipIntensityMax",coords_obj["precipIntensityMax"].toDouble());
            row.insert("precipIntensityMaxTime",coords_obj["precipIntensityMaxTime"].toInt());
            row.insert("precipProbability",coords_obj["precipProbability"].toDouble());
            row.insert("precipType",coords_obj["precipType"].toDouble());
            row.insert("temperatureHigh",coords_obj["temperatureHigh"].toDouble());
            row.insert("temperatureHighTime",coords_obj["temperatureHighTime"].toInt());
            row.insert("temperatureLow",coords_obj["temperatureLow"].toDouble());
            row.insert("temperatureLowTime",coords_obj["temperatureLowTime"].toInt());
            row.insert("apparentTemperatureHigh",coords_obj["apparentTemperatureHigh"].toDouble());
            row.insert("apparentTemperatureLowTime",coords_obj["apparentTemperatureLowTime"].toInt());
            row.insert("dewPoint",coords_obj["dewPoint"].toDouble());
            row.insert("humidity",coords_obj["humidity"].toDouble());
            row.insert("pressure",coords_obj["pressure"].toDouble());
            row.insert("windSpeed",coords_obj["windSpeed"].toDouble());
            row.insert("windGust",coords_obj["windGust"].toDouble());
            row.insert("windGustTime",coords_obj["windGustTime"].toDouble());
            row.insert("windBearing",coords_obj["windBearing"].toDouble());
            row.insert("cloudCover",coords_obj["cloudCover"].toDouble());
            row.insert("uvIndex",coords_obj["uvIndex"].toDouble());
            row.insert("uvIndexTime",coords_obj["uvIndexTime"].toInt());
            row.insert("visibility",coords_obj["visibility"].toDouble());
            row.insert("ozone",coords_obj["ozone"].toInt());
            row.insert("temperatureMin",coords_obj["temperatureMin"].toDouble());
            row.insert("temperatureMinTime",coords_obj["temperatureMinTime"].toInt());
            row.insert("temperatureMax",coords_obj["temperatureMax"].toDouble());
            row.insert("temperatureMaxTime",coords_obj["temperatureMaxTime"].toInt());
            row.insert("apparentTemperatureMin",coords_obj["apparentTemperatureMin"].toDouble());
            row.insert("apparentTemperatureMinTime",coords_obj["apparentTemperatureMinTime"].toInt());
            row.insert("apparentTemperatureMax",coords_obj["apparentTemperatureMax"].toDouble());
            row.insert("apparentTemperatureMaxTime",coords_obj["apparentTemperatureMaxTime"].toInt());

            if(top < coords_obj["temperatureHigh"].toDouble())
                top = coords_obj["temperatureHigh"].toDouble();
            if(low > coords_obj["temperatureLow"].toDouble())
                low = coords_obj["temperatureLow"].toDouble();
            t_daily.push_back(row);
        }

    }
    if(key_list.contains("hourly")){
        QJsonObject hourlyObj = json_obj["hourly"].toObject();
        QJsonArray hourlyArray = hourlyObj["data"].toArray();

        for(int x=0; x<hourlyArray.count(); x++){
            QJsonObject coords_obj = hourlyArray[x].toObject();

            QVariantMap row;
            row.insert("time",coords_obj["time"].toInt());
            row.insert("summary",coords_obj["summary"].toString());
            row.insert("icon",coords_obj["icon"].toString());
            row.insert("precipIntensity",coords_obj["precipIntensity"].toDouble());
            row.insert("precipProbability",coords_obj["precipProbability"].toDouble());
            row.insert("precipType",coords_obj["precipType"].toDouble());
            row.insert("temperature",coords_obj["temperature"].toDouble());
            row.insert("apparentTemperature",coords_obj["apparentTemperature"].toDouble());
            row.insert("dewPoint",coords_obj["dewPoint"].toDouble());
            row.insert("humidity",coords_obj["humidity"].toDouble());
            row.insert("pressure",coords_obj["pressure"].toDouble());
            row.insert("windSpeed",coords_obj["windSpeed"].toDouble());
            row.insert("windGust",coords_obj["windGust"].toDouble());
            row.insert("windBearing",coords_obj["windBearing"].toDouble());
            row.insert("cloudCover",coords_obj["cloudCover"].toDouble());
            row.insert("uvIndex",coords_obj["uvIndex"].toInt());
            row.insert("visibility",coords_obj["visibility"].toDouble());
            row.insert("ozone",coords_obj["ozone"].toDouble());

            if(htop < coords_obj["temperature"].toDouble())
                htop = coords_obj["temperature"].toDouble();
            if(hlow > coords_obj["temperature"].toDouble())
                hlow = coords_obj["temperature"].toDouble();
            t_hourly.push_back(row);
        }
        if(key_list.contains("currently")){
            QJsonObject current = json_obj["currently"].toObject();

            t_current.insert("time", current["time"].toInt());
            t_current.insert("summary", current["summary"].toString());
            t_current.insert("icon", current["icon"].toString());
            t_current.insert("precipIntensity", current["precipIntensity"].toDouble());
            t_current.insert("precipProbability", current["precipProbability"].toDouble());
            t_current.insert("temperature", current["temperature"].toDouble());
            t_current.insert("apparentTemperature", current["apparentTemperature"].toDouble());
            t_current.insert("dewPoint", current["dewPoint"].toDouble());
            t_current.insert("humidity", current["humidity"].toDouble());
            t_current.insert("pressure", current["pressure"].toDouble());
            t_current.insert("humidity", current["humidity"].toDouble());
            t_current.insert("windSpeed", current["windSpeed"].toDouble());
            t_current.insert("windGust", current["windGust"].toDouble());
            t_current.insert("windBearing", current["windBearing"].toDouble());
            t_current.insert("cloudCover", current["cloudCover"].toDouble());
            t_current.insert("uvIndex", current["uvIndex"].toDouble());
            t_current.insert("ozone", current["ozone"].toDouble());
            t_current.insert("topHigh", top);
            t_current.insert("topLow", low);
            t_current.insert("htopHigh", htop);
            t_current.insert("htopLow", hlow);
        }
    }

    QMutexLocker M(&updating);

    m_daily.clear();
    m_daily = t_daily;
    m_hourly.clear();
    m_hourly = t_hourly;
    m_current.clear();
    m_current = t_current;

    emit hourlyChanged();
    emit dailyChanged();
    emit currentChanged();
    emit dataChanged();
    if(m_primary){
        emit primaryChanged();
    }

    qDebug() << "Process Weather" << m_primary;

    return true;
}

QVariantMap Weather::current()
{
    QMutexLocker M(&updating);
    return m_current;
}

void Weather::setCurrent(const QVariantMap &source)
{
    if (source != m_current) {
        QMutexLocker M(&updating);
        m_current = source;
        emit currentChanged();
    }
}

QVariantList Weather::hourly()
{
    QMutexLocker M(&updating);
    return m_hourly;
}

void Weather::setHourly(const QVariantList &source)
{
    if (source != m_hourly) {
        QMutexLocker M(&updating);
        m_hourly = source;
        emit hourlyChanged();
    }
}

QVariantList Weather::dailysource()
{
    QMutexLocker M(&updating);
    return m_daily;
}

void Weather::setDaily(const QVariantList &source)
{
    if (source != m_daily) {
        QMutexLocker M(&updating);
        m_daily = source;
        emit dailyChanged();
    }
}

QString Weather::sname() const
{
    return m_name;
}

void Weather::setName(const QString &source)
{
    if (source != m_name) {
        m_name = source;
        emit nameChanged();
    }
}

QString Weather::sid() const
{
    return m_id;
}

void Weather::setId(const QString &source)
{
    if (source != m_id) {
        m_id = source;
        emit idChanged();
    }
}

bool Weather::hasradar() const
{
    return m_Bom != "";
}

bool Weather::hasdata() const
{
    return m_current.size() > 0;
}

QString Weather::getLatitude() const
{
    return m_latitude;
}

QString Weather::getLongitude() const
{
    return m_longitude;
}

QString Weather::getBOM() const
{
    return m_Bom;
}

bool Weather::isPrimary() const
{
    return m_primary;
}

void Weather::setPrimary(bool v)
{
    m_primary = v;
    if(v){
        if(m_current.size() > 0)
            emit primaryChanged();
    }
}

void Weather::setLatitude(QString v)
{
    m_latitude = v;
}

void Weather::setLongitude(QString v)
{
    m_longitude = v;
}

void Weather::setBOM(QString v)
{
    m_Bom = v;
}

//--------------------------------------------------------------------------------------
WeatherController::WeatherController(QObject *parent) : QObject(parent)
{
    primary = 0;
}

QQmlListProperty<QObject> WeatherController::getList()
{
    //qDebug() << "QML asked for weatherlist";
    QQmlListProperty<QObject> r;
    l_weather.lock();
    r = QQmlListProperty<QObject>(this, weatherList);
    l_weather.unlock();
    return r;
}

int WeatherController::getListSize()
{
    int c;
    l_weather.lock();
    c = weatherList.count();
    l_weather.unlock();
    return c;
}

Q_INVOKABLE void WeatherController::setSettings(QStringList list)
{
    qDebug() << "set Weather Settings" << list;

    QMutexLocker M(&l_weather);
    for(int x=0; x<weatherList.size(); x++){
        delete weatherList.at(x);
    }
    weatherList.clear();

    for(int y=0; y<list.size(); y++){
        qDebug() << "set Weather Settings" ;
        QString j = list.at(y);
        QStringList l = j.split("|");
        l.push_back(m_pathName);
        Weather *w = new Weather(l);

        connect(w, &Weather::primaryChanged, this, &WeatherController::primaryUpdateHandler);

        if(w->isPrimary()) primary = y;

        weatherList.push_back(w);
    }

 //   qDebug() << "Doing Settings";
}

void WeatherController::primaryUpdateHandler()
{
    emit primaryUpdate();
}

QString WeatherController::pathName()
{
    return m_pathName;
}

void WeatherController::setPathName(const QString &pathName)
{
    if (pathName == m_pathName)
        return;

    m_pathName = pathName;
    emit pathNameChanged();
}

QStringList WeatherController::getWeatherOption()
{
if(gdebug) qDebug() << "CRASH WeatherController getWeatherOption";
    QStringList wlist;
    l_weather.lock();
    for(int x=0; x<weatherList.count(); x++){
        Weather *w = (Weather *)weatherList.at(x);

        wlist.append(w->sname());
    }
    l_weather.unlock();
if(gdebug) qDebug() << "CRASH WeatherController getWeatherOption finished";
    return wlist;
}

QStringList WeatherController::getWeatherOptionAt(int x)
{
if(gdebug) qDebug() << "CRASH WeatherController getWeatherOptionAt";
    QStringList wlist;

    l_weather.lock();
    Weather *w = (Weather *)weatherList.at(x);
    wlist.append(w->sname());
    wlist.append(w->getLatitude());
    wlist.append(w->getLongitude());
    wlist.append(w->getBOM());
    wlist.append(w->isPrimary()?"true":"false");
    l_weather.unlock();
if(gdebug) qDebug() << "CRASH WeatherController getWeatherOptionAt finished";
    return wlist;
}

QList<QObject *> * WeatherController::GetAllList()
{
if(gdebug) qDebug() << "CRASH WeatherController GetAllList";
    QList<QObject *> * r;
    l_weather.lock();
    r = &weatherList;
    l_weather.unlock();
if(gdebug) qDebug() << "CRASH WeatherController GetAllList finished";
    return r;
}

QList<QStringList> WeatherController::getSettings()
{
if(gdebug) qDebug() << "CRASH WeatherController getSettings";
    QList<QStringList> list;

    l_weather.lock();
    for(int x=0; x<weatherList.count(); x++){
        QStringList wlist;
        Weather *w = (Weather *)weatherList.at(x);

        wlist.append(w->sname());
        wlist.append(w->getLatitude());
        wlist.append(w->getLongitude());
        wlist.append(w->getBOM());
        wlist.append(w->isPrimary()?"true":"false");

        list.push_back(wlist);
    }
    l_weather.unlock();
if(gdebug) qDebug() << "CRASH WeatherController getSettings finished";
    return list;
}

void WeatherController::addWeatherOption(const QStringList &list)
{
if(gdebug) qDebug() << "CRASH WeatherController addWeatherOption";
    QMutexLocker M(&l_weather);
    Weather *w = new Weather(list);

    connect(w, &Weather::primaryChanged, this, &WeatherController::primaryUpdateHandler);

    weatherList.push_back(w);
if(gdebug) qDebug() << "CRASH WeatherController addWeatherOption finished";
}

void WeatherController::updateWeatherOption(int pos, const QStringList &list)
{
if(gdebug) qDebug() << "CRASH WeatherController updateWeatherOption";
    QMutexLocker M(&l_weather);
    if(pos <weatherList.count() && pos >= 0){
        Weather *w = (Weather *)weatherList.at(pos);

        w->setName(list[0]);
        w->setLatitude(list[1]);
        w->setLongitude(list[2]);
        w->setBOM(list[3]);
    }
if(gdebug) qDebug() << "CRASH WeatherController updateWeatherOption finished";
}

void WeatherController::removeWeatherOption(int pos)
{
    QMutexLocker M(&l_weather);
    if(pos <weatherList.count() && pos >= 0){
        Weather *w = (Weather *)weatherList.at(pos);

        delete w;

        weatherList.removeAt(pos);
    }
}

int WeatherController::getPrimary()
{
    return primary;
}

void WeatherController::setPrimary(int pos)
{
    QMutexLocker M(&l_weather);
    if(primary != pos && pos < weatherList.count() && pos >= 0){
        if(primary != -1){
            Weather *w2 = (Weather *)weatherList.at(primary);
            w2->setPrimary(false);
        }

        Weather *w = (Weather *)weatherList.at(pos);
        w->setPrimary(true);
    }
}

QDateTime adjustDayOfWeek(QDateTime d, int day){
    if(d.date().dayOfWeek() != day){
        for(int x=0; x<7; x++){
           d = d.addDays(1);
           if(d.date().dayOfWeek() == day){
               d.setTime(QTime(0,0));
               return d;
           }
        }
    }
    return d;
}

bool WeatherController::processVoiceCmd(QString cmd, QString &response)
{
    /*
        what is the current/day weather/temperature in/for London
        tell me the current/day weather/temperature in/for Delhi
        what is the weather/temperature for tomorrow/tuesday

what's the weather today?
“Is it going to rain tomorrow?
“Is it sunny today?”
"Will I need an umbrella tomorrow?"
What's the weather this arvo?
//  In Sydney this afternoon, it's predicted to be 24 degrees Celsius and sunny.
    */
//weatherList
    if(cmd.contains("weather") || cmd.contains("temperature")){
if(gdebug) qDebug() << "CRASH WeatherController processVoiceCmd";
        if(cmd.contains("go to")){
qDebug() << "FFFF goto";
            emit pageTrigger("weather", 0);
            return true;
        }
        QString day = "";
        QString location = "";

        if(cmd.contains("show")){
qDebug() << "FFFF show";
            QStringList l = cmd.split(" ");
            int p = l.indexOf("show");
            p++;
            if(p < l.length()){
                location = l[p];
            }
            int index = -1;
            l_weather.lock();
            for(int x=0; x<weatherList.count(); x++){
                Weather *w = (Weather *)weatherList.at(x);
                if(w->sname().toLower().contains(location)){
                   index = x;
                   break;
                }
            }
            l_weather.unlock();

            if(index != -1)
                emit pageTrigger("weather", index);
            return true;
        }

        bool temp = false;
        if(cmd.contains("temperature")) temp = true;

        QDateTime d = QDateTime::currentDateTime();
qDebug() << "FFFF days";
        if(cmd.contains("current")){
            day = "current";
        }else if(cmd.contains("avro")){
            day = "avro";
            d.setTime(QTime(16,0));
        }else if(cmd.contains("afternoon")){
            day = "afternoon";
            d.setTime(QTime(16,0));
        }else if(cmd.contains("tonight")){
            day = "tonight";
            d = d.addDays(1);
            d.setTime(QTime(2,0));
        }else if(cmd.contains("tomorrow")){
            day = "tomorrow";
            d = d.addDays(1);
            d.setTime(QTime(0,0));
        }else if(cmd.contains("week")){
            day = "week";
            if(d.date().dayOfWeek() != 1){
                for(int x=0; x<7; x++){
                   d = d.addDays(1);
                   if(d.date().dayOfWeek() == 1){
                       break;
                   }
                }
            }
            d.setTime(QTime(0,0));
        }else if(cmd.contains("weekend")){
            day = "weekend";
            if(d.date().dayOfWeek() != 6 && d.date().dayOfWeek() != 7){
                for(int x=0; x<7; x++){
                   d = d.addDays(1);
                   if(d.date().dayOfWeek() == 6){
                       break;
                   }
                }
            }
            d.setTime(QTime(0,0));
        }else if(cmd.contains("monday")){
            day = "monday";
            d = adjustDayOfWeek(d, 1);
        }else if(cmd.contains("tuesday")){
            day = "tuesday";
            d = adjustDayOfWeek(d, 2);
        }else if(cmd.contains("wednesday")){
            day = "wednesday";
            d = adjustDayOfWeek(d, 3);
        }else if(cmd.contains("thursday")){
            day = "thursday";
            d = adjustDayOfWeek(d, 4);
        }else if(cmd.contains("friday")){
            day = "friday";
            d = adjustDayOfWeek(d, 5);
        }else if(cmd.contains("saturday")){
            day = "saturday";
            d = adjustDayOfWeek(d, 6);
        }else if(cmd.contains("sunday")){
            day = "sunday";
            d = adjustDayOfWeek(d, 7);
        }

        if(cmd.contains("in")){
qDebug() << "FFFF in";
            QStringList w = cmd.split(" ");
            int p = w.indexOf("in");
            if(p != -1){
                p++;
                if(p < w.length()){
                    location = w[p];
                }
            }
        }/*else if(cmd.contains("for")){
            QStringList w = cmd.split(" ");
            int p = w.indexOf("for");
            if(p != -1){
                p++;
                if(p < w.length()){
                    location = w[p];
                }
            }
        }*/

        if(day != ""){
 qDebug() << "FFFF day" << day << location;
            int index = -1;
            l_weather.lock();
 qDebug() << "FFFF locked";
            Weather *w = NULL;
            if(location == ""){
                index = primary;
                w = (Weather *)weatherList.at(primary);
 qDebug() << "FFFF primary";
            }else{
 qDebug() << "FFFF searching";
                for(int x=0; x<weatherList.count(); x++){
                    Weather *f = (Weather *)weatherList.at(x);
                    if(f->sname().toLower().contains(location)){
                       w = f;
                       index = x;
                       break;
                    }
                }
            }

            if(index != -1 && w != NULL){
qDebug() << "FFFF found details";
                if(day == "current"){
                    QVariantMap c = w->current();
                    int temperature = 0;
                    QString summary;
                    if(!c["temperature"].isNull()){
                        temperature = static_cast<int>(c["temperature"].toDouble());
                    }

                    if(!c["summary"].isNull()){
                        summary = c["summary"].toString().replace(".","");
                    }

                    QString type = "weather";
                    if(temp){
                        type = "temperature";
                    }
                    if(location == "")
                        response = "Current "+type+" is "+QString::number(temperature)+" degrees Celsius and "+summary;
                    else
                        response = "In "+location+" current "+type+" is "+QString::number(temperature)+" degrees Celsius and "+summary;
                }else if(day == "avro" || day == "afternoon"){
                    QVariantList hourly = w->hourly();

                    qint64 at = (d.toMSecsSinceEpoch()/1000);
                    for(int x=0; x<hourly.length(); x++){
                        QVariantMap m = hourly[x].toMap();
                        if(!m["time"].isNull()){
                            qint64 dt = m["time"].toInt();
                            if(dt >= at){
                                //found it
                                int temperature = 0;
                                QString summary;

                                if(!m["temperature"].isNull()){
                                    temperature = static_cast<int>(m["temperature"].toDouble());
                                }

                                if(!m["summary"].isNull()){
                                    summary = m["summary"].toString().replace(".","");
                                }

                                if(!temp){
                                    if(location == "")
                                        response = "This afternoon, it's predicted to be "+summary+" with temperature of "+QString::number(temperature)+" degrees Celsius.";
                                    else
                                        response = "In "+location+" this afternoon, it's predicted to be "+summary+" with temperature of "+QString::number(temperature)+" degrees Celsius.";
                                }else{
                                    if(location == "")
                                        response = "This afternoon, it's predicted to be "+QString::number(temperature)+" degrees Celsius and "+summary;
                                    else
                                        response = "In "+location+" this afternoon, it's predicted to be "+QString::number(temperature)+" degrees Celsius and "+summary;
                                }
                                break;
                            }
                        }
                    }
                }else if(day == "tonight"){
                    QVariantList hourly = w->hourly();

                    qint64 at = (d.toMSecsSinceEpoch()/1000);
                    for(int x=0; x<hourly.length(); x++){
                        QVariantMap m = hourly[x].toMap();
                        if(!m["time"].isNull()){
                            qint64 dt = m["time"].toInt();
                            if(dt >= at){
                                //found it
                                int temperatureLow = 0;
                                QString summary;

                                if(!m["temperature"].isNull()){
                                    temperatureLow = static_cast<int>(m["temperature"].toDouble());
                                }

                                if(!m["summary"].isNull()){
                                    summary = m["summary"].toString().replace(".","");
                                }

                                if(!temp){
                                    if(location == "")
                                        response = "Tonight Weather, it's predicted to be "+summary+" with temperature of "+QString::number(temperatureLow)+" degrees Celsius.";
                                    else
                                        response = "Tonight in "+location+", it's predicted to be "+summary+" with temperature of "+QString::number(temperatureLow)+" degrees Celsius.";
                                }else{
                                    if(location == "")
                                        response = "Tonight temperature, it's predicted to be "+QString::number(temperatureLow)+" degrees Celsius.";
                                    else
                                        response = "Tonight in "+location+", it's predicted to be "+QString::number(temperatureLow)+" degrees Celsius.";
                                }
                                break;
                            }
                        }
                    }
                }else if(day == "tomorrow"){
                    QVariantList daily = w->dailysource();

                    qint64 at = (d.toMSecsSinceEpoch()/1000);
                    for(int x=0; x<daily.length(); x++){
                        QVariantMap m = daily[x].toMap();
                        if(!m["time"].isNull()){
                            qint64 dt = m["time"].toInt();
                            if(dt >= at){
                                //found it
                                int temperatureLow = 0;
                                int temperatureHigh = 0;
                                QString summary;

                                if(!m["temperatureHigh"].isNull()){
                                    temperatureHigh = static_cast<int>(m["temperatureHigh"].toDouble());
                                }

                                if(!m["temperatureLow"].isNull()){
                                    temperatureLow = static_cast<int>(m["temperatureLow"].toDouble());
                                }

                                if(!m["summary"].isNull()){
                                    summary = m["summary"].toString().replace(".","");
                                }

                                if(location == "")
                                    response = "Tomorrow Weather, it's predicted to be "+summary+" with low of "+QString::number(temperatureLow)+" and top of "+QString::number(temperatureHigh)+" degrees Celsius.";
                                else
                                    response = "Tomorrow in "+location+", it's predicted to be "+summary+" with low of "+QString::number(temperatureLow)+" and top of "+QString::number(temperatureHigh)+" degrees Celsius.";
                                break;
                            }
                        }
                    }
                }else if(day == "week"){
                    QVariantList daily = w->dailysource();

                    qint64 at = (d.toMSecsSinceEpoch()/1000);

                    int dayTemp1 = -255;
                    QString daySum1;
                    int dayTemp2 = -255;
                    QString daySum2;
                    int days = 0;

                    for(int x=0; x<daily.length(); x++){
                        QVariantMap m = daily[x].toMap();
                        if(!m["time"].isNull()){
                            qint64 dt = m["time"].toInt();
                            if(dt >= at){
                                //found it
                                if(days == 0){
                                    if(!m["temperatureHigh"].isNull()){
                                        dayTemp1 = static_cast<int>(m["temperatureHigh"].toDouble());
                                    }

                                    if(!m["summary"].isNull()){
                                        daySum1 = m["summary"].toString().replace(".","");
                                    }
                                }else if(days == 4){
                                    if(!m["temperatureHigh"].isNull()){
                                        dayTemp2 = static_cast<int>(m["temperatureHigh"].toDouble());
                                    }

                                    if(!m["summary"].isNull()){
                                        daySum2 = m["summary"].toString().replace(".","");
                                    }
                                    break;
                                }
                                days++;
                            }

                        }
                    }
                    if(dayTemp1 != -255 && dayTemp2 != -255){
                        if(location == "")
                            response = "This week weather, it's predicted to start "+daySum1+" and temperature of "+QString::number(dayTemp1)+", then end with "+daySum2+" with temperature of "+QString::number(dayTemp2)+" degrees Celsius.";
                        else
                            response = "This week weather in "+location+", it's predicted to start "+daySum1+" and temperature of "+QString::number(dayTemp1)+", then end "+daySum2+" with temperature of "+QString::number(dayTemp2)+" degrees Celsius.";
                    }
                }else if(day == "weekend"){
                    QVariantList daily = w->dailysource();

                    qint64 at = (d.toMSecsSinceEpoch()/1000);

                    int dayTemp1 = -255;
                    QString daySum1;
                    int dayTemp2 = -255;
                    QString daySum2;
                    bool first = true;
                    for(int x=0; x<daily.length(); x++){
                        QVariantMap m = daily[x].toMap();
                        if(!m["time"].isNull()){
                            qint64 dt = m["time"].toInt();
                            if(dt >= at){
                                //found it
                                if(first){
                                    if(!m["temperatureHigh"].isNull()){
                                        dayTemp1 = static_cast<int>(m["temperatureHigh"].toDouble());
                                    }

                                    if(!m["summary"].isNull()){
                                        daySum1 = m["summary"].toString().replace(".","");
                                    }

                                    first = false;
                                }else{
                                    if(!m["temperatureHigh"].isNull()){
                                        dayTemp2 = static_cast<int>(m["temperatureHigh"].toDouble());
                                    }

                                    if(!m["summary"].isNull()){
                                        daySum2 = m["summary"].toString().replace(".","");
                                    }
                                    break;
                                }
                            }

                        }
                    }
                    if(dayTemp1 != -255 && dayTemp2 != -255){
                        if(location == "")
                            response = "This weekend weather, it's predicted to start "+daySum1+" and temperature of "+QString::number(dayTemp1)+", then follow by "+daySum2+" with temperature of "+QString::number(dayTemp2)+" degrees Celsius.";
                        else
                            response = "This weekend weather in "+location+", it's predicted to start "+daySum1+" and temperature of "+QString::number(dayTemp1)+", then follow by "+daySum2+" with temperature of "+QString::number(dayTemp2)+" degrees Celsius.";
                    }
                }else if(day == "monday" || day == "tuesday" || day == "wednesday" || day == "thursday" || day == "friday" || day == "saturday" || day == "sunday"){
                    QVariantList daily = w->dailysource();

                    qint64 at = (d.toMSecsSinceEpoch()/1000);
                    for(int x=0; x<daily.length(); x++){
                        QVariantMap m = daily[x].toMap();
                        if(!m["time"].isNull()){
                            qint64 dt = m["time"].toInt();
                            if(dt >= at){
                                //found it
                                int temperatureLow = 0;
                                int temperatureHigh = 0;
                                QString summary;

                                if(!m["temperatureHigh"].isNull()){
                                    temperatureHigh = static_cast<int>(m["temperatureHigh"].toDouble());
                                }

                                if(!m["temperatureLow"].isNull()){
                                    temperatureLow = static_cast<int>(m["temperatureLow"].toDouble());
                                }

                                if(!m["summary"].isNull()){
                                    summary = m["summary"].toString().replace(".","");
                                }

                                if(location == "")
                                    response = "Weather for "+day+", it's predicted to be "+summary+" with low of "+QString::number(temperatureLow)+" and top of "+QString::number(temperatureHigh)+" degrees Celsius.";
                                else
                                    response = "In "+location+" for "+day+", it's predicted to be "+summary+" with low of "+QString::number(temperatureLow)+" and top of "+QString::number(temperatureHigh)+" degrees Celsius.";
                                break;
                            }
                        }
                    }
                }
            }

            l_weather.unlock();
qDebug() <<"VOICE Response" << response;
            if(index != -1)
                emit pageTrigger("weather", index);

            return true;
        }
    }
    return false;
}


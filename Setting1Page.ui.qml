import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    id: settingGallery
    width: 800
    height: 480

    background: Rectangle{
        color:"transparent"
    }

    property alias btAlbumlist: btAlbumlist
    property alias cbImageSlide: cbImageSlide
    property alias cbOtherSlide: cbOtherSlide

    header: Label {
        text: qsTr("Gallery")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    ComboBox {
        id: cbImageSlide
        x: 214
        y: 22
        model: settingGallery.slideList
        font.pointSize: 16
    }

    Label {
        id: label
        x: 32
        y: 32
        text: qsTr("Image Slide Interval")
        font.pointSize: 16
    }

    Label {
        id: label1
        x: 32
        y: 96
        text: qsTr("Other Slide Interval")
        font.pointSize: 16
    }

    ComboBox {
        id: cbOtherSlide
        x: 214
        y: 86
        model: settingGallery.intervalList
        font.pointSize: 16
    }

    Label {
        id: label2
        x: 32
        y: 200
        text: "Awake Brightess"
        font.pointSize: 16
    }

    Button {
        id: btAlbumlist
        x: 500
        y: 22
        width: 120
        height: 35
        text: qsTr("Album List")
    }
}

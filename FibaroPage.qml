import QtQuick 2.0
import QtQuick.Controls 2.4

Page {
    width: imageWidth
    height: imageHeight

    background: Image {
        anchors.fill: parent
        source: "files/icon/fibaroback.jpg"
        fillMode: Image.PreserveAspectCrop
    }

    Button {
        id: btLights
        x: 100
        y: 70
        width: 150
        height: 150
        padding: 10
        spacing: 10
        focusPolicy: Qt.NoFocus
        display: AbstractButton.IconOnly
        background: Rectangle {
            color: "transparent"
        }

        Image {
            id: btLightsImage
            anchors.fill: parent
            source: "files/icon/room.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Connections {
        target: btLights
        onClicked: {
            //controller.fibaroPopulate()
            fibaroRooms.visible = true
        }
    }


    Button {
        id: btScene
        x: 300
        y: 70
        width: 150
        height: 150
        padding: 20
        spacing: 20
        focusPolicy: Qt.NoFocus
        display: AbstractButton.IconOnly
        background: Rectangle {
            color: "transparent"
        }

        Image {
            id: btSceneImage
            anchors.fill: parent
            source: "files/icon/scene.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Button {
        id: btRoom
        x: 500
        y: 70
        width: 150
        height: 150
        padding: 10
        spacing: 10
        focusPolicy: Qt.NoFocus
        display: AbstractButton.IconOnly
        background: Rectangle {
            color: "transparent"
        }

        Image {
            id: btRoomImage
            anchors.fill: parent
            source: "files/icon/Devices.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Connections {
        target: btRoom
        onClicked: {

        }
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 50
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    FibaroRooms {
        id: fibaroRooms
        visible: false
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("FibaroPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

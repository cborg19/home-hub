#include <QtCore/qdebug.h>
#include <QFileInfo>
#include <QProcess>
#ifdef Q_OS_LINUX
#include <pigpio.h>
#endif
#include "gpio.h"

Gpio::Gpio(QObject *parent) : QObject(parent)
{
    #ifdef Q_OS_LINUX
    gpioCfgClock(10, 0, 10);
    if (gpioInitialise() < 0)
    {
       qDebug() << "PIG FAILED";
    }
    else
    {
        qDebug() << "PIG WORKS";
    }
    #endif
}

Gpio::~Gpio()
{
    #ifdef Q_OS_LINUX
    gpioTerminate();
    #endif
}

void Gpio::setAttachInput(int pin, void (*gpioAlertFunc_t) (int gpio, int level, uint32_t tick))
{
    #ifdef Q_OS_LINUX
    gpioSetAlertFunc(pin, gpioAlertFunc_t);
    #else
    Q_UNUSED(pin);
    #endif
    /*//first check to see if it has been initialised before
    QString path = "/sys/class/gpio/gpio"+pin;
    bool exists = QFileInfo::exists(path) && QFileInfo(path).isFile();
    if(!exists){
        QString cmd = "bash -c \"echo \""+pin+"\" > /sys/class/gpio/export\"";
        QProcess process;
        process.start(cmd);
        process.waitForFinished();
    }

    //Next is to set the direction
    QString cmd = "bash -c \"echo \"in\" > /sys/class/gpio/gpio"+pin+"/direction\"";
    QProcess process;
    process.start(cmd);
    process.waitForFinished();

    //gpiowatcher.addPath ("/sys/class/gpio/gpio"+pin+"/value");

    cmd = "bash -c \"echo both > /sys/class/gpio/gpio"+pin+"/edge\"";
    process.start(cmd);
    process.waitForFinished();

    //QObject::connect(&gpiowatcher, SIGNAL (fileChanged(QString)), this, slot);*/
}

void Gpio::setInput(int pin)
{
    #ifdef Q_OS_LINUX
    gpioSetMode(pin, PI_INPUT);
    #else
    Q_UNUSED(pin);
    #endif
    /*//first check to see if it has been initialised before
    QString path = "/sys/class/gpio/gpio"+pin;
    bool exists = QFileInfo::exists(path) && QFileInfo(path).isFile();
    if(!exists){
        QString cmd = "bash -c \"echo \""+pin+"\" > /sys/class/gpio/export\"";
        QProcess process;
        process.start(cmd);
        process.waitForFinished();
    }

    //Next is to set the direction
    QString cmd = "bash -c \"echo \"in\" > /sys/class/gpio/gpio"+pin+"/direction\"";
    QProcess process;
    process.start(cmd);
    process.waitForFinished();*/
}

void Gpio::setOutput(int pin)
{
    #ifdef Q_OS_LINUX
    gpioSetMode(pin, PI_OUTPUT);
    #else
    Q_UNUSED(pin);
    #endif
   /* //first check to see if it has been initialised before
    QString path = "/sys/class/gpio/gpio"+pin;
    bool exists = QFileInfo::exists(path) && QFileInfo(path).isFile();
    if(!exists){
        QString cmd = "bash -c \"echo \""+pin+"\" > /sys/class/gpio/export\"";
        QProcess process;
        process.start(cmd);
        process.waitForFinished();
    }

    //Next is to set the direction
    QString cmd = "bash -c \"echo \"out\" > /sys/class/gpio/gpio"+pin+"/direction\"";
    QProcess process;
    process.start(cmd);
    process.waitForFinished();*/
}

void Gpio::write(int pin, bool value)
{
    #ifdef Q_OS_LINUX
    if(value)
        gpioWrite(pin, 1);
    else gpioWrite(pin, 0);
    #else
    Q_UNUSED(pin);
    Q_UNUSED(value);
    #endif
 /*   QString cmd = "bash -c \"echo \"1\" > /sys/class/gpio/gpio"+pin+"/value\"";
    if(!value) cmd = "bash -c \"echo \"0\" > /sys/class/gpio/gpio"+pin+"/value\"";
    QProcess process;
    process.start(cmd);
    process.waitForFinished();*/
}

bool Gpio::read(int pin)
{
    #ifdef Q_OS_LINUX
    if(gpioRead(pin)) return true;
    return false;
    #else
    Q_UNUSED(pin);
    #endif
  /*  QString cmd = "bash -c \"cat /sys/class/gpio/gpio"+pin+"/value\"";
    QProcess process;
    process.start(cmd);
    process.waitForFinished();
    QString olist = process.readAllStandardOutput();
    if(olist.contains("1")) return true;
    return false;*/
    return false;
}

bool Gpio::openSerial()
{
    serialHandle = 0;
    #ifdef Q_OS_LINUX
    serialHandle = serOpen("/dev/serial0", 9600,0);
    #endif
    if(serialHandle >= 0) return true;
    return false;
}

QString Gpio::readSerial()
{
    QString data;
    #ifdef Q_OS_LINUX
    int bytesAvailable = serDataAvailable(serialHandle);
    if(bytesAvailable > 0){
        char *buf = new char[bytesAvailable];
        serRead(serialHandle, buf, bytesAvailable);
        data = QString(buf);
        delete buf;
    }
    #endif
    return data;
}


#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QObject>


struct NetworkManagerResult
{

    int errorResponse;
    QString response;
    QByteArray binary;
};


class NetworkManager : public QObject
{
    Q_OBJECT
public:
    explicit NetworkManager(QObject *parent = nullptr);


    NetworkManagerResult sendHTTPGetRequestSynchronous(QString url, int timeoutMilliseconds = 10000, QString header = "");
    bool canSend();

signals:
    void busyChanged(bool busy);



public slots:

private slots:




private:
    bool awaitingResponse;

};

#endif // NETWORKMANAGER_H

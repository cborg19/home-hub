function getUVIndex(value){
    if(value < 3) return "Low";
    if(value < 6) return "Moderate";
    if(value < 8) return "High";
    if(value < 11) return "Very High";
    return "Extrene";
}

function getVisibility(value){
    if(value < 3) return "Low";
    if(value < 6) return "Moderate";
    if(value < 8) return "Good";
    if(value < 13) return "Very Good";
    return "Excellent";
}

function timeToString(value){
    var date = new Date(value);
    if(time24hr){
        return Qt.formatDateTime(date, "HH:mm")
    }

    return Qt.formatDateTime(date, "h:mm A")
}

function time24String(value){
    var date = new Date();
    date.setTime(value*1000)
    return Qt.formatDateTime(date, "HH:mm")
}

function dayofWeek(value){
    var date = new Date();
    date.setTime(value*1000)
    return Qt.formatDateTime(date, "dddd")
}

function getDateToString(value){
    var date = new Date(value);
    return Qt.formatDateTime(date, "dddd, d MMMM yyyy HH:mm")
}

function roundDown(value){
    return Math.floor(value).toString();
}

function milltoString(milliseconds){
    //Get hours from milliseconds
    var hours = milliseconds / (1000*60*60);
    var h = Math.floor(hours);

    //Get remainder from hours and convert to minutes
    var minutes = (hours - h) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' +  absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    if(h !== 0)
        return h + ':' + m + ':' + s;
    else return m + ':' + s;
}

function fileext(fname){
  return fname.slice((fname.lastIndexOf(".") - 1 >>> 0) + 2);
}

function basename(fname) {
    return fname.split(".").slice(0, -1).join(".");
}

function filename(fname) {
    var f = fname.toString();
    return f.substring(f.lastIndexOf('/')+1);
}

#include <QDateTime>
#include <QtCore/qdebug.h>
#include "lightsensor.h"

#define TSL_S3       23


LightSensor::LightSensor(Gpio *gpio, QObject *parent) : QObject(parent), m_gpio(gpio)
{
    Initialise();
}

LightSensor::LightSensor(QObject *parent) : QObject(parent), m_gpio(NULL)
{
}

void LightSensor::setGPIO(Gpio *gpio)
{
    m_gpio = gpio;
    #ifdef Q_OS_LINUX
    Initialise();
    #endif
}


void LightSensor::Initialise()
{
    if(!m_gpio) return;


    m_gpio->setInput(TSL_S3);


    m_gpio->openSerial();
/*    #ifdef Q_OS_LINUX
    serial.setPortName("serial0");
    serial.open(QIODevice::ReadWrite);

    serial.setBaudRate(QSerialPort::Baud9600);
    serial.setDataBits(QSerialPort::Data8);
    serial.setParity(QSerialPort::NoParity);
    serial.setStopBits(QSerialPort::OneStop);
    serial.setFlowControl(QSerialPort::NoFlowControl);

    serial.open(QIODevice::ReadWrite);
    if (serial.isOpen() && serial.isWritable())
    {
        qDebug() << "XXXXXXXXXXXXXX Serial is open";
    }else qDebug() << "XXXXXXXXXXXXXX FAILED";
    #endif*/
}

int LightSensor::getValue()
{
    bool r_Up = m_gpio->read(TSL_S3);
    int value = 0;
//    qDebug() << "LIGHT" << r_Up;
//qDebug() << "LightSensor getValue" << r_Up;
/*    #ifdef Q_OS_LINUX
    QByteArray input = serial.readAll();
    QString s = QString::fromUtf8(input);
    if (s.toUtf8() != input) {
      s = QString::fromLatin1(input);
    }
//qDebug() << "LIGHT" << input;
    QStringList l = s.split("\r\n");
    if(l.length() > 1){
//qDebug() << "LIGHT A" << input << l;
        QStringList w = l[l.length()-2].trimmed().split(" ");
        if(w.length() > 0){
            value = w[0].trimmed().toInt();
  //          qDebug() << "LIGHT B" << w[0];
        }
    }else{
     //   if(r_Up && value == 0)
     //       value = 50;
    }
    #endif*/

    QString s = m_gpio->readSerial();
    QStringList l = s.split("\r\n");
    if(l.length() > 1){
//qDebug() << "LIGHT A" << s << l;
        int v = 0;
        int len = l.length();
        if(len > 5) len = 5;
        for(int x=0; x<l.length(); x++){
            QStringList w = l[x].trimmed().split(" ");
            if(w.length() == 2){
                v += w[0].trimmed().toInt();
                v /= 2;
            }
        }
        value = v;
//        QStringList w = l[l.length()-2].trimmed().split(" ");
//        if(w.length() > 0){
//            value = w[0].trimmed().toInt();
//            qDebug() << "LIGHT B" << w[0];
//        }
    }

    if(l_last.tryLock()){
        lastValue = value;
        l_last.unlock();
    }
    return value;
}

/*

pinB  Chip ColourWrite GPIO
1 GND 3/4
2 S3  8    grey        gpio 23   4
3 S2  7    orange      gpio 22   3
4 S1  2    purple      gpio 17   0
5 S0  1    yellow      gpio 18   1
6 OUT 6    green       gpio 27   2
7 VCC 5

*/
/*
#define TSL_FREQ_PIN 27 //2 // output use digital pin2 for interrupt
#define TSL_S0       24
#define TSL_S1       17
#define TSL_S2       22
#define TSL_S3       23

#define HIGH true
#define LOW false

#define READ_TM 1000

#define SAMPLESIZE 20

long long pulse_cnt = 0;

LightSensor::LightSensor(Gpio *gpio, QObject *parent) : QObject(parent), m_gpio(gpio)
{
    Initialise();
}

LightSensor::LightSensor(QObject *parent) : QObject(parent), m_gpio(NULL)
{
}

void LightSensor::setGPIO(Gpio *gpio)
{
    m_gpio = gpio;
    #ifdef Q_OS_LINUX
//    Initialise();
    #endif
}

void aFunction(int gpio, int level, uint32_t tick)
{
    if(TSL_FREQ_PIN == gpio && level == 1){
        pulse_cnt++;
    }
}

void LightSensor::Initialise()
{
/*    if(!m_gpio) return;

    m_gpio->setAttachInput(TSL_FREQ_PIN, aFunction);

    m_gpio->setOutput(TSL_S0);
    m_gpio->setOutput(TSL_S1);
    m_gpio->setOutput(TSL_S2);
    m_gpio->setOutput(TSL_S3);

    m_gpio->write(TSL_S0, HIGH);
    m_gpio->write(TSL_S1, LOW);
    m_gpio->write(TSL_S2, HIGH);
    m_gpio->write(TSL_S3, HIGH);

    freq_mult = 100;
    calc_sensitivity = 10;

    pulse_cnt = 0;

    lastValue = 0;
    pos = 0;
    for(int x=0; x<SAMPLESIZE; x++)
        sample[x] = 0;

//    gpiowatcher.addPath ("/sys/class/gpio/gpio27/value");

//    QObject::connect(&gpiowatcher, SIGNAL (fileChanged(QString)), this, SLOT(interrupt(QString)));
*//*
}

int LightSensor::getValue()
{
    // get our current frequency reading

    long long frequency = get_tsl_freq();

    // calculate radiant energy

    float uw_cm2 = calc_uwatt_cm2( frequency );

    // calculate illuminance

    float lux = calc_lux_single( uw_cm2, 0.175 );

    int v = static_cast<int>(lux);

    sample[pos] = v;
    pos++;
    if(pos >= SAMPLESIZE) pos = 0;

    lastValue = 0;
    for(int x=0; x<SAMPLESIZE; x++){
        if(sample[x] == 0) continue;

        lastValue += sample[x];
        lastValue /= 2;
    }

    //qDebug() << "luxlastvalue" << lastValue;

    //emit sensorChanged();
    return lastValue;
}

long long LightSensor::get_tsl_freq() {

  // we have to scale out the frequency --
  // Scaling on the TSL230R requires us to multiply by a factor
  // to get actual frequency

  long long freq = pulse_cnt * freq_mult;

    // reset the pulse counter

  pulse_cnt = 0;

  return(freq);
}

float LightSensor::calc_lux_single(float uw_cm2, float efficiency) {

  // calculate lux (lm/m^2), using standard formula:
  // Xv = Xl * V(l) * Km
  // Xl is W/m^2 (calculate actual receied uW/cm^2, extrapolate from sensor size (0.0136cm^2)
  // to whole cm size, then convert uW to W)
  // V(l) = efficiency function (provided via argument)
  // Km = constant, lm/W @ 555nm = 683 (555nm has efficiency function of nearly 1.0)
  //
  // Only a single wavelength is calculated - you'd better make sure that your
  // source is of a single wavelength... Otherwise, you should be using
  // calc_lux_gauss() for multiple wavelengths

    // convert to w_m2

  float w_m2 = (uw_cm2 / (float) 1000000) * (float) 100;

    // calculate lux

  float lux = w_m2 * efficiency * (float) 683;

  return(lux);
}

float LightSensor::calc_uwatt_cm2(long long freq) {

  // get uW observed - assume 640nm wavelength
  // calc_sensitivity is our divide-by to map to a given signal strength
  // for a given sensitivity (each level of greater sensitivity reduces the signal
  // (uW) by a factor of 10)

  float uw_cm2 = (float) freq / (float) calc_sensitivity;

    // extrapolate into entire cm2 area

  uw_cm2 *= ( (float) 1 / (float) 0.0136 );

  return(uw_cm2);

}

void LightSensor::set_scaling ( int what ) {

  // set output frequency scaling
  // adjust frequency multiplier and set proper pin values
  // e.g.:
  // scale = 2 == freq_mult = 2
  // scale = 10 == freq_mult = 10
  // scale = 100 == freq_mult = 100

  int pin_2 = HIGH;
  int pin_3 = HIGH;

  switch( what ) {
    case 2:
      pin_3 = LOW;
      freq_mult = 2;
      break;
    case 10:
      pin_2 = LOW;
      freq_mult = 10;
      break;
    case 100:
      freq_mult = 100;
      break;
    default:
        // don't do anything with levels
        // we don't recognize
      return;
  }

    // set the pins to their appropriate levels
  m_gpio->write(TSL_S2, pin_2);
  m_gpio->write(TSL_S3, pin_3);

  return;
}

void LightSensor::sensitivity( bool dir ) {

  // adjust sensitivity in 3 steps of 10x either direction

  int pin_0;
  int pin_1;

  if( dir == true ) {

      // increasing sensitivity

      // -- already as high as we can get
    if( calc_sensitivity == 1000 )
      return;

    if( calc_sensitivity == 100 ) {
        // move up to max sensitivity
      pin_0 = HIGH;
      pin_1 = HIGH;
    }
    else {
        // move up to med. sesitivity
      pin_0 = LOW;
      pin_1 = HIGH;
    }

      // increase sensitivity divider
    calc_sensitivity *= 10;
  }
  else {
      // reducing sensitivity

      // already at lowest setting

    if( calc_sensitivity == 10 )
      return;

    if( calc_sensitivity == 100 ) {
        // move to lowest setting
      pin_0 = HIGH;
      pin_1 = LOW;
    }
    else {
        // move to medium sensitivity
      pin_0 = LOW;
      pin_1 = HIGH;
    }

      // reduce sensitivity divider
    calc_sensitivity = calc_sensitivity / 10;
  }

    // make any necessary changes to pin states
  m_gpio->write(TSL_S0, pin_0);
  m_gpio->write(TSL_S1, pin_1);

 return;
}*/

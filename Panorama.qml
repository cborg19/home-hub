import QtQuick 2.11
import QtQuick.Controls 2.4

Item {
     id: panorama
     width: imageWidth
     height: imageHeight

     MouseArea {
         id: btInfo2
         x:300
         y:140
         z: 2
         height: 340
         width: 500
         onClicked: {
             if(view.currentId === slideId){
                 console.log("Panorama switchToInfo");
                 window.switchToInfo(true);
             }
         }
     }

     Canvas {
         id: panCanvas
         width: imageWidth
         height: imageHeight

         property int test: view.currentId === slideId ? shown() : stopped()

         property string imagefile: galleryPath + sourceList[0].name
         property int panWidth: 0
         property int panHeight: 0
         property bool initialised: false
         property int w: 0
         property int src_h: 0
         property int distance: 0
         property int pos: 0

         property int display_time: slideInterval*2 - 5000
         property int frame_time: (1 / frames_per_second) * 1000
         property date start_time: new Date()
         property bool start: false

         property Image panorama: Image {
             id: img_file
             //source: galleryPath + imageSource.get(0).name
             Component.onCompleted: {
                 if(img_file.sourceSize.width !== 0){
                     panCanvas.panWidth = sourceList[0].width
                     panCanvas.panHeight = sourceList[0].height
                     panCanvas.fit(panCanvas.panWidth, panCanvas.panHeight, imageWidth, imageHeight);
                 }
             }
         }

         Component.onCompleted:{
             console.log("HHH",name,type)
              delayLoad.start()
         }

         onPaint: {
             if (panCanvas.initialised) {
                var ctx = getContext("2d");
                ctx.save();
                ctx.drawImage(panCanvas.imagefile, panCanvas.pos, 0, panCanvas.w, panCanvas.src_h, 0, 0, imageWidth, imageHeight);
                ctx.restore();
             }
         }

         BusyIndicator {
            id: busyIndication
            anchors.centerIn: parent
            // 'running' defaults to 'true'
         }

         function loadStatus(){
             console.log("LoadStatus",img_file.sourceSize.width);
             if(img_file.sourceSize.width !== 0){
                 panCanvas.panWidth = img_file.sourceSize.width
                 panCanvas.panHeight = img_file.sourceSize.height
                 panCanvas.fit(panCanvas.panWidth, panCanvas.panHeight, imageWidth, imageHeight);
                 panCanvas.initialised = true;
                 busyIndication.running = false;
                 panCanvas.render_image()
                // console.log('loadStatus set to True');
                 return true
             }
             return false
         }

         Timer {
            id: delayLoad
            interval: 2000
            running: false
            repeat: false
            onTriggered:{
              //  console.log("delay");
                img_file.source = galleryPath + sourceList[0].name
                if(panCanvas.loadStatus()){
                    delayStatus.start();
                }

            }
        }

        Timer {
            id: delayStatus
            interval: 1000
            running: false
            repeat: true
            onTriggered:{
                if(panCanvas.loadStatus()){
                    delayStatus.stop();
                    if(panCanvas.start){
                        panCanvas.start_time = new Date()
                        moveTimer.start();
                    }
                }
            }
        }

        Button {
             id: leftButton
             x: 0
             y: 100
             width: 200
             height: 250
             background: Rectangle {
                 color: "transparent"
             }

             MouseArea {
                anchors.fill: parent
                onPressed:{
                    flickTimer.stop();
                    moveTimer.stop();
                    leftTimer.start();
                }
                onReleased:{
                    panCanvas.start_time = new Date()
                    flickTimer.start();
                    moveTimer.start();
                    leftTimer.stop();
                }
             }
         }

         Button {
             id: rightButton
             x: 600
             y: 100
             width: 200
             height: 250
             background: Rectangle {
                 color: "transparent"
             }

             MouseArea {
                anchors.fill: parent
                onPressed:{
                    flickTimer.stop();
                    moveTimer.stop();
                    rightTimer.start();
                }
                onReleased:{
                    panCanvas.start_time = new Date()
                    flickTimer.start();
                    moveTimer.start();
                    rightTimer.stop();
                }
             }
         }

         function render_image() {
             // Renders a frame of the effect
             if (pos < 0 || pos === undefined) {
                 return;
             }

             panCanvas.requestPaint();
         }

         function fit(srcw, srch, dstw, dsth) {
           //  console.log('fit',srcw, srch, dstw, dsth)
             // Finds the best-fit rect so that the destination can be covered
             var sh = srch / dsth;
             panCanvas.w = dstw * sh;
             panCanvas.src_h = srch;
             panCanvas.distance = srcw - w;
         }

         function get_time() {
             var d = new Date();
             return d.getTime() - panCanvas.start_time.getTime();
         }

         Timer {
            id: leftTimer
            interval: panCanvas.frame_time
            running: false
            repeat: true
            onTriggered:{
                if(panCanvas.pos > 0)
                    panCanvas.pos -= 5;

                panCanvas.render_image()
            }
         }

         Timer {
            id: rightTimer
            interval: panCanvas.frame_time
            running: false
            repeat: true
            onTriggered:{
                if(panCanvas.pos > panCanvas.distance)
                    panCanvas.pos = panCanvas.distance
                else
                    panCanvas.pos += 5;

                panCanvas.render_image()
            }
        }

        Timer {
            id: moveTimer
            interval: panCanvas.frame_time
            running: false
            repeat: true
            onTriggered:{
                var update_time = panCanvas.get_time();

                var x_frame = Math.floor(update_time / panCanvas.display_time * panCanvas.distance);

                var stop = false;
                if (update_time >= panCanvas.display_time){
              //      console.log("stoptime",update_time);
                    stop = true;
                }

                if (stop) {
             //     console.log("stop timer 2");
                  moveTimer.stop();
                  return;
                }

                panCanvas.pos = x_frame
                panCanvas.render_image()
            }
        }

        Timer {
           id: delayLoadStart
           interval: 2000
           running: false
           repeat: false
           onTriggered:{
             //  console.log('delayLoadStart');
               moveTimer.start();
           }
        }

        /*Connections {
            target: panorama
            onVisibleChanged:
                if(visible){
                    console.log("PANORAMA Shown")
                    shown();
                }else{
                    console.log("PANORAMA stopped")
                    stopped();
                }

            ignoreUnknownSignals: true
        }*/

        function shown(){
            console.log('SHOWN PAN', sourceList[0].name);
            if(!panCanvas.initialised){
                if(panCanvas.loadStatus()){
                    //console.log("PB")
                    delayStatus.stop();
                }
            }

            panCanvas.start = true
            if(panCanvas.initialised){
                //console.log("PC")
                delayStatus.stop();
                panCanvas.start_time = new Date()
                delayLoadStart.start();
            }
            return 1
        }

        function stopped(){
            panCanvas.start = false
            moveTimer.stop();
            delayLoadStart.stop();
            return 1
        }
     }
}

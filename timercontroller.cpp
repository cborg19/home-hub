#include "timercontroller.h"
#include <QVariant>
#include <QtCore/qdebug.h>
#include "global.h"

TimerController::TimerController(QObject *parent) : QObject(parent)
{
    connect(&t_timer, &QTimer::timeout, this, &TimerController::updateTimer);
    t_timer.setInterval(1000);
    t_timer.setSingleShot(false);

    connect(this, &TimerController::startTimer, this, &TimerController::timerStart);
    connect(this, &TimerController::stopTimer, this, &TimerController::timerStop);
}

QVariantList TimerController::getTimerList()
{
    QVariantList l;
    l_list.lock();
    l = m_list;
    l_list.unlock();
    return l;
}

void TimerController::timerStart()
{
    if(!t_timer.isActive()){
        lastTime = QDateTime::currentDateTime();
qDebug() <<"START TIMER";
        t_timer.start();
    }
}

void TimerController::timerStop()
{
    t_timer.stop();
}

unsigned int TimerController::fromString(QStringList l)
{
    unsigned int time = 0;
    //5 minutes, 1 hour and 10 minutes, 1 hour and 5 seconds, 1 hour 10 minutes and 20 seconds
    int hs = l.indexOf("hour");
    hs--;
    if(hs < l.length() && hs > -1){
        QString s = l[hs].trimmed();
        int v = s.toInt(0);
        if(v > 0){
            time += static_cast<unsigned int>(v) * 60 * 60 * 1000;
        }
    }

    int ms = l.indexOf("minute");
    ms--;
    if(ms < l.length() && ms > -1){
        QString s = l[ms].trimmed();
        int v = s.toInt(0);
        if(v > 0){
            time += static_cast<unsigned int>(v) * 60 * 1000;
        }
    }

    int ss = l.indexOf("second");
    ss--;
    if(ss < l.length() && ss > -1){
        QString s = l[ss].trimmed();
        int v = s.toInt(0);
        if(v > 0){
            time += static_cast<unsigned int>(v) * 1000;
        }
    }

    return time;
}

void TimerController::updateTimer()
{
    QDateTime now = QDateTime::currentDateTime();
    qint64 millisecondsDiff = lastTime.msecsTo(now);

    bool stop = true;
    l_list.lock();
    for(int x=0; x<m_list.length(); x++){
        QVariantMap t = m_list[x].toMap();
        if(!t["enable"].toBool())continue;
        unsigned int c = t["current"].toUInt();
        if(c <= 0) continue;

        c -= millisecondsDiff;

        if(c > 4000000000){
            qDebug() << "updateTimer trigger";
            t["enable"] = false;
            t["current"] = 0;
            t["percentage"] = 1.0;
            t["currentStr"] = "Done";

            QVariantMap alarm;
            alarm["enabled"] = true;
            alarm["message"] = t["name"];
            alarm["snooze"] = false;
            alarm["clock"] = false;

            alarm["video"] = "";
            alarm["sound"] = "standard";
            emit triggerAlarm(alarm);

            m_list[x] = t;
            continue;
        }
        t["current"] = c;
        unsigned int l = t["length"].toUInt();
        unsigned int d = l - c;

        double p = (static_cast<double>(d)/static_cast<double>(l));
        t["percentage"] = p;

        t["currentStr"] = toTimeString(c);

        stop = false;
        m_list[x] = t;
    }
    l_list.unlock();

    emit timerListChanged();
    if(stop){
        emit stopTimer();
    }
    lastTime = now;
}

void TimerController::createTimer(QString Title, unsigned int time)
{
    QVariantMap m_time;
    m_time.insert("name", toCamelCase(Title));
    m_time.insert("length",time);
    m_time.insert("current",time);
    m_time.insert("percentage",0.0);
    m_time.insert("currentStr",toTimeString(time));
    m_time.insert("strlength","");
    m_time.insert("enable",true);

    l_list.lock();
    m_list.push_back(m_time);
    l_list.unlock();

    emit timerListChanged();
    emit startTimer();
}

void TimerController::pauseTimer(int index)
{
    l_list.lock();
    if(index < m_list.length() && index >= 0){
        QVariantMap t = m_list[index].toMap();
        t["enable"] = false;
        m_list[index] = t;
        if(m_list.length() == 1) emit stopTimer();
    }
    l_list.unlock();

    emit timerListChanged();
}

void TimerController::resumeTimer(int index)
{
    l_list.lock();
    if(index < m_list.length() && index >= 0){
        QVariantMap t = m_list[index].toMap();
        t["enable"] = true;
        m_list[index] = t;
    }
    l_list.unlock();

    emit timerListChanged();
    emit startTimer();
}

void TimerController::cancelTimer(int index)
{
    l_list.lock();
    if(index < m_list.length() && index >= 0){
        m_list.removeAt(index);
        if(m_list.length() == 1) emit stopTimer();
    }
    l_list.unlock();

    emit timerListChanged();
}

void TimerController::addMinuteToTimer(int index)
{
    l_list.lock();
    if(index < m_list.length() && index >= 0){
        QVariantMap t = m_list[index].toMap();
        t["length"] = t["length"].toUInt()+60*1000;
        t["current"] = t["current"].toUInt()+60*1000;
        m_list[index] = t;
    }
    l_list.unlock();

    emit timerListChanged();
}

bool TimerController::processVoiceCmd(QString cmd, QString &response)
{
    /*
    set Cookie timer for 5 minutes
    set breat timer for 1 hour and 10 minutes
    cancel timer
    cancel bread timer
    cancel all timers
    pause timer
    pause bread timer
    resume timer
    resume bread timer
    add 5 minutes to bread timer

    ok 5 minutes starting now
    second timer cookie for 8 minutes, starting now
    third timer bread for 12 minutes, starting now
    */

    if(cmd.contains("go to") && cmd.contains("timer")){
        emit pageTrigger("timer", 0);
        return true;
    }

    if(cmd.contains("timer")){
        cmd.replace("hours", "hour");
        cmd.replace("minutes", "minute");
        cmd.replace("seconds", "second");
        QStringList l = cmd.split(" ");

        if(cmd.contains("set")){
            QString name;
            //get name of the timer
            int p = l.indexOf("set");
            p++;
            if(p < l.length()){
                QString s = l[p].trimmed();
                if(s != "timer"){
                    name = s;
                }
            }
            //Get length
            int f = l.indexOf("for");
            f++;
            QStringList len;
            for(int x=f; x<l.length(); x++){
                len.push_back(l[x]);
            }
            unsigned int time = fromString(len);

            QString timeStr = len.join(" ");
            QVariantMap m_time;
            m_time.insert("name", toCamelCase(name));
            m_time.insert("length",time);
            m_time.insert("current",time);
            m_time.insert("percentage",0.0);
            m_time.insert("currentStr",toTimeString(time));
            m_time.insert("strlength",timeStr);
            m_time.insert("enable",true);
            int t = 0;
            l_list.lock();
            m_list.push_back(m_time);
            t = m_list.length();
            l_list.unlock();

            if(t == 1){
                if(name == ""){
                    name = timeStr;
                }
                response = "ok "+name+" timer starting now";
            }else{
                if(t == 2)
                    response = "second timer ";
                else if(t == 3)
                    response = "third timer ";
                else if(t == 4)
                    response = "fourth timer ";
                if(name != ""){
                   response += name + " ";
                }

                response += "for "+timeStr+", starting now";
            }
            emit pageTrigger("timer", 0);
            emit timerListChanged();

            emit startTimer();
        }else if(cmd.contains("pause")){
            QString name;
            int p = l.indexOf("pause");
            p++;
            if(p < l.length()){
                QString s = l[p].trimmed();
                if(s != "timer"){
                    name = s;
                }
            }

            if(name == "all"){
                l_list.lock();
                for(int x=0; x<m_list.length(); x++){
                    QVariantMap t = m_list[x].toMap();
                    t["enable"] = false;
                    m_list[x] = t;
                }

                l_list.unlock();

                response += "all timers paused";
                emit stopTimer();
                emit timerListChanged();
            }else if(name != ""){
                l_list.lock();
                for(int x=0; x<m_list.length(); x++){
                    QVariantMap t = m_list[x].toMap();
                    if(t["name"].toString().toLower() == name){
                        t["enable"] = false;
                        m_list[x] = t;
                        break;
                    }
                }

                if(m_list.length() == 1){
                   emit stopTimer();
                }

                l_list.unlock();

                response += name+" timer paused";
                emit timerListChanged();
            }else{
                l_list.lock();
                if(m_list.length() == 0){

                }else if(m_list.length() == 1){
                    emit stopTimer();
                    QVariantMap t = m_list[0].toMap();
                    t["enable"] = false;
                    m_list[0] = t;
                    response += "timer paused";
                    emit timerListChanged();
                }else if(m_list.length() > 1){
                    response = "Please specify which timers";
                }
                l_list.unlock();
            }
        }else if(cmd.contains("resume")){
            QString name;
            int p = l.indexOf("resume");
            p++;
            if(p < l.length()){
                QString s = l[p].trimmed();
                if(s != "timer"){
                    name = s;
                }
            }

            if(name == "all"){
                l_list.lock();
                for(int x=0; x<m_list.length(); x++){
                    QVariantMap t = m_list[x].toMap();
                    t["enable"] = true;
                    m_list[x] = t;
                }

                l_list.unlock();

                response += "all timers resumed";
                emit startTimer();
                emit timerListChanged();
            }else if(name != ""){
                l_list.lock();
                for(int x=0; x<m_list.length(); x++){
                    QVariantMap t = m_list[x].toMap();
                    if(t["name"].toString().toLower() == name){
                        t["enable"] = true;
                        m_list[x] = t;
                        break;
                    }
                }

                if(m_list.length() > 0){
                   emit startTimer();
                }

                l_list.unlock();

                response += name+" timer resumed";
                emit timerListChanged();
            }else{
                l_list.lock();
                if(m_list.length() == 0){

                }else if(m_list.length() == 1){
                    QVariantMap t = m_list[0].toMap();
                    t["enable"] = true;
                    m_list[0] = t;
                    response += "timer resumed";
                    if(m_list.length() > 0){
                       emit startTimer();
                    }
                    emit timerListChanged();
                }else if(m_list.length() > 1){
                    response = "Please specify which timers";
                }
                l_list.unlock();
            }
        }else if(cmd.contains("add")){
            QString name;
            //get name of the timer
            int p = l.indexOf("to");
            p++;
            if(p < l.length()){
                QString s = l[p].trimmed();
                if(s != "timer"){
                    name = s;
                }
            }

            int f = l.indexOf("add");
            f++;
            QStringList len;
            for(int x=f; x<l.length(); x++){
                len.push_back(l[x]);
            }
            unsigned int time = fromString(len);

            l_list.lock();
            for(int x=0; x<m_list.length(); x++){
                QVariantMap t = m_list[x].toMap();
                if(t["name"].toString().toLower() == name){
                    QVariantMap t = m_list[x].toMap();
                    t["length"] = t["length"].toUInt()+time;
                    t["current"] = t["length"].toUInt()+time;
                    m_list[x] = t;
                    break;
                }
            }
            l_list.unlock();

            emit timerListChanged();
            response = "Added extra time to "+name+" timer";
        }else if(cmd.contains("cancel")){
            QString name;
            //get name of the timer
            int p = l.indexOf("cancel");
            p++;
            if(p < l.length()){
                QString s = l[p].trimmed();
                if(s != "timer"){
                    name = s;
                }
            }

            if(name == "all"){
                l_list.lock();
                m_list.clear();
                l_list.unlock();

                response += "all timers stopped";
                emit stopTimer();
                emit timerListChanged();
                emit pageTrigger("standard", 0);
            }else if(name != ""){
                l_list.lock();
                for(int x=0; x<m_list.length(); x++){
                    QVariantMap t = m_list[x].toMap();
                    if(t["name"].toString().toLower() == name){
                        m_list.removeAt(x);
                        break;
                    }
                }

                if(m_list.length() < 1){
                    emit stopTimer();
                    emit pageTrigger("standard", 0);
                }
                l_list.unlock();

                emit timerListChanged();
                response += "timer "+name+" stopped";
            }else{
                l_list.lock();
                if(m_list.length() == 0){

                }else if(m_list.length() == 1){
                    emit stopTimer();
                    m_list.clear();
                    response += "timer stopped";

                    emit timerListChanged();
                    emit pageTrigger("standard", 0);
                }else if(m_list.length() > 1){
                    response = "Please specify which timers";
                }
                l_list.unlock();
            }
        }
        return true;
    }

    return false;
}

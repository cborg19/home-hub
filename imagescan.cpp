#include "imagescan.h"

#include <QtCore/qdebug.h>
#include <QDirIterator>
#include <QImageReader>
#include <QUuid>
#include <time.h>
#include <QDateTime>
#include <QMovie>
#include "global.h"
#include <QCryptographicHash>

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#define COLLAGE_SIZE     9
/*
SlideObject::SlideObject(QObject *parent)
    : QObject(parent)
{
}

SlideObject::SlideObject(const QString &name, const QString &type, const QString &slideId, const QVariantList &source, QObject *parent)
    : QObject(parent), m_name(name), m_type(type), m_slideId(slideId), m_source(source)
{
}

SlideObject::~SlideObject()
{

}

QString SlideObject::name() const
{
    return m_name;
}

void SlideObject::setName(const QString &name)
{
    if (name != m_name) {
        m_name = name;
        emit nameChanged();
    }
}

QString SlideObject::type() const
{
    return m_type;
}

void SlideObject::setType(const QString &type)
{
    if (type != m_type) {
        m_type = type;
        emit typeChanged();
    }
}

QString SlideObject::slideId() const
{
    return m_slideId;
}

void SlideObject::setSlideId(const QString &slideId)
{
    if (slideId != m_slideId) {
        m_slideId = slideId;
        emit slideIdChanged();
    }
}

QVariantList SlideObject::source() const
{
    return m_source;
}

void SlideObject::setSource(const QVariantList &source)
{
    if (source != m_source) {
        m_source = source;
        emit sourceChanged();
    }
}
*/
//---------------------------------------------------------------------

Imagescan::Imagescan(QObject *parent) : QObject(parent)
{
    qDebug() << "Seed ImagesScan" << QTime::currentTime().msec();
    qsrand(QTime::currentTime().msec());

    m_slideInterval = 30000;
    m_weatherInterval = 10;
    m_otherInterval = 20;
    scan_day = -1;
    m_doWeather = true;
    nameFilters << "*.jpg" << "*.JPEG" << "*.png" << "*.PNG" << "*.jpeg" << "*.JPEG" << "*.mp4" << "*.MP4";// << "*.MOV" << "*.mov";
//TODO    fileGalleryList = new QList<QObject*>;
}

QString Imagescan::pathName()
{
    return m_pathName;
}

void Imagescan::setPathName(const QString &pathName)
{
    if (pathName == m_pathName)
        return;

    m_pathName = pathName;
}

int Imagescan::slideInterval()
{
    return m_slideInterval;
}

void Imagescan::setslideInterval(const int &source)
{
    if (source == m_slideInterval)
        return;

    m_slideInterval = source;
    emit slideIntervalChanged();
}

int Imagescan::weatherInterval()
{
    return m_weatherInterval;
}

void Imagescan::setWeatherInterval(const int &source)
{
    if (source == m_weatherInterval)
        return;

    m_weatherInterval = source;
    emit weatherIntervalChanged();
}

int Imagescan::otherInterval()
{
    return m_otherInterval;
}

void Imagescan::setOtherInterval(const int &source)
{
    if (source == m_otherInterval)
        return;

    m_otherInterval = source;
    emit otherIntervalChanged();
}

void Imagescan::clearAll()
{
    //qDeleteAll(fileGalleryList);
    //for(int x=0; x<fileGalleryList.count(); x++){
        //delete fileGalleryList->at(x);
    //}
    fileGalleryList.clear();
}

Q_INVOKABLE void Imagescan::scanGallery(){
    qint64 start = QDateTime::currentMSecsSinceEpoch();

    qDebug() << "Seed ImagesScan" << QTime::currentTime().msec();
    qsrand(QTime::currentTime().msec());

    qDebug() << "scanGallery " << m_pathName;
    clearAll();

    //assume the directory exists and contains some files and you want all jpg and JPG files
    QDirIterator it(m_pathName+"assets/", nameFilters, QDir::Files);

    QList<QVariantMap> protraits;
//int count = 0;
    while (it.hasNext()) {
        QString path = it.next();

        QFileInfo fi(path);
        QString ext = fi.suffix().toLower();
        QString name = fi.fileName();
        QString uuid = QUuid::createUuid().toString();
        QVariantList source;
        if(ext == "mp4" || ext == "mov"){
            //Video
            QVariantMap newElement;  // QVariantMap will implicitly translates into JS-object
            newElement.insert("name", "assets/"+name);

            source.push_back(newElement);

            QVariantMap slide;
            slide.insert("name", "video");
            slide.insert("type", "Video");
            slide.insert("slideId", uuid);
            slide.insert("sourceList",source);
#ifdef Q_OS_LINUX
            getVideoImages(m_pathName+"assets/"+name);
            fileGalleryList.push_back(slide);
#endif
        }else{
            QImageReader reader(path);
            QSize sizeOfImage = reader.size();
            int height = sizeOfImage.height();
            int width = sizeOfImage.width();

            QVariantMap newElement;  // QVariantMap will implicitly translates into JS-object
            newElement.insert("name", "assets/"+name);
            newElement.insert("width", width);
            newElement.insert("height", height);

            QString type = "";
            if(width > height * 2){
                type = "Panorama";
            }else if(width < height){
                protraits.push_back(newElement);
                continue;
            }else{
                int randomValue = qrand() % 100;
                type = "Landscape";
                if(randomValue < 30) type = "Kenburns";
            }

            source.push_back(newElement);

            QVariantMap slide;
            slide.insert("name", "image");
            slide.insert("type", type);
            slide.insert("slideId", uuid);
            slide.insert("sourceList",source);

            fileGalleryList.push_back(slide);
            //TODOfileGalleryList.push_back(new SlideObject("image", type, uuid, source));
        }
    }

    //Loop through all portraits images
    for(int x=0; x<protraits.count(); x++){
        QVariantMap item = protraits.at(x);
        QString name = item.value("name").toString();
        int width = item.value("width").toInt();
        int height = item.value("height").toInt();

        QString uuid = QUuid::createUuid().toString();
        QVariantList source;
        QString type = "Portrait";

        QVariantMap newElement;  // QVariantMap will implicitly translates into JS-object
        newElement.insert("name", name);
        newElement.insert("width", width);
        newElement.insert("height", height);
        source.push_back(newElement);

        int randomValue = qrand() % 100;
        if(randomValue < 15){
            type = "Kenburns";
        }else if(randomValue < 30){
            type = "PortraitRotate";
        }else if(randomValue < 65){
            if((x+1) < protraits.count()){
                x++;

                item = protraits.at(x);
                name = item.value("name").toString();
                width = item.value("width").toInt();
                height = item.value("height").toInt();

                QVariantMap newElement2;  // QVariantMap will implicitly translates into JS-object
                newElement2.insert("name", name);
                newElement2.insert("width", width);
                newElement2.insert("height", height);
                source.push_back(newElement2);

                type = "PortraitDouble";
            }
        }

        QVariantMap slide;
        slide.insert("name", "image");
        slide.insert("type", type);
        slide.insert("slideId", uuid);
        slide.insert("sourceList",source);
        fileGalleryList.push_back(slide);
//if(count < 5){
 //  count++;
  //TODO      fileGalleryList.push_back(new SlideObject("image", type, uuid, source));
//}
    }

    ShuffleList();

    //Add Weather to list
    if(m_doWeather && m_weatherInterval != 0) addWeatherSlides();
    if(m_otherInterval != 0) addOtherSlides();

    getAlbumSlides();

    qint64 end = QDateTime::currentMSecsSinceEpoch();
    qDebug() << "scanGallery finished" << fileGalleryList.count() << " in " << end-start;

    emit listChanged();
}

void Imagescan::addOtherSlides()
{
    qDebug() << "addLiveSlides";
    int insertCount = (m_otherInterval*60)/(m_slideInterval/1000);

    QMap<QString, QString> location, world;
    location["Sydney, Australia"] = "https://api.deckchair.com/v1/viewer/camera/599d6375096641f2272bacf4?width=1280&height=720&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg";
    location["Sydney, Australia"] = "https://images.lookr-cdn.com/normal/1179853135-Sydney-CBD.jpg?token=lookr.com:3eea6a5897fcade263a4210497732e2d:1563224081";
    location["Paris, France"] = "https://images.lookr-cdn.com/normal/1549489174-Paris.jpg?token=lookr.com:6fff609d2737cdb044e24b09fa3bfcb9:1563224399";

    world["Barcelona, Spain"] = "https://api.deckchair.com/v1/viewer/camera/556834007b2853502527ffbf?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg";
    world["Paris, France"] = "https://api.deckchair.com/v1/viewer/camera/5568862a7b28535025280c72?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg";
    world["london Bridge, England"] = "https://api.deckchair.com/v1/viewer/camera/5a328a521a95bca41de2ae83?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg";
    world["Tower Bridge, England"] = "https://api.deckchair.com/v1/viewer/camera/58c66765e9694f2b2e00ef42?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg";
    world["Arc de Triomphe, France"] = "https://api.deckchair.com/v1/viewer/camera/599d6237096641f2272bacf2?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg";
    world["Athen, Greece"] = "https://api.deckchair.com/v1/viewer/camera/556881bb7b28535025280b9a?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg";
    world["Munich, Germany"] = "https://images.lookr-cdn.com/normal/1171308478-Munich.jpg?token=lookr.com:16ed542948132cb35a03262d5663aef1:1563227411";
    world["london, England"] = "https://images.lookr-cdn.com/normal/1496437860-London.jpg?token=lookr.com:6850fa058d424629b1a8a6fb8472bcdf:1563227475";
    world["Venice, Italy"] = "https://images.lookr-cdn.com/normal/1473374057-Venice.jpg?token=lookr.com:c1ed8f96b823ad0faf3c815a7f4cb6b5:1563227518";
    world["Edinburgh, Scotland"] = "https://images.lookr-cdn.com/normal/1512053094-Edinburgh.jpg?token=lookr.com:aa32a515348e360469d2b400150a41c6:1563225425";
    world["Lake Pukaki, New Zeland"] = "https://images.lookr-cdn.com/normal/1496397168-Lake-Pukaki.jpg?token=lookr.com:c004a4490155061866f00d2b17d60e59:1563225597";
    world["Hohenschwangau, Germany"] = "https://images.lookr-cdn.com/normal/1370448817-Hohenschwangau.jpg?token=lookr.com:d62faec7dca837dfe365b2d8a9fa2f83:1563226209";
    world["La Sagrada, Spain"] = "https://images.lookr-cdn.com/normal/1448811166-La-Sagrada-Fam%C3%ADlia.jpg?token=lookr.com:6f10438169317781d7cc964937240083:1563226068";
   // world[""] = "https://images.lookr-cdn.com/normal/1349866805-Fumoto.jpg?token=lookr.com:1cd5eb702cd523a3f72850ec36daa815:1563226287";
    world["Leuggelbach, Switzerland"] = "https://images.lookr-cdn.com/normal/1485347879-Leuggelbach.jpg?token=lookr.com:b2bb4d6ea0f330f20d8c1f3afb3e9175:1563227598";

/*    QStringList webUrl, worldUrl;
    webUrl << "https://api.deckchair.com/v1/viewer/camera/599d6375096641f2272bacf4?width=1280&height=720&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg"
           << "https://images.lookr-cdn.com/normal/1179853135-Sydney-CBD.jpg?token=lookr.com:3eea6a5897fcade263a4210497732e2d:1563224081" //<- sydney webcam
           << "https://images.lookr-cdn.com/normal/1549489174-Paris.jpg?token=lookr.com:6fff609d2737cdb044e24b09fa3bfcb9:1563224399"; //https://www.lookr.com/lookout/1549489174-Paris

    worldUrl << "https://api.deckchair.com/v1/viewer/camera/556834007b2853502527ffbf?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg" // Barcelona
      << "https://api.deckchair.com/v1/viewer/camera/5568862a7b28535025280c72?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg" //
      << "https://api.deckchair.com/v1/viewer/camera/5a328a521a95bca41de2ae83?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg" //
      << "/https://api.deckchair.com/v1/viewer/camera/58c66765e9694f2b2e00ef42?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg" //
      << "https://api.deckchair.com/v1/viewer/camera/599d6237096641f2272bacf2?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg" //
      << "https://api.deckchair.com/v1/viewer/camera/556881bb7b28535025280b9a?width=1920&height=1080&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg" //
      << "https://images.lookr-cdn.com/normal/1171308478-Munich.jpg?token=lookr.com:16ed542948132cb35a03262d5663aef1:1563227411" //     https://www.lookr.com/lookout/1171308478-Munich
      << "https://images.lookr-cdn.com/normal/1496437860-London.jpg?token=lookr.com:6850fa058d424629b1a8a6fb8472bcdf:1563227475" //     https://www.lookr.com/lookout/1496437860-London
      << "https://images.lookr-cdn.com/normal/1473374057-Venice.jpg?token=lookr.com:c1ed8f96b823ad0faf3c815a7f4cb6b5:1563227518" //     https://www.lookr.com/lookout/1473374057-Venice
      << "https://images.lookr-cdn.com/normal/1512053094-Edinburgh.jpg?token=lookr.com:aa32a515348e360469d2b400150a41c6:1563225425" //  https://www.lookr.com/lookout/1512053094-Edinburgh
      << "https://images.lookr-cdn.com/normal/1496397168-Lake-Pukaki.jpg?token=lookr.com:c004a4490155061866f00d2b17d60e59:1563225597" // https://www.lookr.com/lookout/1496397168-Lake-Pukaki
      << "https://images.lookr-cdn.com/normal/1370448817-Hohenschwangau.jpg?token=lookr.com:d62faec7dca837dfe365b2d8a9fa2f83:1563226209" // https://www.lookr.com/lookout/1370448817-Hohenschwangau
      << "https://images.lookr-cdn.com/normal/1448811166-La-Sagrada-Fam%C3%ADlia.jpg?token=lookr.com:6f10438169317781d7cc964937240083:1563226068" // https://www.lookr.com/lookout/1448811166-La-Sagrada-Fam%C3%ADlia
      << "https://images.lookr-cdn.com/normal/1349866805-Fumoto.jpg?token=lookr.com:1cd5eb702cd523a3f72850ec36daa815:1563226287" //     https://www.lookr.com/lookout/1349866805-Fumoto
      << "https://images.lookr-cdn.com/normal/1485347879-Leuggelbach.jpg?token=lookr.com:b2bb4d6ea0f330f20d8c1f3afb3e9175:1563227598"; // https://www.lookr.com/lookout/1485347879-Leuggelbach
*/

    for (int i = insertCount; i < fileGalleryList.count(); i += insertCount){
        QVariantList source;
        QString uuid = QUuid::createUuid().toString();
        QVariantMap newElement;
        QString type = "";

        int slidetype = 0;
        if(slidetype == 0){
            int l = (location.size() * 3) + world.size();
            int p = qrand() % l;
            if(p < (location.size() * 3)){
                int i = p/3;
                if(i < location.size()){
                    newElement.insert("name", location[location.keys()[i]]);
                    newElement.insert("title", location.keys()[i]);
                }else{
                    newElement.insert("name", location[location.keys()[0]]);
                    newElement.insert("title", location.keys()[0]);
                }
            }else{
                int i = p - (location.size() * 3);
                if(i > world.size())
                    i = i % world.size();
                newElement.insert("name", world[world.keys()[i]]);
                newElement.insert("title", world.keys()[i]);
            }
            newElement.insert("width", 800);
            newElement.insert("height", 480);
            type = "LiveImages";
        }

        source.push_back(newElement);

        QVariantMap slide;
        slide.insert("name", "image");
        slide.insert("type", type);
        slide.insert("slideId", uuid);
        slide.insert("sourceList",source);
        fileGalleryList.insert(i, slide);
    }
}

void Imagescan::addWeatherSlides()
{
    qDebug() << "addWeatherSlides";
    int insertCount = (m_weatherInterval*60)/(m_slideInterval/1000);

    QStringList f;
    f << "sydney*.jpg" << "global*.png";
    QDir directory(m_pathName+"weather/");
    QStringList weatherImages = directory.entryList(f,QDir::Files);

    int lastPos = 0;
    for (int i = insertCount; i < fileGalleryList.count(); i += insertCount){
        int p = qrand() % weatherImages.length();

        QFileInfo fi(m_pathName+"weather/"+weatherImages[p]);
        QString ext = fi.suffix().toLower();
        QString name = fi.fileName();
        QString uuid = QUuid::createUuid().toString();
        QVariantList source;
        QImageReader reader(m_pathName+"weather/"+weatherImages[p]);
        QSize sizeOfImage = reader.size();
        int height = sizeOfImage.height();
        int width = sizeOfImage.width();

        QVariantMap newElement;  // QVariantMap will implicitly translates into JS-object
        newElement.insert("name", "weather/"+name);
        newElement.insert("width", width);
        newElement.insert("height", height);

        QString type = "";
        type = "WeatherSlide";

        source.push_back(newElement);

        QVariantMap slide;
        slide.insert("name", "image");
        slide.insert("type", type);
        slide.insert("slideId", uuid);
        slide.insert("sourceList",source);
        fileGalleryList.insert(i, slide);
   //TODO     fileGalleryList.insert(i, new SlideObject("image", type, uuid, source));
        lastPos = i;
    }

    if((lastPos%insertCount) > insertCount/2){
        int p = qrand() % weatherImages.length();

        QFileInfo fi(m_pathName+"weather/"+weatherImages[p]);
        QString ext = fi.suffix().toLower();
        QString name = fi.fileName();
        QString uuid = QUuid::createUuid().toString();
        QVariantList source;
        QImageReader reader(m_pathName+"weather/"+weatherImages[p]);
        QSize sizeOfImage = reader.size();
        int height = sizeOfImage.height();
        int width = sizeOfImage.width();

        QVariantMap newElement;  // QVariantMap will implicitly translates into JS-object
        newElement.insert("name", "weather/"+name);
        newElement.insert("width", width);
        newElement.insert("height", height);

        QString type = "";
        type = "WeatherSlide";

        source.push_back(newElement);

        QVariantMap slide;
        slide.insert("name", "image");
        slide.insert("type", type);
        slide.insert("slideId", uuid);
        slide.insert("sourceList",source);
        fileGalleryList.push_back(slide);
  //TODO      fileGalleryList.push_back(new SlideObject("image", type, uuid, source));
    }

    qDebug() << "addWeatherSlides Done";
}

void Imagescan::getAlbumSlides()
{
    qDebug() << "getAlbumSlides";
    QMutexLocker M(&updating);

    QString jsonFile = m_pathName+"albums/index.txt";

    QMap<QString, QStringList> mapList;

    bool exists = QFileInfo::exists(jsonFile) && QFileInfo(jsonFile).isFile();
    if(!exists) return;
//qDebug() << "getAlbumSlides exists";
    QString val;
    QFile file;
    file.setFileName(jsonFile);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();

    QStringList linesPaths = val.split("\n");
    if(linesPaths.length() < 2) return;
//qDebug() << "A";
    QStringList g = linesPaths[0].split("|");

    int n = g[0].toInt();
    int f = g[2].toInt();
//qDebug() << "B"<<g;
    int total = n + f * 5;
    int p = qrand() % total;
    int index = -1;
//qDebug() << "a"<<n<<f<<p;
    if(p < (f * 5)){
//qDebug() << "b";
        int t = (p / 5);
//qDebug() << "c"<<t;
        QStringList w = g[3].split(",");
//qDebug() << "d"<<w;
        index = w[t].toInt();
    }else{
        index = p - (f * 5);
    }
//qDebug() << "C"<<index;
    QString path;
    QStringList details;
    for(int x=index+1; x<linesPaths.length(); x++){
        //qDebug() << "a"<<x;
        QStringList l = linesPaths[x].split("|");
        //qDebug() << "b"<<l;
        if(l.length() < 1) continue;
        if(l[1] == "b") continue;

        path = l[0];
        details = l;
        index = x;
        break;
    }
//qDebug() << "D";
    if(path == "") return;
    QString searchPath = m_pathName+"albums/"+path;

    QVariantMap album;
    album.insert("name", details.join("|"));
    album.insert("path", searchPath);
    album.insert("index", index);

    QDir directory(searchPath);
    QStringList galerryImages = directory.entryList(nameFilters,QDir::Files);

    int insertPos = qrand() % fileGalleryList.count();
    QVariantList colarge;
    for (int i = 0; i < galerryImages.length(); i++){
        if(galerryImages[i] == "") continue;
        QFileInfo fi(searchPath+"/"+galerryImages[i]);
        QString ext = fi.suffix().toLower();
        QString name = fi.fileName();
        QString uuid = QUuid::createUuid().toString();
        QVariantList source;

        if(ext == "mp4" || ext == "mov"){
            //Video
            QVariantMap newElement;  // QVariantMap will implicitly translates into JS-object
            newElement.insert("name", "albums/"+path+"/"+name);
            source.push_back(newElement);

            QVariantMap slide;
            slide.insert("name", "video");
            slide.insert("type", "Video");
            slide.insert("slideId", uuid);
            slide.insert("sourceList",source);
            slide.insert("album", album);
            fileGalleryList.insert(insertPos+i, slide);
 //TODO           fileGalleryList.insert(insertPos+i, new SlideObject("video", "Video", uuid, source));
        }else{
            QImageReader reader(searchPath+"/"+name);
            QSize sizeOfImage = reader.size();
            int height = sizeOfImage.height();
            int width = sizeOfImage.width();

            QVariantMap newElement;  // QVariantMap will implicitly translates into JS-object
            newElement.insert("name", "albums/"+path+"/"+name);
            newElement.insert("width", width);
            newElement.insert("height", height);

            source.push_back(newElement);
            if(colarge.size() < COLLAGE_SIZE) {
                //qDebug() << "getAlbumSlides colarge" << name;
                colarge.push_back(newElement);
            }
            QString type = "";
            if(width > height * 2){
                type = "Panorama";
            }else if(width < height){
                int randomValue = qrand() % 100;
                QString type = "Portrait";
                if(randomValue < 15){
                    type = "Kenburns";
                }else if(randomValue < 30){
                    type = "PortraitRotate";
                }else if(randomValue < 65){
                   for (int x = i+1; x < galerryImages.length(); x++){
                       QFileInfo fi(searchPath+"/"+galerryImages[x]);
                       if(ext == "mp4" || ext == "mov") continue;

                       QImageReader reader(searchPath+"/"+galerryImages[x]);
                       sizeOfImage = reader.size();
                       height = sizeOfImage.height();
                       width = sizeOfImage.width();
                       if(width < height){
                           QVariantMap newElement2;  // QVariantMap will implicitly translates into JS-object
                           newElement2.insert("name", "albums/"+path+"/"+galerryImages[x]);
                           newElement2.insert("width", width);
                           newElement2.insert("height", height);
                           source.push_back(newElement2);
                           if(colarge.size() < COLLAGE_SIZE) {
                               //qDebug() << "getAlbumSlides colarge" << galerryImages[x];
                               colarge.push_back(newElement2);
                           }

                           type = "PortraitDouble";
                       }
                       galerryImages[x] = "";
                   }
                }

                QVariantMap slide;
                slide.insert("name", "image");
                slide.insert("type", type);
                slide.insert("slideId", uuid);
                slide.insert("sourceList",source);
                slide.insert("album", album);
                fileGalleryList.insert(insertPos+i, slide);
//qDebug() << "getAlbumSlides insert" << name;
//TODO                fileGalleryList.insert(insertPos+i, new SlideObject("image", type, uuid, source));
                continue;
            }else{
                int randomValue = qrand() % 100;
                type = "Landscape";
                if(randomValue < 30) type = "Kenburns";
            }

            QVariantMap slide;
            slide.insert("name", "image");
            slide.insert("type", type);
            slide.insert("slideId", uuid);
            slide.insert("sourceList",source);
            slide.insert("album", album);
            fileGalleryList.insert(insertPos+i, slide);
  //TODO          fileGalleryList.insert(insertPos+i, new SlideObject("image", type, uuid, source));
        }
    }

    QVariantMap slide;
    slide.insert("name", details.join("|"));
    slide.insert("type", "Album");
    slide.insert("slideId", QUuid::createUuid().toString());
    slide.insert("sourceList",colarge);
    slide.insert("album", album);
    fileGalleryList.insert(insertPos, slide);
    qDebug() << "getAlbumSlides done";
}

/*QQmlListProperty<QObject> Imagescan::getList()
{
    //qDebug() << "QML asked for FileList";
    return QQmlListProperty<QObject>(this, fileGalleryList);
}*/

QVariantList Imagescan::getList()
{
    //qDebug() << "QML asked for FileList";
    return fileGalleryList;
}

Q_INVOKABLE int Imagescan::getListSize()
{
    //qDebug() << m_size << (*fileGalleryList).count() << (*fileGalleryList).size();
    return fileGalleryList.count();
}

void Imagescan::ShuffleList()
{
    std::random_shuffle ( fileGalleryList.begin(), fileGalleryList.end() );
}

QString MD5File(QString filename){
    QFile file(filename);

    if (file.open(QIODevice::ReadOnly)) {
        QByteArray fileData = file.readAll();
        QByteArray hashData = QCryptographicHash::hash(fileData, QCryptographicHash::Md5);

        return hashData.toHex();
    }
    return "";
}

void Imagescan::syncGallery()
{
    QDirIterator it(m_pathName+"gallery/", nameFilters, QDir::Files, QDirIterator::Subdirectories);

    QString newPath = m_pathName+"assets/";
    while (it.hasNext()) {
        QString path = it.next();

        QFileInfo fi(path);
        QString name = fi.fileName();

        bool exists = QFileInfo::exists(newPath+name) && QFileInfo(newPath+name).isFile();
        bool existsLower = QFileInfo::exists(newPath+name.toLower()) && QFileInfo(newPath+name.toLower()).isFile();
        if(!exists && !existsLower){
            QString bHex = MD5File(m_pathName+"gallery/"+name);
            setWrite();
            QFile::copy(m_pathName+"gallery/"+name, newPath+name.toLower()+".tmp");

            QString fHex = MD5File(newPath+name.toLower()+".tmp");
            if(bHex == fHex){
                QFile::rename(newPath+name.toLower()+".tmp", newPath+name.toLower());
            }
            setReadOnly();
        }
    }
}

int Imagescan::getLastScanDay()
{
    return scan_day;
}

void Imagescan::syncAlbum(){
    qDebug() << "syncAlbum";

    QMutexLocker M(&updating);

    QString jsonFile = m_pathName+"albums/index.txt";

    QMap<QString, QStringList> mapList;

    bool exists = QFileInfo::exists(jsonFile) && QFileInfo(jsonFile).isFile();
    if(exists){
        QString val;
        QFile file;
        file.setFileName(jsonFile);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        val = file.readAll();
        file.close();

        QStringList linesPaths = val.split("\n");
        for(int x=1; x<linesPaths.length(); x++){
            QStringList l = linesPaths[x].split("|");
            mapList.insert(l[0],l);
        }
    }

    scanPath(m_pathName+"albums/", &mapList);

    QMap<QString, QStringList>::const_iterator i = mapList.constBegin();
    QString filedata;
    QFile file;
    file.setFileName(jsonFile);
    file.open(QIODevice::WriteOnly);
    QTextStream outStream(&file);

    int fav = 0;
    int block = 0;
    int normal = 0;
    QStringList favList;

    int x = 0;
    while (i != mapList.constEnd()) {
        QStringList l = i.value();
        filedata += l.join("|") + "\n";
        if(l.length() > 2){
            if(l[1] == "n")
                normal++;
            else if(l[1] == "b")
                block++;
            else if(l[1] == "f"){
                fav++;
                favList.push_back(QString::number(x));
            }
        }
//        cout << i.key() << ": " << i.value() << endl;
        ++i;
        x++;
    }

    outStream << normal << "|" << block << "|" << fav <<"|" << favList.join(",") << endl << filedata << endl;
    file.close();

    QDateTime now = QDateTime::currentDateTime();
    scan_day = now.date().day();
}

void Imagescan::getVideoImages(QString fileName)
{
    qDebug() << "getVideoImages"<<fileName;
    QMovie movie;
    movie.setFileName(fileName);
    movie.jumpToFrame(0);
    qDebug() << "frames"<<movie.frameCount();
    //first image
    /*QFile file(m_pathName+"test.png");
    QImage first = movie.currentImage();
    first.save(file.fileName(),"PNG");
    movie.jumpToFrame(movie.frameCount()-1);
    QImage last = movie.currentImage();
    last.save(m_pathName+"last.png");
    qDebug() << "getVideoImages complete";*/
}

void Imagescan::scanPath(QString dir, QMap<QString, QStringList> *lines)
{
    //qDebug() << "scanPath" << dir;
    //1. Check to see if there any known types. If so we have album
    QDir directory(dir);
    QStringList weatherImages = directory.entryList(nameFilters,QDir::Files);
    if(weatherImages.count() > 0){
        QStringList words = dir.split("/");
        #ifdef Q_OS_LINUX
        for (int i = 0; i < 5; ++i)
            words.removeFirst();
        #else
        for (int i = 0; i < 9; ++i)
            words.removeFirst();
        #endif

        QString p = words.join("/");
        QMap<QString, QStringList>::const_iterator i = lines->find(p);
        if(i != lines->end()){
            return;
        }
        QString date = "";
        QStringList title;
        for(int x=0; x<words.length(); x++){
            if(words[x].contains(words[0])){
                QStringList d = words[x].split(" ");
                date = d[0];
                d.removeFirst();
                if(d.length() > 0)
                    title.push_back(d.join(" "));
            }else if(words[x] != ""){
                title.push_back(words[x]);
            }
        }

        QStringList l;
        l << p << "n" << words[0] << date << title;
        lines->insert(p, l);

        //qDebug() << "FOUND" << p;

        //QDir d(dir);
        //d.dirName()
        return;
    }

    QDirIterator it(dir, QDir::Dirs | QDir::NoDotAndDotDot);

    //2. Otherwise check for directories
    while (it.hasNext()) {
       QString path = it.next();
       scanPath(path, lines);
    }
}

void Imagescan::changeTypeAlbum(int pos, QString type)
{
    QMutexLocker M(&updating);

    QString jsonFile = m_pathName+"albums/index.txt";

    QMap<QString, QStringList> mapList;

    bool exists = QFileInfo::exists(jsonFile) && QFileInfo(jsonFile).isFile();
    if(!exists) return;

    QString val;
    QFile file;
    file.setFileName(jsonFile);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();

    QStringList linesPaths = val.split("\n");

    int oldn = 0, oldf = 0, oldb = 0;
    if(pos < linesPaths.length()-1){
        QStringList l = linesPaths[pos].split("|");
        if(l[1] == 'b'){
          qDebug() << "bbb";
          oldb++;
        }else if(l[1] == 'f'){
          qDebug() << "fff";
          oldf++;
        }else if(l[1] == 'n'){
          qDebug() << "nnn";
          oldn++;
        }
        l[1] = type;
        linesPaths[pos] = l.join("|");
    }

    QStringList t = linesPaths[0].split("|");

    int n = t[0].toInt(0);
    int b = t[1].toInt(0);
    int f = t[2].toInt(0);
    QStringList w = t[3].trimmed().split(",");
    if(oldf){
        qDebug() << "a";
        f--;
        if(f < 0) f = 0;
        qDebug() << "b" << f;
        QString p = QString::number(pos);
        if(w.contains(p)){
            w.removeAll(p);
            qDebug() << "c" << pos << w;
        }
    }

    if(oldn){
        n--;
        if(n < 0) n = 0;
    }

    if(oldb){
        b--;
        if(b < 0) b = 0;
    }

    if(type == "b"){
        b++;
    }else if(type == "f"){
        f++;
        w.push_back(QString::number(pos));
    }else if(type == "n"){
        n++;
    }

    t[0] = QString::number(n);
    t[1] = QString::number(b);
    t[2] = QString::number(f);
    w.removeAll("");w.removeAll(" ");
    t[3] = w.join(",");

    linesPaths[0] = t.join("|");

    QString d = linesPaths.join("\n");

    file.open(QIODevice::WriteOnly | QIODevice::Text);
    file.write(d.toUtf8());
    file.close();
}

QVariantList Imagescan::getAlbumList()
{
    QVariantList list;

    QMutexLocker M(&updating);

    QString jsonFile = m_pathName+"albums/index.txt";

    QMap<QString, QStringList> mapList;
//qDebug() << "jsonFile" << jsonFile << QFileInfo::exists(jsonFile) << QFileInfo(jsonFile).isFile();
    bool exists = QFileInfo::exists(jsonFile) && QFileInfo(jsonFile).isFile();
    if(!exists) return list;
//qDebug() << "exists";
    QString val;
    QFile file;
    file.setFileName(jsonFile);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();

    QStringList linesPaths = val.split("\n");
    for(int x=1; x<linesPaths.length(); x++){
        QStringList l = linesPaths[x].split("|");

        QVariantMap row;
        if(l.length() >= 2){
            row.insert("path",l[0]);
            row.insert("state",l[1]);
        }else{
            row.insert("path","Unknown");
            row.insert("state","b");
        }
        list.push_back(row);
    }

    return list;
}


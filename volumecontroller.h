#ifndef VOLUMECONTROLLER_H
#define VOLUMECONTROLLER_H

#include <QObject>
#include <QMutex>
#include <QTimer>
#include "gpio.h"

class VolumeController : public QObject
{
    Q_OBJECT
public:
    explicit VolumeController(QObject *parent = nullptr);

    void setGPIO(Gpio *gpio);
    void Initialise();
    void getInitialVolume();

    int getVolume();
    void setVolume(int);

    void detectVolButtons();
signals:
    void volumeChange(int);

//    void storeVolume();
public slots:
private slots:
//    void storeSoundState();
//    void storeTimer();
private:
//    QTimer timer;

    Gpio * m_gpio;
    QMutex m_lock;
    int lastVolume;
    bool b_Up, b_Down;

    void setSystemVolume(int);
    void muteToggle();
};

#endif // VOLUMECONTROLLER_H

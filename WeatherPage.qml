import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.0

import "js/skycons.js" as WeatherIcon
import "js/simpletools.js" as SimpleTools

Page {
    id: weatherp
    anchors.fill: parent

    width: imageWidth
    height: imageHeight

    property bool hasLoaded: false

    background: Rectangle{
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#4e4eae" }
            GradientStop { position: 1.0; color: "#40bdd9" }
        }
    }

    property int slideCount: controller.weatherDataSize > 5?5:controller.weatherDataSize
    property int slideDuration: 900

    function setPageIndex(index){
        view.currentIndex = index;
    }

    Connections {
        target: weatherView
        onVisibleChanged:
            if(visible){
                timeout.start();
                if(!weatherp.hasLoaded)
                    busyIndication.running = true;
            }else{
                busyIndication.running = false;
            }

        ignoreUnknownSignals: true
    }

    Component {
        id: nodata
        Label {
            x: 20
            y: 20
            //anchors.centerIn: parent;
            text: qsTr("No Internet or Weather Data")
            font.pointSize: 20
            font.family: "Helvetica"
            font.bold: true
            Component.onCompleted: {
                weatherp.hasLoaded = true;
                busyIndication.running = false;
            }
        }
    }

    Component {
        id: delegateWeather
        Item {
            id: itemView;
            width: imageWidth
            height: imageHeight
            //anchors.fill: parent;

            property variant modelHourly: modelData.hourly
            property variant modelDaily: modelData.daily
            property variant modelCurrent: modelData.current

            Label {
                id: location
                anchors.horizontalCenter: itemView.horizontalCenter
                text: qsTr(modelData.name)//
                color: "white"
                font.pointSize: 40
                font.family: "Helvetica"
                font.bold: true
                Component.onCompleted: {
                    weatherp.hasLoaded = true;
                    busyIndication.running = false;
                }
            }

            Item {
                id: secondRow
                x: 250
                y: 60

                property int canvasWidth: 50
                property int canvasHeight: 50
                property date startTime: new Date()

                Canvas {
                    id: panCanvas
                    width: secondRow.canvasWidth
                    height: secondRow.canvasHeight
                    Component.onCompleted: {

                    }
                    onPaint: {
                        var ctx = getContext("2d");
                        ctx.save();
                        ctx.clearRect(0, 0, secondRow.canvasWidth, secondRow.canvasHeight);

                        var diff = new Date() - secondRow.startTime
                        WeatherIcon.skycons(ctx,modelData.current.icon, diff);
                        ctx.restore();
                    }

                    Timer {
                       id: delayLoad
                       interval: 1000 / 60
                       running: true
                       repeat: true
                       onTriggered:{
                           panCanvas.requestPaint();
                       }
                    }
                }
                Label {
                    id: clockDatHalf
                    anchors.left: panCanvas.right
                    text: qsTr(SimpleTools.roundDown(modelData.current.temperature)+"°C")
                    font.pointSize: 40
                    font.family: "Helvetica"
                    font.bold: true
                    color: "white"
                }
                Label {
                    y:5
                    id: weathertext
                    text: qsTr(modelData.current.summary)
                    anchors.left: clockDatHalf.right
                    //anchors.horizontalCenter: parent.horizontalCenter
                    //anchors.top: secondRow.bottom
                    color: "white"
                    font.pointSize: 20
                    font.family: "Helvetica"
                    font.bold: true
                }
                Label {
                    id: feellike
                    anchors.left: clockDatHalf.right
                    text: qsTr(SimpleTools.roundDown(modelData.daily[0].temperatureHigh)+"°C/"+SimpleTools.roundDown(modelData.daily[0].temperatureLow)+"°C Feels like "+SimpleTools.roundDown(modelData.current.apparentTemperature)+"°C")
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: weathertext.bottom
                    color: "white"
                    font.pointSize: 10
                    font.family: "Helvetica"
                    font.bold: true
                }

            }
            Button {
                id: btRadar
                height: 50
                width: 50
                padding: 10
                spacing: 10
                x: 700
                y: 60
                visible: modelData.hasRadar
                display: AbstractButton.IconOnly
                focusPolicy: Qt.NoFocus
                background: Rectangle {
                    color: "transparent"
                }

                Image {
                    id: btFrameImage
                    anchors.fill: parent
                    source: "files/icon/radar.png"
                }
            }

            Connections {
                target: btRadar
                onClicked: {
                    timeout.restart();
                    window.alive();
                    if(modelData.hasRadar){
                        busyIndication.running = true;
                        delayRadarLoad.start();
//                        radardata.load();
                    }else{
                        console.log("TODO no data")
                    }
                }
            }
            Row {
                id: detailRow
                y: 120
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 20
                Column{
                    Label {
                        text: qsTr("Precipitation")
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                    Label {
                        text: qsTr(SimpleTools.roundDown(modelData.current.precipProbability*100)+"%")
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                }
                Column{
                    Label {
                        text: qsTr("UV index")
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                    Label {
                        text: qsTr(SimpleTools.getUVIndex(modelData.current.uvIndex))
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                       }
                }
                Column{
                    Label {
                        text: qsTr("Sunrise")
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                    Label {
                        text: qsTr(SimpleTools.timeToString(modelData.daily[0].sunriseTime))
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                }
                Column{
                    Label {
                        text: qsTr("Sunset")
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                    Label {
                        text: qsTr(SimpleTools.timeToString(modelData.daily[0].sunsetTime))
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                }
                Column{
                    Label {
                        text: qsTr("Humidity")
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                    Label {
                        text: qsTr(SimpleTools.roundDown(modelData.current.humidity)+"%")
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                }
                Column{
                    Label {
                        text: qsTr("Visibility")
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                    Label {
                        text: qsTr(SimpleTools.getVisibility(modelData.current.visibility))
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                }
            }
            Row {
                id: hourlyRow
                anchors.top: detailRow.bottom
                anchors.topMargin: 5
                anchors.horizontalCenter: parent.horizontalCenter

                property int hourlyWidth: 30
                property int hourlyHeight: 30

                Column{
                    Label {
                        id: hourText
                        text: qsTr("Hourly")
                        //anchors.top: hourlyRow.bottom
                        anchors.topMargin: 8
                        anchors.horizontalCenter: parent.horizontalCenter
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                    Row {
                        //anchors.top: hourText.bottom
                        anchors.horizontalCenter: hourText.horizontalCenter
                        spacing: 20
                        Repeater {
                            model: 6; // just define the number you want, can be a variable too
                            delegate: Column{
                                Label {
                                    id: hour1Time
                                    text: qsTr(SimpleTools.time24String(modelHourly[index*2].time))
                                    color: "white"
                                    font.pointSize: 14
                                    font.family: "Helvetica"
                                    font.bold: true
                                }
                                Canvas {
                                    id: hour1Canvas
                                    anchors.horizontalCenter: hour1Time.horizontalCenter
                                    width: hourlyRow.hourlyWidth
                                    height: hourlyRow.hourlyHeight
                                    x:0
                                    y:0

                                    onPaint: {
                                        var ctx = getContext("2d");
                                        ctx.save();
                                        ctx.clearRect(0, 0, secondRow.hourlyWidth, secondRow.hourlyHeight);

                                        WeatherIcon.skycons(ctx,modelHourly[index*2].icon, 0);
                                        ctx.restore();
                                    }
                                }
                                Label {
                                    anchors.horizontalCenter: hour1Time.horizontalCenter
                                    text: qsTr(SimpleTools.roundDown(modelHourly[index*2].precipProbability*100)+"%")
                                    color: "white"
                                    font.pointSize: 10
                                    font.family: "Helvetica"
                                }
                                Label {
                                    anchors.horizontalCenter: hour1Time.horizontalCenter
                                    text: qsTr(qsTr(SimpleTools.roundDown(modelHourly[index*2].temperature)+"°C"))
                                    color: "white"
                                    font.pointSize: 20
                                    font.family: "Helvetica"
                                }
//                                Canvas {
//                                    width: 5
//                                    height: 30
//                                    x:0
//                                    y:0
//                                    anchors.horizontalCenter: hour1Time.horizontalCenter

//                                    onPaint: {
//                                        var ctx = getContext("2d");
//                                        ctx.save();
//                                        ctx.clearRect(0, 0, secondRow.hourlyWidth, secondRow.hourlyHeight);

//                                        ctx.strokeStyle = "#ffffff";
//                                        ctx.lineWidth = 5;
//                                        ctx.lineCap = "round";

//                                        ctx.beginPath();
//                                        ctx.moveTo(2, 25);
//                                        ctx.lineTo(2, (15-((hourly[index*2].temperature-current.htopLow)/daily[0].temperatureHigh)*15));
//                                        ctx.stroke();

//                                        ctx.restore();
//                                    }
//                                }
                            }
                        }
                    }
                }
            }
            Row {
                id: dailyRow
                y: 280
                anchors.horizontalCenter: parent.horizontalCenter

                property int dailyWidth: 30
                property int dailyHeight: 30

                Column{
                    Label {
                        id: dailyText
                        text: qsTr("Daily")
                        anchors.horizontalCenter: parent.horizontalCenter
                        color: "white"
                        font.pointSize: 10
                        font.family: "Helvetica"
                    }
                    Row {
                        y: 10
                    //    anchors.top: dailyText.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.topMargin: 30
                        spacing: 20

                        Repeater {
                            model: 7; // just define the number you want, can be a variable too
                            delegate: Column{
                                Label {
                                    id: dailyTimeText
                                    text: index==0?qsTr("Today"):qsTr(SimpleTools.dayofWeek(modelDaily[index].time))
                                    color: "white"
                                    font.pointSize: 15
                                    font.family: "Helvetica"
                                    font.bold: true
                                }
                                Canvas {
                                    id: daily1Canvas
                                    width: dailyRow.dailyWidth
                                    height: dailyRow.dailyHeight
                                    x:0
                                    y:0
                                    anchors.horizontalCenter: dailyTimeText.horizontalCenter

                                    onPaint: {
                                        var ctx = getContext("2d");
                                        ctx.save();
                                        ctx.clearRect(0, 0, secondRow.dailyWidth, secondRow.dailyHeight);

                                        WeatherIcon.skycons(ctx,modelDaily[index].icon, 0);
                                        ctx.restore();
                                    }
                                }
                                Label {
                                    anchors.horizontalCenter: dailyTimeText.horizontalCenter
                                    text: qsTr(SimpleTools.roundDown(modelDaily[index].precipProbability*100)+"%")
                                    color: "white"
                                    font.pointSize: 10
                                    font.family: "Helvetica"
                                }
                                Label {
                                    anchors.horizontalCenter: dailyTimeText.horizontalCenter
                                    text: qsTr((SimpleTools.roundDown(modelDaily[index].temperatureHigh))+"°C")
                                    color: "white"
                                    font.pointSize: 20
                                    font.family: "Helvetica"
                                }
                                Canvas {
                                    width: 5
                                    height: 30
                                    x:0
                                    y:0
                                    anchors.horizontalCenter: dailyTimeText.horizontalCenter

                                    onPaint: {
                                        var ctx = getContext("2d");
                                        ctx.save();
                                        ctx.clearRect(0, 0, secondRow.hourlyWidth, secondRow.hourlyHeight);

                                        ctx.strokeStyle = "#ffffff";
                                        ctx.lineWidth = 5;
                                        ctx.lineCap = "round";

                                        ctx.beginPath();
                                        ctx.moveTo(2, 25 - (10-(modelCurrent.topLow/modelDaily[index].temperatureLow)*10));
                                        ctx.lineTo(2, 5 + (10-(modelDaily[index].temperatureHigh/modelCurrent.topHigh)*10));
                                        ctx.stroke();

                                        ctx.restore();
                                    }
                                }
                                Label {
                                    anchors.horizontalCenter: dailyTimeText.horizontalCenter
                                    text: qsTr(SimpleTools.roundDown(modelDaily[index].temperatureLow)+"°C")
                                    color: "white"
                                    font.pointSize: 20
                                    font.family: "Helvetica"
                                }
                            }

                        }
                    }
                }
            }

            Label {
                x: 550
                y: 0
                text: qsTr(SimpleTools.getDateToString(modelData.current.time))
                color: "white"
                font.pointSize: 10
                font.family: "Helvetica"
                font.bold: true
            }
        }
    }

    Component {
        id: delegate

        Loader {
            id: wloader
            // Explicitly set the size of the
            // Loader to the parent item's size
            //anchors.fill: parent
            width: imageWidth
            height: imageHeight
            sourceComponent: hasData?delegateWeather:nodata
            property variant modelData: controller.weatherDataModel[index]
        }
    }

    ListView {
        id: view
        anchors.fill: parent
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        highlightRangeMode: PathView.StrictlyEnforceRange
        highlightMoveDuration: slideDuration

        property string currentId: ""

        //when manually flicked, stop the auto timer and reset when the move is finished
        /*onMovingChanged: {
             if(this.moving)
             {
                 console.log("Stopping weather Timer")
             }
             else
             {
                 console.log("Restart weather Timer")
             }
        }

        onCurrentItemChanged: {
            if(controller.weatherDataSize){
             //   currentId = controller.weatherDataModel[view.currentIndex].slideId
            }
        }*/

        model: controller.weatherDataModel
        delegate: delegate
        orientation: ListView.Horizontal
    }

    Page {
        id: weatherSwipeViewDots
        z: 2
        x: 400 - (view.count/2)*(5+5)
        y: 448
        width: view.count*(5+5)
        height: 10
        visible: view.visible

        background: Rectangle {
            color: "transparent"
        }

        RowLayout {
            spacing: 5
            Repeater {
                model: view.count

                delegate: Rectangle {
                    color: view.currentIndex==index?"black":"white"
                    height: 5
                    width: 5
                    radius: 2
                }
            }
        }
    }

    Item {
        id: radardata
        x: (imageWidth-480)/2
        width: 480
        height: 480

        property variant modelData: controller.weatherDataModel[0]
        function load(){
            var component = Qt.createComponent("RadarDisplay.qml");
            if( component.status !== Component.Ready )
            {
                if( component.status === Component.Error )
                    console.debug("Error:"+ component.errorString() );
                busyIndication.running = false;
                return; // or maybe throw
            }
            radardata.modelData.downloadRadarImages();
            busyIndication.running = false;
            component.createObject(radardata);
        }
    }

    Timer {
        id: delayRadarLoad
        interval: 100
        running: false
        repeat: false
        onTriggered:{
            radardata.load();
        }
    }

    BusyIndicator {
       z: 2
       id: busyIndication
       anchors.centerIn: parent
       running: false
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("WeatherPge1 switchToMenu");
            timeout.stop();
            window.switchToMenu(true)
        }
    }

    Timer {
        id: timeout
        interval: 120 * 1000
        running: false
        repeat: false
        onTriggered:{
            console.log("WeatherPage2 switchToMenu");
            window.switchToMenu()
        }
    }
}

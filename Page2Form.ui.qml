import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 800
    height: 480

    background: Rectangle{
        color:"transparent"
    }

    Grid {
        id: grid
        x: 25
        y: 29
        width: 738
        height: 416
        spacing: 20
        transformOrigin: Item.TopLeft
        rows: 3
        columns: 6

        Button {
            id: btFrame
            height: 100
            width: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            display: AbstractButton.IconOnly
            focusPolicy: Qt.NoFocus
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btFrameImage
                anchors.fill: parent
                source: "files/icon/frame.png"
            }
        }

        Connections {
            target: btFrame
            onClicked: {
                window.switchToPhoto(true)
            }
        }
        Button {
            id: btClock
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btClockImage
                width: 100
                height: 100
                source: "files/icon/clock.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btClock
            onClicked: {
                window.switchToClock(true)
            }
        }

        Button {
            id: btYouTube
            width: 100
            height: 100
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btYouTubeImage
                width: 100
                height: 100
                source: "files/icon/youtube.png"
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
            }
        }

        Connections {
            target: btYouTube
            onClicked: {
                window.switchToYouTube(true)
            }
        }

        Button {
            id: btNight
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btNightImage
                width: 100
                height: 100
                source: "files/icon/night.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btNight
            onClicked: {
                window.switchToClock(true)
            }
        }

        Button {
            id: btMirror
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btMirrorImage
                width: 100
                height: 100
                source: "files/icon/mirror.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btMirror
            onClicked: {
                window.switchToMirror(true)
            }
        }

        Button {
            id: btAlarm
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btAlarmImage
                width: 100
                height: 100
                source: "files/icon/alarm.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btAlarm
            onClicked: {
                window.switchToAlarm(true)
            }
        }

        Button {
            id: btTimer
            width: 100
            height: 100
            text: qsTr("Button")
            padding: 10
            spacing: 10
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btTimerImage
                width: 100
                height: 100
                source: "files/icon/timer.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btTimer
            onClicked: {
                window.switchToTimer(true)
            }
        }
    }

}

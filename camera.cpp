#include "camera.h"
#include <QPen>
#include <QColor>
#include <QPainter>

CameraWorker::CameraWorker() : cameraRunning(false), m_running(false)
{
    qRegisterMetaType<QImage>("QImage&");
#ifdef Q_OS_LINUX
    data = new unsigned char[camera.getImageTypeSize(RASPICAM_FORMAT_RGB)];
#endif
}

CameraWorker::~CameraWorker()
{
}

void CameraWorker::doWork()
{
    // Open the camera
#ifdef Q_OS_LINUX
    if (!camera.open()) {
        qDebug() << "Error opening camera";
        cameraRunning = false;
    } else {
        cameraRunning = true;
    }
#else
    cameraRunning = true;
#endif
    if(!cameraRunning)
        return;
    else{
        QMutexLocker M(&l_running);

        m_running = true;
    }

    // Wait for the camera
    QThread::sleep(1);

    bool run = true;
    // While the camera is on (the user has clicked the button), capture
    while (run) {
        // Capture
#ifdef Q_OS_LINUX
        camera.grab();
        camera.retrieve(data, RASPICAM_FORMAT_RGB);

        // Convert the data and send to the caller to handle
        QImage image = QImage(data, camera.getWidth(), camera.getHeight(), QImage::Format_RGB888);
        emit handleImage(image);
#else
        QImage image(100, 50, QImage::Format_ARGB32_Premultiplied);
        QPainter painter(&image);
        painter.fillRect(image.rect(), Qt::yellow);
        painter.drawText(image.rect(), Qt::AlignCenter | Qt::AlignVCenter, "hello, world");
        emit handleImage(image);
#endif
        // Make the app process stopWork() if necessary
        qApp->processEvents();
        QThread::usleep(200);

        if(l_running.tryLock()){
            if(!m_running){
                run = false;
            }
            l_running.unlock();
        }
    }

    stopWork();
}

void CameraWorker::stopWork()
{
    // Set the flag to false
    cameraRunning = false;

    emit finished();
}

void CameraWorker::Stop()
{
    QMutexLocker M(&l_running);

    m_running = false;
}



Camera::Camera(QQuickItem *parent) : QQuickPaintedItem(parent),
    cameraRunning(false)
{
    // Initialize the thread and worker
    worker = new CameraWorker;
}

void Camera::setRunning(bool s)
{
    if(s)
        initialise();
    else{
        //stop
        worker->Stop();
    }
}

void Camera::initialise()
{
    // Check to make sure the camera is not already running
    // If it is, return without doing anything.
    if (cameraRunning) {
        return;
    }

    // Setup the thread
    worker->moveToThread(&workerThread);

    // Connect signals to slots
    connect(&workerThread, SIGNAL(started()), worker, SLOT(doWork()));
    connect(worker, SIGNAL(finished()), &workerThread, SLOT(quit()));
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(worker, SIGNAL(finished()), this, SLOT(cameraFinished()));
    connect(worker, SIGNAL(handleImage(QImage &)), this, SLOT(handleImage(QImage &)));
    workerThread.start();

    // Update the running flag
    cameraRunning = true;
    emit connectedChange();

}

void Camera::paint(QPainter *painter)
{
    painter->drawImage(boundingRect(), m_lastImage);
}

void Camera::handleImage(QImage &image)
{
    // Update the image shown
    m_lastImage = image;

    // Force an update of the UI so that the image is shown immediately.
    //QApplication::processEvents();
    update();
}

void Camera::cameraFinished()
{
    // Update running flag
    cameraRunning = false;
    emit connectedChange();
}

#ifndef GPIO_H
#define GPIO_H

#include <QObject>
//#include <QFileSystemWatcher>
class Gpio : public QObject
{
    Q_OBJECT
public:
    explicit Gpio(QObject *parent = nullptr);
    ~Gpio();

    void setAttachInput(int pin, void (*gpioAlertFunc_t) (int gpio, int level, uint32_t tick));
    void setInput(int pin);
    void setOutput(int pin);

    void write(int pin, bool value);
    bool read(int pin);

    bool openSerial();
    QString readSerial();
signals:
private:
    int serialHandle;
    //QFileSystemWatcher gpiowatcher;
  //  void aFunction(int gpio, int level, uint32_t tick);
};

#endif // GPIO_H

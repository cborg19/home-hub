#ifndef LCDCONTROLLER_H
#define LCDCONTROLLER_H

#include <QObject>

class LCDController : public QObject
{
    Q_OBJECT
public:
    explicit LCDController(QObject *parent = nullptr);

    void setBrightness(int value);
    void lcdOff();
    void lcdOn();

    int currentBrightness();
signals:

public slots:
private:
    int m_brightness;
};

#endif // LCDCONTROLLER_H

#ifndef FIBAROCONTROLLER_H
#define FIBAROCONTROLLER_H

#include <QObject>
#include <QQmlListProperty>
#include <QThreadPool>
#include <QJsonDocument>
#include <QMutex>
#include <QTimer>

extern bool gdebug;

class AsyncFibaroResponse : public QObject, public QRunnable
{
    Q_OBJECT
public:
    AsyncFibaroResponse(const QString &path, const QString &basic, const QString &type)
        : m_type(type), m_path(path), m_basic(basic)
    {

    }

    void run() override;
signals:
    void listChanged(QVariantList, QString type);
private:
    QString m_type, m_path, m_basic;
    QVariantList m_list;
};

class FibaroController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList RoomList READ getRoomList NOTIFY roomListChanged)
    Q_PROPERTY(QVariantList SectionList READ getSectionList NOTIFY sectionListChanged)
    Q_PROPERTY(QVariantList DeviceList READ getDeviceList NOTIFY deviceListChanged)
    Q_PROPERTY(QVariantList SceneList READ getSceneList NOTIFY sceneListChanged)
    Q_PROPERTY(QVariantList IconList READ getIconList NOTIFY iconListChanged)

    Q_PROPERTY(QVariantList CustomList READ getCustomList NOTIFY customListChanged)
public:
    explicit FibaroController(QObject *parent = nullptr);

    void setSettings(bool enabled, QString ip, QString user, QString password, int port);

    void populate();
    void getRoomDevices(int id);

    QVariantList getRoomList();
    QVariantList getSectionList();
    QVariantList getDeviceList();
    QVariantList getSceneList();
    QVariantList getIconList();
    QVariantList getCustomList();

    void deviceAction(int id, QString action);
    void deviceToggleIndex(int index);

    QString getIconURL(QString type, QString id, int index);

    void setEnabled(bool);
    bool getEnabled();

    QStringList getSettings();
    void setSettings(QStringList);

    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void roomListChanged();
    void sectionListChanged();
    void deviceListChanged();
    void sceneListChanged();
    void iconListChanged();

    void customListChanged();

    void alertChange(QString id, bool value);
    void pageTrigger(QString page, int index);
public slots:
    void listChanged(QVariantList, QString);
private slots:
    void tryConnect();
    void tryRefresh();
private:
    QString m_basic;
    QString m_ip;
    int m_port;
    QString m_uname;
    QString m_password;

    bool m_enabled;

    int m_lastTime;
    QTimer timerConenct, timerDuration;
    bool m_connected;

    QVariantList m_rooms, m_sections, m_devices, m_scenes, m_icons, m_cusIndex;
    QList<int> m_custom;
    QMap<int, int> mapDevices;

    QMutex l_devices;

    QThreadPool pool;

    bool sendCommand(QString url, QString Method, QString body = "");

    void populateRefresh(QVariantList list);

    void indexRooms();
    void parseRoomDevices();
};

#endif // FIBAROCONTROLLER_H

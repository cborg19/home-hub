import QtQuick 2.11
import QtQuick.Controls 2.4

Item {
     id: album
     width: imageWidth
     height: imageHeight

     function getLabelY(pos){
         if(pos === 0 && album.title !== "")
             return 10;
         else if(pos === 1 && album.title !== "" && album.subtitle !== "")
             return 90;
         else if(pos === 1 && album.title === "" && album.subtitle !== "")
             return 10;
         else if(pos === 2 && album.title !== "" && album.subtitle !== "" && album.date !== "")
             return 130;
         else if(pos === 2 && album.title === "" && album.subtitle !== "" && album.date !== "")
             return 60;
         else if(pos === 2 && album.title !== "" && album.subtitle === "" && album.date !== "")
             return 90;
         else if(pos === 2 && album.title === "" && album.subtitle === "" && album.date !== "")
             return 10;
         return 600;
     }

     function getPosX(pos){
        return (pos % coloum) * album.imgWidth;
     }

     function getPosY(pos){
        return Math.floor(pos / coloum) * album.imgHeight;
     }

     function getImgDim(){
         var coloum = 3;

         var sourceLength = sourceList.length;
//console.log("ALBUMS a",sourceLength);
         if(sourceLength < coloum) coloum = sourceLength;

         var c = (Math.floor(sourceLength/coloum))*coloum;
//console.log("ALBUMS b",c);
         sourceLength = c;
//console.log("ALBUMS c",sourceLength);

         var words = name.split("|");
         album.title = words[4];
         album.subtitle = words[2];
         album.date = words[3];
         album.coloum = coloum;
         album.imgCount = sourceLength;
         album.imgHeight = imageHeight/(Math.floor(sourceLength/coloum));
         album.imgWidth = imageWidth/(Math.floor(coloum));
//console.log("ALBUMS A",coloum,sourceLength,album.imgHeight,album.imgWidth);
     }

     property int imgWidth: 0
     property int imgHeight: 0
     property int imgCount: 0
     property int coloum: 0
     property string title: ""
     property string subtitle: ""
     property string date: ""

     Item {
         anchors.fill: parent;

         Repeater {
             id: imgDisplay
             model: album.imgCount;
             Image {
                 x: album.getPosX(index)
                 y: album.getPosY(index)
                 //source: sourceList[index].name
                 visible: false
                 cache: true
                 sourceSize.width: imgWidth
                 sourceSize.height: imgHeight
                 fillMode: Image.PreserveAspectFit
             }
         }

         Rectangle {
             z:1
             anchors.fill: parent;
             color: Qt.hsla(0,0,0,0.3)
         }

         Label {
             z:1
             x: 20
             y: album.getLabelY(0)
             text: qsTr(album.title)
             color: "white"
             font.pointSize: 60
             font.family: "Helvetica"
             font.bold: true
         }

         Label {
             z:1
             x: 20
             y: album.getLabelY(1)
             text: qsTr(album.subtitle)
             color: "white"
             font.pointSize: 30
             font.family: "Helvetica"
             font.bold: true
         }

         Label {
             z:1
             x: 20
             y: album.getLabelY(2)
             text: qsTr(album.date)
             color: "white"
             font.pointSize: 20
             font.family: "Helvetica"
             font.bold: true
         }
     }

     Button {
         id: btMenu
         x:0
         y:0
         height: imageWidth
         width: imageHeight
         focusPolicy: Qt.NoFocus
         background: Rectangle {
             color: "transparent"
         }
     }

     Connections {
         target: btMenu
         onClicked: {
             window.switchToInfo(true);
         }
     }

     property int test: view.currentId === slideId ? shown() : stopped()
     property bool initialised: false
     property bool sourceLoaded: false

     function loadInitial(){
         if(!album.sourceLoaded && album.imgCount !== 0){
             //console.log("loadInitial album",album.imgCount);
             for(var x=0; x<album.imgCount; x++){
                 var pointer = imgDisplay.itemAt(x);
                 pointer.source = galleryPath + sourceList[x].name
                 //console.log("loadInitial path",galleryPath + sourceList[x].name);

                 pointer.x = album.getPosX(x)
                 pointer.y = album.getPosY(x)
                 pointer.visible = true
                 pointer.sourceSize.width = album.imgWidth
                 pointer.sourceSize.height = album.imgHeight
             }
             album.sourceLoaded = true
         }

         delayCheck.start();
     }

     Component.onCompleted: {
         album.getImgDim();
         delayLoad.start();
     }

     Timer {
        id: delayLoad
        interval: 1500
        running: false
        repeat: false
        onTriggered:{
            loadInitial()
        }
    }

    Timer {
        id: delayCheck
        interval: 250
        running: false
        repeat: true
        onTriggered:{
            var allDone = true;
            //console.log("loadInitial delayCheck",album.imgCount);
            for(var x=0; x<album.imgCount; x++){
                var pointer = imgDisplay.itemAt(x);
                //console.log("loadInitial delayCheck a",pointer.sourceSize.width);
                if(pointer.sourceSize.width === 0){
                    //console.log("loadInitial delayCheck b");
                    allDone = false;
                    break;
                }
            }

            if(allDone && album.imgCount !== 0){
                album.initialised = true;
                busyIndication.running = false;
                delayCheck.stop();
            }
        }
    }

    BusyIndicator {
       id: busyIndication
       anchors.centerIn: parent
       running: false
       // 'running' defaults to 'true'
    }

    /*Connections {
        target: landscape
        onVisibleChanged:
            if(visible){
                console.log("LANDSCAPE Shown")
                shown();
            }else{
                console.log("LANDSCAPE stopped")
                stopped();
            }

        ignoreUnknownSignals: true
    }*/

    function shown(){
        console.log('SHOWN ALBUM', sourceList[0].name);
        if(!album.sourceLoaded){
           album.loadInitial();
           delayLoad.stop();
        }

        if(!album.initialised){
            busyIndication.running = true;
        }
        return 1
    }

    function stopped(){
         return 1
    }
}

import QtQuick 2.11
import QtQuick.Controls 2.4

Item {
     id: portrait
     width: imageWidth
     height: imageHeight
	 Rectangle {
         width: imageWidth
         height: imageHeight
         anchors.fill: parent;
         color: "#fff"
     }

     MouseArea {
         id: btInfo2
         x:300
         y:140
         z: 2
         height: 340
         width: 500
         onClicked: {
             console.log(mouseX,mouseY)
             if(view.currentId === slideId){
                 console.log("Portrait switchToInfo");
                 window.switchToInfo(true);
             }
         }
     }

     Image {
         id: img1
         x: 0
         fillMode: Image.PreserveAspectFit
         cache: true
         //Using SourceSize will store the scaled down image in memory instead of the whole original image
         sourceSize.width: imageWidth
         sourceSize.height: imageHeight
         Rectangle {
             anchors.fill: parent;
             color: Qt.hsla(0,0,0,0.4)
         }
     }
     Image {
         id: img2
         x: imageWidth/4
         fillMode: Image.PreserveAspectFit
         cache: true
         //Using SourceSize will store the scaled down image in memory instead of the whole original image
         sourceSize.width: imageWidth/2
         sourceSize.height: imageHeight
     }

     property int test: view.currentId === slideId ? shown() : stopped()
     property bool initialised: false
     property bool sourceLoaded: false

     function loadInitial(){
         if(!portrait.sourceLoaded){
             img1.source = galleryPath + sourceList[0].name
             img2.source = galleryPath + sourceList[0].name
             portrait.sourceLoaded = true
         }

         delayCheck.start();
     }

     Component.onCompleted: {
         delayLoad.start();
         console.log("portrait",imageWidth,imageHeight)
     }

     Timer {
        id: delayLoad
        interval: 1500
        running: false
        repeat: false
        onTriggered:{
            portrait.loadInitial()
        }
    }

    Timer {
        id: delayCheck
        interval: 250
        running: false
        repeat: true
        onTriggered:{
            if(img1.sourceSize.width !== 0 && img2.sourceSize.width !== 0){
                portrait.initialised = true;
                busyIndication.running = false;
                delayCheck.stop();
            }
        }
    }

    BusyIndicator {
       id: busyIndication
       anchors.centerIn: parent
       running: false
       // 'running' defaults to 'true'
    }

    /*Connections {
        target: portrait
        onVisibleChanged:
            if(visible){
                console.log("PORTRAIT Shown")
                shown();
            }else{
                console.log("PORTRAIT stopped")
                stopped();
            }

        ignoreUnknownSignals: true
    }*/

    function shown(){
        console.log('SHOWN PORT', sourceList[0].name);
        if(!portrait.sourceLoaded){
           portrait.loadInitial();
           delayLoad.stop();
        }

        if(!portrait.initialised){
            busyIndication.running = true;
        }
        return 1
    }

    function stopped(){
         return 1
    }
}

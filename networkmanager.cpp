#include "networkmanager.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <QEventLoop>
#include <QTimer>
#include <QtCore/qdebug.h>

#include <QNetworkInterface>


NetworkManager::NetworkManager(QObject *parent) : QObject(parent)
{
    awaitingResponse = false;
}


bool NetworkManager::canSend(){return !awaitingResponse; }

NetworkManagerResult NetworkManager::sendHTTPGetRequestSynchronous(QString url, int timeoutMilliseconds /* = 3000*/, QString header){

    awaitingResponse = true;
    emit busyChanged(true);

    NetworkManagerResult result;

    QNetworkAccessManager *manager = new QNetworkAccessManager();
    QNetworkRequest request;
    QEventLoop loop;
    QTimer getTimer;

    request.setUrl(QUrl(url));
    //request.setRawHeader("User-Agent", "Mozilla Firefox");
    if(header != "")
        request.setRawHeader("Authorization", header.toLocal8Bit());

    QSslConfiguration conf = request.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(conf);

    // connect the timeout() signal of getTimer object to quit() slot of event loop
    QTimer::connect(&getTimer,SIGNAL(timeout()),&loop, SLOT(quit()));
    QObject::connect(manager, SIGNAL(finished(QNetworkReply*)),&loop, SLOT(quit()));
    QNetworkReply *resp = manager->get( request );
    getTimer.start(timeoutMilliseconds); // milliSeconds wait period for get() method to work properly
    loop.exec();

    if(resp == nullptr || (resp != nullptr && resp->isFinished() == false))
    {
        //check if response didnt finish due to TimeOut.

        if(resp!=nullptr) resp->abort();

        result.response = "Network Timeout Error";
        result.errorResponse = -1;
    }
    else if( resp->error() != QNetworkReply::NoError )
    {
        // Error - SIGNAL(finished()) was raised but get() opn failed & returned with error

        result.response = resp->errorString();
        result.errorResponse = -2;
    }
    else
    {
        // get() operation was Successful !.
        // read the response available in the 'resp' variable as a QString & parse it.
        // Obtain the necessary result and etc.
        result.binary = resp->readAll();
        result.errorResponse = 0; //success
    }

    delete resp;
    delete manager;

    awaitingResponse = false;
    emit busyChanged(false);


    return result;
}

//TODO:: HTTP POST






import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 800
    height: 480

    background: Rectangle{
        color:"transparent"
    }

    header: Label {
        text: qsTr("Music")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Button {
        id: btShutdown
        x: 40
        y: 50
        width: 100
        height: 35
        text: qsTr("Scan Directory")
    }

    Connections {
        target: btShutdown
        onClicked: {
            controller.scanForMusic();
        }
    }
}

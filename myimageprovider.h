#ifndef MYIMAGEPROVIDER_H
#define MYIMAGEPROVIDER_H
#include <QQuickImageProvider>
#include <QDebug>
#include <QImage>
#include <QThreadPool>
#include <QDate>
#include "networkmanager.h"

class AsyncImageResponse : public QQuickImageResponse, public QRunnable
{
public:
    AsyncImageResponse(const QString &id, const QSize &requestedSize)
     : m_id(id), m_requestedSize(requestedSize)
    {
        this->thread()->setPriority(QThread::LowestPriority);
        setAutoDelete(false);
    }

    ~AsyncImageResponse();

    QQuickTextureFactory *textureFactory() const override
    {
        return QQuickTextureFactory::textureFactoryForImage(m_image);
    }

    void run() override;

    QString m_id;
    QSize m_requestedSize;
    QImage m_image;
private:
    //QNetworkAccessManager *manager;
    void getURLImage(QString url, QString params);
};

class MyImageProvider : public QQuickAsyncImageProvider
{
public:
    MyImageProvider();

    QQuickImageResponse *requestImageResponse(const QString &id, const QSize &requestedSize) override;
private:
    QThreadPool pool;

    QImage data;
    QString md5;

    struct imgData {
        QImage data;
        QString md5;
    };

    QMap<QString, imgData> list;

    void addToCache();
    void clearCache();
};

class CacheImageProvider : public QObject, public QQuickImageProvider
{
    Q_OBJECT
public:
    CacheImageProvider();

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize){
        if(m_image.isNull()){
            qDebug() << "Erro ao prover a imagem";
        }

        return m_image;
    }

public slots:
    void carregaImagem(QImage i) { m_image = i; }

private:
    QImage m_image;
};
#endif // MYIMAGEPROVIDER_H

#ifndef VOICEASSISTANT_H
#define VOICEASSISTANT_H

#include <QObject>
#include <QThreadPool>

class AsyncVoiceResponse : public QObject, public QRunnable
{
    Q_OBJECT
public:
    AsyncVoiceResponse(const QString &response)
        : m_response(response)
    {

    }

    void run() override;
signals:
    void voiceChanged();
private:
    QString m_response;
};

class VoiceAssistant : public QObject
{
    Q_OBJECT
public:
    explicit VoiceAssistant(QObject *parent = nullptr);

    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void assistantVoice(QString);

    void pageTrigger(QString page, int index);
public slots:
    void processAssistant(QString);
private slots:
    void voiceChanged();
private:
    QThreadPool pool;
};

#endif // VOICEASSISTANT_H

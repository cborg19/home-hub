import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    id: calendarItem
    signal complete()

    property date select: null
    Calendar {
        minimumDate: new Date()
        maximumDate: new Date(2020, 0, 1)
        onPressed: {
            calendarItem.select = date
            calendarItem.complete()
        }
    }
}

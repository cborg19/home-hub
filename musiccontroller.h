#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H

#include <QObject>
#include <QQmlListProperty>
#include <QThreadPool>
#include <QJsonDocument>
#include <QMutex>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QImage>
#include "networkmanager.h"

class AsyncMusicFilesResponse : public QObject, public QRunnable
{
    Q_OBJECT

    Q_PROPERTY(QVariantList list READ getList NOTIFY listChanged)

public:
    AsyncMusicFilesResponse(const QString &path, const QString &type)
        : m_type(type), m_path(path)
    {

    }

    void run() override;

    QVariantList getList() { return m_list; }
signals:
    void listChanged(QVariantList, QList<int>);
private:
    QString m_type, m_path;
    QVariantList m_list;
    QList<int> m_index;
};

class AsyncInternetRadioResponse : public QObject, public QRunnable
{
    Q_OBJECT

    Q_PROPERTY(QVariantList list READ getList NOTIFY listChanged)

public:
    AsyncInternetRadioResponse(const QString &path, const QString &url, const QString &type)
        : m_url(url), m_type(type), m_path(path)
    {

    }

    void run() override;

    QVariantList getList() { return m_list; }
signals:
    void listChanged(QVariantList, QList<int>);
private:
    QString m_url, m_type, m_path;
    QVariantList m_list;
    QList<int> m_index;

    void parseWorld(QJsonDocument doc);
    void parseListStation(QJsonDocument doc, QString);
    void parseLanguage(QJsonDocument doc);
    void parseTag(QJsonDocument doc);
    void parseFav(QJsonDocument doc);
};

class MusicController : public QMediaPlayer
{
    Q_OBJECT

    Q_PROPERTY(QString pathName READ pathName WRITE setPathName)
    Q_PROPERTY(QVariantList internetRadio READ getInternetRadioList NOTIFY internetRadioListChanged)
    Q_PROPERTY(QList<int> internetRadioIndex READ getInternetRadioListIndex NOTIFY internetRadioListIndexChanged)
    Q_PROPERTY(QString InternetRadioPosition READ getInternetRadioPosition NOTIFY internetRadioPositionChanged)

    Q_PROPERTY(QVariantList MusicList READ getMusicList NOTIFY musicListChanged)
    Q_PROPERTY(QList<int> MusicListIndex READ getMusicListIndex NOTIFY musicListIndexChanged)

    Q_PROPERTY(int volumeAlarm READ getVolumeAlarm WRITE setVolumeAlarm NOTIFY volumeAlarmChanged)

    Q_PROPERTY(bool repeat READ getRepeat WRITE setRepeat NOTIFY repeatChanged)
    Q_PROPERTY(bool isPlaying READ getIsPlaying NOTIFY isPlayingChanged)
    Q_PROPERTY(QVariantMap mediaData READ getMediaData NOTIFY mediaDataChanged)

    //Q_PROPERTY(bool source READ getIsPlaying  NOTIFY isPlayingChanged)
   // Q_PROPERTY(qint64 currentPosition READ getCurrentPosition WRITE setCurrentPosition NOTIFY currentPositionChanged)
     Q_PROPERTY(bool pauseVoice READ getVoice WRITE setVoice)
public:
    explicit MusicController(QMediaPlayer *parent = nullptr);

    QString pathName();
    void setPathName(const QString &pathName);

    QVariantList getInternetRadioList();
    QList<int> getInternetRadioListIndex();
    QString getInternetRadioPosition();

    QVariantList getMusicList();
    QList<int> getMusicListIndex();
    void setMusicList(QString);

    void dismissSounds();

    void changeInternetRadioList(QString v, QString p);
    void addInternetRadioFav(QString name, QString icon, QString tag, QString params);

    void scanMusicDirectory();

    //music controls
    Q_INVOKABLE void playSource(QString file);
    Q_INVOKABLE void playlistSource(QString file);
    Q_INVOKABLE void playlistIndex(int);

    Q_INVOKABLE void musicShuffle();
    Q_INVOKABLE void musicNext();
    Q_INVOKABLE void musicPrevious();

    Q_INVOKABLE void musicStop();

    Q_INVOKABLE QImage getMediaImage();
    //void setPlayList(QString file);
    int getVolumeAlarm();
    void setVolumeAlarm(int);
    bool getRepeat();
    void setRepeat(bool);
    bool getIsPlaying();
    //qint64 getCurrentPosition();
    //void setCurrentPosition(qint64);

    QVariantMap getMediaData();

    bool getVoice(){ return m_voice; }
    void setVoice(bool v){ m_voice = v; }

    //assistant
    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void internetRadioListChanged();
    void internetRadioListIndexChanged();
    void internetRadioPositionChanged();

    void musicListChanged();
    void musicListIndexChanged();
    void pageTrigger(QString page, int index);

    void volumeAlarmChanged();

    void repeatChanged();
    void isPlayingChanged(bool);
    void mediaDataChanged(QVariantMap);
    void mediaImageChanged();
   // void currentPositionChanged(qint64 position);
public slots:
    void triggerSound(QString);
    void listChanged(QVariantList, QList<int>);
    void FileslistChanged(QVariantList, QList<int>);
    void ProcessAssistant(QString);

private slots:
    void mediaStatusCheckChanged(QMediaPlayer::MediaStatus status);
    void stateChangedCheckChanged(QMediaPlayer::State state);
    void metaDataCheckChanged();
  //  void positionChanged(qint64 position);
private:
    QMutex l_scanning;
    QString i_position, m_pathName;
    QVariantList i_list;
    QVariantList i_musicList;
    QList<int> i_index, i_musicindex;

    QThreadPool pool;

    void getIRMainPage();

    //QMediaPlayer mediaPlayer;
    int v_alarm;
    bool m_repeat;
    bool m_playing;
    qint64 m_position;

    QVariantMap m_metadata;
    QImage m_image;
    QMediaPlaylist m_playlist;

    bool m_voice, m_playingVoice;
    bool pauseData;
    QMediaContent pauseMedia;
    qint64 pausePosition;
    QMediaPlaylist * pausePlaylist;
    int pausePlayListPos;
};

#endif // MUSICPLAYER_H

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import QtMultimedia 5.11

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    x: imageWidth - 255
    y: 5
    width: 250
    height: 80

    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    Item {
        x: 2
        y: 2
        width: 75
        height: 76

        Rectangle{
            color:"black"
        }

        Image {
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            source: controller.musicMediaData.songArt
        }
    }

    Item {
        id: songLabelContainer
        x: 90
        y: 5
        width: parent.width - 90
        height: 25

        clip: true

        Layout.fillWidth: true
        Layout.preferredHeight: songNameLabel.implicitHeight

        SequentialAnimation {
            running: true
            loops: Animation.Infinite

            PauseAnimation {
                duration: 10000
            }
            ParallelAnimation {
                XAnimator {
                    target: songNameLabel
                    from: 0
                    to: -(songLabelContainer.width - songNameLabel.implicitWidth)
                    duration: 10000
                }
            }
            ParallelAnimation {
                XAnimator {
                    target: songNameLabel
                    from: 0
                    to: 0
                    duration: 100
                }
            }
            PauseAnimation {
                duration: 10000
            }
        }

        Rectangle {
            id: leftGradient
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#0d0d0e57"
                }
                GradientStop {
                    position: 1
                    color: "#000000ba"
                }
            }

            width: height
            height: parent.height
            anchors.left: parent.left
            z: 1
            rotation: -90
            opacity: 0
        }

        Label {
            id: songNameLabel
            text: controller.musicMediaData.Title
            color: "black"
            font.pointSize: 12
            font.family: "Helvetica"
            font.bold: true
        }

        Rectangle {
            id: rightGradient
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#000000ba"
                }
                GradientStop {
                    position: 1
                    color: "#0d0d0e57"
                }
            }

            width: height
            height: parent.height
            anchors.right: parent.right
            rotation: -90
        }
    }

    Item {
        x: 90
        y: 20
        ColumnLayout {
            spacing: 2
            Label {
                text: controller.musicMediaData.Artist
                color: "black"
                font.pointSize: 10
                font.family: "Helvetica"
            }
            Label {
                text: controller.musicMediaData.Album
                color: "black"
                font.pointSize: 10
                font.family: "Helvetica"
            }
        }
    }

    Label {
        x: 90
        y: 50
        text: SimpleJsTools.milltoString(controller.musicPosition)
        color: "black"
        font.pointSize: 8
        font.family: "Helvetica"
    }

    Label {
        x: parent.width - 30
        y: 50
        text: SimpleJsTools.milltoString(controller.musicDuration)
        color: "black"
        font.pointSize: 8
        font.family: "Helvetica"
    }

    Slider {
        id: seekSlider
        value: controller.musicPosition
        to: controller.musicDuration

        x: 90
        y: 60
        width: parent.width - 95
        height: 10

        handle: Rectangle {
            x: seekSlider.visualPosition * (seekSlider.width - width)
            y: (seekSlider.height - height) / 2
            width: 10
            height: 10

            radius: 5
            color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
            border.color: "#9c27b0"
        }

        background: Rectangle {
            y: (seekSlider.height - height) / 2
            height: 5
            radius: 2
            color: "#686868" //background slider

            Rectangle {
                width: seekSlider.visualPosition * parent.width
                height: parent.height
                color: "#b92ed1" //done part
                radius: 2
            }
        }
    }

    Label {
        x: 90+35
        y: 70
        text: musicplayer.currentSongIndex===-1?"":"Now Playing - "+ (musicplayer.currentSongIndex+1) +" of "+musicplaylist.itemCount
        color: "black"
        font.pointSize: 6
        font.family: "Helvetica"
        font.bold: true
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            window.switchToMusic()
        }
    }
}

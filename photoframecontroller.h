#ifndef PHOTOFRAMECONTROLLER_H
#define PHOTOFRAMECONTROLLER_H

#include <QObject>
#include <QThread>
#include "imagescan.h"
#include "settings.h"
#include "weather.h"
#include "gpio.h"
#include "lightsensor.h"
#include "musiccontroller.h"
#include "lcdcontroller.h"
#include "workerthread.h"
#include "earthdata.h"
#include "volumecontroller.h"
#include "kodicontroller.h"
#include "myimageprovider.h"
#include "sydneytrains.h"
#include "fibarocontroller.h"
#include "foodcontroller.h"
#include "timercontroller.h"
#include "voiceassistant.h"
#include "alarmcontroller.h"
#include "newscontroller.h"

extern bool gdebug;

class PhotoFrameController : public QObject
{
    Q_OBJECT
    QThread wThread, bThread;

    //Gallery/Photos
    Q_PROPERTY(QString pathGallery READ pathName WRITE setPathName )
    Q_PROPERTY(QVariantList galleryDataModel READ getList NOTIFY listChanged)
//    Q_PROPERTY(QQmlListProperty<QObject> galleryDataModel READ getList NOTIFY listChanged)
    Q_PROPERTY(int galleryDataSize READ getSlideSize NOTIFY slideSizeChanged)
    Q_PROPERTY(int slideInterval READ slideInterval WRITE setslideInterval)
    Q_PROPERTY(int otherInterval READ otherInterval WRITE setOtherInterval)

    //Weather
    Q_PROPERTY(QQmlListProperty<QObject> weatherDataModel READ getWeather NOTIFY weatherChanged)
    Q_PROPERTY(int weatherDataSize READ getWeatherSize NOTIFY weatherSizeChanged)
    Q_PROPERTY(QStringList weatherOption READ getWeatherOption NOTIFY weatherOptionChanged)
    Q_PROPERTY(int weatherInterval READ weatherInterval WRITE setWeatherInterval)
    Q_PROPERTY(int weatherPrimary READ getWeatherPrimary NOTIFY weatherPrimaryChanged)

    //Music
    Q_PROPERTY(int volume READ getVolume )
    Q_PROPERTY(QVariantList internetRadio READ getInternetRadioList NOTIFY internetRadioListChanged)
    Q_PROPERTY(QList<int> internetRadioIndex READ getInternetRadioListIndex NOTIFY internetRadioListIndexChanged)
    Q_PROPERTY(QString InternetRadioPosition READ getInternetRadioPosition NOTIFY internetRadioPositionChanged)
    Q_PROPERTY(QVariantList MusicList READ getMusicList NOTIFY musicListChanged)
    Q_PROPERTY(QList<int> MusicListIndex READ getMusicListIndex NOTIFY musicListIndexChanged)

    Q_PROPERTY(bool isMusicPlaying READ getMusicIsPlaying NOTIFY isPlayingMusicChanged)
    Q_PROPERTY(qint64 musicPosition READ getMusicPosition NOTIFY musicPositionChanged)
    Q_PROPERTY(qint64 musicDuration READ getMusicDuration NOTIFY musicDurationChanged)
    Q_PROPERTY(QVariantMap musicMediaData READ getMusicMediaData NOTIFY musicMediaDataChanged)
    //Trains
    Q_PROPERTY(QVariantList sydneyTrainList READ getSydneyTrainList NOTIFY sydneyTrainListChanged)

    //Kodi
    Q_PROPERTY(bool kodiEnabled READ getKodiEnabled WRITE setKodiEnabled NOTIFY kodiEnableChanged)

    Q_PROPERTY(bool kodiConnected READ getKodiConnection NOTIFY kodiConnectionChanged)
    Q_PROPERTY(bool kodiPaused READ getKodiPaused NOTIFY kodiPausedChanged)
    Q_PROPERTY(bool kodiPlaying READ getKodiPlaying NOTIFY kodiPlayingChanged)

    Q_PROPERTY(double kodiVolume READ getKodiVolume NOTIFY kodiVolumeChanged)
    Q_PROPERTY(bool kodiMute READ getKodiMute NOTIFY kodiMuteChanged)

    Q_PROPERTY(QVariantMap kodiCurrentMetaData READ getKodiCurrentMetaData NOTIFY kodiCurrentMetaDataChanged)
    Q_PROPERTY(double kodiCurrentPercentage READ getKodiPercentage NOTIFY KodiCurrentPercentageChanged)
    Q_PROPERTY(QVariantMap kodiCurrentTime READ getKodiTime NOTIFY KodiCurrentTimeChanged)
    Q_PROPERTY(QVariantMap kodiCurrentTotalTime READ getKodiTotalTime NOTIFY KodiCurrentTotalTimeChanged)

    Q_PROPERTY(QVariantList KodiList READ getKodiList NOTIFY kodiListChanged)
    Q_PROPERTY(QList<int> KodiListIndex READ getKodiListIndex NOTIFY kodiListIndexChanged)
    Q_PROPERTY(QString KodiMenu READ getKodiMenu NOTIFY kodiMenuChanged)

    //Fibaro
    Q_PROPERTY(bool fibaroEnabled READ getFibaroEnabled WRITE setFibaroEnabled NOTIFY fibaroEnableChanged)

    Q_PROPERTY(QVariantList FibaroRoomList READ getFibaroRoomList NOTIFY roomFibaroListChanged)
    Q_PROPERTY(QVariantList FibaroSectionList READ getFibaroSectionList NOTIFY sectionFibaroListChanged)
    Q_PROPERTY(QVariantList FibaroDeviceList READ getFibaroDeviceList NOTIFY deviceFibaroListChanged)
    Q_PROPERTY(QVariantList FibaroSceneList READ getFibaroSceneList NOTIFY sceneFibaroListChanged)
    Q_PROPERTY(QVariantList FibaroCustomList READ getFibaroCustomList NOTIFY customFibaroListChanged)

    //Recipe
    Q_PROPERTY(bool recipeEnabled READ getRecipeEnabled WRITE setRecipeEnabled NOTIFY recipeEnableChanged)
    Q_PROPERTY(QVariantList recipeCategoryList READ getRecipeCategoryList NOTIFY categoryListChanged)
    Q_PROPERTY(QVariantList recipeList READ getRecipeList NOTIFY recipeListChanged)
    Q_PROPERTY(QVariantList recipeCurrent READ getRecipeCurrent NOTIFY currentRecipeChanged)

    //Timer
    Q_PROPERTY(QVariantList timerList READ getTimerList NOTIFY timerListChanged)

    //Alarms
    Q_PROPERTY(QVariantMap currentAlarm READ getCurrentAlarm NOTIFY currentAlarmChanged)
    Q_PROPERTY(QVariantList alarmList READ getAlarmList NOTIFY alarmListChanged)

    //Setting Option
    Q_PROPERTY(QString pathSettings READ pathSettings WRITE setPathSettings )
    Q_PROPERTY(QString nightBgColour READ nightBgColour WRITE setNightBgColour )
    Q_PROPERTY(QString nightFontColour READ nightFontColour WRITE setNightFontColour )

    Q_PROPERTY(int screenDayTrigger READ getScreenDayTrigger WRITE setScreenDayTrigger )
    Q_PROPERTY(int screenDayOn READ getScreenDayOn WRITE setScreenDayOn )
    Q_PROPERTY(int screenDayOff READ getScreenDayOff WRITE setScreenDayOff )
    Q_PROPERTY(int screenClockTrigger READ getScreenClockTrigger WRITE setScreenClockTrigger )
    Q_PROPERTY(int screenClockOn READ getScreenClockOn WRITE setScreenClockOn )
    Q_PROPERTY(int screenClockOff READ getScreenClockOff WRITE setScreenClockOff )
    Q_PROPERTY(int screenNightTrigger READ getScreenNightTrigger WRITE setScreenNightTrigger )
    Q_PROPERTY(int screenNightOn READ getScreenNightOn WRITE setScreenNightOn )
    Q_PROPERTY(int screenNightOff READ getScreenNightOff WRITE setScreenNightOff )

    //Noficiation
    Q_PROPERTY(bool assistantStatus READ getAssistantStatus NOTIFY assistantStatusChanged)
    Q_PROPERTY(QString assistantCommand READ getAssistantCommand NOTIFY assistantCommandChanged)

    Q_PROPERTY(bool showNotification READ getShowNotification WRITE setShowNotification)
    Q_PROPERTY(bool notificationAlwaysOn READ getNotificationAlwaysOn WRITE setNotificationAlwaysOn)
    Q_PROPERTY(QStringList allowNotification READ getAllowNotification WRITE setAllowNotification )

    Q_PROPERTY(QObject* MusicController READ getMusicController )
public:
    explicit PhotoFrameController(QObject *parent = nullptr);
    ~PhotoFrameController();

    QString pathName();
    void setPathName(const QString &pathName);
    QString pathSettings();
    void setPathSettings(const QString &pathName);

    //Gallery/Photos
    int slideInterval();
    void setslideInterval(const int &pathName);
    int otherInterval();
    void setOtherInterval(const int &pathName);

    //QQmlListProperty<QObject> getList();
    QVariantList getList();
    Q_INVOKABLE int getSlideSize();
    Q_INVOKABLE void scanGallery();

    Q_INVOKABLE void blockAlbum(int);
    Q_INVOKABLE void favouriteAlbum(int);
    Q_INVOKABLE void neturalAlbum(int);
    Q_INVOKABLE QVariantList getAlbumList();

    //Weather
    int weatherInterval();
    void setWeatherInterval(const int &pathName);

    QQmlListProperty<QObject> getWeather();
    int getWeatherSize();

    int getWeatherPrimary();

    QStringList getWeatherOption();
    Q_INVOKABLE QStringList getWeatherOptionAt(int x);
    Q_INVOKABLE void addWeatherOption(const QStringList &list);
    Q_INVOKABLE void updateWeatherOption(int pos, const QStringList &list);
    Q_INVOKABLE void removeWeatherOption(int pos);
    Q_INVOKABLE void makeWeatherPrimary(int pos);

    //Notifications
    bool getShowNotification();
    void setShowNotification(bool);
    bool getNotificationAlwaysOn();
    void setNotificationAlwaysOn(bool);
    QStringList getAllowNotification();
    void setAllowNotification(QStringList);

    bool getAssistantStatus();
    QString getAssistantCommand();

    //Kodi
    Q_INVOKABLE void keyPress(int keycode);
    bool getKodiConnection();
    bool getKodiPaused();
    bool getKodiPlaying();
    double getKodiVolume();
    bool getKodiMute();

    QVariantList getKodiList();
    QList<int> getKodiListIndex();
    QVariantList getKodiPlayLists();
    QString getKodiMenu();

    QVariantMap getKodiCurrentMetaData();

    double getKodiPercentage();
    QVariantMap getKodiTime();
    QVariantMap getKodiTotalTime();

    Q_INVOKABLE void setKodiPlayer(QString);
    Q_INVOKABLE void setKodiSettings(QStringList);
    Q_INVOKABLE QStringList getKodiSettings();
    Q_INVOKABLE bool getKodiEnabled();
    Q_INVOKABLE void setKodiEnabled(bool);
    Q_INVOKABLE void setKodiVolume(double);
    Q_INVOKABLE void getKodiTvShows(int start, int max = 0);
    Q_INVOKABLE void getKodiTvShow(int);
    Q_INVOKABLE void getKodiMovies(int start, int max = 0);
    Q_INVOKABLE void getKodiSeason(int, int);
    Q_INVOKABLE void getKodiVideo(int);
    Q_INVOKABLE void getKodiShowDetails(int);
    Q_INVOKABLE void getKodiPlayList();
    Q_INVOKABLE void addKodiToPlayList(QString);
    Q_INVOKABLE void showKodiGallery(QString);
    Q_INVOKABLE void showKodiPicture(QStringList);

    //Night Mode
    Q_INVOKABLE void alive();
    Q_INVOKABLE void screenPhoto();
    Q_INVOKABLE void swittchAwayMode();
    QString nightBgColour() const;
    void setNightBgColour(const QString &source);

    QString nightFontColour() const;
    void setNightFontColour(const QString &source);

    int getScreenDayTrigger();
    void setScreenDayTrigger(int);
    int getScreenDayOn();
    void setScreenDayOn(int);
    int getScreenDayOff();
    void setScreenDayOff(int);
    int getScreenClockTrigger();
    void setScreenClockTrigger(int);
    int getScreenClockOn();
    void setScreenClockOn(int);
    int getScreenClockOff();
    void setScreenClockOff(int);
    int getScreenNightTrigger();
    void setScreenNightTrigger(int);
    int getScreenNightOn();
    void setScreenNightOn(int);
    int getScreenNightOff();
    void setScreenNightOff(int);

    //settings
    void loadSettings();
    Q_INVOKABLE QString getSettingString(QString group, QString key, QString initial);
    Q_INVOKABLE void setSettingString(QString group, QString key, QString value);
    Q_INVOKABLE bool getSettingBool(QString group, QString key, bool initial);
    Q_INVOKABLE void setSettingBool(QString group, QString key, bool value);
    Q_INVOKABLE int getSettingInt(QString group, QString key, int initial);
    Q_INVOKABLE void setSettingInt(QString group, QString key, int value);

    //Audio
    Q_INVOKABLE void testMusic();
    int getVolume();
    Q_INVOKABLE void setVolume(int v);
    QVariantList getInternetRadioList();
    QList<int> getInternetRadioListIndex();
    QString getInternetRadioPosition();
    Q_INVOKABLE void changeInternetRadioList(QString, QString);
    Q_INVOKABLE void addInternetRadioFav(QString name, QString icon, QString tag, QString params);
    Q_INVOKABLE void scanForMusic();
    Q_INVOKABLE void setMusicList(QString);
    QVariantList getMusicList();
    QList<int> getMusicListIndex();
    bool getMusicIsPlaying();
    qint64 getMusicPosition();
    qint64 getMusicDuration();
    QVariantMap getMusicMediaData();
    Q_INVOKABLE QImage getMediaImage();

    //Trains
    Q_INVOKABLE void nextDefaultTrains();
    QVariantList getSydneyTrainList();

    //Fibaro
    Q_INVOKABLE void fibaroPopulate();
    Q_INVOKABLE void fibaroDeviceAction(int id, QString action);
    Q_INVOKABLE void fibaroDeviceToggleIndex(int index);
    Q_INVOKABLE void fibaroGetRoomDevices(int id);
    Q_INVOKABLE QString fibaroGetIconURL(QString type, QString id, int index);

    QVariantList getFibaroRoomList();
    QVariantList getFibaroDeviceList();
    QVariantList getFibaroSceneList();
    QVariantList getFibaroSectionList();
    QVariantList getFibaroCustomList();

    Q_INVOKABLE bool getFibaroEnabled();
    Q_INVOKABLE void setFibaroEnabled(bool);
    Q_INVOKABLE void setFibaroSettings(QStringList);
    Q_INVOKABLE QStringList getFibaroSettings();

    //Recipe
    Q_INVOKABLE bool getRecipeEnabled();
    Q_INVOKABLE void setRecipeEnabled(bool);
    Q_INVOKABLE void setRecipeSettings(QStringList);
    Q_INVOKABLE QStringList getRecipeSettings();

    QVariantList getRecipeCategoryList();
    QVariantList getRecipeList();
    QVariantList getRecipeCurrent();

    Q_INVOKABLE void recipeGetAllCategory();
    Q_INVOKABLE void recipeGetCategory(QString );
    Q_INVOKABLE void recipeGetRecipe(int );

    //Timers
    QVariantList getTimerList();
    Q_INVOKABLE void createTimer(QString Title, unsigned int time);
    Q_INVOKABLE void pauseTimer(int index);
    Q_INVOKABLE void resumeTimer(int index);
    Q_INVOKABLE void stopTimer(int index);
    Q_INVOKABLE void addMinuteToTimer(int index);

    //Alarms
    QVariantMap getCurrentAlarm();
    QVariantList getAlarmList();
    Q_INVOKABLE void setAlarm(int index, QString title, int timeHr, int timeMin, QString date, QString day, QString type, QString sound, bool snooze);
    Q_INVOKABLE void deleteAlarm(int index);
    Q_INVOKABLE void setAlarmEnable(int index, bool results);

    Q_INVOKABLE void dismissAlarm();

    //System Controls
    Q_INVOKABLE void reboot();
    Q_INVOKABLE void shutdown();

    MusicController * getMusicController(){ return &m_musicplayer; }
signals:
    void listChanged();
    void slideSizeChanged();
    void weatherChanged();
    void weatherSizeChanged();
    void weatherOptionChanged();

    void kodiEnableChanged();
    void kodiConnectionChanged();
    void kodiPlayingChanged();
    void kodiPausedChanged();
    void kodiCurrentMetaDataChanged();
    void KodiCurrentPercentageChanged();
    void KodiCurrentTimeChanged();
    void KodiCurrentTotalTimeChanged();
    void kodiVolumeChanged();
    void kodiMuteChanged();
    void kodiListChanged();
    void kodiListIndexChanged();
    void kodiMenuChanged();

    void operateScreen(const QString & screen);
    void primaryWeather();
    void weatherPrimaryChanged();

    void musicAllChanged();
    void musicCountChanged();
    void volume(const int vol);
    void internetRadioListChanged();
    void internetRadioListIndexChanged();
    void internetRadioPositionChanged();
    void musicListChanged();
    void musicListIndexChanged();
    void isPlayingMusicChanged(bool);
    void musicPositionChanged(qint64);
    void musicDurationChanged(qint64);
    void musicMediaDataChanged(QVariantMap);

    void mediaImageChanged();

    void sydneyTrainListChanged();

    void roomFibaroListChanged();
    void sectionFibaroListChanged();
    void deviceFibaroListChanged();
    void sceneFibaroListChanged();
    void customFibaroListChanged();
    void fibaroEnableChanged();

    void recipeEnableChanged();
    void categoryListChanged();
    void recipeListChanged();
    void currentRecipeChanged();

    void timerListChanged();

    void currentAlarmChanged();
    void alarmListChanged();

    void assistantStatusChanged();
    void assistantCommandChanged();
    void processAssistant(QString);

    void alertIcons(QString iconId, bool iconValue);
    void switchToPage(QString page, int index);
public slots:
    void handleScreen(const QString &);
    void handleWeatherPrimary();

    void handleMusicFilesChange();

//    void onMediaStatusChanged(QMediaPlayer::MediaStatus status);
private slots:
    //void syncTime();
    void kodiConnection(bool);
    void kodiIsPaused(bool);
    void kodiIsPlaying(bool);
    void kodiMetaDataChange();
    void KodiVolume(bool, double);
    void kodiPercentageChange();
    void kodiTimeChange();
    void kodiTimeTotalChange();

    void kodiTvListChange();
    void kodiMoviesListChange();

    void volumeChange(int);
    void InternetRadioListChange();
    void InternetRadioListIndexChange();
    void InternetRadioPosChange();
    void MusicListChange();
    void MusicPosChange();
    void MusicSongIsPlayingChange(bool);
    void MusicPosChanged(qint64);
    void MusicDurChanged(qint64);
    void MusicDataChanged(QVariantMap);

    void musicImageChanged();

    void fibaroRoomChange();
    void fibaroSectionChange();
    void fibaroDeviceChange();
    void fibaroSceneChange();
    void fibaroCustomChange();

    void sdyneyTrainListChange();

    void recipeCategoryListChanged();
    void recipefoodListChanged();
    void recipeCurrentChanged();

    void timersListChanged();

    void alarmCurrentChanged();
    void alarmListsChanged();

    void imagelistChanged();

    void assistantChange(bool, QString);
    void assistantVoice(QString);
    void assistantTimer();
    void pageTrigger(QString page, int index);
    void alertIconChange(QString id, bool value);
private:
    QString m_pathName;
    QString m_nightBgColor;
    QString m_nightFontColor;

    bool m_assistandEnabled;
    QString m_assistantCommand;
    QTimer t_assistant;

    Imagescan m_imagescan;
    Settings m_settings;
    WeatherController m_weather;
    Gpio m_gpio;
    LightSensor m_lightSensor;
    LCDController m_LCD;
    EarthData m_earth;
    VolumeController m_volume;
    KodiController m_kodi;
    SydneyTrains m_trains;
    FibaroController m_fibaro;
    FoodController m_recipe;
    VoiceAssistant m_assistant;
    TimerController m_timers;
    AlarmController m_alarms;
    MusicController m_musicplayer;
    NewsController m_news;

    WorkerThread *worker;
    BackgroundThread *background;

    //QTimer t_LoadDelay;
//    QMediaPlayer *m_player;
};

#endif // PHOTOFRAMECONTROLLER_H

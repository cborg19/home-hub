import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 800
    height: 480

    background: Rectangle{
        color:"transparent"
    }

    header: Label {
        text: qsTr("Recipe")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    property alias cbRecipeEnable: cbRecipeEnable
    property alias gbRecipeSettings: gbRecipeSettings
    property alias tfIpAddress: tfIpAddress
    property alias tfPort: tfPort
    property alias tfUserName: tfUserName
    property alias tfPassword: tfPassword
    property alias btUpdate: btUpdate

    CheckBox {
        id: cbRecipeEnable
        x: 38
        y: 22
        width: 328
        height: 40
        text: qsTr("Enable Recipe Connection")
        font.pointSize: 16
    }

    GroupBox {
        id: gbRecipeSettings
        x: 52
        y: 88
        width: 440
        height: 282
        font.pointSize: 12
        title: qsTr("Network Settings")

        Label {
            id: label
            x: 0
            y: 0
            text: qsTr("IP Address")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        Label {
            id: label1
            x: 6
            y: 50
            width: 70
            height: 24
            text: qsTr("Port")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        Label {
            id: label2
            x: 222
            y: 50
            height: 24
            text: qsTr("Http Port")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        Label {
            id: label3
            x: 5
            y: 102
            text: qsTr("Username")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        Label {
            id: label4
            x: 9
            y: 149
            text: qsTr("Password")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        TextField {
            id: tfIpAddress
            x: 92
            y: -10
            text: qsTr("")
            placeholderText: "Ip Address"
            validator: RegExpValidator {
                regExp: /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
            }
        }

        TextField {
            id: tfPort
            x: 92
            y: 42
            width: 90
            height: 40
            text: qsTr("")
            placeholderText: "Port"
        }

        TextField {
            id: tfUserName
            x: 92
            y: 93
            text: qsTr("")
            placeholderText: "UserName"
        }

        TextField {
            id: tfPassword
            x: 92
            y: 139
            text: qsTr("")
            font.family: "Arial"
            font.weight: Font.ExtraLight
            opacity: 0.8
            renderType: Text.NativeRendering
            placeholderText: "Password"
            echoMode: TextInput.PasswordEchoOnEdit
        }

        Button {
            id: btUpdate
            x: 316
            y: -10
            height: 42
            text: qsTr("Update")
            font.pointSize: 16
        }
    }
}

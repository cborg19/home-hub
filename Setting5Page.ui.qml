import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Dialogs 1.0

Page {
    width: 800
    height: 480

    background: Rectangle{
        color:"transparent"
    }

    header: Label {
        text: qsTr("Clock and Night Mode")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    property alias tBGColour: tBGColour
    property alias tFontColour: tFontColour
    property alias colorDialog: colorDialog
    property alias colorFontDialog: colorFontDialog

    Label {
        id: label
        x: 60
        y: 38
        text: qsTr("Background Night Colour")
        font.pointSize: 16
    }

    TextField {
        id: tBGColour
        x: 264
        y: 28
        text: qsTr("")
    }

    Button {
        id: btBGColour
        x: 500
        y: 28
        width: 120
        height: 35
        text: qsTr("Select")
    }

    Connections {
        target: btBGColour
        onClicked: {
            colorDialog.visible = true
        }
    }

    ColorDialog {
        id: colorDialog
        visible: false
        title: "Please choose a color"
        onAccepted: {
            tBGColour.text = colorDialog.color
            colorDialog.visible = false
        }
        onRejected: {
            colorDialog.visible = false
        }
    }

    Label {
        id: label1
        x: 60
        y: 103
        text: qsTr("Font Night Colour")
        font.pointSize: 16
    }

    TextField {
        id: tFontColour
        x: 264
        y: 93
        text: qsTr("")
    }

    Button {
        id: btFontColour
        x: 500
        y: 93
        width: 120
        height: 35
        text: qsTr("Select")
    }

    Connections {
        target: btFontColour
        onClicked: {
            colorFontDialog.visible = true
        }
    }

    ColorDialog {
        id: colorFontDialog
        visible: false
        title: "Please choose a color"
        onAccepted: {
            tFontColour.text = colorFontDialog.color
            colorFontDialog.visible = false
        }
        onRejected: {
            colorFontDialog.visible = false
        }
    }

    Label {
        id: label2
        x: 50
        y: 206
        text: qsTr("Night Brightness")
        font.pointSize: 16
    }
}

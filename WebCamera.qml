import QtQuick 2.11
import QtQuick.Controls 2.4

import QtWebEngine 1.4

Item {
     id: webCamera
     width: imageWidth
     height: imageHeight
     Image {
         id: img
         anchors.fill: parent;
         //source: galleryPath + imageSource.get(0).name
         //fillMode: Image.PreserveAspectFit
         cache: true
         //Using SourceSize will store the scaled down image in memory instead of the whole original image
         sourceSize.width: imageWidth
         sourceSize.height: imageHeight
     }

     Button {
         id: btMenu
         x:300
         y:140
         height: 340
         width: 500
         focusPolicy: Qt.NoFocus
         background: Rectangle {
             color: "transparent"
         }
     }

     Connections {
         target: btMenu
         onClicked: {
             window.switchToInfo(true);
         }
     }

     property int test: view.currentId === slideId ? shown() : stopped()
     property bool initialised: false
     property bool sourceLoaded: false

     function loadInitial(){
      //  theWebEngineView.url = "https://captiveye-kirribilli.qnetau.com/stream_scaling/public/default_iframe_n.asp?wmode=transparent"
     }

     Component.onCompleted: {
         delayLoad.start();
     }

     WebEngineView {
         id: theWebEngineView
         anchors.fill: parent
         url: "https://get.adobe.com/jp/flashplayer/"//"https://webcamsydney.com/"
         //url: "file:///Users/stephen/Downloads/pictureframehtml/index.html"
     }

     Timer {
        id: delayLoad
        interval: 1500
        running: false
        repeat: false
        onTriggered:{
            loadInitial()
        }
    }


    BusyIndicator {
       id: busyIndication
       anchors.centerIn: parent
       running: false
       // 'running' defaults to 'true'
    }


    function shown(){
        console.log('SHOWN LAND', sourceList[0].name);
        if(!webCamera.sourceLoaded){
           webCamera.loadInitial();
           delayLoad.stop();
        }

        if(!webCamera.initialised){
            busyIndication.running = true;
        }
        return 1
    }

    function stopped(){
         return 1
    }
}

import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools


Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle {
        color: "transparent"
    }

    Image {
        visible: controller.kodiCurrentMetaData.label!==undefined?true:false
        id: imgVideo
        x: 0
        y: 0
        width: 200
        height: imageHeight
        fillMode: Image.PreserveAspectCrop
        source: controller.kodiCurrentMetaData.fanImage !== undefined?"image://imageprovider/"+controller.kodiCurrentMetaData.fanImage:"" //"image://imageprovider/"+
    }

    RowLayout{
        x: 85
        y: 0
        spacing: 5
        visible: controller.kodiCurrentMetaData.label!==undefined?true:false

        Rectangle {
            width: 150
            height: 100
            color: "black"
            Image {
                id: imgPoster
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
                source: controller.kodiCurrentMetaData.bgImage !== undefined?"image://imageprovider/"+controller.kodiCurrentMetaData.bgImage:"" //
            }
        }

        ColumnLayout{
            spacing: 0
            Label {
                text: controller.kodiCurrentMetaData.label!==undefined?controller.kodiCurrentMetaData.label:""
                color: "white"
                font.pointSize: 26
                font.family: "Helvetica"
            }
            Label {
                text: kodiSwipeView.getShowTitle()
                color: "white"
                font.pointSize: 20
                font.family: "Helvetica"
            }

            RowLayout {
                id: timeRow
                spacing: 450
                function convertToString(v){
                    if(v === undefined) return "00:00"
                    var str = ""
                    if(v.hours > 0)
                        str += v.hours+":"
                    if(v.minutes < 10)
                        str += "0"+v.minutes+":"
                    else
                        str += v.minutes+":"
                    if(v.seconds < 10)
                        str += "0"+v.seconds
                    else
                        str += v.seconds
                    return str
                }

                Label {
                    text: timeRow.convertToString(controller.kodiCurrentTime)
                    color: "white"
                    font.pointSize: 10
                    font.family: "Helvetica"
                }

                Label {
                    text: timeRow.convertToString(controller.kodiCurrentTotalTime)
                    color: "white"
                    font.pointSize: 10
                    font.family: "Helvetica"
                }
            }
        }
    }

    Slider {
        id: seekSlider
        value: controller.kodiCurrentPercentage!==undefined?controller.kodiCurrentPercentage:0
        to: 100
        visible: controller.kodiCurrentMetaData.label!==undefined?true:false

        x: 240
        y: 95
        width: 520
        height: 15

        handle: Rectangle {
            x: seekSlider.visualPosition * (seekSlider.width - width)
            y: (seekSlider.height - height) / 2
            width: 12
            height: 12

            radius: 6
            color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
            border.color: "#9c27b0"
        }

        background: Rectangle {
            y: (seekSlider.height - height) / 2
            height: 8
            radius: 4
            color: "#686868" //background slider

            Rectangle {
                width: seekSlider.visualPosition * parent.width
                height: parent.height
                color: "#b92ed1" //done part
                radius: 4
            }
        }
    }

    RowLayout {
        x: 240
        y: 110
        spacing: 55
        visible: controller.kodiCurrentMetaData.label!==undefined?true:false

        Layout.alignment: Qt.AlignHCenter

        Rectangle {
            id: btPrevious
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_step_backward
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_MediaPrevious)
                }
            }
        }
        Rectangle {
            id: btFastBackwards
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_fast_backward
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_AudioRewind)
                }
            }
        }
        Rectangle {
            id: btPause
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: controller.kodiPaused?FontAwesome.icons.fa_play:FontAwesome.icons.fa_pause
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(controller.kodiPaused){
                        controller.keyPress(Qt.Key_MediaPlay)
                    }else{
                        controller.keyPress(Qt.Key_MediaPause)
                    }
                }
            }
        }
        Rectangle {
            id: btStop
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_stop
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_MediaStop)
                }
            }
        }
        Rectangle {
            id: btFastForward
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_fast_forward
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_AudioForward)
                }
            }
        }
        Rectangle {
            id: btForward
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_step_forward
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_MediaNext)
                }
            }
        }
    }
    RowLayout {
        x: 240
        y: 160
        spacing: 34

        Layout.alignment: Qt.AlignHCenter

        visible: controller.kodiCurrentMetaData.label!==undefined?true:false

        Rectangle {
            id: btVoluneMute
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: controller.kodiMute?FontAwesome.icons.fa_volume_off:FontAwesome.icons.fa_volume_up
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    controller.keyPress(Qt.Key_VolumeMute)
                }
            }
        }

        Rectangle {
            width: 220
        }

        Rectangle {
            id: btUndo
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_undo
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {

                }
            }
        }

        Rectangle {
            id: btRandom
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_random
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {

                }
            }
        }

        Rectangle {
            id: btellips
            width: 40
            height: 40
            radius: 20
            color: "#ce6ddf"
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 20
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_ellipsis_v
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {

                }
            }
        }
    }
    Slider {
        id: volumeSlider
        value: controller.kodiVolume
        to: 100

        x: 300
        y: 170
        width: 220
        height: 15

        visible: controller.kodiCurrentMetaData.label!==undefined?true:false

        handle: Rectangle {
            x: volumeSlider.visualPosition * (volumeSlider.width - width)
            y: (volumeSlider.height - height) / 2
            width: 24
            height: 24

            radius: 12
            color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
            border.color: "#9c27b0"
        }

        background: Rectangle {
            y: (volumeSlider.height - height) / 2
            height: 8
            radius: 4
            color: "#686868" //background slider

            Rectangle {
                width: volumeSlider.visualPosition * parent.width
                height: parent.height
                color: "#b92ed1" //done part
                radius: 4
            }
        }

        onPressedChanged: {
            controller.setKodiVolume(volumeSlider.value)
        }
    }

    RowLayout {
        x: 240
        y: 210
        spacing: 2

        visible: controller.kodiCurrentMetaData.label!==undefined?true:false

        Label {
            text: controller.kodiCurrentMetaData.rating!==undefined?controller.kodiCurrentMetaData.rating.toFixed(1):""
            color: "white"
            font.pointSize: 22
            font.family: "Helvetica"
        }

        ColumnLayout{
            spacing: 1
            Label {
                text: controller.kodiCurrentMetaData.votes!==undefined?controller.kodiCurrentMetaData.votes+" votes":""
                color: "white"
                font.pointSize: 8
                font.family: "Helvetica"
            }
            Label {
                text: "/10"
                color: "white"
                font.pointSize: 8
                font.family: "Helvetica"
            }
        }
    }

    ColumnLayout{
        id: tvEpisodeDetails
        y: 210
        spacing: 0
        visible: controller.kodiCurrentMetaData.type==="episode"?true:false

        anchors.right: parent.right

        function episode(s, e){
            if(e === undefined || s === undefined) return "";
            var st = "Season "
            if(s < 10) st+="0"
            st += s.toString()+" |Episode ";
            if(e < 10) st+="0"
            st += e.toString()
            return st;
        }

        Text {
            anchors.right: parent.right
            anchors.rightMargin: 50
            text: controller.kodiCurrentMetaData.premiered!==undefined?controller.kodiCurrentMetaData.premiered:""
            color: "white"
            font.pointSize: 10
            font.family: "Helvetica"
        }
        Text {
            anchors.right: parent.right
            anchors.rightMargin: 50
            text: controller.kodiCurrentMetaData.season!==undefined?tvEpisodeDetails.episode(controller.kodiCurrentMetaData.season,controller.kodiCurrentMetaData.episode):""
            color: "white"
            font.pointSize: 10
            font.family: "Helvetica"
        }
    }

    ColumnLayout{
        id: moveDetail
        y: 210
        spacing: 0
        visible: controller.kodiCurrentMetaData.type==="movie"?true:false

        anchors.right: parent.right

        function getMovieDetail(){
            if(controller.kodiCurrentMetaData.genre === undefined) return ""
            if(controller.kodiCurrentMetaData.genre.length === 0) return ""
            //console.log("GENE",controller.kodiCurrentMetaData.genre,controller.kodiCurrentMetaData.genre.length);
            return controller.kodiCurrentMetaData.genre.join(", ")
        }

        Text {
            anchors.right: parent.right
            anchors.rightMargin: 50
            text: controller.kodiCurrentMetaData.year!==undefined?controller.kodiCurrentMetaData.year:""
            color: "white"
            font.pointSize: 10
            font.family: "Helvetica"
        }
        Text {
            anchors.right: parent.right
            anchors.rightMargin: 50
            text: controller.kodiCurrentMetaData.genre!==undefined?moveDetail.getMovieDetail():""
            color: "white"
            font.pointSize: 10
            font.family: "Helvetica"
        }
    }

    Text {
        x: 240
        y: 240
        width: 520
        height: 100

        visible: controller.kodiCurrentMetaData.label!==undefined?true:false

        text: controller.kodiCurrentMetaData.plot!==undefined?controller.kodiCurrentMetaData.plot:""
        color: "white"
        font.pointSize: 14
        font.family: "Helvetica"
        wrapMode: Text.WordWrap
    }

    Item {
        x: 240
        y: 360
        width: 520
        height: 100

        visible: controller.kodiCurrentMetaData.label!==undefined?true:false
        RowLayout {
            spacing: 0
            Rectangle {
                id: btleftCast
                width: 40
                height: 40
                radius: 20
                color: "#ce6ddf"
                Text {
                    color: "white"
                    anchors.centerIn: parent
                    font.pointSize: 20
                    font.family: fontAwesome.name
                    text: FontAwesome.icons.fa_arrow_circle_left
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        cast.castback()
                    }
                }
            }

            Rectangle {
                id: cast
                width: 430
                height: 100

                property int posCast: 0
                function castLength(){
                    if(controller.kodiCurrentMetaData.cast !== undefined)
                        if(controller.kodiCurrentMetaData.cast.length > 4)
                            return 4
                        else return controller.kodiCurrentMetaData.cast.length
                    return 0
                }

                function castback(){
                    if(controller.kodiCurrentMetaData.cast === undefined) return
                    cast.posCast--;
                    if(cast.posCast < 0) cast.posCast = 0;
                }

                function castNext(){
                    if(controller.kodiCurrentMetaData.cast === undefined) return
                    cast.posCast++;
                    if(cast.posCast >= (controller.kodiCurrentMetaData.cast.length - 4)) cast.posCast = controller.kodiCurrentMetaData.cast.length-5;
                }

                /*function getCastPost(index){
                    if(controller.kodiCurrentMetaData.cast === undefined) return;
                    if(index >= controller.kodiCurrentMetaData.cast.length) return;
                    if(controller.kodiCurrentMetaData.cast[index].thumbnail !== undefined){
                        var httpReq = new XMLHttpRequest()
                        var url = 'http://192.168.178.27:8081/image/'+encodeURIComponent(controller.kodiCurrentMetaData.cast[index].thumbnail);

                        httpReq.open("GET", url, true);
                        httpReq.responseType = 'arraybuffer';

                        // Send the proper header information along with the request
                        httpReq.setRequestHeader("Authorization", "Basic ");

                        httpReq.onreadystatechange = (function(myxhr) {
                            return function() {
                                if(myxhr.readyState === 4 && myxhr.status === 200) {
                                    //console.log("KODI readyState",myxhr.responseText.length);
                                    var response = new Uint8Array(myxhr.response);
                                    var raw = "";
                                    for (var i = 0; i < response.byteLength; i++) {
                                        raw += String.fromCharCode(response[i]);
                                    }
                                    var image = 'data:image/png;base64,' + kodiSwipeView.base64Encode(raw);
                                    //console.log("GOT IMAGE", index)
                                    var pointer = castDisplay.itemAt(index)
                                    if(pointer !== null){
                                        pointer.height = 100
                                        pointer.width = 100
                                        pointer.source = image
                                    }
                                }
                            }
                        })(httpReq);

                        httpReq.send();
                    }
                    return ""
                }*/

                RowLayout {
                    spacing: 10
                    Repeater {
                        id: castDisplay
                        model: controller.kodiCurrentMetaData.cast !== undefined? cast.castLength() : 0
                        Rectangle {
                            height: 100
                            width: 100
                            color: "black"

                            Image {
                                anchors.fill: parent
                                fillMode: Image.PreserveAspectCrop
                                source: controller.kodiCurrentMetaData.cast !== undefined?"image://imageprovider/"+controller.kodiCurrentMetaData.cast[index+ cast.posCast].image : ""
                            }

                            Label {
                                anchors.bottom: parent.bottom
                                color: "white"
                                font.pointSize: 8
                                font.family: "Helvetica"
                                text: controller.kodiCurrentMetaData.cast[index + cast.posCast].role
                                Label {
                                    anchors.bottom: parent.top
                                    color: "white"
                                    font.pointSize: 8
                                    font.family: "Helvetica"
                                    font.bold: true
                                    text: controller.kodiCurrentMetaData.cast[index + cast.posCast].name
                                }
                            }
                        }
                    }
                }
            }

            Rectangle {
                id: btrightCast
                width: 40
                height: 40
                radius: 20
                color: "#ce6ddf"
                Text {
                    color: "white"
                    anchors.centerIn: parent
                    font.pointSize: 20
                    font.family: fontAwesome.name
                    text: FontAwesome.icons.fa_arrow_circle_right
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        cast.castNext()
                    }
                }
            }
        }
    }
}

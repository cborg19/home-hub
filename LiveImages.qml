import QtQuick 2.11
import QtQuick.Controls 2.4

Item {
     id: liveImages
     width: imageWidth
     height: imageHeight
     Image {
         id: img
         anchors.fill: parent;
         //source: galleryPath + imageSource.get(0).name
         //fillMode: Image.PreserveAspectFit
         cache: true
         //Using SourceSize will store the scaled down image in memory instead of the whole original image
         sourceSize.width: imageWidth
         sourceSize.height: imageHeight

         Text {
             x: 5
             y: 5
             font.pointSize: 20
             color: "white"
             text: sourceList[0].title
             font.bold: true
         }
     }

     MouseArea {
         id: btInfo2
         x:300
         y:140
         z: 2
         height: 340
         width: 500
         onClicked: {
             console.log(mouseX,mouseY)
             if(view.currentId === slideId){
                 console.log("liveImagesPage switchToInfo");
                 window.switchToInfo(true);
             }
         }
     }

     property int test: view.currentId === slideId ? shown() : stopped()
     property bool initialised: false
     property bool sourceLoaded: false

     function loadInitial(){
         if(!liveImages.sourceLoaded){
             img.source = sourceList[0].name
             liveImages.sourceLoaded = true
         }

         delayCheck.start();
     }

     Component.onCompleted: {
         delayLoad.start();
     }

     Timer {
        id: delayLoad
        interval: 1500
        running: false
        repeat: false
        onTriggered:{
            loadInitial()
        }
    }

    Timer {
        id: delayCheck
        interval: 250
        running: false
        repeat: true
        onTriggered:{
            if(img.sourceSize.width !== 0){
                liveImages.initialised = true;
                busyIndication.running = false;
                delayCheck.stop();
            }
        }
    }

    BusyIndicator {
       id: busyIndication
       anchors.centerIn: parent
       running: false
       // 'running' defaults to 'true'
    }

    /*Connections {
        target: landscape
        onVisibleChanged:
            if(visible){
                console.log("LANDSCAPE Shown")
                shown();
            }else{
                console.log("LANDSCAPE stopped")
                stopped();
            }

        ignoreUnknownSignals: true
    }*/

    function shown(){
        console.log('SHOWN LIVEIMAGE', sourceList[0].name);
        if(!liveImages.sourceLoaded){
           liveImages.loadInitial();
           delayLoad.stop();
        }

        if(!liveImages.initialised){
            busyIndication.running = true;
        }
        return 1
    }

    function stopped(){
         return 1
    }
}

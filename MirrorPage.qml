import QtQuick 2.0
import QtQuick.Controls 2.4
import Camera 1.0

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle {
        anchors.fill: parent
        color: "black"
    }

    function start() {
        console.log("MIRROR PAGE STARTED");
        camera.running = true
    }

    CameraWidget {
        id: camera
        anchors.fill: parent
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 50
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("MIRROR PAGE STOPED");
            camera.running = false
            console.log("MirrorPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

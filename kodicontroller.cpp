#include "kodicontroller.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrl>

#define LISTLIMIT 200

KodiController::KodiController(QObject *parent) : QObject(parent),
    RPC_prolog("{ \"jsonrpc\": \"2.0\""),
    RPC_epilog(", \"id\": 1 }"),
    response()
{
    m_connected = false;
    m_paused = false; //TODO get intial valve
    m_playing = false; //TODO get intial valve
    m_enabled = false;
    m_muted = false; //TODO get intial valve
    m_volume = 0; //TODO get intial valve
    m_timeCount = 0;
    m_total = 0;
    //movies_current = 0;
    connect(&socket, &QWebSocket::connected, this, &KodiController::connectionEstablished);
    connect(&socket, &QWebSocket::disconnected, this, &KodiController::connectionClosed);
    connect(&timerConenct, SIGNAL(timeout()), this, SLOT(tryConnect()));
    connect(&timerDuration, SIGNAL(timeout()), this, SLOT(duration()));

    timerConenct.start(5000);
}

void KodiController::setSettings(bool enabled, QString host, int port, int httpport, QString uname, QString password, bool tcp, bool eventserver)
{
    m_host = host;
    m_port = port;
    m_httpport = httpport;
    m_uname = uname;
    m_password = password;
    QString concatenated = m_uname + ":" + m_password;
    QByteArray data = concatenated.toLocal8Bit().toBase64();
    m_basic = "Basic " + data;
    m_tcp = tcp;
    m_eventserver = eventserver;
    m_connected = false;
    m_enabled = enabled;
    if(!enabled) timerConenct.stop();
}

void KodiController::tryConnect()
{
if(gdebug) qDebug() <<"CRASH KodiController tryConnect";
    timerConenct.stop();
    qDebug() << "KODI tryConnect";
    QString u = "ws://" + m_host + QStringLiteral(":") + QString::number(m_port) + QStringLiteral("/jsonrpc");
    timerConenct.start(20000);
    socket.open(QUrl(u));
if(gdebug) qDebug() <<"CRASH KodiController tryConnect";
}

void KodiController::connectionEstablished()
{
if(gdebug) qDebug() << "CRASH KodiController connectionEstablished";
    timerConenct.stop();
    m_connected = true;
    connect(&socket, &QWebSocket::textMessageReceived, this, &KodiController::record_response);
    emit connection(true);

    call_method("Player.GetActivePlayers", {});
if(gdebug) qDebug() << "CRASH KodiController connectionEstablished finished";
}

void KodiController::connectionClosed()
{
if(gdebug) qDebug() << "CRASH KodiController connectionClosed";
    if(m_connected){
        qDebug() << "KODI connection Closed";
        m_connected = false;
        timerConenct.start();
        emit connection(false);
    }
if(gdebug) qDebug() << "CRASH KodiController connectionClosed finished";
}

void KodiController::record_response(QString response)
{
if(gdebug) qDebug() << "CRASH KodiController record_response";
    QString copy = response;
    qDebug() << "Response received: \'" << copy << "\'.";
    this->response = response;

    QJsonObject obj;
    QJsonDocument doc = QJsonDocument::fromJson(response.toUtf8());
    if(!doc.isNull())
    {
        if(doc.isObject())
        {
            obj = doc.object();
            QString method = obj.value(QString("method")).toString();
            if(method == "Player.OnPause"){
                m_paused = true;
                m_playing = true;
                emit ispaused(true);
                timerDuration.stop();
            }else if(method == "Player.OnResume"){
                m_paused = false;
                m_playing = true;
                emit ispaused(false);
                m_timeCount = 0;
                timerDuration.start(1000);
            }else if(method == "Player.OnPlay"){
                m_paused = false;
                m_playing = true;
                emit ispaused(false);
                m_timeCount = 0;
                timerDuration.start(1000);

                QJsonObject parmObj = obj["params"].toObject();
                if(!parmObj.isEmpty()){
                    QJsonObject dataObj = parmObj["data"].toObject();
                    if(!dataObj.isEmpty()){
                        QJsonObject itemObj = dataObj["item"].toObject();
                        if(!itemObj.isEmpty()){
                            int id = obj.value(QString("id")).toInt();
                            QString type = obj.value(QString("type")).toString();

                            if(m_currentMetaData.count() == 0){
                                call_method("Player.GetItem", {{ "properties", "[\"title\",\"album\",\"artist\",\"season\",\"episode\",\"duration\",\"showtitle\",\"tvshowid\",\"thumbnail\",\"file\",\"fanart\",\"streamdetails\",\"year\",\"rating\",\"originaltitle\",\"plot\",\"art\",\"genre\",\"votes\",\"cast\",\"premiered\",\"plotoutline\",\"description\",\"tagline\",\"comment\"]"},{ "playerid", "1"  }});
                                call_method("Player.GetProperties", {{ "properties", "[\"type\",\"speed\",\"time\",\"percentage\",\"totaltime\",\"playlistid\",\"position\"]"},{ "playerid", "1"  }});
                            }else{
                                if(m_currentMetaData["id"] != id){
                                    call_method("Player.GetItem", {{ "properties", "[\"title\",\"album\",\"artist\",\"season\",\"episode\",\"duration\",\"showtitle\",\"tvshowid\",\"thumbnail\",\"file\",\"fanart\",\"streamdetails\",\"year\",\"rating\",\"originaltitle\",\"plot\",\"art\",\"genre\",\"votes\",\"cast\",\"premiered\",\"plotoutline\",\"description\",\"tagline\",\"comment\"]"},{ "playerid", "1"  }});
                                    call_method("Player.GetProperties", {{ "properties", "[\"type\",\"speed\",\"time\",\"percentage\",\"totaltime\",\"playlistid\",\"position\"]"},{ "playerid", "1"  }});
                                }
                            }
                        }
                    }
                }
                //"{\"jsonrpc\":\"2.0\",\"method\":\"Player.OnPlay\",\"params\":{\"data\":{\"item\":{\"id\":564,\"type\":\"movie\"},\"player\":{\"playerid\":1,\"speed\":1}},\"sender\":\"xbmc\"}}"
            }else if(method == "Player.OnStop"){
                m_paused = false;
                m_playing = false;
                m_currentMetaData.clear();
                emit currentMetaDataChanged();
                emit ispaused(false);
                emit isplaying(false);
                timerDuration.stop();
                m_timeCount = 0;
            }else if(method == "Playlist.OnClear"){
                //clear playlist
                m_playlist.clear();
            }else if(method == "Playlist.OnAdd"){
                //add to playlist
                QJsonObject parmObj = obj["params"].toObject();
                if(!parmObj.isEmpty()){
                    QJsonObject dataObj = parmObj["data"].toObject();
                    if(!dataObj.isEmpty()){
                        /*QJsonObject itemObj = dataObj["item"].toObject();
                        if(!itemObj.isEmpty()){
                            int id = obj.value(QString("id")).toInt();
                            QString type = obj.value(QString("type")).toString();
                            if(type == "episode"){

                            }else if(type == "movie"){

                            }
                        }*/
                        int id = dataObj["playlistid"].toInt(-1);
                        if(id >= 0){
                            call_method("Playlist.GetItems", {{"playlistid", QString::number(id)}, { "properties", "[\"title\",\"album\",\"artist\",\"season\",\"episode\",\"duration\",\"showtitle\",\"tvshowid\",\"thumbnail\",\"file\",\"fanart\",\"streamdetails\",\"year\",\"rating\",\"originaltitle\",\"plot\",\"art\",\"genre\",\"votes\",\"cast\",\"premiered\",\"plotoutline\",\"description\",\"tagline\",\"comment\"]"}},"libplaylist");
                        }
                    }
                }
            }else if(method == "Player.OnAVChange"){
                //"{\"jsonrpc\":\"2.0\",\"method\":\"Player.OnAVChange\",\"params\":{\"data\":{\"item\":{\"id\":564,\"type\":\"movie\"},\"player\":{\"playerid\":1,\"speed\":1}},\"sender\":\"xbmc\"}}"
            }else if(method == "Player.OnAVStart"){
                //"{\"jsonrpc\":\"2.0\",\"method\":\"Player.OnAVStart\",\"params\":{\"data\":{\"item\":{\"id\":564,\"type\":\"movie\"},\"player\":{\"playerid\":1,\"speed\":1}},\"sender\":\"xbmc\"}}"
            }else if(method == "Application.OnVolumeChanged"){
                QJsonObject parmObj = obj["params"].toObject();
                if(!parmObj.isEmpty()){
                    QJsonObject dataObj = parmObj["data"].toObject();
                    if(!dataObj.isEmpty()){
                        m_muted = dataObj["muted"].toBool();
                        m_volume = dataObj["volume"].toDouble();

                        emit volume(m_muted, m_volume);
                    }
                }
            }

            QString id = obj.value(QString("id")).toString();
            if(id == "libTvShows"){
                QJsonObject resultObj = obj["result"].toObject();
                if(!resultObj.isEmpty()){
                    QJsonObject limitsObj = resultObj["limits"].toObject();
                    if(!limitsObj.isEmpty()){
                        m_total = limitsObj.value("total").toInt(0);
                        m_index[26] = m_total;
                        //tv_current = limitsObj.value("end").toInt(0);
                        if(limitsObj.value("start").toInt(-1) == 0)
                            m_list.clear();
                    }

                    if(resultObj["tvshows"].isArray()){
                       QJsonArray tvArray = resultObj["tvshows"].toArray();
                       if(tvArray.size() > 0){
                           for(int x=0; x<tvArray.size(); x++){
                               QJsonObject tvObj = tvArray[x].toObject();

                               //int showId = -1;
                               QVariantMap tvShow;
                               foreach(const QString& keyShowId, tvObj.keys()) {
                                   QJsonValue value = tvObj.value(keyShowId);
                                   if(keyShowId == "art"){
                                       QJsonObject artObj = value.toObject();
                                       QVariantMap art;
                                       QString u;
                                       foreach(const QString& keyArt, artObj.keys()) {
                                           QJsonValue valueArt = artObj.value(keyArt);
                                           art.insert(keyArt,valueArt.toString());
                                           if(keyArt == "poster")
                                               u = valueArt.toString();
                                           else if(keyArt == "tvshow.poster")
                                               u = valueArt.toString();
                                           else if(keyArt == "thumb")
                                               u = valueArt.toString();
                                       }

                                       tvShow.insert(keyShowId,art);
                                       QString p = u;
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       tvShow.insert("fanImage",input);
                                       continue;
                                   }else if(keyShowId == "tvshowid"){
                                       //showId = value.toInt(-1);
                                       tvShow.insert(keyShowId,value.toInt());
                                   }else if(keyShowId == "title"){
                                       QString title = value.toString();
                                       tvShow.insert(keyShowId,title);

                                       QString n = title.toUpper();
                                       if(n.length() > 0){
                                           int c = n.at(0).toLatin1();
                                           if(c >= 65 && c <= 90){
                                               if(m_index[c-65] == -1){
                                                   m_index[c-65] = m_list.length()+1;
                                               }
                                           }
                                       }
                                   }else if(keyShowId == "thumbnail"){
                                       QString p = value.toString();
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       tvShow.insert(keyShowId,input);

                                   }else if(keyShowId == "genre"){
                                       QJsonArray genreArray = value.toArray();
                                       if(!genreArray.isEmpty()){
                                           QStringList genre;
                                           for(int y=0; y<genreArray.size(); y++){
                                               genre.append(genreArray[y].toString());
                                           }
                                           tvShow.insert(keyShowId,genre);
                                       }
                                   }else if(value.isString())
                                       tvShow.insert(keyShowId,value.toString());
                                   else if(value.isDouble())
                                       tvShow.insert(keyShowId,value.toDouble());
                                   else tvShow.insert(keyShowId,value.toInt());
                               }

                               m_list << tvShow;
                           }
                       }
                    }
                    emit listChanged();
                }
            }else if(id == "libMovies"){
                QJsonObject resultObj = obj["result"].toObject();
                if(!resultObj.isEmpty()){
                    QJsonObject limitsObj = resultObj["limits"].toObject();
                    if(!limitsObj.isEmpty()){
                        m_total = limitsObj.value("total").toInt(0);
                        m_index[26] = m_total;
//qDebug() << "LIMITS"<<m_total;
                        //movies_current = limitsObj.value("end").toInt(0);
                        if(limitsObj.value("start").toInt(-1) == 0)
                            m_list.clear();
                    }

                    if(resultObj["movies"].isArray()){
                       QJsonArray tvArray = resultObj["movies"].toArray();
                       if(tvArray.size() > 0){
                           for(int x=0; x<tvArray.size(); x++){
                               QJsonObject tvObj = tvArray[x].toObject();

                               //int showId = -1;
                               QVariantMap show;
                               foreach(const QString& keyShowId, tvObj.keys()) {
                                   QJsonValue value = tvObj.value(keyShowId);
                                   if(keyShowId == "art"){
                                       QJsonObject artObj = value.toObject();
                                       QVariantMap art;
                                       QString u;
                                       foreach(const QString& keyArt, artObj.keys()) {
                                           QJsonValue valueArt = artObj.value(keyArt);
                                           art.insert(keyArt,valueArt.toString());
                                           if(keyArt == "poster"){
                                               u = valueArt.toString();
                                           }else if(keyArt == "tvshow.poster")
                                               u = valueArt.toString();
                                           else if(keyArt == "thumb")
                                               u = valueArt.toString();
                                           else if(keyArt == "fanart")
                                               u = valueArt.toString();
                                       }

                                       show.insert(keyShowId,art);
                                       QString p = u;
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       show.insert("fanImage",input);
                                       continue;
                                   }else if(keyShowId == "title"){
                                       QString title = value.toString();
                                       show.insert(keyShowId,title);

                                       QString n = title.toUpper();
                                       if(n.length() > 0){
                                           int c = n.at(0).toLatin1();
                                           if(c >= 65 && c <= 90){
                                               if(m_index[c-65] == -1){
                                                   m_index[c-65] = m_list.length()+1;
                                               }
                                           }
                                       }
                                   }else if(keyShowId == "movieid"){
                                       //showId = value.toInt(-1);
                                       show.insert(keyShowId,value.toInt());
                                   }else if(keyShowId == "thumbnail"){
                                       QString p = value.toString();
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       show.insert(keyShowId,input);
                                   }else if(keyShowId == "genre" || keyShowId == "studio"){
                                       QJsonArray genreArray = value.toArray();
                                       if(!genreArray.isEmpty()){
                                           QStringList genre;
                                           for(int y=0; y<genreArray.size(); y++){
                                               genre.append(genreArray[y].toString());
                                           }
                                           show.insert(keyShowId,genre);
                                       }
                                   }else if(value.isString())
                                       show.insert(keyShowId,value.toString());
                                   else if(value.isDouble())
                                       show.insert(keyShowId,value.toDouble());
                                   else show.insert(keyShowId,value.toInt());
                               }

                               m_list << show;
                           }
                       }
                    }
                    emit listChanged();
                }
            }else if(id == "libTvSeason"){
                m_list.clear();
                QJsonObject resultObj = obj["result"].toObject();
                if(!resultObj.isEmpty()){
                    QJsonObject limitsObj = resultObj["limits"].toObject();
                    if(!limitsObj.isEmpty()){
                        m_total = limitsObj.value("total").toInt(0);
                        if(limitsObj.value("start").toInt(-1) == 0)
                            m_list.clear();
                    }

                    if(resultObj["seasons"].isArray()){
                       QJsonArray tvArray = resultObj["seasons"].toArray();
                       if(tvArray.size() > 0){
                           for(int x=0; x<tvArray.size(); x++){
                               QJsonObject tvObj = tvArray[x].toObject();

                               QVariantMap tvShow;
                               foreach(const QString& keyShowId, tvObj.keys()) {
                                   QJsonValue value = tvObj.value(keyShowId);
                                   if(keyShowId == "tvshowid"){
                                       //showId = value.toInt(-1);
                                       tvShow.insert(keyShowId,value.toInt());
                                   }else if(keyShowId == "thumbnail"){
                                       QString p = value.toString();
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       tvShow.insert(keyShowId,input);
                                   }else if(keyShowId == "art"){
                                       QJsonObject artObj = value.toObject();
                                       QVariantMap art;
                                       QString u;
                                       foreach(const QString& keyArt, artObj.keys()) {
                                           QJsonValue valueArt = artObj.value(keyArt);
                                           art.insert(keyArt,valueArt.toString());
                                           if(keyArt == "poster")
                                               u = valueArt.toString();
                                           else if(keyArt == "tvshow.poster")
                                               u = valueArt.toString();
                                           else if(keyArt == "thumb")
                                               u = valueArt.toString();
                                       }

                                       tvShow.insert(keyShowId,art);
                                       QString p = u;
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;

                                       tvShow.insert("fanImage",input);
                                   }else if(keyShowId == "fanart"){
                                       tvShow.insert(keyShowId,value.toString());

                                       QString p = value.toString();

                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");
                                       //d = d.split(":").join("%3A");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       tvShow.insert("bgImage",input);
                                   }else if(value.isString())
                                       tvShow.insert(keyShowId,value.toString());
                                   else if(value.isDouble())
                                       tvShow.insert(keyShowId,value.toDouble());
                                   else tvShow.insert(keyShowId,value.toInt());
                               }

                               m_list << tvShow;
                           }
                       }
                    }


                    emit listChanged();
                }
            }else if(id == "libTvSeasonDetails"){
                m_list.clear();
                QJsonObject resultObj = obj["result"].toObject();
                if(!resultObj.isEmpty()){
                    QJsonObject limitsObj = resultObj["limits"].toObject();
                    if(!limitsObj.isEmpty()){
                        m_total = limitsObj.value("total").toInt(0);
                        if(limitsObj.value("start").toInt(-1) == 0)
                            m_list.clear();
                    }

                    if(resultObj["episodes"].isArray()){
                       QJsonArray tvArray = resultObj["episodes"].toArray();
                       if(tvArray.size() > 0){
                           for(int x=0; x<tvArray.size(); x++){
                               QJsonObject tvObj = tvArray[x].toObject();

                               QVariantMap tvShow;
                               foreach(const QString& keyShowId, tvObj.keys()) {
                                   QJsonValue value = tvObj.value(keyShowId);
                                   if(keyShowId == "tvshowid"){
                                       //showId = value.toInt(-1);
                                       tvShow.insert(keyShowId,value.toInt());
                                   }else if(keyShowId == "episodeid"){
                                       tvShow.insert(keyShowId,value.toInt());
                                   }else if(keyShowId == "thumbnail"){
                                       QString p = value.toString();
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       tvShow.insert(keyShowId,input);
                                   }else if(keyShowId == "art"){
                                       QJsonObject artObj = value.toObject();
                                       QVariantMap art;
                                       QString u;
                                       foreach(const QString& keyArt, artObj.keys()) {
                                           QJsonValue valueArt = artObj.value(keyArt);
                                           art.insert(keyArt,valueArt.toString());
                                           if(keyArt == "poster")
                                               u = valueArt.toString();
                                           else if(keyArt == "tvshow.poster")
                                               u = valueArt.toString();
                                           else if(keyArt == "thumb")
                                               u = valueArt.toString();
                                       }

                                       tvShow.insert(keyShowId,art);
                                       QString p = u;
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;

                                       tvShow.insert("fanImage",input);
                                   }else if(keyShowId == "fanart"){
                                       tvShow.insert(keyShowId,value.toString());

                                       QString p = value.toString();

                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");
                                       //d = d.split(":").join("%3A");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       tvShow.insert("bgImage",input);
                                   }else if(keyShowId == "streamdetails"){
                                       QJsonObject streamObj = value.toObject();
                                       QVariantMap stream;
                                       foreach(const QString& keyArt, streamObj.keys()) {
                                           QJsonValue valueStream = streamObj.value(keyArt);
                                           QJsonArray sArray = valueStream.toArray();
                                           QVariantMap typeStream;
                                           if(!sArray.isEmpty()){
                                               for(int y=0; y<sArray.size(); y++){
                                                   QJsonObject crewObj = sArray[y].toObject();

                                                   foreach(const QString& keytype, crewObj.keys()) {
                                                       QJsonValue value = crewObj.value(keytype);
                                                       if(value.isString())
                                                          typeStream.insert(keytype,value.toString());
                                                       else if(value.isDouble())
                                                          typeStream.insert(keytype,value.toDouble());
                                                       else typeStream.insert(keytype,value.toInt());
                                                       if(keyArt == "video" && keytype == "duration"){
                                                           int dur = value.toInt() / 60;
                                                           tvShow.insert("duration",dur);
                                                       }
                                                   }
                                               }
                                           }

                                           stream.insert("keyArt",typeStream);

                                       }
                                       tvShow.insert("stream",stream);
                                   }else if(value.isString())
                                       tvShow.insert(keyShowId,value.toString());
                                   else if(value.isDouble())
                                       tvShow.insert(keyShowId,value.toDouble());
                                   else tvShow.insert(keyShowId,value.toInt());
                               }

                               m_list << tvShow;
                           }
                       }
                    }

                    emit listChanged();
                }
            }else if(id == "libEpisodeDetail"){
                m_list.clear();
                QJsonObject resultObj = obj["result"].toObject();
                if(!resultObj.isEmpty()){
                    QJsonObject detailObj = resultObj["episodedetails"].toObject();
                    if(!detailObj.isEmpty()){
                        QVariantMap show;
                        foreach(const QString& key, detailObj.keys()) {
                            QJsonValue value = detailObj.value(key);
                            if(key == "art"){
                                QJsonObject artObj = value.toObject();
                                QVariantMap art;
                                QString u;
                                foreach(const QString& keyArt, artObj.keys()) {
                                    QJsonValue valueArt = artObj.value(keyArt);
                                    art.insert(keyArt,valueArt.toString());
                                    if(keyArt == "poster")
                                        u = valueArt.toString();
                                    else if(keyArt == "tvshow.poster")
                                        u = valueArt.toString();
                                    else if(keyArt == "thumb")
                                        u = valueArt.toString();
                                }

                                show.insert(key,art);
                                QString p = u;
                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;

                                show.insert("fanImage",input);
                                continue;
                            }else if(key == "year" || key == "tvshowid" || key == "season" || key == "id" || key == "episode"){
                                show.insert(key,value.toInt());
                                continue;
                            }else if(key == "rating"){
                                show.insert(key,value.toDouble());
                                continue;
                            }else if(key == "cast"){
                                QJsonArray castArray = value.toArray();
                                QVariantList cast;
                                if(!castArray.isEmpty()){
                                    for(int y=0; y<castArray.size(); y++){
                                        QJsonObject crewObj = castArray[y].toObject();
                                        QVariantMap crew;
                                        foreach(const QString& keyCrew, crewObj.keys()) {
                                            QJsonValue valueCrew = crewObj.value(keyCrew);
                                            crew.insert(keyCrew,valueCrew.toString());
                                            if(keyCrew == "thumbnail"){
                                                QString p = valueCrew.toString();
                                                p = p.split("%3a").join("#X3A");
                                                p = p.split("%2f").join("#X2F");
                                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;

                                                crew.insert("image",input);
                                            }
                                        }
                                        cast.append(crew);
                                    }
                                }

                                show.insert(key,cast);
                                continue;
                            }else if(key == "fanart"){
                                show.insert(key,value.toString());

                                QString p = value.toString();

                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");
                                //d = d.split(":").join("%3A");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                qDebug() <<"BG"<<input;
                                show.insert("bgImage",input);
                                continue;
                            }else if(key == "thumbnail"){
                                show.insert(key,value.toString());

                                QString p = value.toString();

                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                show.insert("thumbnail",input);
                                qDebug() <<"thumbnail"<<input;                                
                                continue;
                            }else if(key == "streamdetails"){
                                QJsonObject streamObj = value.toObject();
                                QVariantMap stream;
                                foreach(const QString& keyArt, streamObj.keys()) {
                                    QJsonValue valueStream = streamObj.value(keyArt);
                                    QJsonArray sArray = valueStream.toArray();
                                    QVariantMap typeStream;
                                    if(!sArray.isEmpty()){
                                        for(int y=0; y<sArray.size(); y++){
                                            QJsonObject crewObj = sArray[y].toObject();

                                            foreach(const QString& keytype, crewObj.keys()) {
                                                QJsonValue value = crewObj.value(keytype);
                                                if(value.isString())
                                                   typeStream.insert(keytype,value.toString());
                                                else if(value.isDouble())
                                                   typeStream.insert(keytype,value.toDouble());
                                                else typeStream.insert(keytype,value.toInt());
                                            }
                                        }
                                    }

                                    stream.insert("keyArt",typeStream);

                                }
                                show.insert("stream",stream);
                            }
                            show.insert(key,value.toString());
                        }
                        m_list << show;
                        emit listChanged();
                    }
                }
            }else if(id == "libMoviesDetail"){
                m_list.clear();
                QJsonObject resultObj = obj["result"].toObject();
                if(!resultObj.isEmpty()){
                    QJsonObject detailObj = resultObj["moviedetails"].toObject();
                    if(!detailObj.isEmpty()){
                        QVariantMap show;
                        foreach(const QString& key, detailObj.keys()) {
                            QJsonValue value = detailObj.value(key);
                            if(key == "art"){
                                QJsonObject artObj = value.toObject();
                                QVariantMap art;
                                QString u;
                                foreach(const QString& keyArt, artObj.keys()) {
                                    QJsonValue valueArt = artObj.value(keyArt);
                                    art.insert(keyArt,valueArt.toString());
                                    if(keyArt == "poster")
                                        u = valueArt.toString();
                                    else if(keyArt == "tvshow.poster")
                                        u = valueArt.toString();
                                    else if(keyArt == "thumb")
                                        u = valueArt.toString();
                                }

                                show.insert(key,art);
                                QString p = u;
                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;

                                show.insert("fanImage",input);
                                continue;
                            }else if(key == "year" || key == "tvshowid" || key == "season" || key == "id" || key == "episode"){
                                show.insert(key,value.toInt());
                                continue;
                            }else if(key == "rating"){
                                show.insert(key,value.toDouble());
                                continue;
                            }else if(key == "cast"){
                                QJsonArray castArray = value.toArray();
                                QVariantList cast;
                                if(!castArray.isEmpty()){
                                    for(int y=0; y<castArray.size(); y++){
                                        QJsonObject crewObj = castArray[y].toObject();
                                        QVariantMap crew;
                                        foreach(const QString& keyCrew, crewObj.keys()) {
                                            QJsonValue valueCrew = crewObj.value(keyCrew);
                                            crew.insert(keyCrew,valueCrew.toString());
                                            if(keyCrew == "thumbnail"){
                                                QString p = valueCrew.toString();
                                                p = p.split("%3a").join("#X3A");
                                                p = p.split("%2f").join("#X2F");
                                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;

                                                crew.insert("image",input);
                                            }
                                        }
                                        cast.append(crew);
                                    }
                                }

                                show.insert(key,cast);
                                continue;
                            }else if(key == "genre"){
                                QJsonArray genreArray = value.toArray();
                                if(!genreArray.isEmpty()){
                                    QStringList genre;
                                    for(int y=0; y<genreArray.size(); y++){
                                        genre.append(genreArray[y].toString());
                                    }
                                    show.insert(key,genre);
                                    continue;
                                }
                            }else if(key == "fanart"){
                                show.insert(key,value.toString());

                                QString p = value.toString();

                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");
                                //d = d.split(":").join("%3A");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                show.insert("bgImage",input);
                                continue;
                            }else if(key == "thumbnail"){
                                show.insert(key,value.toString());

                                QString p = value.toString();

                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                show.insert("bgImage",input);
                                continue;
                            }else if(key == "streamdetails"){
                                QJsonObject streamObj = value.toObject();
                                QVariantMap stream;
                                foreach(const QString& keyArt, streamObj.keys()) {
                                    QJsonValue valueStream = streamObj.value(keyArt);
                                    QJsonArray sArray = valueStream.toArray();
                                    QVariantMap typeStream;
                                    if(!sArray.isEmpty()){
                                        for(int y=0; y<sArray.size(); y++){
                                            QJsonObject crewObj = sArray[y].toObject();

                                            foreach(const QString& keytype, crewObj.keys()) {
                                                QJsonValue value = crewObj.value(keytype);
                                                if(value.isString())
                                                   typeStream.insert(keytype,value.toString());
                                                else if(value.isDouble())
                                                   typeStream.insert(keytype,value.toDouble());
                                                else typeStream.insert(keytype,value.toInt());
                                            }
                                        }
                                    }

                                    stream.insert("keyArt",typeStream);

                                }
                                show.insert("stream",stream);
                            }
                            show.insert(key,value.toString());
                        }
                        m_list << show;
                        emit listChanged();
                    }
                }
            }else if(id == "libplaylist"){
                m_playlist.clear();
                QJsonObject resultObj = obj["result"].toObject();
                if(!resultObj.isEmpty()){
                    if(resultObj["items"].isArray()){
                       QJsonArray tvArray = resultObj["items"].toArray();
                       if(tvArray.size() > 0){
                           for(int x=0; x<tvArray.size(); x++){
                               QJsonObject tvObj = tvArray[x].toObject();

                               //int showId = -1;
                               QVariantMap tvShow;
                               foreach(const QString& keyShowId, tvObj.keys()) {
                                   QJsonValue value = tvObj.value(keyShowId);
                                   if(keyShowId == "tvshowid"){
                                       //showId = value.toInt(-1);
                                       tvShow.insert(keyShowId,value.toInt());
                                   }else if(keyShowId == "thumbnail"){
                                       QString p = value.toString();
                                       p = p.split("%3a").join("#X3A");
                                       p = p.split("%2f").join("#X2F");

                                       QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                       tvShow.insert(keyShowId,input);
                                   }else if(keyShowId == "genre"){
                                       QJsonArray genreArray = value.toArray();
                                       if(!genreArray.isEmpty()){
                                           QStringList genre;
                                           for(int y=0; y<genreArray.size(); y++){
                                               genre.append(genreArray[y].toString());
                                           }
                                           tvShow.insert(keyShowId,genre);
                                       }
                                   }else if(value.isString())
                                       tvShow.insert(keyShowId,value.toString());
                                   else if(value.isDouble())
                                       tvShow.insert(keyShowId,value.toDouble());
                                   else tvShow.insert(keyShowId,value.toInt());
                               }

                               m_playlist << tvShow;
                           }
                       }
                    }
                    emit playlistChanged();
                    if(m_menu == "playlist"){
                        m_list = m_playlist;
                        emit listChanged();
                    }
                }
            }

            if(obj["result"].isArray()){
                QJsonArray resultArray = obj["result"].toArray();
                if(resultArray.size() > 0){
                    QJsonObject resultObj = resultArray[0].toObject();
                    if(resultObj["playertype"].toString() == "internal" && resultObj["type"].toString() == "video"){
                        call_method("Player.GetItem", {{ "properties", "[\"title\",\"album\",\"artist\",\"season\",\"episode\",\"duration\",\"showtitle\",\"tvshowid\",\"thumbnail\",\"file\",\"fanart\",\"streamdetails\",\"year\",\"rating\",\"originaltitle\",\"plot\",\"art\",\"genre\",\"votes\",\"cast\",\"premiered\",\"plotoutline\",\"description\",\"tagline\",\"comment\"]"},{ "playerid", "1"  }});
                        call_method("Player.GetProperties", {{ "properties", "[\"type\",\"speed\",\"time\",\"percentage\",\"totaltime\",\"playlistid\",\"position\"]"},{ "playerid", "1"  }});

//                        call_method("Playlist.GetItems", {{ "properties", "[\"runtime\", \"showtitle\", \"season\", \"title\", \"artist\", \"thumbnail\"]"},{ "playerid", "1"  }});
//                        call_method("VideoLibrary.GetTVShows", {{ "limits", "{\"start\":0, \"end\": 75}"}, {"properties", "[\"art\", \"genre\", \"plot\", \"title\", \"originaltitle\", \"year\", \"rating\", \"thumbnail\", \"playcount\", \"file\", \"fanart\",\"premiered\",\"watchedepisodes\",\"episode\"]"}, {"sort", "{\"order\": \"ascending\", \"method\": \"label\" }"}});
//                        call_method("VideoLibrary.GetSeasons", {{ "sort", "{\"order\": \"ascending\", \"method\": \"season\"}"},{"tvshowid":tvshowid, "properties":["fanart","thumbnail","tvshowid","watchedepisodes","art","userrating","showtitle","playcount","episode","season"] }});
//                        call_method("VideoLibrary.GetMovies", {{ "filter", "{\"field\": \"playcount\", \"operator\": \"is\", \"value\": \"0\"}"}, {"limits", "{ \"start\" : 0, \"end\": 75 }"}, {"properties", "[\"art\", \"rating\", \"thumbnail\", \"playcount\", \"file\",\"premiered\", \"plot\", \"title\", \"originaltitle\",\"studio\",\"votes\",\"year\",\"runtime\",\"tagline\"]"}, {"sort", "{ \"order\": \"ascending\", \"method\": \"label\", \"ignorearticle\": true }"}});
                    }
                }
            }else{
                QJsonObject resultObj = obj["result"].toObject();
                if(!resultObj.isEmpty()){
                    QJsonObject itemObj = resultObj["item"].toObject();
                    if(!itemObj.isEmpty()){
                        m_currentMetaData.clear();
                        foreach(const QString& key, itemObj.keys()) {
                            QJsonValue value = itemObj.value(key);
                            if(key == "art"){
                                QJsonObject artObj = value.toObject();
                                QVariantMap art;
                                QString u;
                                foreach(const QString& keyArt, artObj.keys()) {
                                    QJsonValue valueArt = artObj.value(keyArt);
                                    art.insert(keyArt,valueArt.toString());
                                    if(keyArt == "poster")
                                        u = valueArt.toString();
                                    else if(keyArt == "tvshow.poster")
                                        u = valueArt.toString();
                                    else if(keyArt == "thumb")
                                        u = valueArt.toString();
                                }

                                m_currentMetaData.insert(key,art);
                                QString p = u;
                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;

                                m_currentMetaData.insert("fanImage",input);
                                continue;
                            }else if(key == "year" || key == "tvshowid" || key == "season" || key == "id" || key == "episode"){
                                m_currentMetaData.insert(key,value.toInt());
                                //qDebug() << "Key = " << key << ", Value = " << value.toInt();
                                continue;
                            }else if(key == "rating"){
                                m_currentMetaData.insert(key,value.toDouble());
                                //qDebug() << "Key = " << key << ", Value = " << value.toDouble();
                                continue;
                            }else if(key == "cast"){
                                QJsonArray castArray = value.toArray();
                                QVariantList cast;
                                if(!castArray.isEmpty()){
                                    for(int y=0; y<castArray.size(); y++){
                                        QJsonObject crewObj = castArray[y].toObject();
                                        QVariantMap crew;
                                        foreach(const QString& keyCrew, crewObj.keys()) {
                                            QJsonValue valueCrew = crewObj.value(keyCrew);
                                            crew.insert(keyCrew,valueCrew.toString());
                                            if(keyCrew == "thumbnail"){
                                                QString p = valueCrew.toString();
                                                p = p.split("%3a").join("#X3A");
                                                p = p.split("%2f").join("#X2F");
                                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;

                                                crew.insert("image",input);
                                            }
                                        }
                                        cast.append(crew);
                                    }
                                }

                                m_currentMetaData.insert(key,cast);
                                continue;
                            }else if(key == "genre"){
                                QJsonArray genreArray = value.toArray();
                                if(!genreArray.isEmpty()){
                                    QStringList genre;
                                    for(int y=0; y<genreArray.size(); y++){
                                        //qDebug() << "ggggg" << genreArray[y].toString();
                                        genre.append(genreArray[y].toString());
                                    }
                                    //qDebug() << "kkkkk" << genre;
                                    m_currentMetaData.insert(key,genre);
                                    continue;
                                }
                            }else if(key == "fanart"){
                                m_currentMetaData.insert(key,value.toString());

                                QString p = value.toString();

                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");
                                //d = d.split(":").join("%3A");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
                                //qDebug() << "INPUT" << input << "http://192.168.178.27:8081/image/"+d<<p;
                                m_currentMetaData.insert("bgImage",input);
                                //m_currentMetaData.insert("bgImage",getImage(input));
                                continue;
                            }else if(key == "thumbnail"){
                                qDebug() << "thumbnail";
                                m_currentMetaData.insert(key,value.toString());

                                QString p = value.toString();

                                p = p.split("%3a").join("#X3A");
                                p = p.split("%2f").join("#X2F");
                                //d = d.split(":").join("%3A");

                                QString input = "http://192.168.178.27:8081/image/|"+p+"|"+m_basic;
//  qDebug() << "INPUT" << input << "http://192.168.178.27:8081/image/"<<p;
                                m_currentMetaData.insert("bgImage",input);
                                //m_currentMetaData.insert("bgImage",getImage(input));
                                continue;
                            }else if(key == "streamdetails"){
                                QJsonObject sObj = value.toObject();
                                //strJson(sObj);
                                //m_currentMetaData.insert(key,sObj.toString());
                                continue;
                            }
                            //qDebug() << "Key = " << key << ", Value = " << value.toString();
                            m_currentMetaData.insert(key,value.toString());
                        }

                        emit currentMetaDataChanged();
                    }

                    QJsonObject timeObj = resultObj["time"].toObject();
                    if(!timeObj.isEmpty()){
                        //TODO maybe a bug timerDuration.stop();
                        int tmp = timeObj.value("hours").toInt(0);
                        //int t = tmp * 60 * 60;
                        m_time.insert("hours",tmp);
                        tmp = timeObj.value("minutes").toInt(0);
                        //t += tmp * 60;
                        m_time.insert("minutes",tmp);
                        tmp = timeObj.value("seconds").toInt(0);
                        //t += tmp;
                        m_time.insert("seconds",tmp);
                        //m_time.insert("total",t);
                        emit currentTimeChanged();
                    }

                    QJsonObject totaltimeObj = resultObj["totaltime"].toObject();
                    if(!totaltimeObj.isEmpty()){
                        int tmp = totaltimeObj.value("hours").toInt(0);
                        //int t = tmp * 60 * 60;
                        m_totaltime.insert("hours",tmp);
                        tmp = totaltimeObj.value("minutes").toInt(0);
                        //t += tmp * 60;
                        m_totaltime.insert("minutes",tmp);
                        tmp = totaltimeObj.value("seconds").toInt(0);
                        //t += tmp;
                        m_totaltime.insert("seconds",tmp);
                        //m_totaltime.insert("total",t);
                        //qDebug() <<"AAAAA TOTALTIME" <<m_totaltime;
                        emit currentTotalTimeChanged();
                    }

                    QJsonObject percentageObj = resultObj["percentage"].toObject();
                    if(percentageObj.isEmpty()){
                        QJsonValue value = resultObj.value("percentage");
                        double v = value.toDouble();
                        if(v != 0){
                            m_percentage = value.toDouble();
                            emit currentPercentageChanged();

                            QJsonValue value2 = resultObj.value("speed");
                            if(value2.toInt(0) == 1){
                                m_paused = false;
                                m_playing = true;
                                emit ispaused(false);
                                timerDuration.start(1000);
                            }
                        }
                    }
                }
            }

            qDebug() << "method" << method;
        }
    }
if(gdebug) qDebug() << "CRASH KodiController record_response finished";
}

void KodiController::call_method(QString method, QList<QPair<QString, QString>> parameters, QString id)
{
if(gdebug) qDebug() << "CRASH KodiController call_method";
    QString request = RPC_prolog + ",\"method\":\"" + method + '\"';

    if(!parameters.empty())
    {
        QString params = ",\"params\":{\"" + parameters.first().first + "\":" + parameters.first().second;
        for(int i = 1; i < parameters.length(); ++i)
        {
            const auto& parameter = parameters[i];
            params += ",\"" + parameter.first + "\":" + parameter.second;
        }
        params += '}';
        request += params;
    }
    if(id == "")
        request += RPC_epilog;
    else request += ", \"id\": \""+id+"\" }";
    qDebug() << "Sending request: \'" << request << "\'.";

    socket.sendTextMessage(request);
if(gdebug) qDebug() << "CRASH KodiController call_method finished";
}

void KodiController::keypress(int keycode)
{
if(gdebug) qDebug() << "CRASH KodiController keypress";
    qDebug() << "CONTROLLER KEYPRESS" << keycode << Qt::Key_MediaStop;
    Qt::Key key = static_cast<Qt::Key>(keycode);
    switch(key)
    {
        case Qt::Key_Up:
            call_method("Input.Up");
            break;
        case Qt::Key::Key_Down:
            call_method("Input.Down");
            break;
        case Qt::Key_Left:
            call_method("Input.Left");
            break;
        case Qt::Key_Right:
            call_method("Input.Right");
            break;
        case Qt::Key_Enter:
            call_method("Input.Select");
            break;
        case Qt::Key_Back:
            call_method("Input.Back");
            break;
        case Qt::Key_Menu:
            call_method("Input.Menu");
            break;
        case Qt::Key_Home:
            call_method("Input.Home");
            break;
        case Qt::Key_Info:
            call_method("Input.Info");
            break;
        case Qt::Key_Documents:
            call_method("Input.showOSD");
            break;
        case Qt::Key_MediaPlay:
            call_method("Input.ExecuteAction", {{ "action", "\"play\"" }});
            break;
        case Qt::Key_MediaPause:
            call_method("Input.ExecuteAction", {{ "action", "\"pause\"" }});
            break;
        case Qt::Key_MediaStop:
            call_method("Input.ExecuteAction", {{ "action", "\"stop\"" }});
            break;
        case Qt::Key_AudioForward:
            call_method("Input.ExecuteAction", {{ "action", "\"fastforward\"" }});
            break;
        case Qt::Key_AudioRewind:
            call_method("Input.ExecuteAction", {{ "action", "\"rewind\"" }});
            break;
        case Qt::Key_MediaNext:
            call_method("Input.ExecuteAction", {{ "action", "\"skipnext\"" }});
            break;
        case Qt::Key_MediaPrevious:
            call_method("Input.ExecuteAction", {{ "action", "\"skipprevious\"" }});
            break;
        case Qt::Key_F1:
            call_method("GUI.ActivateWindow", {{"window","\"video\""},{"parameters","[\"videodb://tvshows/\"]"}});
            break;
        case Qt::Key_F2:
            call_method("GUI.ActivateWindow", {{"window","\"video\""},{"parameters","[\"videodb://movies/\"]"}});
            break;
        case Qt::Key_F3:
            call_method("GUI.ActivateWindow", {{"window","\"root\""},{"parameters","[\"videodb://music/\"]"}});
            break;
        case Qt::Key_F4:
            call_method("GUI.ActivateWindow", {{"window","\"video\""},{"parameters","[\"videodb://tvshows/\"]"}});
            break;
        case Qt::Key_VolumeMute:
            call_method("Application.SetMute", {{"mute","\"toggle\""}});
            break;
        default:
            call_method("Input.ExecuteAction", {{ "action", "\"noop\"" }});
    }
if(gdebug) qDebug() << "CRASH KodiController keypress finished";
}

void KodiController::setEnabled(bool v)
{
    if(v){
       m_enabled = true;
       timerConenct.start(2000);
    }else{
       timerConenct.stop();
       socket.close();
       m_enabled = false;
    }
}

bool KodiController::getEnabled()
{
    return m_enabled;
}

QStringList KodiController::getSettings()
{
    QStringList v;
    v << m_host << QString::number(m_port) << QString::number(m_httpport) << m_uname << m_password << QString::number(m_tcp) << QString::number(m_eventserver);
    return v;
}

void KodiController::setSettings(QStringList v)
{
    if(v.length() >= 7){
       m_host = v[0];
       m_port = v[1].toInt();
       m_httpport = v[2].toInt();
       m_uname = v[3];
       m_password = v[4];
       m_tcp = v[5].toInt()==1?true:false;
       m_eventserver = v[6].toInt()==1?true:false;

       socket.close();
       timerConenct.start(2000);
    }
}

QVariantMap KodiController::getCurrentMetaData()
{
    return m_currentMetaData;
}

QString KodiController::getImage(QString url)
{
if(gdebug) qDebug() << "CRASH KodiController getImage";
    NetworkManager networkManager;
    NetworkManagerResult result = networkManager.sendHTTPGetRequestSynchronous(url, 30000, "Basic eGJtYzpNYWNyb3NzMQ==");

    if(result.errorResponse == 0 ){
        return "data:image/png;base64,"+result.binary.toBase64();
    } else if(result.errorResponse == -1 ){
        qDebug() << "Error network AAAAAAAAAAAAAAAAA" << result.response;
        //Network timeout
    } else {
        qDebug() << "Error bad AAAAAAAAAA" << result.response;
    }    
    return "";
}

void KodiController::setVolume(double v)
{
if(gdebug) qDebug() << "CRASH KodiController setVolume";
    int x = static_cast<int>(v);
    call_method("Application.SetVolume", {{ "volume", QString::number(x)}});
if(gdebug) qDebug() << "CRASH KodiController setVolume finished";
}

double KodiController::getPercentage()
{
    return m_percentage;
}

QVariantMap KodiController::getTime()
{
    return m_time;
}

QVariantMap KodiController::getTotalTime()
{
    return m_totaltime;
}

void KodiController::duration()
{
if(gdebug) qDebug() << "CRASH KodiController duration";
    if(m_time.count() < 1) return;
    int t = m_time["seconds"].toInt();
    int m = m_time["minutes"].toInt();
    int h = m_time["hours"].toInt();
    t++;
    if(t > 59){
        t = 0;
        m++;
    }
    if(m > 59){
        m = 0;
        h++;
    }

    m_time["seconds"] = t;
    m_time["minutes"] = m;
    m_time["hours"] = h;
    m_timeCount++;
    if(m_timeCount > 10){
        call_method("Player.GetProperties", {{ "properties", "[\"type\",\"speed\",\"time\",\"percentage\",\"totaltime\",\"playlistid\",\"position\"]"},{ "playerid", "1"  }});

        m_timeCount = 0;
    }
    emit currentTimeChanged();
if(gdebug) qDebug() << "CRASH KodiController duration finished";
}

void KodiController::getTvShows(int start, int max)
{
if(gdebug) qDebug() << "CRASH KodiController getTvShows";
    int limit = LISTLIMIT;
    if(max != 0)
        limit = max;
    //,\"art\" , \"plot\", \"fanart\"
    call_method("VideoLibrary.GetTVShows", {{ "limits", "{\"start\":"+QString::number(start)+", \"end\": "+QString::number(start+limit)+"}"}, {"properties", "[ \"art\",\"title\", \"genre\", \"originaltitle\", \"year\", \"rating\", \"thumbnail\", \"playcount\", \"file\",\"watchedepisodes\",\"episode\"]"}, {"sort", "{\"order\": \"ascending\", \"method\": \"label\" }"}},"libTvShows");
    m_menu = "tv";
    if(start == 0){
        m_list.clear();
        m_index.clear();
        for(int x=0; x<27; x++)
            m_index.append(-1);
        emit menuChanged();
        emit listChanged();
    }
if(gdebug) qDebug() << "CRASH KodiController getTvShows finished";
}

void KodiController::getMovies(int start, int max)
{
if(gdebug) qDebug() << "CRASH KodiController getMovies";
    int limit = LISTLIMIT;
    if(max != 0)
        limit = max;
    //\"plot\", \"premiered\", \"file\" \"\"runtime
    call_method("VideoLibrary.GetMovies", {{ "limits", "{\"start\":"+QString::number(start)+", \"end\": "+QString::number(start+limit)+"}"}, {"properties", "[\"art\",\"tag\",\"genre\",\"thumbnail\",\"rating\",\"title\", \"originaltitle\",\"studio\",\"votes\",\"year\",\"tagline\"]"}, {"sort", "{\"order\": \"ascending\", \"method\": \"label\" }"}},"libMovies");
    m_menu = "movies";
    if(start == 0){
        m_index.clear();
        m_list.clear();
        for(int x=0; x<27; x++)
            m_index.append(-1);
        emit menuChanged();
        emit listChanged();
    }
if(gdebug) qDebug() << "CRASH KodiController getMovies finished";
}

void KodiController::getMusic(int)
{
//    call_method("VideoLibrary.GetTVShows", {{ "limits", "{\"start\":0, \"end\": 75}"}, {"properties", "[\"art\", \"genre\", \"plot\", \"title\", \"originaltitle\", \"year\", \"rating\", \"thumbnail\", \"playcount\", \"file\", \"fanart\",\"premiered\",\"watchedepisodes\",\"episode\"]}, {"sort", "{\"order\": \"ascending\", \"method\": \"label\" }"}},"libTvShows");
}

void KodiController::getAlbums(int)
{
//    call_method("VideoLibrary.GetTVShows", {{ "limits", "{\"start\":0, \"end\": 75}"}, {"properties", "[\"art\", \"genre\", \"plot\", \"title\", \"originaltitle\", \"year\", \"rating\", \"thumbnail\", \"playcount\", \"file\", \"fanart\",\"premiered\",\"watchedepisodes\",\"episode\"]}, {"sort", "{\"order\": \"ascending\", \"method\": \"label\" }"}},"libTvShows");
}

void KodiController::getArtists(int)
{
//    call_method("VideoLibrary.GetTVShows", {{ "limits", "{\"start\":0, \"end\": 75}"}, {"properties", "[\"art\", \"genre\", \"plot\", \"title\", \"originaltitle\", \"year\", \"rating\", \"thumbnail\", \"playcount\", \"file\", \"fanart\",\"premiered\",\"watchedepisodes\",\"episode\"]}, {"sort", "{\"order\": \"ascending\", \"method\": \"label\" }"}},"libTvShows");
}

void KodiController::getPlaylist(int)
{
//    call_method("VideoLibrary.GetTVShows", {{ "limits", "{\"start\":0, \"end\": 75}"}, {"properties", "[\"art\", \"genre\", \"plot\", \"title\", \"originaltitle\", \"year\", \"rating\", \"thumbnail\", \"playcount\", \"file\", \"fanart\",\"premiered\",\"watchedepisodes\",\"episode\"]}, {"sort", "{\"order\": \"ascending\", \"method\": \"label\" }"}},"libTvShows");
}

void KodiController::getTvShow(int id)
{
if(gdebug) qDebug() << "CRASH KodiController getTvShow";
    call_method("VideoLibrary.GetSeasons", {{"tvshowid", QString::number(id)},{ "properties", "[\"fanart\",\"thumbnail\",\"tvshowid\",\"watchedepisodes\",\"art\",\"userrating\",\"showtitle\",\"playcount\",\"episode\",\"season\"]"}},"libTvSeason");
    m_menu = "season";
    emit menuChanged();
    m_list.clear();
    m_index.clear();
    for(int x=0; x<27; x++)
        m_index.append(-1);
    emit listChanged();
if(gdebug) qDebug() << "CRASH KodiController getTvShow finished";
}

void KodiController::getSeason(int tvshowId, int seasonId)
{
if(gdebug) qDebug() << "CRASH KodiController getSeason";
    //{ "limits", "{\"start\":0, \"end\": 75}"},  ,"libTvSeasonDetails" ,{ "properties", "[\"fanart\"]"} "tvshowid", QString::number(tvshowId), "seasonid", QString::number(seasonId)}
    //,\"thumbnail\",\"tvshowid\",\"art\",\"userrating\",\"showtitle\",\"playcount\",\"episode\",\"season\",\"runtime\",\"firstaired\"
//    call_method("VideoLibrary.GetSeasonDetails", {{"seasonid", QString::number(id)},{ "properties", "[\"fanart\",\"thumbnail\",\"tvshowid\",\"watchedepisodes\",\"art\",\"userrating\",\"showtitle\",\"playcount\",\"episode\",\"season\"]"}},"libTvSeasonDetails");
    call_method("VideoLibrary.GetEpisodes", {{"tvshowid", QString::number(tvshowId)},{"season", QString::number(seasonId)},{ "properties", "[\"file\",\"streamdetails\",\"fanart\",\"thumbnail\",\"tvshowid\",\"art\",\"userrating\",\"showtitle\",\"playcount\",\"episode\",\"season\",\"runtime\",\"firstaired\"]"}},"libTvSeasonDetails");
    m_menu = "tvSeason";
    emit menuChanged();
    m_list.clear();
    m_index.clear();
    for(int x=0; x<27; x++)
        m_index.append(-1);
    emit listChanged();
if(gdebug) qDebug() << "CRASH KodiController getSeason finished";
}

void KodiController::getVideo(int id)
{
if(gdebug) qDebug() << "CRASH KodiController getVideo";
    call_method("VideoLibrary.GetMovieDetails", {{"movieid", QString::number(id)},{ "properties", "[\"title\",\"year\",\"rating\",\"originaltitle\",\"plot\",\"art\",\"genre\",\"votes\",\"cast\",\"premiered\",\"thumbnail\",\"fanart\",\"file\",\"streamdetails\"]"}},"libMoviesDetail");
    m_menu = "moviedetail";
    emit menuChanged();
    m_list.clear();
    m_index.clear();
    for(int x=0; x<27; x++)
        m_index.append(-1);
    emit listChanged();
if(gdebug) qDebug() << "CRASH KodiController getVideo finished";
}

void KodiController::getShowDetails(int id)
{
if(gdebug) qDebug() << "CRASH KodiController getShowDetails";
    call_method("VideoLibrary.GetEpisodeDetails", {{"episodeid", QString::number(id)},{ "properties", "[\"title\",\"rating\",\"originaltitle\",\"plot\",\"art\",\"cast\",\"firstaired\",\"thumbnail\",\"fanart\",\"file\",\"streamdetails\"]"}},"libEpisodeDetail");
    m_menu = "episodedetail";
    emit menuChanged();
    m_list.clear();
    m_index.clear();
    for(int x=0; x<27; x++)
        m_index.append(-1);
    emit listChanged();
if(gdebug) qDebug() << "CRASH KodiController getShowDetails finished";
}

QVariantList KodiController::getList()
{
    return m_list;
}

QVariantList KodiController::getplayLists()
{
    return m_playlist;
}

QString KodiController::getMenu()
{
    return m_menu;
}

void KodiController::getPlayList()
{
    m_menu = "playlist";
    emit menuChanged();
    m_list.clear();
    m_index.clear();
    for(int x=0; x<27; x++)
        m_index.append(-1);
    m_list = m_playlist;
    emit listChanged();
}

void KodiController::addToPlayList(QString file)
{
    call_method("Player.Open", {{"playlistid","1"},{ "item", "{\"file\":\""+file+"\"}"}});
}

void KodiController::showGallery(QString path)
{
    //show directory path of images on kodi
}

void KodiController::showPicture(QStringList file)
{
    //qDebug() << "file"<<file;
    //show images on kodi
}

void KodiController::setPlayer(QString file)
{
    QString i = file;
    if(file.startsWith("stack://")){
        i.remove("stack://");
        QStringList w = i.split(",");
        i = w[0].trimmed();
    }
    call_method("Player.Open", {{ "item", "{\"file\":\""+i+"\"}"}});

    //play song
    //{"jsonrpc":"2.0","id":"1","method":"Player.Open","params":{"item":{"file":"file:///C:/Kodi/file.mp3"}}}
    // show pictures
    //http://192.168.15.117/jsonrpc?request={"jsonrpc":"2.0","id":"1","method":"Player.Open","params":{"item":{"directory":"Images/"}}}
    //list files in directory
    /*
     http://192.168.15.117/jsonrpc?request={"jsonrpc":"2.0","id":1,"method":"Files.GetDirectory","params":{"directory":"Media","media":"video"}}

     "id":1,"jsonrpc":"2.0","result":{"files":[{"file":"Media/Big_Buck_Bunny_720.strm","filetype":"file","label":"Big_Buck_Bunny_720.strm","type":"unknown"},{"file":"Media/Big_Buck_Bunny_1080p.mov","filetype":"file","label":"Big_Buck_Bunny_1080p.mov","type":"unknown"}],"limits":{"end":2,"start":0,"total":2}}}
    */

    //\"file\":\"smb://IRONHIDE/Multimedia/TV Shows/Altered Carbon/Season1/Altered Carbon S01E02 - Fallen Angel 1080p.mkv\"

    //add to playlist
   // call_method("Playlist.Add", {{ "item", "{\"file\":\""+i+"\"}"}});
//    http://192.168.15.117/jsonrpc?request={"jsonrpc":"2.0","id":1,"method":"Playlist.Add","params":{"playlistid":1,"item":{"file":"Media/Big_Buck_Bunny_1080p.mov"}}}
}

bool KodiController::processVoiceCmd(QString cmd, QString &response)
{
    if(cmd.contains("kodi")){
        if(cmd.contains("go to")){
            emit pageTrigger("kodi", 1);
            return true;
        }
    }
    return false;
}

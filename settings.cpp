#include "settings.h"
#include "global.h"

Settings::Settings(QObject *parent) : QObject(parent),
    m_settings(NULL)
{

}

QString Settings::pathName()
{
    return m_pathName;
}

void Settings::setPathName(const QString &pathName)
{
    if (pathName == m_pathName)
        return;

    m_pathName = pathName;

    if(m_settings){
        delete m_settings;
    }

    m_settings = new QSettings(m_pathName+"photoframe.ini", QSettings::IniFormat);

    emit pathNameChanged();
}

Q_INVOKABLE QString Settings::getString(QString group, QString key, QString initial)
{
    m_settings->beginGroup(group);
    if(!m_settings->contains(key)){
        m_settings->endGroup();
        return initial;
    }

    QString val = m_settings->value(key).toString();
    m_settings->endGroup();
    return val;
}

Q_INVOKABLE void Settings::setString(QString group, QString key, QString value)
{
    setWrite();
    m_settings->beginGroup(group);
    m_settings->setValue(key, value);
    m_settings->endGroup();
    setReadOnly();
}

Q_INVOKABLE bool Settings::getBool(QString group, QString key, bool initial)
{
    m_settings->beginGroup(group);
    if(!m_settings->contains(key)){
        m_settings->endGroup();
        return initial;
    }

    bool val = m_settings->value(key).toBool();
    m_settings->endGroup();
    return val;
}

Q_INVOKABLE void Settings::setBool(QString group, QString key, bool value)
{
    setWrite();
    m_settings->beginGroup(group);
    m_settings->setValue(key, value);
    m_settings->endGroup();

    setReadOnly();
}

Q_INVOKABLE int Settings::getInt(QString group, QString key, int initial)
{
    m_settings->beginGroup(group);
    if(!m_settings->contains(key)){
        m_settings->endGroup();
        return initial;
    }

    int val = m_settings->value(key).toInt();
    m_settings->endGroup();
    return val;
}

Q_INVOKABLE void Settings::setInt(QString group, QString key, int value)
{
    setWrite();
    m_settings->beginGroup(group);
    m_settings->setValue(key, value);
    m_settings->endGroup();
    setReadOnly();
}

Q_INVOKABLE QList<QStringList> Settings::getStringArray(QString group, QStringList initial)
{
    QList<QStringList> array;

    int size = m_settings->beginReadArray(group);

    for (int i = 0; i < size; ++i) {
        m_settings->setArrayIndex(i);

        QStringList g;
        QStringList keys = m_settings->childKeys();
        for(int y=0; y<keys.size(); y++){
            g.push_back(m_settings->value(keys.at(y)).toString());
        }

        array.push_back(g);
    }

    m_settings->endArray();

    return array;
}

Q_INVOKABLE void Settings::setStringArray(QString group, QStringList keys, QList<QStringList> value)
{
    setWrite();
//keys = settings.childKeys();
    m_settings->beginWriteArray(group);
    for (int i = 0; i < value.size(); ++i) {
        m_settings->setArrayIndex(i);

        QStringList g;
        for(int y=0; y<keys.size(); y++){
            QString key = keys.at(y);
            QStringList l = value.at(i);
            QString value = l.at(y);
            m_settings->setValue(key, value);
        }
    }

    m_settings->endArray();

    setReadOnly();
}

Q_INVOKABLE QStringList Settings::getStringList(QString group, QStringList initial)
{
    QStringList array;

    QStringList g = m_settings->childGroups();
    if(!g.contains(group)) return initial;

    int size = m_settings->beginReadArray(group);
    for (int i = 0; i < size; ++i) {
        m_settings->setArrayIndex(i);

        array.push_back(m_settings->value("list").toString());
    }

    m_settings->endArray();

    return array;
}

Q_INVOKABLE void Settings::setStringList(QString group, QStringList value)
{
    setWrite();
//settings.childGroups();
    m_settings->beginWriteArray(group);
    for (int i = 0; i < value.size(); ++i) {
        m_settings->setArrayIndex(i);

        m_settings->setValue("list", value.at(i));
    }

    m_settings->endArray();

    setReadOnly();
}


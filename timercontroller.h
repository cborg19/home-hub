#ifndef TIMERCONTROLLER_H
#define TIMERCONTROLLER_H

#include <QObject>
#include <QTimer>
#include <QVariant>
#include <QMutex>
#include <QDateTime>

class TimerController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList list READ getTimerList NOTIFY timerListChanged)
public:
    explicit TimerController(QObject *parent = nullptr);

    QVariantList getTimerList();

    void createTimer(QString Title, unsigned int time);
    void pauseTimer(int index);
    void resumeTimer(int index);
    void cancelTimer(int index);
    void addMinuteToTimer(int index);

    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void timerListChanged();
    void pageTrigger(QString page, int index);
    void startTimer();
    void stopTimer();

    void triggerAlarm(QVariantMap);
public slots:

private slots:
    void updateTimer();

    void timerStart();
    void timerStop();
private:
    QMutex l_list;
    QVariantList m_list;

    unsigned int fromString(QStringList);

    QTimer t_timer;
    QDateTime lastTime;
};

#endif // TIMERCONTROLLER_H

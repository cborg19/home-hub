#include <QtCore/qdebug.h>
#include <QProcess>
#include <QDateTime>
#include <QThread>
#include <QMediaPlaylist>
#include <QDirIterator>
#include <QBuffer>
#include <QFileInfo>

#include "workerthread.h"

WorkerThread::WorkerThread(QObject *parent) : QObject(parent),
    running(true), m_timeSyncCompleted(false),
    m_screenEnable(false), screenMode(Standard), doAlive(false), doAway(false), sharedMemory("QVoiceControl")
{
    pAlive = 0;
    pLight = 0;
    pAway = false;
    lightNight = 1;
    lightNightOn = 15;
    lightNightOff = 0;
    lightClock = 1;
    lightClockOn = 150;
    lightClockOff = 15;
    lightDay = 1;
    lightDayOn = 255;
    lightDayOff = 150;

    nightModeFinish.setHMS(6,0,0,0);
    nightModeStart.setHMS(20,0,0,0);
}

void WorkerThread::Initialise(Controllers c)
{
    control = c;

    QDateTime cd = QDateTime::currentDateTime();
    qsrand(cd.toTime_t());

    qDebug() << "thread initialise";
}

void WorkerThread::run()
{
    while(running && !m_timeSyncCompleted){
        SyncTime();

        QThread::msleep(3000);
    }

    qsrand(QTime::currentTime().msec());

    QTime time = QTime::currentTime();
    QTime sunrise = control.m_earth->getSunRise();
    QTime sunset = QTime(19,0);//m_earth->getSunSet();
    if(sunrise.hour() >= 7){
        sunrise.setHMS(6, 59, 0, 0);
    }
    timeStarted(time, sunrise, sunset);

    bool isRunning = true, voiceEnabled = false;
    int lastMinute = 0, lastSecond = 0;
    int lightSensor = 0;
    while(isRunning){
        qint64 c = QDateTime::currentMSecsSinceEpoch();
        //check for time diff
        time = QTime::currentTime();
        if(time.minute() != lastMinute){
            if(time.minute() == 0 && time.hour() == 0){
if(gdebug) qDebug() << "CRASH WorkerThread run A";
                //sun rise
                sunrise = control.m_earth->getSunRise();
                sunset = QTime(19,0);//m_earth->getSunSet();
                //sync time every day
                SyncTime();
            }

            //Check for Alarms
            control.m_alarms->checkAlarms();

            if(pAlive == 0 && !doAlive && !pAway) timeState(time, sunrise, sunset);
            lastMinute = time.minute();
        }

        //check for light sensor
        if(lastSecond != time.second()){
            #ifdef Q_OS_LINUX
            if((c - pLight) > 1000){
                //bool lights = m_lightSensor->isLightOn();
                lightSensor = control.m_lightSensor->getValue();
//qDebug() <<"lightSensor" << lightSensor;
//qDebug() <<"lightSensor" << lightSensor << lightNight << m_screenEnable;
                if(screenMode == Night && pAlive == 0){
                    if(lightSensor >= lightNight && !m_screenEnable){
                        qDebug() << "LIGHT SENSOR ON";
                        control.m_LCD->lcdOn();
                        m_screenEnable = true;
                        control.m_LCD->setBrightness(lightNightOn);
                    }else if(lightSensor < lightNight && m_screenEnable){
                        qDebug() << "LIGHT SENSOR OFF";
                        control.m_LCD->lcdOff();
                        m_screenEnable = false;
                    }
/*                }else if(screenMode == Clock && pAlive == 0){
                    if(lightSensor >= lightClock && control.m_LCD->currentBrightness() != lightClockOn){
                        qDebug() << "CLOCK ON";
                        control.m_LCD->setBrightness(lightClockOn);
                    }else if(lightSensor < lightClock && control.m_LCD->currentBrightness() != lightClockOff){
                        qDebug() << "CLOCK OFF";
                        control.m_LCD->setBrightness(lightClockOff);
                    }
                }else{
                    if(lightSensor >= lightDay && control.m_LCD->currentBrightness() != lightDayOn){
                        qDebug() << "LIGHT SENSOR ON";
                        control.m_LCD->setBrightness(lightDayOn);
                    }else if(lightSensor < lightDay && control.m_LCD->currentBrightness() != lightDayOff){
                        qDebug() << "LIGHT SENSOR OFF";
                        control.m_LCD->setBrightness(lightDayOff);
                        //qDebug() << "lux" << v;
                    }*/
                }
                pLight = c;
            }

            #endif
        }

        //Check button presses
        #ifdef Q_OS_LINUX
        control.m_volume->detectVolButtons();
        #endif

        //Check for voice commands
        if (sharedMemory.attach()) {
            QBuffer buffer;
            QDataStream in(&buffer);
            QString text;

            sharedMemory.lock();
            buffer.setData((char*)sharedMemory.constData(), sharedMemory.size());
            buffer.open(QBuffer::ReadOnly);
            in >> text;

            if(text == "1"){
                sharedMemory.unlock();
                //qDebug() << "Speech Enabled";
                voiceEnabled = true;
                emit assistantChange(true, "");
            }else if(text == "" && voiceEnabled){
                sharedMemory.unlock();
                //qDebug() << "Speech closed";
                voiceEnabled = false;
                emit assistantChange(false, "");
            }else if(text != ""){
                char *to = (char*)sharedMemory.data();
                memset(to, '\0', sharedMemory.size());
                sharedMemory.unlock();
                //qDebug() << "Speech got something" << text;
                voiceEnabled = true;
                if(text != ""){
                    text = autoCorrect(text);
                    if(text.contains("lights")){
                        text = text.replace("lights","light");
                    }

                    emit assistantChange(true, text);

                    qDebug() << "VOICE COMMAND - " <<text;
                    QString response;
                    if(control.m_fibaro->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_weather->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_trains->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_musicplayer->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_kodi->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_recipe->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_timers->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_alarms->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_news->processVoiceCmd(text, response)){
                        voiceAlive();
                    }else if(control.m_assistant->processVoiceCmd(text, response)){
                        voiceAlive();
                    }
                    if(response != ""){
                        emit assistantVoice(response);
                    }
                }
            }else{
                sharedMemory.unlock();
            }

            sharedMemory.detach();
        }

        //Keep alive
        if(pAlive){
            if(c > pAlive){
                timeStarted(time, sunrise, sunset, lightSensor);
                pAlive = 0;
            }
        }

        lastSecond = time.second();

        QThread::msleep(100);

        if(l_alive.tryLock()){
            bool a = doAlive;
            doAlive = false;

            bool b = doAway;
            doAway = false;
            l_alive.unlock();
            if(a){
                alive(a);
            }
            if(b){
                away();
            }
        }

        if(l_stop.tryLock()){
            isRunning = running;
            l_stop.unlock();
        }
    }
}

void WorkerThread::voiceAlive()
{
    int timeout = 5 * 60 * 1000;
    if(screenMode == Night || screenMode == Clock){
        timeout = 30 * 1000;
    }
    if(pAlive == 0){
        if(!m_screenEnable){
            control.m_LCD->lcdOn();
            m_screenEnable = true;
            screenMode = Standard;
        }
        control.m_LCD->setBrightness(255);
    }
    //doAlive = false;
    pAlive = QDateTime::currentMSecsSinceEpoch() + 30*1000;
}

void WorkerThread::stayalive()
{
if(gdebug) qDebug() << "CRASH WorkerThread stayalive";
    QMutexLocker M(&l_alive);

    doAlive = true;
if(gdebug) qDebug() << "CRASH WorkerThread stayalive finished";
}

void WorkerThread::backHome()
{
    QMutexLocker M(&l_alive);
    pAlive = 0;
    doAlive = true;
}

void WorkerThread::alive(bool results)
{
if(gdebug) qDebug() << "CRASH WorkerThread alive";
    if(pAlive == 0){
        QTime time = QTime::currentTime();
qDebug() << "A";
        if((time.hour() > sunrise.hour() || (time.hour() == sunrise.hour() && time.minute() > sunrise.minute())) &&
                (time.hour() < sunset.hour() || (time.hour() == sunset.hour() && time.minute() < sunset.minute()))){
            qDebug() << "B";
            setStandard();
        }else if(!m_screenEnable){
            qDebug() << "C";
            setSunRise();
        }else if(results){
            qDebug() << "D";
            setStandard();
        }
        control.m_LCD->setBrightness(255);
    }
    //doAlive = false;
    pAlive = QDateTime::currentMSecsSinceEpoch() + 5 * 60*1000;
if(gdebug) qDebug() << "CRASH WorkerThread alive finished";
}

void WorkerThread::awayMode()
{
    QMutexLocker M(&l_alive);
    doAway = true;
}

void WorkerThread::away()
{
    setSleepMode();
    pAway = true;
    pAlive = 0;
}

void WorkerThread::SyncTime()
{
    qDebug() << "SyncTime";
    QProcess process1;
    process1.start("sudo /usr/sbin/ntpdate 192.168.178.1");
//Fri  4 Nov 04:27:24 AEDT 2016
    process1.waitForFinished();
    QDateTime current = QDateTime::currentDateTime();
    if(current.date().year() < 2019){
       qDebug() << "SyncTime failed";
       return;
    }
    qDebug() << "SyncTime completed";
    m_timeSyncCompleted = true;
}

void WorkerThread::timeStarted(QTime time, QTime sunrise, QTime sunset, int light)
{
if(gdebug) qDebug() << "CRASH WorkerThread timeStarted";
    if(time.hour() < nightModeFinish.hour() ||
            (time.hour() == nightModeFinish.hour() && time.minute() < nightModeFinish.minute())){
        qDebug() << "timeStarted night before";
        setSleepMode(light);
if(gdebug) qDebug() << "CRASH WorkerThread timeStarted finish5";
        return;
    }

    if(time.hour() < sunrise.hour() || (time.hour() == sunrise.hour() && time.minute() < sunrise.minute())){
        qDebug() << "timeStarted sun rise";
        setSunRise(light);
if(gdebug) qDebug() << "CRASH WorkerThread timeStarted finish4";
        return;
    }

    //sun set
    if(time.hour() < sunset.hour() || (time.hour() == sunset.hour() && time.minute() < sunset.minute())){
        qDebug() << "timeStarted standard";
        setStandard(light);
if(gdebug) qDebug() << "CRASH WorkerThread timeStarted finish3";
        return;
    }


    //Night Modes
    if(time.hour() <= nightModeStart.hour() ||
            (time.hour() == nightModeStart.hour() && time.minute() < nightModeStart.minute())){
         qDebug() << "timeStarted sun set";
        setSunSet(light);
if(gdebug) qDebug() << "CRASH WorkerThread timeStarted finish2";
        return;
    }

    qDebug() << "timeStarted sleep";
    setSleepMode(light);
if(gdebug) qDebug() << "CRASH WorkerThread timeStarted finish1";
}

void WorkerThread::timeState(QTime time, QTime sunrise, QTime sunset)
{
if(gdebug) qDebug() << "CRASH WorkerThread timeState";
    //sun rise
    if(time.hour() == sunrise.hour() && time.minute() == sunrise.minute()){
        setStandard();
    }

    //sun set
    if(time.hour() == sunset.hour() && time.minute() == sunset.minute()){
        setSunSet();
    }

    //Night Modes
    if(time.hour() == nightModeStart.hour() &&
            time.minute() == nightModeStart.minute()){
        setSleepMode();
    }
    if(time.hour() == nightModeFinish.hour() &&
            time.minute() == nightModeFinish.minute()){
        setSunRise();
    }
if(gdebug) qDebug() << "CRASH WorkerThread timeState finished";
}

void WorkerThread::setSleepMode(int light){
  qDebug() << "setNightMode" << light;
  if(light != -1){
      if(light > lightNight){
          control.m_LCD->lcdOn();
          if(control.m_LCD->currentBrightness() != lightNightOn)
            control.m_LCD->setBrightness(lightNightOn);
          m_screenEnable = true;
      }else{
          if(control.m_LCD->currentBrightness() != lightNightOn)
            control.m_LCD->setBrightness(lightNightOn);
          control.m_LCD->lcdOff();
          m_screenEnable = false;
      }
  }else{
      control.m_LCD->lcdOff();
       m_screenEnable = false;
      if(control.m_LCD->currentBrightness() != lightNightOn)
        control.m_LCD->setBrightness(lightNightOn);
  }

  screenMode = Night;

  emit switchScreen("sleep");
if(gdebug) qDebug() << "CRASH sleep finished";
}

void WorkerThread::setSunRise(int light){
  qDebug() << "setSunRise" << light;
  if(!m_screenEnable) control.m_LCD->lcdOn();
  m_screenEnable = true;
  if(light != -1){
      if(light > lightClock){
          if(control.m_LCD->currentBrightness() != lightClockOn)
            control.m_LCD->setBrightness(lightClockOn);
      }else{
          if(control.m_LCD->currentBrightness() != lightClockOff)
            control.m_LCD->setBrightness(lightClockOff);
      }
  }else{
      if(control.m_LCD->currentBrightness() != lightClockOn)
        control.m_LCD->setBrightness(lightClockOn);
  }
  screenMode = Clock;
  emit switchScreen("night");
if(gdebug) qDebug() << "CRASH night finished";
}

void WorkerThread::setSunSet(int light){
  qDebug() << "setSunSet" << light;
  if(!m_screenEnable) control.m_LCD->lcdOn();
  m_screenEnable = true;
  if(light != -1){
      if(light > lightClock){
          if(control.m_LCD->currentBrightness() != lightClockOn)
            control.m_LCD->setBrightness(lightClockOn);
      }else{
          if(control.m_LCD->currentBrightness() != lightClockOff)
            control.m_LCD->setBrightness(lightClockOff);
      }
  }else{
      if(control.m_LCD->currentBrightness() != lightClockOn)
        control.m_LCD->setBrightness(lightClockOn);
  }

  screenMode = Clock;
  emit switchScreen("sleep");
if(gdebug) qDebug() << "CRASH setSunSet finished";
}

void WorkerThread::setStandard(int light){
    qDebug() << "setStandard" << light;
  if(!m_screenEnable){
      control.m_LCD->lcdOn();
  }

  if(light != -1){
      if(light > lightDay){
          if(control.m_LCD->currentBrightness() != lightDayOn)
            control.m_LCD->setBrightness(lightDayOn);
      }else{
          if(control.m_LCD->currentBrightness() != lightDayOff)
            control.m_LCD->setBrightness(lightDayOff);
      }
  }else{
      if(control.m_LCD->currentBrightness() != lightDayOn)
        control.m_LCD->setBrightness(lightDayOn);
  }

  m_screenEnable = true;
  screenMode = Standard;
  emit switchScreen("standard");
if(gdebug) qDebug() << "CRASH setStandard finished";
}

QString WorkerThread::autoCorrect(QString text)
{
    text = text.toLower();
    if(text.contains("go to")){
        if(text.contains("fibre"))
            text = text.replace("fibre","fibaro");
    }

    return text;
}

//----------------------------------------------------------

BackgroundThread::BackgroundThread(QObject *parent) : QObject(parent),
    running(true), pLastWeather(0), pLastGalleryScan(0), scanMusic(false)
{
    m_weatherUpdate = (60 * 60 * 1000) + qrand() % 5000;
    m_galleryUpdate = (90 * 60 * 1000);

    //connect(&watcher, SIGNAL(directoryChanged(QString)), this, &BackgroundThread::showModifiedDir);
}

void BackgroundThread::Initialise(Controllers c)
{
    control = c;
    qDebug() << "BackgroundThread Initialise";
}

void BackgroundThread::run()
{
qDebug() << "BackgroundThread started";
    int lastHr = 0;
    bool isRunning = true, runMusic = false;
    QDateTime datetime = QDateTime::currentDateTime();
    while(isRunning){
        datetime = QDateTime::currentDateTime();
        qint64 c = QDateTime::currentMSecsSinceEpoch();
        QList<QObject *> * wList = control.m_weather->GetAllList();
//qDebug() << "WEATHER COUNT" << wList->count();
        for(int x=0; x<wList->count(); x++){
//qDebug() << "open weather" << x;
            Weather *w = (Weather *)wList->at(x);
            if((c - w->lastUpdate()) > m_weatherUpdate){
                bool doBreak = w->lastUpdate() != 0;
                w->update();
                if(doBreak)
                    break;
            }
        }

        //Check news
        if(lastHr != datetime.time().hour()){
            control.m_news->checkUpdate();

            lastHr = datetime.time().hour();
        }

        if(datetime.time().hour() == 0 && datetime.time().minute() < 10){
if(gdebug) qDebug() << "CRASH BackgroundThread SCAN A";
            if(control.m_imagescan->getLastScanDay() != datetime.date().day()){
                control.m_imagescan->syncAlbum();
            }
        }
        if((c - pLastGalleryScan) > m_galleryUpdate){
if(gdebug) qDebug() << "CRASH BackgroundThread SCAN B";
            pLastGalleryScan = c;
            control.m_imagescan->syncGallery();
        }

       if(l_music.tryLock()){
if(gdebug) qDebug() << "CRASH BackgroundThread SCAN C";
            runMusic = scanMusic;
            l_music.unlock();

            if(scanMusic){
if(gdebug) qDebug() << "CRASH BackgroundThread SCAN D";
                scanMusicDirectory();
                //l_music.lock();
                scanMusic = false;
                //l_stop.unlock();
            }
        }

        QThread::msleep(10000);
if(gdebug) qDebug() << "CRASH BackgroundThread SCAN E";
        if(l_stop.tryLock()){
if(gdebug) qDebug() << "CRASH BackgroundThread SCAN F";
            isRunning = running;
            l_stop.unlock();
        }
    }
    qDebug() << "THREAD STOP";
}

void BackgroundThread::processMusic()
{
    QMutexLocker M(&l_music);

    scanMusic = true;
}

void BackgroundThread::scanMusicDirectory()
{
    control.m_musicplayer->scanMusicDirectory();
}

QString BackgroundThread::pathName()
{
    return m_pathName;
}

void BackgroundThread::setPathName(const QString &pathName)
{
    if (pathName == m_pathName)
        return;

    m_pathName = pathName;

   // watcher.addPath(m_pathName+"music/");
}

void BackgroundThread::showModifiedDir(const QString& str)
{
    Q_UNUSED(str)
    QMutexLocker M(&l_music);

    scanMusic = true;
}

#ifndef LIGHTSENSOR_H
#define LIGHTSENSOR_H

#include <QObject>
#include "gpio.h"

#include <QFileSystemWatcher>
#include <QMutex>
#ifdef Q_OS_LINUX
//#include <QSerialPort>
#endif

class LightSensor : public QObject
{
    Q_OBJECT

    //Q_PROPERTY(int sensor READ sensor NOTIFY sensorChanged)
public:
    explicit LightSensor(QObject *parent = nullptr);
    explicit LightSensor(Gpio *gpio, QObject *parent = nullptr);

    void setGPIO(Gpio *gpio);

    //int sensor() const;

    //bool isLightOn();
    int getValue();

private slots:
    //void syncTime();
private:
    Gpio * m_gpio;
    int lastValue;
    QMutex l_last;

    int sample[20];
    int pos;
      // set our frequency multiplier to a default of 1
      // which maps to output frequency scaling of 100x

    int freq_mult;

      // need to measure what to divide freq by
      // 1x sensitivity = 10,
      // 10x sens = 100,
      // 100x sens = 1000

    int calc_sensitivity;

    void Initialise();
    long long get_tsl_freq();

    void set_scaling ( int what );
    float calc_lux_single(float uw_cm2, float efficiency);
    float calc_uwatt_cm2(long long freq);
    void sensitivity( bool dir );

    QFileSystemWatcher gpiowatcher;

#ifdef Q_OS_LINUX
 //   QSerialPort serial;
#endif
    QString lastSerial;
};

#endif // LIGHTSENSOR_H

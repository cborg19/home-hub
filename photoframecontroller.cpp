#include <QtCore/qdebug.h>
#include <QProcess>
#include <QTimer>
#include <QDateTime>

#include "photoframecontroller.h"
//#define ID3LIB_LINKOPTION 3
//#include "id3.h"

/* TODO
    BUGS
    When in sleep mode switch on light goes to info page.. Then timesout and then detects lights still on..
it should not go to sleep mode if lights still on
    Night mode <- when goes into night mode it doesnt check light status
    Light sensor should be set manually in settings
    Voice reg manual settings
    When do voice reg it need to show processing icon and what is said
    remember the setting for recipe, fibaro, kodi enbale/disable
    light sensor adjust in settings

    MISSING
    Quotes
    Weather Slides to be able to switch background image base on primary location, option to select all,
    Sydney Web Camera <- NEED html 5 url version

    https://api.deckchair.com/v1/camera/599d6375096641f2272bacf4/images?to=1558039140
    https://api.deckchair.com/v1/viewer/camera/599d6375096641f2272bacf4?width=1280&height=720&resizeMode=fill&gravity=Auto&quality=90&panelMode=false&format=jpg

    Sydney 24hrs snapshot
        //https://webcamsydney.com/
        //https://www.lunaparksydney.com/webcam

    Albums search and display <- need menu to say this album good or push to kodi
    when picture list is complete to refresh in nice way
    Image random is broken and needs swap. may do first load before time sincy only 20 photos at a time
    image random photo of the day - https://unsplash.com/documentation#get-a-random-photo

    Kodi remote
    Kodi - Pass video of camera shot to kodi.. pictues as well
        https://github.com/jbottel/send-image-to-kodi/blob/master/contextMenu.js
    Music <-problems with loading all song as playlist. Also id3 tags getting them for playlist
    Recipes
    Abc news RSS Feed - https://www.abc.net.au/news/feed/51120/rss.xml
    YouTube viewer
    radio internet <-Done

    Fibaro controller <-Done
        https://manuals.fibaro.com/content/other/FIBARO_System_REST_API.pdf
    Fibaro - away mode, when motion detection goes off wake device
    Google Calendar
        Client ID       1091151845201-rqa21vn0jcft23f19k3qjcssh7b0ppg8.apps.googleusercontent.com
        Client secret   m5odXZz0d8gQu-H6nxXHwpG7
    Alarm Clock <-Done
    Add all setting controls

    Speech
        aoss swift -n allison "Hello, this is Callie."
        pico2wave -w lookdave.wav "Look Dave, I can see you're really upset about this." && aplay lookdave.wav


    Hardware switch to cut power to camera and microphone
    Hardware Add wireless charging with detector
    Hardware leap motion
    Hardware Voice control
    Hardware design case

    hands free calls
    https://scribles.net/enabling-hands-free-profile-on-raspberry-pi-raspbian-stretch-by-using-pulseaudio/

apt-get
sudo apt-get install pigpio libtag1-dev ntpdate
sudo apt-get install libasound2-dev pulseaudio libpulse-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-plugins-bad gstreamer1.0-pulseaudio gstreamer1.0-tools gstreamer1.0-alsa gstreamer-tools gstreamer1.0-libav

camera
sudo raspistill -v -o test.jpg

taglib Install
sudo apt-get install libtag1-dev
https://github.com/taglib/taglib/blob/master/INSTALL.md

asound.conf mono
pcm.onboard {
type hw
card 0
}

pcm.!default {
type plug
slave.pcm "onboard_dmix"
}

pcm.onboard_dmix {
type dmix
ipc_key 1024
ipc_key_add_uid 1
ipc_perm 0666
slave {
pcm "onboard"
channels 2
}
bindings {
0 0
1 1
}
}

pcm.zone1 {
type plug
slave.pcm onboard_dmix
slave.channels 2
ttable {
0.0 0.5
1.0 0.5
0.1 0
1.1 0
}
}

pcm.zone2 {
type plug
slave.pcm onboard_dmix
slave.channels 2
ttable {
0.0 0
1.0 0
0.1 0.5
1.1 0.5
}
}

asound.conf
pcm.hifiberry {
  type hw card 0
}

pcm.!default {
  type asym
  capture.pcm "mic"
  playback.pcm "speaker"
}

pcm.mic {
  type plug
  slave {
    pcm "hw:1,0"
  }
}

pcm.speaker {
  type dmix
  ipc_key 1024
  slave {
    pcm "hifiberry"
    channels 2
  }
}



fstab
/root/.cache/dconf/ tmpfs nodev,nosuid 0 0
/var/lib/pulse tmpfs nodev,nosuid 0 0
//ironhide/Multimedia	/mnt/Multimedia	cifs	defaults,rw,credentials=/home/pi/nas-credentials,vers=1.0,sec=ntlm,cache=none,noauto,x-systemd.automount 0 0
//ironhide/Public	/mnt/Public	cifs	defaults,rw,credentials=/home/pi/nas-credentials,vers=1.0,sec=ntlm,cache=none,noauto,x-systemd.automount 0 0

google assits
cd Download
source env/bin/activate
https://developers.google.com/assistant/sdk/guides/service/python/embed/install-sample
googlesamples-assistant-pushtotalk --project-id homeframe-1560629486145 --device-model-id homeframe-1560629486145-homeframe-xy9ei6
googlesamples-assistant-pushtotalk -i input.wav -o output.wav

python3 demog.py --project-id homeframe-1560629486145 --device-model-id homeframe-1560629486145-homeframe-xy9ei6 --hotword-model saved_model.pmdl

sudo pip3 install --upgrade google-assistant-grpc
pip3 install --upgrade google-auth-oauthlib[tool]
sudo pip3 install google-auth -U
sudo pip3 install requests
sudo pip3 install tenacity
sudo pip3 install sounddevice

pocketsphinx
https://howchoo.com/g/ztbhyzfknze/how-to-install-pocketsphinx-on-a-raspberry-pi
sudo arecord --format=S16_LE --duration=5 --rate=16000 --file-type=wav input.wav
pocketsphinx_continuous -hmm /usr/local/share/pocketsphinx/model/en-us/en-us -lm 4455.lm -dict 4455.dic -samprate 16000/8000/48000 -inmic yes
pocketsphinx_continuous -lm 4455.lm -dict 4455.dic -infile input.wav  2>voice2.log

raspi camera
https://github.com/cedricve/raspicam
*/

bool gdebug = false;

PhotoFrameController::PhotoFrameController(QObject *parent) : QObject(parent)
{
    #ifdef Q_OS_LINUX
    qDebug() << "Running on Raspberry Pi";
    QString Path = "/home/pi/Frame/";
    #else
    QString Path = "/Users/Chris/Documents/Development/reactjs/hubhomeqt/PhotoFrame/";
    #endif

    m_assistandEnabled = false;
    m_assistantCommand = "";

    m_earth.setLocation(-33.8667, -151.2167, -10);
    m_lightSensor.setGPIO(&m_gpio);
    m_volume.setGPIO(&m_gpio);

    Controllers Control;
    Control.m_LCD = &m_LCD;
    Control.m_imagescan = &m_imagescan;
    Control.m_weather = &m_weather;
    Control.m_gpio = &m_gpio;
    Control.m_lightSensor = &m_lightSensor;
    Control.m_earth = &m_earth;
    Control.m_volume = &m_volume;
    Control.m_musicplayer = &m_musicplayer;
    Control.m_kodi = &m_kodi;
    Control.m_trains = &m_trains;
    Control.m_fibaro = &m_fibaro;
    Control.m_recipe = &m_recipe;
    Control.m_assistant = &m_assistant;
    Control.m_timers = &m_timers;
    Control.m_alarms = &m_alarms;
    Control.m_news = &m_news;

    //Volume
    connect(&m_volume, &VolumeController::volumeChange, this, &PhotoFrameController::volumeChange);

    //High level thread
    worker = new WorkerThread;
    worker->Initialise(Control);
    worker->moveToThread(&wThread);

    connect(&wThread, &QThread::finished, worker, &QObject::deleteLater);
    connect(&wThread, SIGNAL(started()), worker, SLOT(run()));
    //connect(this, &PhotoFrameController::wakeUpAlive, worker, &WorkerThread::stayalive);
    connect(worker, &WorkerThread::switchScreen, this, &PhotoFrameController::handleScreen);

    //assistant
    connect(worker, &WorkerThread::assistantChange, this, &PhotoFrameController::assistantChange);
    connect(worker, &WorkerThread::assistantVoice, this, &PhotoFrameController::assistantVoice);
    connect(this, &PhotoFrameController::processAssistant, &m_assistant, &VoiceAssistant::processAssistant);
    connect(&m_trains, &SydneyTrains::assistantVoice, &m_assistant, &VoiceAssistant::processAssistant);
    connect(&m_assistant, &VoiceAssistant::assistantVoice, &m_musicplayer, &MusicController::ProcessAssistant);
    connect(&t_assistant, &QTimer::timeout, this, &PhotoFrameController::assistantTimer);

    //Low Level thread
    background = new BackgroundThread;
    background->Initialise(Control);
    background->moveToThread(&bThread);

    connect(&bThread, &QThread::finished, background, &QObject::deleteLater);
    connect(&bThread, SIGNAL(started()), background, SLOT(run()));

    //slot connections
    connect(&m_imagescan, &Imagescan::listChanged, this, &PhotoFrameController::imagelistChanged);
    connect(&m_weather, &WeatherController::primaryUpdate, this, &PhotoFrameController::handleWeatherPrimary);
    connect(&m_weather, &WeatherController::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_kodi, &KodiController::connection, this, &PhotoFrameController::kodiConnection);
    connect(&m_kodi, &KodiController::isplaying, this, &PhotoFrameController::kodiIsPlaying);
    connect(&m_kodi, &KodiController::ispaused, this, &PhotoFrameController::kodiIsPaused);
    connect(&m_kodi, &KodiController::currentMetaDataChanged, this, &PhotoFrameController::kodiMetaDataChange);
    connect(&m_kodi, &KodiController::volume, this, &PhotoFrameController::KodiVolume);
    connect(&m_kodi, &KodiController::currentPercentageChanged, this, &PhotoFrameController::kodiPercentageChange);
    connect(&m_kodi, &KodiController::currentTimeChanged, this, &PhotoFrameController::kodiTimeChange);
    connect(&m_kodi, &KodiController::currentTotalTimeChanged, this, &PhotoFrameController::kodiTimeTotalChange);
    connect(&m_kodi, &KodiController::listChanged, this, &PhotoFrameController::kodiTvListChange);
    connect(&m_kodi, &KodiController::menuChanged, this, &PhotoFrameController::kodiMoviesListChange);
    connect(&m_kodi, &KodiController::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_musicplayer, &MusicController::internetRadioListChanged, this, &PhotoFrameController::InternetRadioListChange);
    connect(&m_musicplayer, &MusicController::internetRadioListIndexChanged, this, &PhotoFrameController::InternetRadioListIndexChange);
    connect(&m_musicplayer, &MusicController::internetRadioPositionChanged, this, &PhotoFrameController::InternetRadioPosChange);
    connect(&m_musicplayer, &MusicController::musicListChanged, this, &PhotoFrameController::MusicListChange);
    connect(&m_musicplayer, &MusicController::musicListIndexChanged, this, &PhotoFrameController::MusicPosChange);
    connect(&m_musicplayer, &MusicController::isPlayingChanged, this, &PhotoFrameController::MusicSongIsPlayingChange);
    connect(&m_musicplayer, &MusicController::positionChanged, this, &PhotoFrameController::MusicPosChanged);
    connect(&m_musicplayer, &MusicController::durationChanged, this, &PhotoFrameController::MusicDurChanged);
    connect(&m_musicplayer, &MusicController::mediaDataChanged, this, &PhotoFrameController::MusicDataChanged);
    connect(&m_musicplayer, &MusicController::mediaImageChanged, this, &PhotoFrameController::musicImageChanged);
    connect(&m_musicplayer, &MusicController::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_trains, &SydneyTrains::trainListChanged, this, &PhotoFrameController::sdyneyTrainListChange);
    connect(&m_trains, &SydneyTrains::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_fibaro, &FibaroController::roomListChanged, this, &PhotoFrameController::fibaroRoomChange);
    connect(&m_fibaro, &FibaroController::sectionListChanged, this, &PhotoFrameController::fibaroSectionChange);
    connect(&m_fibaro, &FibaroController::deviceListChanged, this, &PhotoFrameController::fibaroDeviceChange);
    connect(&m_fibaro, &FibaroController::sceneListChanged, this, &PhotoFrameController::fibaroSceneChange);
    connect(&m_fibaro, &FibaroController::customListChanged, this, &PhotoFrameController::fibaroCustomChange);
    connect(&m_fibaro, &FibaroController::alertChange, this, &PhotoFrameController::alertIconChange);
    connect(&m_fibaro, &FibaroController::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_recipe, &FoodController::categoryListChanged, this, &PhotoFrameController::recipeCategoryListChanged);
    connect(&m_recipe, &FoodController::recipeListChanged, this, &PhotoFrameController::recipefoodListChanged);
    connect(&m_recipe, &FoodController::currentRecipeChanged, this, &PhotoFrameController::recipeCurrentChanged);
    connect(&m_recipe, &FoodController::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_timers, &TimerController::timerListChanged, this, &PhotoFrameController::timersListChanged);
    connect(&m_timers, &TimerController::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_timers, &TimerController::triggerAlarm, &m_alarms, &AlarmController::triggerAlarm);
    connect(&m_alarms, &AlarmController::currentAlarmChanged, this, &PhotoFrameController::alarmCurrentChanged);
    connect(&m_alarms, &AlarmController::alarmListChanged, this, &PhotoFrameController::alarmListsChanged);
    connect(&m_alarms, &AlarmController::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_alarms, &AlarmController::triggerSound, &m_musicplayer, &MusicController::triggerSound);
    connect(&m_assistant, &VoiceAssistant::pageTrigger, this, &PhotoFrameController::pageTrigger);
    connect(&m_news, &NewsController::pageTrigger, this, &PhotoFrameController::pageTrigger);

    m_settings.setPathName(Path);
    m_imagescan.setPathName(Path);
    m_musicplayer.setPathName(Path);
    background->setPathName(Path);

    loadSettings();

    wThread.start();
    wThread.setPriority(QThread::HighPriority);//LowPriority
    bThread.start();
    bThread.setPriority(QThread::LowPriority);

//    m_player = new QMediaPlayer(this);
//    connect(m_player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(onMediaStatusChanged(QMediaPlayer::MediaStatus)));
}

PhotoFrameController::~PhotoFrameController()
{
    worker->stop();
    background->stop();
    wThread.quit();
    bThread.quit();
    wThread.wait();
    bThread.wait();
}

void PhotoFrameController::loadSettings()
{
    qDebug() << "Setting loading";
    m_nightBgColor = getSettingString("nightmode", "backgroundcolour", "black");
    m_nightFontColor = getSettingString("nightmode", "fountcolour", "white");
    m_imagescan.setWeatherInterval(getSettingInt("weatherInterval", "weatherInterval", 10));
    m_imagescan.setOtherInterval(getSettingInt("gallery", "otherInterval", 20));
    m_imagescan.setslideInterval(getSettingInt("gallery", "slideInterval", 30000));
    m_volume.setVolume(getSettingInt("Audio", "volume", 50));
    m_kodi.setSettings(
      getSettingBool("kodi", "enable", true),
      getSettingString("kodi", "host", "192.168.178.27"),
      getSettingInt("kodi", "port", 9090),
      getSettingInt("kodi", "httpport", 8081),
      getSettingString("kodi", "username", "xbmc"),
      getSettingString("kodi", "password", "Macross1"),
      getSettingBool("kodi", "tcp", true),
      getSettingBool("kodi", "eventserver", true)
    );
    m_trains.setSettings(
      getSettingString("trains", "from", "10101417"),
      getSettingString("trains", "to", "10101103") //Central 10101100 Circular Quay 10101103 Wolli Creek 10101328 Sutherland 10101345
    );
    m_fibaro.setSettings(
      getSettingBool("fibaro", "enable", true),
      getSettingString("fibaro", "ip", "192.168.178.10"),
      getSettingString("fibaro", "user", "photoframe"),
      getSettingString("fibaro", "password", "a2K9*6qbuu"),
      getSettingInt("fibaro", "port", 80)
    );
    m_recipe.setSettings(
      getSettingBool("recipe", "enable", true),
      getSettingString("recipe", "ip", "220.233.119.142"),
      getSettingString("recipe", "user", "cjb19"),
      getSettingString("recipe", "password", "wreck-it-ralf"),
      getSettingInt("recipe", "port", 8080)
    );
    worker->setDayTime(
      getSettingInt("screen", "daytime", 1),
      getSettingInt("screen", "daytimeon", 255),
      getSettingInt("screen", "daytimeoff", 150)
    );
    worker->setClockTime(
      getSettingInt("screen", "clocktime", 1),
      getSettingInt("screen", "clocktimeon", 150),
      getSettingInt("screen", "clocktimeoff", 15)
    );
    worker->setNightTime(
      getSettingInt("screen", "nighttime", 1),
      getSettingInt("screen", "nighttimeon", 15),
      getSettingInt("screen", "nighttimeoff", 0)
    );

    QStringList weatherInitial;
    int weatherCount = getSettingInt("weather", "count", 0);

    if(weatherCount > 0){
        for(int x=0; x<weatherCount; x++){
            QString w = getSettingString("weather", "item-"+QString::number(x), "");
            if(w.length() > 1){
                weatherInitial.push_back(QString::number(x)+"|"+w);
            }
        }
    }else{
        weatherInitial.push_back("0|Sydney|-33.881830|151.206450|IDR713|true");
        weatherInitial.push_back("1|Wollongong|-34.4251|150.8931|IDR033|false");
        weatherInitial.push_back("2|Revesby|-33.957818|151.011542|IDR033|false");
    }

    m_weather.setPathName(m_imagescan.pathName());
    m_weather.setSettings(weatherInitial);

    int alarmCount = getSettingInt("alarm", "count", 0);
    if(alarmCount > 0){
        for(int x=0; x<alarmCount; x++){
            m_alarms.setAlarm(-1,
                              getSettingString("alarm", "title"+QString::number(x), ""),
                              getSettingInt("alarm", "timeHr"+QString::number(x), 6),
                              getSettingInt("alarm", "timeMin"+QString::number(x), 0),
                              getSettingString("alarm", "date"+QString::number(x), ""),
                              getSettingString("alarm", "day"+QString::number(x), "0|0|0|0|0|0|0"),
                              getSettingString("alarm", "type"+QString::number(x), "day"),
                              getSettingString("alarm", "sound"+QString::number(x), ""),
                              getSettingBool("alarm", "snooze"+QString::number(x), false),
                              getSettingBool("alarm", "enabled"+QString::number(x), false)
            );
        }
    }

    qDebug() << "Setting All Loaded";
}

void PhotoFrameController::handleWeatherPrimary()
{
    qDebug() << "PhotoFrameController primary weather update";
    m_imagescan.hasWeather(true);
    emit primaryWeather();
}

void PhotoFrameController::handleScreen(const QString &screen)
{
    emit operateScreen(screen);
}

QString PhotoFrameController::pathName()
{
    return m_imagescan.pathName();
}

void PhotoFrameController::setPathName(const QString &pathName)
{
    m_imagescan.setPathName(pathName);
}

int PhotoFrameController::slideInterval()
{
    return m_imagescan.slideInterval();
}

void PhotoFrameController::setslideInterval(const int &source)
{
    m_imagescan.setslideInterval(source);
    setSettingInt("gallery", "slideInterval", source);
}

int PhotoFrameController::otherInterval()
{
    return m_imagescan.otherInterval();
}

void PhotoFrameController::setOtherInterval(const int &source)
{
    m_imagescan.setOtherInterval(source);
    setSettingInt("gallery", "otherInterval", source);
}

int PhotoFrameController::weatherInterval()
{
    return m_imagescan.weatherInterval();
}

void PhotoFrameController::setWeatherInterval(const int &source)
{
    m_imagescan.setWeatherInterval(source);
    setSettingInt("weather", "weatherInterval", source);
}

bool PhotoFrameController::getShowNotification()
{
    return getSettingBool("notification", "show", true);
}

void PhotoFrameController::setShowNotification(bool v)
{
    setSettingBool("notification", "show", v);
}

bool PhotoFrameController::getNotificationAlwaysOn()
{
    return getSettingBool("notification", "alwayson", false);
}

void PhotoFrameController::setNotificationAlwaysOn(bool v)
{
    setSettingBool("notification", "alwayson", v);
}

QStringList PhotoFrameController::getAllowNotification()
{
    return getSettingString("notification", "allowed", "kodi|music").split("|");
}

void PhotoFrameController::setAllowNotification(QStringList v)
{
    setSettingString("notification", "allowed", v.join("|"));
}

QVariantList PhotoFrameController::getList()
{
//    qDebug() << "PhotoFrameController getList";
    return m_imagescan.getList();
}

//QQmlListProperty<QObject> PhotoFrameController::getList()
//{
//    qDebug() << "PhotoFrameController getList";
//    return m_imagescan.getList();
//}

Q_INVOKABLE void PhotoFrameController::scanGallery()
{
    qDebug() << "PhotoFrameController scanGallery finished";
    m_imagescan.scanGallery();
    qDebug() << "PhotoFrameController scanGallery finished";
}

void PhotoFrameController::imagelistChanged()
{
    qDebug() << "PhotoFrameController imagelistChanged";
    emit listChanged();
}

Q_INVOKABLE int PhotoFrameController::getSlideSize()
{
    return m_imagescan.getListSize();
}

Q_INVOKABLE void PhotoFrameController::blockAlbum(int v)
{
    m_imagescan.changeTypeAlbum(v,"b");
}

Q_INVOKABLE void PhotoFrameController::favouriteAlbum(int v)
{
    m_imagescan.changeTypeAlbum(v,"f");
}

Q_INVOKABLE void PhotoFrameController::neturalAlbum(int v)
{
    m_imagescan.changeTypeAlbum(v,"n");
}

Q_INVOKABLE QVariantList PhotoFrameController::getAlbumList()
{
    return m_imagescan.getAlbumList();
}

QQmlListProperty<QObject> PhotoFrameController::getWeather()
{
    return m_weather.getList();
}

int PhotoFrameController::getWeatherSize()
{
    return m_weather.getListSize();
}

QStringList PhotoFrameController::getWeatherOption()
{
    return m_weather.getWeatherOption();
}

Q_INVOKABLE void PhotoFrameController::addWeatherOption(const QStringList &list)
{
    m_weather.addWeatherOption(list);
    QList<QStringList> allList = m_weather.getSettings();

    setSettingInt("weather", "count", allList.length());
    for(int x=0; x<allList.length(); x++){
        QString s = allList[x].join("|");
        setSettingString("weather", "item-"+QString::number(x), s);
    }
}

Q_INVOKABLE void PhotoFrameController::updateWeatherOption(int pos, const QStringList &list)
{
    m_weather.updateWeatherOption(pos, list);
    QList<QStringList> allList = m_weather.getSettings();

    setSettingInt("weather", "count", allList.length());
    for(int x=0; x<allList.length(); x++){
        QString s = allList[x].join("|");
        setSettingString("weather", "item-"+QString::number(x), s);
    }
}

Q_INVOKABLE void PhotoFrameController::removeWeatherOption(int pos)
{
    m_weather.removeWeatherOption(pos);
    QList<QStringList> allList = m_weather.getSettings();

    setSettingInt("weather", "count", allList.length());
    for(int x=0; x<allList.length(); x++){
        QString s = allList[x].join("|");
        setSettingString("weather", "item-"+QString::number(x), s);
    }
}

Q_INVOKABLE void PhotoFrameController::makeWeatherPrimary(int pos)
{
    m_weather.setPrimary(pos);

    QList<QStringList> list = m_weather.getSettings();

    setSettingInt("weather", "count", list.length());
    for(int x=0; x<list.length(); x++){
        QString s = list[x].join("|");
        setSettingString("weather", "item-"+QString::number(x), s);
    }
}

int PhotoFrameController::getWeatherPrimary()
{
    int v = m_weather.getPrimary();
    if(v == -1) return 0;
    return v;
}

Q_INVOKABLE QStringList PhotoFrameController::getWeatherOptionAt(int x)
{
    return m_weather.getWeatherOptionAt(x);
}
/*
QString PhotoFrameController::getRadarBgImage(int pos)
{
    return m_weather.getR
}*/

QString PhotoFrameController::pathSettings()
{
    return m_settings.pathName();
}

void PhotoFrameController::setPathSettings(const QString &pathName)
{
    m_settings.setPathName(pathName);
}

Q_INVOKABLE QString PhotoFrameController::getSettingString(QString group, QString key, QString initial)
{
    return m_settings.getString(group, key, initial);
}

Q_INVOKABLE void PhotoFrameController::setSettingString(QString group, QString key, QString value)
{
    m_settings.setString(group, key, value);
}

Q_INVOKABLE bool PhotoFrameController::getSettingBool(QString group, QString key, bool initial)
{
    return m_settings.getBool(group, key, initial);
}

Q_INVOKABLE void PhotoFrameController::setSettingBool(QString group, QString key, bool value)
{
    m_settings.setBool(group, key, value);
}

Q_INVOKABLE int PhotoFrameController::getSettingInt(QString group, QString key, int initial)
{
    return m_settings.getInt(group, key, initial);
}

Q_INVOKABLE void PhotoFrameController::setSettingInt(QString group, QString key, int value)
{
    m_settings.setInt(group, key, value);
}

QString PhotoFrameController::nightBgColour() const
{
    return m_nightBgColor;
}

void PhotoFrameController::setNightBgColour(const QString &source)
{
    if(source != m_nightBgColor){
        m_nightBgColor = source;
        setSettingString("nightmode", "backgroundcolour", source);
    }
}

QString PhotoFrameController::nightFontColour() const
{
    return m_nightFontColor;
}

void PhotoFrameController::setNightFontColour(const QString &source)
{
    if(source != m_nightFontColor){
        m_nightFontColor = source;
        setSettingString("nightmode", "fountcolour", source);
    }
}

int PhotoFrameController::getScreenDayTrigger()
{
    return 0;
}

void PhotoFrameController::setScreenDayTrigger(int)
{

}

int PhotoFrameController::getScreenDayOn()
{
    return 0;
}

void PhotoFrameController::setScreenDayOn(int)
{

}

int PhotoFrameController::getScreenDayOff()
{
    return 0;
}

void PhotoFrameController::setScreenDayOff(int)
{

}

int PhotoFrameController::getScreenClockTrigger()
{
    return 0;
}

void PhotoFrameController::setScreenClockTrigger(int)
{

}

int PhotoFrameController::getScreenClockOn()
{
    return 0;
}

void PhotoFrameController::setScreenClockOn(int)
{

}

int PhotoFrameController::getScreenClockOff()
{
    return 0;
}

void PhotoFrameController::setScreenClockOff(int)
{

}

int PhotoFrameController::getScreenNightTrigger()
{
    return 0;
}

void PhotoFrameController::setScreenNightTrigger(int)
{

}

int PhotoFrameController::getScreenNightOn()
{
    return 0;
}

void PhotoFrameController::setScreenNightOn(int)
{

}

int PhotoFrameController::getScreenNightOff()
{
    return 0;
}

void PhotoFrameController::setScreenNightOff(int)
{

}

Q_INVOKABLE void PhotoFrameController::alive()
{
    worker->stayalive();
}


Q_INVOKABLE void PhotoFrameController::swittchAwayMode()
{
    worker->awayMode();
}

Q_INVOKABLE void PhotoFrameController::screenPhoto()
{
    m_LCD.setBrightness(255);
}


Q_INVOKABLE void PhotoFrameController::reboot()
{
    #ifdef Q_OS_LINUX
    QProcess process1;
    process1.start("sudo reboot");

    if(!process1.waitForFinished()){
       return;
    }
    #endif
}

Q_INVOKABLE void PhotoFrameController::shutdown()
{
    #ifdef Q_OS_LINUX
    QProcess process1;
    process1.start("sudo halt");

    if(!process1.waitForFinished()){
       return;
    }
    #endif
}

void PhotoFrameController::handleMusicFilesChange()
{
    emit musicAllChanged();
}

Q_INVOKABLE void PhotoFrameController::testMusic()
{
//qDebug() << "music TEST";
//    m_player->setMedia(QUrl::fromLocalFile("C:\\Users\\Chris\\Documents\\Development\\reactjs\\hubhomeqt\\PhotoFrame\\music\\3\\01 - Seven Nation Army.mp3"));

    //m_musicplayer->scanDirectory();
    /*qDebug() << "Music Test 101";
    player = new QMediaPlayer;
    // ...
    player->setMedia(QUrl::fromLocalFile("/home/pi/Downloads/Vance_Joy_Riptide_Official_Video.mp3"));
    player->setVolume(80);
    player->play();
    qDebug() << "playing";*/
}
/*
void PhotoFrameController::onMediaStatusChanged(QMediaPlayer::MediaStatus status)
{
    if (status == QMediaPlayer::LoadedMedia){
        // Get the list of keys there is metadata available for
          QStringList metadatalist = m_player->availableMetaData();

          // Get the size of the list
          int list_size = metadatalist.size();

          qDebug() << m_player->isMetaDataAvailable() << list_size;

          // Define variables to store metadata key and value
          QString metadata_key;
          QVariant var_data;

          for (int indx = 0; indx < list_size; indx++)
          {
            // Get the key from the list
            metadata_key = metadatalist.at(indx);

            // Get the value for the key
            var_data = m_player->metaData(metadata_key);

           qDebug() << metadata_key << var_data.toString();
           if(metadata_key == "ThumbnailImage"){
               QImage i(var_data.toByteArray());
               qDebug() << i.size();
           }
          }
    }
}
*/
void PhotoFrameController::volumeChange(int v)
{
    qDebug() << "PhotoFrameController" << v;
    setSettingInt("Audio", "volume", v);
    emit volume(v);
}

int PhotoFrameController::getVolume()
{
    return m_volume.getVolume();
}

Q_INVOKABLE void PhotoFrameController::setVolume(int v)
{
    m_volume.setVolume(v);
}

Q_INVOKABLE void PhotoFrameController::scanForMusic()
{
    m_musicplayer.scanMusicDirectory();
}

Q_INVOKABLE void PhotoFrameController::keyPress(int keycode)
{
    qDebug() << "KODI KEYPRESS";
    m_kodi.keypress(keycode);
}

bool PhotoFrameController::getKodiConnection()
{
    return m_kodi.isConnected();
}

void PhotoFrameController::kodiConnection(bool v)
{
    Q_UNUSED(v);
    emit kodiConnectionChanged();
}

void PhotoFrameController::kodiIsPaused(bool v)
{
    Q_UNUSED(v);
    emit kodiPausedChanged();
}

void PhotoFrameController::kodiIsPlaying(bool v)
{
    Q_UNUSED(v);
    emit kodiPlayingChanged();
}

bool PhotoFrameController::getKodiPaused()
{
    return m_kodi.isPaused();
}

bool PhotoFrameController::getKodiPlaying()
{
    return m_kodi.isPlaying();
}

Q_INVOKABLE void PhotoFrameController::setKodiSettings(QStringList v)
{
    m_kodi.setSettings(v);
    if(v.length() >= 7){
        setSettingString("kodi", "host", v[0]);
        setSettingInt("kodi", "port", v[1].toInt());
        setSettingInt("kodi", "httpport", v[2].toInt());
        setSettingString("kodi", "username", v[3]);
        setSettingString("kodi", "password", v[4]);
        setSettingBool("kodi", "tcp", v[5].toInt()==1?true:false);
        setSettingBool("kodi", "eventserver", v[6].toInt()==1?true:false);
    }
}

Q_INVOKABLE QStringList PhotoFrameController::getKodiSettings()
{
    return m_kodi.getSettings();
}

Q_INVOKABLE bool PhotoFrameController::getKodiEnabled()
{
    return m_kodi.getEnabled();
}

Q_INVOKABLE void PhotoFrameController::setKodiEnabled(bool v)
{
    m_kodi.setEnabled(v);
    setSettingBool("kodi", "enable", v);
    emit kodiEnableChanged();
}

QVariantMap PhotoFrameController::getKodiCurrentMetaData()
{
    return m_kodi.getCurrentMetaData();
}

void PhotoFrameController::kodiMetaDataChange()
{
    emit kodiCurrentMetaDataChanged();
}

double PhotoFrameController::getKodiVolume()
{
    return m_kodi.getVolume();
}

bool PhotoFrameController::getKodiMute()
{
    return m_kodi.isMute();
}

void PhotoFrameController::KodiVolume(bool m, double v)
{
    Q_UNUSED(m);
    Q_UNUSED(v);
    emit kodiVolumeChanged();
    emit kodiMuteChanged();
}

Q_INVOKABLE void PhotoFrameController::setKodiVolume(double v)
{
    m_kodi.setVolume(v);
}

double PhotoFrameController::getKodiPercentage()
{
    return m_kodi.getPercentage();
}

QVariantMap PhotoFrameController::getKodiTime()
{
    return m_kodi.getTime();
}

QVariantMap PhotoFrameController::getKodiTotalTime()
{
    return m_kodi.getTotalTime();
}

void PhotoFrameController::kodiPercentageChange()
{
    emit KodiCurrentPercentageChanged();
}

void PhotoFrameController::kodiTimeChange()
{
    emit KodiCurrentTimeChanged();
}

void PhotoFrameController::kodiTimeTotalChange()
{
    emit KodiCurrentTotalTimeChanged();
}

Q_INVOKABLE void PhotoFrameController::getKodiTvShows(int start, int max)
{
    m_kodi.getTvShows(start, max);
}

Q_INVOKABLE void PhotoFrameController::getKodiTvShow(int v)
{
    m_kodi.getTvShow(v);
}

Q_INVOKABLE void PhotoFrameController::getKodiMovies(int start, int max)
{
    m_kodi.getMovies(start, max);
}

Q_INVOKABLE void PhotoFrameController::getKodiSeason(int t, int s)
{
    m_kodi.getSeason(t,s);
}

Q_INVOKABLE void PhotoFrameController::getKodiVideo(int v)
{
    m_kodi.getVideo(v);
}

Q_INVOKABLE void PhotoFrameController::getKodiShowDetails(int v)
{
    m_kodi.getShowDetails(v);
}

Q_INVOKABLE void PhotoFrameController::setKodiPlayer(QString v)
{
    m_kodi.setPlayer(v);
}

Q_INVOKABLE void PhotoFrameController::getKodiPlayList()
{
    m_kodi.getPlayList();
}

Q_INVOKABLE void PhotoFrameController::addKodiToPlayList(QString f)
{
    m_kodi.addToPlayList(f);
}

Q_INVOKABLE void PhotoFrameController::showKodiGallery(QString v)
{
    m_kodi.showGallery(v);
}

Q_INVOKABLE void PhotoFrameController::showKodiPicture(QStringList v)
{
    m_kodi.showPicture(v);
}

QVariantList PhotoFrameController::getKodiList()
{
    return m_kodi.getList();
}

QList<int> PhotoFrameController::getKodiListIndex()
{
    return m_kodi.getListIndex();
}

QVariantList PhotoFrameController::getKodiPlayLists()
{
    return m_kodi.getplayLists();
}

QString PhotoFrameController::getKodiMenu()
{
    return m_kodi.getMenu();
}

void PhotoFrameController::kodiTvListChange()
{
    emit kodiListChanged();
}

void PhotoFrameController::kodiMoviesListChange()
{
    emit kodiMenuChanged();
}

QVariantList PhotoFrameController::getInternetRadioList()
{
    return m_musicplayer.getInternetRadioList();
}

QList<int> PhotoFrameController::getInternetRadioListIndex()
{
    return m_musicplayer.getInternetRadioListIndex();
}

QString PhotoFrameController::getInternetRadioPosition()
{
    return m_musicplayer.getInternetRadioPosition();
}

void PhotoFrameController::InternetRadioListChange()
{
    emit internetRadioListChanged();
}

void PhotoFrameController::InternetRadioListIndexChange()
{
    emit internetRadioListIndexChanged();
}

void PhotoFrameController::InternetRadioPosChange()
{
    emit internetRadioPositionChanged();
}

Q_INVOKABLE void PhotoFrameController::setMusicList(QString v)
{
    m_musicplayer.setMusicList(v);
}

QVariantList PhotoFrameController::getMusicList()
{
    return m_musicplayer.getMusicList();
}

QList<int> PhotoFrameController::getMusicListIndex()
{
    return m_musicplayer.getMusicListIndex();
}

bool PhotoFrameController::getMusicIsPlaying()
{
    return m_musicplayer.getIsPlaying();
}

qint64 PhotoFrameController::getMusicPosition()
{
    return m_musicplayer.position();
}

qint64 PhotoFrameController::getMusicDuration()
{
    return m_musicplayer.duration();
}

QVariantMap PhotoFrameController::getMusicMediaData()
{
    return m_musicplayer.getMediaData();
}

Q_INVOKABLE QImage PhotoFrameController::getMediaImage()
{
    return m_musicplayer.getMediaImage();
}

void PhotoFrameController::MusicPosChanged(qint64 p)
{
    emit musicPositionChanged(p);
}

void PhotoFrameController::MusicDurChanged(qint64 d)
{
    emit musicDurationChanged(d);
}

void PhotoFrameController::MusicDataChanged(QVariantMap m)
{
    emit musicMediaDataChanged(m);
}

void PhotoFrameController::musicImageChanged()
{
    emit mediaImageChanged();
}

void PhotoFrameController::MusicListChange()
{
    emit musicListChanged();
}

void PhotoFrameController::MusicPosChange()
{
    emit musicListIndexChanged();
}

void PhotoFrameController::MusicSongIsPlayingChange(bool v)
{
    emit isPlayingMusicChanged(v);
}

Q_INVOKABLE void PhotoFrameController::changeInternetRadioList(QString v, QString p)
{
    m_musicplayer.changeInternetRadioList(v, p);
}

Q_INVOKABLE void PhotoFrameController::addInternetRadioFav(QString name, QString icon, QString tag, QString params)
{
    m_musicplayer.addInternetRadioFav(name, icon, tag, params);
}

QVariantList PhotoFrameController::getSydneyTrainList()
{
    return m_trains.getTrainList();
}

void PhotoFrameController::sdyneyTrainListChange()
{
    emit sydneyTrainListChanged();
}

Q_INVOKABLE void PhotoFrameController::nextDefaultTrains()
{
    m_trains.NextDefaultTrains();
}

Q_INVOKABLE void PhotoFrameController::fibaroPopulate()
{
    m_fibaro.populate();
}

Q_INVOKABLE void PhotoFrameController::fibaroDeviceAction(int id, QString action)
{
    m_fibaro.deviceAction(id, action);
}

Q_INVOKABLE void PhotoFrameController::fibaroDeviceToggleIndex(int index)
{
    m_fibaro.deviceToggleIndex(index);
}

Q_INVOKABLE void PhotoFrameController::fibaroGetRoomDevices(int id)
{
    m_fibaro.getRoomDevices(id);
}

Q_INVOKABLE QString PhotoFrameController::fibaroGetIconURL(QString type, QString id, int index)
{
    return m_fibaro.getIconURL(type, id, index);
}

QVariantList PhotoFrameController::getFibaroRoomList()
{
    return m_fibaro.getRoomList();
}

QVariantList PhotoFrameController::getFibaroDeviceList()
{
    return m_fibaro.getDeviceList();
}

QVariantList PhotoFrameController::getFibaroSceneList()
{
    return m_fibaro.getSceneList();
}

QVariantList PhotoFrameController::getFibaroSectionList()
{
    return m_fibaro.getSectionList();
}

QVariantList PhotoFrameController::getFibaroCustomList()
{
    return m_fibaro.getCustomList();
}

void PhotoFrameController::fibaroRoomChange()
{
    emit roomFibaroListChanged();
}

void PhotoFrameController::fibaroSectionChange()
{
    emit sectionFibaroListChanged();
}

void PhotoFrameController::fibaroDeviceChange()
{
    emit deviceFibaroListChanged();
}

void PhotoFrameController::fibaroSceneChange()
{
    emit sceneFibaroListChanged();
}

void PhotoFrameController::fibaroCustomChange()
{
    emit customFibaroListChanged();
}

Q_INVOKABLE void PhotoFrameController::setFibaroSettings(QStringList v)
{
    m_fibaro.setSettings(v);
    if(v.length() >= 4){
        setSettingString("fibaro", "ip", v[0]);
        setSettingString("fibaro", "user", v[2]);
        setSettingString("fibaro", "password", v[3]);
        setSettingInt("fibaro", "port", v[1].toInt());
    }
}

Q_INVOKABLE QStringList PhotoFrameController::getFibaroSettings()
{
    return m_fibaro.getSettings();
}

Q_INVOKABLE bool PhotoFrameController::getFibaroEnabled()
{
    return m_fibaro.getEnabled();
}

Q_INVOKABLE void PhotoFrameController::setFibaroEnabled(bool v)
{
    m_fibaro.setEnabled(v);
    setSettingBool("fibaro", "enable", v);
    emit fibaroEnableChanged();
}

Q_INVOKABLE void PhotoFrameController::setRecipeSettings(QStringList v)
{
    m_recipe.setSettings(v);
    if(v.length() >= 4){
        setSettingString("recipe", "ip", v[0]);
        setSettingString("recipe", "user",  v[2]);
        setSettingString("recipe", "password", v[3]);
        setSettingInt("recipe", "port", v[1].toInt());
    }
}

Q_INVOKABLE QStringList PhotoFrameController::getRecipeSettings()
{
    return m_recipe.getSettings();
}

Q_INVOKABLE bool PhotoFrameController::getRecipeEnabled()
{
    return m_recipe.getEnabled();
}

Q_INVOKABLE void PhotoFrameController::setRecipeEnabled(bool v)
{
    m_recipe.setEnabled(v);
    setSettingBool("recipe", "enable", v);
    emit recipeEnableChanged();
}

Q_INVOKABLE void PhotoFrameController::recipeGetAllCategory()
{
   m_recipe.getAllCategory();
}

Q_INVOKABLE void PhotoFrameController::recipeGetCategory(QString id)
{
   m_recipe.getCategory(id);
}

Q_INVOKABLE void PhotoFrameController::recipeGetRecipe(int path)
{
   m_recipe.getRecipe(path);
}

QVariantList PhotoFrameController::getRecipeCategoryList()
{
    return m_recipe.getCategoryList();
}

QVariantList PhotoFrameController::getRecipeList()
{
    return m_recipe.getRecipeList();
}

QVariantList PhotoFrameController::getRecipeCurrent()
{
    return m_recipe.getCurrentRecipe();
}

void PhotoFrameController::recipeCategoryListChanged()
{
    emit categoryListChanged();
}

void PhotoFrameController::recipefoodListChanged()
{
    emit recipeListChanged();
}

void PhotoFrameController::recipeCurrentChanged()
{
    emit currentRecipeChanged();
}

QVariantList PhotoFrameController::getTimerList()
{
    return m_timers.getTimerList();
}

void PhotoFrameController::timersListChanged()
{
    emit timerListChanged();
}

Q_INVOKABLE void PhotoFrameController::createTimer(QString Title, unsigned int time)
{
    m_timers.createTimer(Title, time);
}

Q_INVOKABLE void PhotoFrameController::pauseTimer(int index)
{
    m_timers.pauseTimer(index);
}

Q_INVOKABLE void PhotoFrameController::resumeTimer(int index)
{
    m_timers.resumeTimer(index);
}

Q_INVOKABLE void PhotoFrameController::stopTimer(int index)
{
    m_timers.cancelTimer(index);
}

Q_INVOKABLE void PhotoFrameController::addMinuteToTimer(int index)
{
    m_timers.addMinuteToTimer(index);
}

QVariantMap PhotoFrameController::getCurrentAlarm()
{
    return m_alarms.getCurrentAlarm();
}

QVariantList PhotoFrameController::getAlarmList()
{
    return m_alarms.getAlarmList();
}

Q_INVOKABLE void PhotoFrameController::setAlarm(int index, QString title, int timeHr, int timeMin, QString date, QString day, QString type, QString sound, bool snooze)
{
    qDebug() << "setAlarm" << index;
    m_alarms.setAlarm(index, title, timeHr, timeMin, date, day, type, sound, snooze, true);

    QVariantList list = m_alarms.getAlarmList();
    setSettingInt("alarm", "count", list.length());
    for(int x=0; x<list.length(); x++){
        QVariantMap a = list[x].toMap();
        QTime t = a["Time"].toTime();
        setSettingBool("alarm", "enabled"+QString::number(x), a["enable"].toBool());
        setSettingString("alarm", "title"+QString::number(x), a["messsage"].toString());
        setSettingInt("alarm", "timeHr"+QString::number(x), t.hour());
        setSettingInt("alarm", "timeMin"+QString::number(x), t.minute());
        setSettingString("alarm", "date"+QString::number(x), a["date"].toString());
        QVariantList d = a["day"].toList();
        QString day = "";
        for(int x=0; x < d.length(); x++){
            if(x != 0) day += "|";
            day += d[x].toString();
        }
        setSettingString("alarm", "day"+QString::number(x), day);
        setSettingString("alarm", "type"+QString::number(x), a["type"].toString());
        QString s = a["sound"].toString();
        if(s == ""){
            s = a["video"].toString();
        }
        setSettingString("alarm", "sound"+QString::number(x), s);
        setSettingString("alarm", "sound"+QString::number(x), a["video"].toString());
        setSettingBool("alarm", "snooze"+QString::number(x), a["snooze"].toBool());
    }
}

Q_INVOKABLE void PhotoFrameController::deleteAlarm(int index)
{
    m_alarms.deleteAlarm(index);

    QVariantList list = m_alarms.getAlarmList();
    setSettingInt("alarm", "count", list.length());
    for(int x=0; x<list.length(); x++){
        QVariantMap a = list[x].toMap();
        QTime t = a["Time"].toTime();
        setSettingBool("alarm", "enabled"+QString::number(x), a["enabled"].toBool());
        setSettingString("alarm", "title"+QString::number(x), a["messsage"].toString());
        setSettingInt("alarm", "timeHr"+QString::number(x), t.hour());
        setSettingInt("alarm", "timeMin"+QString::number(x), t.minute());
        setSettingString("alarm", "date"+QString::number(x), a["date"].toString());
        QVariantList d = a["day"].toList();
        QString day = "";
        for(int x=0; x < d.length(); x++){
            if(x != 0) day += "|";
            day += d[x].toString();
        }
        setSettingString("alarm", "day"+QString::number(x), day);
        setSettingString("alarm", "type"+QString::number(x), a["type"].toString());
        QString s = a["sound"].toString();
        if(s == ""){
            s = a["video"].toString();
        }
        setSettingString("alarm", "sound"+QString::number(x), s);
        setSettingString("alarm", "sound"+QString::number(x), a["video"].toString());
        setSettingBool("alarm", "snooze"+QString::number(x), a["snooze"].toBool());
    }
}

Q_INVOKABLE void PhotoFrameController::setAlarmEnable(int index, bool results)
{
    qDebug() << "setAlarmEnable" << index << results;
    m_alarms.setAlarmEnable(index, results);

    setSettingBool("alarm", "enabled"+QString::number(index), results);
}

Q_INVOKABLE void PhotoFrameController::dismissAlarm()
{
    m_alarms.dismissAlarm();
    m_musicplayer.dismissSounds();
}

void PhotoFrameController::alarmCurrentChanged()
{
    emit currentAlarmChanged();
}

void PhotoFrameController::alarmListsChanged()
{
    emit alarmListChanged();
}

void PhotoFrameController::assistantChange(bool enabled, QString command)
{
    if(m_assistandEnabled && !enabled && !t_assistant.isActive()){
        t_assistant.setSingleShot(true);
        t_assistant.start(5000);
        //pause music
        m_musicplayer.ProcessAssistant("pause");
        return;
    }else if(t_assistant.isActive() && !enabled){
        return;
    }

    m_assistandEnabled = enabled;
    m_assistantCommand = command;

    emit assistantStatusChanged();
    emit assistantCommandChanged();

    if(m_assistandEnabled){
        t_assistant.stop();
    }
}

void PhotoFrameController::assistantVoice(QString response)
{
    emit processAssistant(response);
}

void PhotoFrameController::assistantTimer()
{
    m_assistandEnabled = false;
    m_assistantCommand = "";
    emit assistantStatusChanged();
    emit assistantCommandChanged();
    //restart music
    m_musicplayer.ProcessAssistant("resume");
}

bool PhotoFrameController::getAssistantStatus()
{
    return m_assistandEnabled;
}

QString PhotoFrameController::getAssistantCommand()
{
    return m_assistantCommand;
}

void PhotoFrameController::alertIconChange(QString id, bool value)
{
    emit alertIcons(id, value);
}

void PhotoFrameController::pageTrigger(QString page, int index)
{
    if(page == "alive"){
        worker->backHome();
        //emit switchToPage(page, index);
        return;
    }else if(page == "away"){
        worker->awayMode();
        //emit switchToPage(page, index);
        return;
    }

    emit switchToPage(page, index);
}

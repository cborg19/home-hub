import QtQuick 2.0
import QtQuick.Controls 2.4

Item {

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("YouTubePage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

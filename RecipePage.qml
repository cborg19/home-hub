import QtQuick 2.0
import QtQuick.Controls 2.4

Page {
    width: imageWidth
    height: imageHeight

    background: Image {
        anchors.fill: parent
        source: "files/icon/FoodBackground.jpg"
        fillMode: Image.PreserveAspectCrop
    }

    function setPageIndex(index){
        if(index === 1){
            controller.recipeGetAllCategory();
            recipeList.visible = true;
            btMenu.visible = false;
            recipeList.showCategoryList();
        }else if(index === 10){
            recipeList.visible = true;
            btMenu.visible = false;
            recipeList.showRecipeList();
        }else if(index === 11){
            recipeList.visible = false
            recipeShow.load();
        }else if(index === 12){
            recipeShow.pageDown()
        }else if(index === 13){
            recipeShow.pageUp()
        }
    }

    Button {
        id: btCatergory
        x: 170
        y: 150
        width: 150
        height: 150
        padding: 10
        spacing: 10
        focusPolicy: Qt.NoFocus
        display: AbstractButton.IconOnly
        background: Rectangle {
            color: "transparent"
            radius: 10
        }

        Rectangle {
            anchors.fill: parent
            radius: 5
            Image {
                width: parent.width - 20
                height: parent.height - 20
                id: btCatergoryImage
                anchors.centerIn: parent
                source: "files/icon/book.png"
                fillMode: Image.PreserveAspectFit
            }
        }
    }

    Connections {
        target: btCatergory
        onClicked: {
            controller.recipeGetAllCategory();
            recipeList.visible = true;
            btMenu.visible = false;
        }
    }


    Button {
        id: btCart
        x: 450
        y: 150
        width: 150
        height: 150
        padding: 20
        spacing: 20
        focusPolicy: Qt.NoFocus
        display: AbstractButton.IconOnly
        Rectangle {
            anchors.fill: parent
            radius: 5
            Image {
                width: parent.width - 20
                height: parent.height - 20
                id: btCartImage
                anchors.centerIn: parent
                source: "files/icon/cart.png"
                fillMode: Image.PreserveAspectFit
            }
        }
    }

    Connections {
        target: btCart
        onClicked: {

        }
    }

    RecipeList {
        id: recipeList
        visible: false
    }

//    RecipeShow {
//        id: recipeShow
//        visible: false
//    }

    Item {
        id: recipeShow
        width: imageWidth
        height: imageHeight

        function pageDown(){
            recipeDetail.pageDown()
        }

        //property variant modelData: controller.weatherDataModel[0]
        function load(){
            var component = Qt.createComponent("RecipeShow.qml");
            if( component.status !== Component.Ready )
            {
                if( component.status === Component.Error )
                    console.debug("Error:"+ component.errorString() );
                return; // or maybe throw
            }
            component.createObject(recipeShow);
        }
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 50
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("FibaroPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

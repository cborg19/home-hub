#include "volumecontroller.h"
#include <QtCore/qdebug.h>
#include <QProcess>

#define INPUT_UP 24
#define INPUT_DOWN 25

VolumeController::VolumeController(QObject *parent) : QObject(parent)
{
    lastVolume = 0;
    b_Up = true;
    b_Down = true;

//    connect(&timer, SIGNAL(timeout()), this, SLOT(storeSoundState()));
//    timer.setSingleShot(true);

//    connect(this, &VolumeController::storeVolume, this, &VolumeController::storeTimer);
}

void VolumeController::setGPIO(Gpio *gpio)
{
    m_gpio = gpio;
    #ifdef Q_OS_LINUX
    Initialise();
    //getInitialVolume();
    setSystemVolume(80);
    #endif
}

void VolumeController::getInitialVolume()
{
    #ifdef Q_OS_LINUX
    QString cmd = "bash -c \"sudo amixer -c 0 sget Digital | grep 'Left:' | awk -F'[][]' '{ print $2 }'";
    QProcess process;

    QString volume;
    process.start(cmd);
    process.waitForFinished();

    volume = process.readAllStandardOutput();
    volume = volume.replace('%',' ').trimmed();

    qDebug() << "Initial Volume" << volume;
    lastVolume = volume.toInt();
    #endif
}

void VolumeController::Initialise()
{
    #ifdef Q_OS_LINUX
    m_gpio->setInput(INPUT_UP);
    m_gpio->setInput(INPUT_DOWN);
    qDebug() << "VOLUME INITIALISE GPIO";
    #endif
}

void VolumeController::setVolume(int v)
{
    QMutexLocker M(&m_lock);
    lastVolume = v;
    //qDebug() << "SET VOLUME" << v;
}

void VolumeController::setSystemVolume(int v)
{
    #ifdef Q_OS_LINUX
    QString cmd = "bash -c \"sudo amixer -c 0 set Digital "+QString::number(v)+"%";
    QProcess process;

    process.start(cmd);
    process.waitForFinished();
    #endif
}

void VolumeController::muteToggle()
{
    //sudo amixer -c 0 set Digital toggle
}

int VolumeController::getVolume()
{
    int v;
    m_lock.lock();
    v = lastVolume;
    m_lock.unlock();
    return v;
}

void VolumeController::detectVolButtons()
{
    #ifdef Q_OS_LINUX
    bool r_Up = m_gpio->read(INPUT_UP);
    bool r_Down = m_gpio->read(INPUT_DOWN);
//qDebug() << "BUTTONS" << r_Up << r_Down;
    if(b_Up != r_Up){
        if(r_Up && !b_Up){
            QMutexLocker M(&m_lock);
            lastVolume += 10;
            if(lastVolume > 100) lastVolume = 100;
            //setSystemVolume(lastVolume);
            emit volumeChange(lastVolume);
            //emit storeVolume();
        }
        b_Up = r_Up;
    }

    if(b_Down != r_Down){
        if(r_Down && !b_Down){
            QMutexLocker M(&m_lock);
            lastVolume -= 10;
            if(lastVolume < 0) lastVolume = 0;
            //setSystemVolume(lastVolume);
            emit volumeChange(lastVolume);
            //emit storeVolume();
        }
        b_Down = r_Down;
    }
    #endif
}
/*
void VolumeController::storeTimer()
{
    timer.stop();
    timer.start(5000);
}

void VolumeController::storeSoundState()
{
    #ifdef Q_OS_LINUX
    QString cmd = "bash -c \"sudo alsactl store";
    QProcess process;

    process.start(cmd);
    process.waitForFinished();
    #endif
}*/

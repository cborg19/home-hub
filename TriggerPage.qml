import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.0
import QtMultimedia 5.8

import "js/skycons.js" as WeatherIcon
import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    id: timeView
    width: imageWidth
    height: imageHeight

    background: Rectangle{
        color: controller.currentAlarm.video === ""?"#393a7a":"black"
    }

    function refreshCanvas(){
        panCanvas.requestPaint();
        panCanvasTop.requestPaint();
    }

    function setWeather(currentDate){
        var hr = parseInt(Qt.formatDateTime(currentDate, "HH"));
        if(hr < 12){
            topTemp.visible = true
        }else{
            topTemp.visible = false
        }
    }

    Connections {
        target: triggerView
        onVisibleChanged:
            if(visible){
                clockTimer.start();
                timeView.displayTime();
            //    bounceAnimation.start();
            }else{
                clockTimer.stop();
                bounceAnimation.stop();
            }

        ignoreUnknownSignals: true
    }

    property string currentTime: ""
    property string currentHalf: ""
    property string currentDate: ""

    function displayTime(){
        var currentDate = new Date();
        if(time24hr){
            timeView.currentTime = Qt.formatDateTime(currentDate, "HH:mm")
            timeView.currentHalf = ""
        }else{
            var time = Qt.formatDateTime(currentDate, "h:mm|A").split("|")
            timeView.currentTime = time[0]
            timeView.currentHalf = time[1]
        }

        timeView.currentDate = Qt.formatDateTime(currentDate, "dddd, d MMMM yyyy")
        timeView.setWeather(currentDate)
    }

    Timer {
        id: clockTimer
        interval: 1000
        running: false
        repeat: true
        onTriggered:{
            timeView.displayTime();
        }
    }

    Column {
        visible: controller.currentAlarm.video === ""?true:false
        anchors.centerIn: parent
        spacing: 20

        Label {
            width: parent.width
            text: controller.currentAlarm.messsage
            color: "white"
            font.pointSize: 30
            font.family: "Helvetica"
            font.bold: true
            //anchors.horizontalCenter: parent
            horizontalAlignment: Text.AlignHCenter
        }


        Item{
            width: parent.width
            height: 35

            Row {
                id: topRow
                spacing: 0
                anchors.centerIn: parent

                Label {
                    text: qsTr(timeView.currentTime)
                    font.pointSize: 24
                    font.family: "Helvetica"
                    font.bold: true
                    color: "white"
                }
                Label {
                    text: qsTr(timeView.currentHalf)
                    font.pointSize: 24
                    font.family: "Helvetica"
                    font.bold: true
                    color: "white"
                }
            }
        }

        Label {
            width: parent.width
            id: dateRow
            text: qsTr(timeView.currentDate)
            font.pointSize: 20
            color: "white"
            horizontalAlignment: Text.AlignHCenter
        }

        Row {
            spacing: 10
            Rectangle {
                height: 200
                width: 50
                color: "transparent"
                Text {
                    z: 3
                    anchors.centerIn: parent
                    color: "white"
                    font.pointSize: 35
                    font.family: fontAwesome.name
                    text: FontAwesome.icons.fa_eye_slash
                    visible: controller.currentAlarm.snooze

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {

                        }
                    }
                }
            }

            Rectangle {
                width: 200
                height: 200
                radius: 100
                color: "#595a90"

                Image {
                    anchors.centerIn: parent
                    id: btAlarmImage
                    width: 100
                    height: 100
                    source: "files/icon/trigger.png"
                    fillMode: Image.PreserveAspectFit

                    transform: Scale {
                        id: scaleTransform
                        property real scale: 1
                        xScale: scale
                        yScale: scale
                    }

                    SequentialAnimation {
                        id: bounceAnimation
                        loops: Animation.Infinite
                        PropertyAnimation {
                            target: scaleTransform
                            properties: "scale"
                            from: 1.0
                            to: 1.02
                            duration: 2000
                        }
                        PropertyAnimation {
                            target: scaleTransform
                            properties: "scale"
                            from: 1.02
                            to: 1.0
                            duration: 2000
                        }
                    }
                }
            }

            Rectangle {
                height: 200
                width: 50
                color: "transparent"
                Text {
                    z: 3
                    anchors.centerIn: parent
                    color: "white"
                    font.pointSize: 35
                    font.family: fontAwesome.name
                    text: FontAwesome.icons.fa_clock_o
                    visible: controller.currentAlarm.clock

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {

                        }
                    }
                }
            }
        }
    }

    Video {
        visible: controller.currentAlarm.video !== ""?true:false
        id: videofile
        anchors.fill: parent
        autoLoad: true
        autoPlay: true
        source: galleryPath+controller.currentAlarm.video
        volume: window.volume

        Label {
            x: 20
            y: 20
            width: parent.width
            text: controller.currentAlarm.messsage
            color: "white"
            font.pointSize: 30
            font.family: "Helvetica"
            font.bold: true
        }

        Row {
            x: 600
            y: 20
            spacing: 0

            Label {
                text: qsTr(timeView.currentTime)
                font.pointSize: 24
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                text: qsTr(timeView.currentHalf)
                font.pointSize: 24
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }

    }

    Page {
        id: info
        x: 20
        y: 400
        property int canvasWidth: 50
        property int canvasHeight: 50

        background: Rectangle {
            color: "transparent"
        }

        Item {
            id: secondRow
            property date lastUpdateWeather: new Date("2010-01-01")

            function getTemp(){
                //if(controller.weatherDataSize === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].current.temperature === undefined) return "";
                return SimpleJsTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].current.temperature);
            }
            function getTempName(){
                //console.log("dd",controller.weatherDataSize)
                //if(controller.weatherDataSize === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].current.temperature === undefined) return "";
                return "°C";
            }

            Canvas {
                id: panCanvas
                width: info.canvasWidth
                height: info.canvasHeight
                x:0
                y:10
                Component.onCompleted: {

                }
                onPaint: {
                    if(controller.weatherDataModel[controller.weatherPrimary] !== undefined){
                        var ctx = getContext("2d");
                        ctx.save();
                        ctx.clearRect(0, 0, info.canvasWidth, info.canvasHeight);

                        WeatherIcon.skycons(ctx,controller.weatherDataModel[controller.weatherPrimary].current.icon, 0);
                        ctx.restore();
                        secondRow.lastUpdateWeather = new Date();
                    }
                }
            }
            Label {
                anchors.left: panCanvas.left
                anchors.bottom: panCanvas.top
                text:  qsTr("Current")
                font.pointSize: 10
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: panCanvas.right
                anchors.top: panCanvas.top
                id: tempText
                text:  qsTr(secondRow.getTemp())
                font.pointSize: 40
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: tempText.right
                anchors.top: tempText.top
                text:  qsTr(secondRow.getTempName())
                font.pointSize: 20
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }
    }

    Page {
        id: details

        background: Rectangle {
            color: "transparent"
        }

        Item {
            id: topTemp
            visible: false
            x: 650
            y: 400

            function getTemp(){
                if(controller.weatherDataModel[controller.weatherPrimary].daily.length === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].daily[0].temperatureHigh === undefined) return "";
                return SimpleJsTools.roundDown(controller.weatherDataModel[controller.weatherPrimary].daily[0].temperatureHigh);
            }
            function getTempName(){
                if(controller.weatherDataModel[controller.weatherPrimary].daily.length === 0) return "";
                if(controller.weatherDataModel[controller.weatherPrimary].daily[0].temperatureHigh === undefined) return "";
                return "°C";
            }

            Canvas {
                id: panCanvasTop
                width: info.canvasWidth
                height: info.canvasHeight
                x:0
                y:10
                Component.onCompleted: {

                }
                onPaint: {
                    if(controller.weatherDataModel[controller.weatherPrimary].daily.length > 0){
                        var ctx = getContext("2d");
                        ctx.save();
                        ctx.clearRect(0, 0, info.canvasWidth, info.canvasHeight);

                        WeatherIcon.skycons(ctx,controller.weatherDataModel[controller.weatherPrimary].daily[0].icon, 0);
                        ctx.restore();
                        secondRow.lastUpdateWeather = new Date();
                    }
                }
            }
            Label {
                anchors.left: panCanvasTop.left
                anchors.bottom: panCanvasTop.top
                text:  qsTr("Top")
                font.pointSize: 10
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: panCanvasTop.right
                anchors.top: panCanvasTop.top
                id: tempTop
                text:  qsTr(topTemp.getTemp())
                font.pointSize: 40
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
            Label {
                anchors.left: tempTop.right
                anchors.top: tempTop.top
                text:  qsTr(topTemp.getTempName())
                font.pointSize: 20
                font.family: "Helvetica"
                font.bold: true
                color: "white"
            }
        }
    }



    Button {
        id: btMenu
        x:0
        y:0
        width: imageWidth
        height: imageHeight
        z: 1
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("TriggerPage switchToPhoto");
            bounceAnimation.stop();
            videofile.stop();
            controller.dismissAlarm();
            window.switchToPhoto(true)
        }
    }
}

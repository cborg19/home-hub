import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import QtMultimedia 5.11
import Qt.labs.folderlistmodel 2.11

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle{
        color:"#D0D0D0"
    }

    Timer {
        id: timer
        function setTimeout(cb, delayTime) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.triggered.connect(function release () {
                timer.triggered.disconnect(cb); // This is important
                timer.triggered.disconnect(release); // This is important as well
            });
            timer.start();
        }
    }

    Item {
        id: header
        x: 0
        y: 0
        z: 1
        width: 740
        height: 35
        Rectangle{
            anchors.fill: parent
            color:"#b7b7b7"
        }

        Button {
            id: btExitSetting
            x: 5
            y: 4
            width: 20
            height: 20
            focusPolicy: Qt.NoFocus
            display: AbstractButton.IconOnly
            background: Rectangle {
                color: "transparent"
            }

            Image {
                id: btSleepImage
                width: 20
                height: 20
                source: "files/icon/cancel.png"
                fillMode: Image.PreserveAspectFit
            }
        }

        Connections {
            target: btExitSetting
            onClicked: {
                console.log("MusicList switchToMenu");
                window.switchToMenu(true)
            }
        }

        Label {
            id: headerText
            x: 40
            y: 2
            color: "white"
            font.pointSize: 18
            font.family: "Helvetica"
            text: filesFrame.headerTitle
        }

        RowLayout{
            spacing: 10
            Layout.alignment: Qt.AlignRight
            x: 600

            Text {
                id: btAdd
                visible: true
                color: !filesFrame.selectListMode?"white":"red"
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_check_square
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(filesFrame.selectListIndex === 1){
                            if(filesFrame.selectListMode){
                                filesFrame.selectListMode = false;
                            }else{
                                filesFrame.selectListMode = true;
                            }
                        }else if(filesFrame.selectListIndex < 4 || filesFrame.selectListIndex === 6){
                            if(filesFrame.selectListMode === false){
                                filesFrame.selectListMode = true;
                                btNew.visible = true;
                            }else{
                                filesFrame.selectListMode = false
                                btNew.visible = false;
                            }
                        }
                    }
                }
            }

            Text {
                id: btNew
                visible: false
                color: "white"
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_plus_square
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(filesFrame.selectListIndex < 3 || filesFrame.selectListIndex === 6){
                            if(filesFrame.selectListModeSource !== null)
                                playListAdd.visible = true
                        }else if(filesFrame.selectListIndex === 3){
                            //open text edit
                            playListNew.visible = true
                        }
                    }
                }
            }

            Text {
                id: btRemove
                visible: false
                color: "white"
                font.pointSize: 25
                font.family: fontAwesome.name
                text: FontAwesome.icons.fa_minus_square
                MouseArea {
                    anchors.fill: parent
                    onClicked: {

                    }
                }
            }

        }
    }

    Item {
        id: filesFrame
        x: 10
        y: 30
        height: imageHeight - 30
        width: 730

        //Layout.fillWidth: true
        //Layout.fillHeight: true

        property int selectListIndex: 0
        property bool selectListMode: false
        property var selectListModeSource: null
        property string selectListDataPlaylist: ""
        property string selectListData: ""
        property int selectListDataIndex: -1
        property string headerTitle: "All Files"

        function isSelected(s){
            //console.log("isSelected",filesFrame.selectListModeArray)
            if(filesFrame.selectListMode !== false){
                if(filesFrame.selectListModeArray.indexOf(s) !== -1)
                    return "#ff6666";
            }
            return "#D0D0D0";
        }

        function addSelected(s){
            if(filesFrame.selectListMode !== false){
                if(filesFrame.selectListModeArray.indexOf(s) === -1){
                    filesFrame.selectListModeArray.push(s)
                //    console.log("add",filesFrame.selectListModeArray)
                }
            }
        }

        ListModel {
            id: artistsList
        }

        ListModel {
            id: albumList
        }

        Playlist {
            id: allfiles
            Component.onCompleted: {
                allfiles.load(galleryPath+"/music/allfiles.m3u")
            }

            onLoaded: {
                //for(var x=0; x<allfiles.itemCount; x++){

                //}
            }
        }

        /*Component {
            id: delegateAllfiles
            ItemDelegate {
                width: parent.width
                height: 45

                Rectangle {
                    anchors.fill: parent
                    color: mainList.currentIndex === index && filesFrame.selectListMode? "#ff6666" : "#D0D0D0"
                }

                RowLayout {
                    Item{
                        width: 40
                        height: 40
                        Image {
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                            source: "/files/icon/menu.jpg"
                        }
                    }

                    Label {
                        text: source?SimpleJsTools.filename(source):"" //Maybe becuase switching delegate and model not at the same time?
                        color: "white"
                        font.pointSize: 18
                        font.family: "Helvetica"
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        mainList.currentIndex = index
                        if(filesFrame.selectListMode === false){
                            console.debug(index,'Debug: User pressed '+source)
                            //musicplaylist.load(galleryPath+"/music/allfiles.m3u")
                            //musicplaylist.currentIndex = index
                            musicplayer.stop()
                            musicplayer.source = source;
                            musicplayer.play();
                            musicSwipeView.currentIndex = 0
                        }else{
                            //playlist
                            filesFrame.selectListModeSource = source
                        }
                    }
                }
            }
        }*/

        Component {
            id: delegateAllfiles
            ItemDelegate {
                width: parent.width
                height: 45

                Rectangle {
                    anchors.fill: parent
                    color: mainList.currentIndex === index && filesFrame.selectListMode? "#ff6666" : "#D0D0D0"
                }

                RowLayout {
                    Item{
                        width: 40
                        height: 40
                        Image {
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                            source: controller.MusicList[index].cover!==""?galleryPath+"music/cache/"+controller.MusicList[index].cover:""
                        }
                    }

                    Label {
                        text: controller.MusicList[index].title
                        color: "white"
                        font.pointSize: 18
                        font.family: "Helvetica"
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        mainList.currentIndex = index
                        if(filesFrame.selectListMode === false){
                            console.debug(index,'Debug: User pressed '+controller.MusicList[index].source)
                            //musicplaylist.load(galleryPath+"/music/allfiles.m3u")
                            //musicplaylist.currentIndex = index
                            controller.MusicController.stop();
                            controller.MusicController.playSource(controller.MusicList[index].source);
                            //musicplayer.source = "file://" + controller.MusicList[index].source;
                            controller.MusicController.play();
                            musicSwipeView.currentIndex = 0
                        }else{
                            //playlist
                            filesFrame.selectListModeSource = controller.MusicList[index].source
                        }
                    }
                }
            }
        }

        Component {
            id: delegateAlbums

            ItemDelegate {
                width: parent.width
                height: 45

                Rectangle {
                    anchors.fill: parent
                    color: mainList.currentIndex === index && filesFrame.selectListMode? "#ff6666" : "#D0D0D0"
                }

                RowLayout {
                    Item{
                        width: 40
                        height: 40
                        Image {
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                         //   source: "image://imageprovider/mp3/"+controller.MusicList[index].source
                        }
                    }

                    Label {
                        text: controller.MusicList[index].album + " ("+controller.MusicList[index].size+")"
                        color: "white"
                        font.pointSize: 18
                        font.family: "Helvetica"
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        btAdd.visible = true
                        btNew.visible = false
                        btRemove.visible = false
                        filesFrame.headerTitle = "Album: "+controller.MusicList[index].album
                        filesFrame.selectListDataIndex = index
                        filesFrame.selectListIndex = 6
                    }
                }
            }
        }

        Component {
            id: delegateArtist
            ItemDelegate {
                width: parent.width
                height: 45

                Rectangle {
                    anchors.fill: parent
                    color: mainList.currentIndex === index && filesFrame.selectListMode? "#ff6666" : "#D0D0D0"
                }

                RowLayout {
                    Item{
                        width: 40
                        height: 40
                        Image {
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                         //   source: "image://imageprovider/mp3/"+controller.MusicList[index].source
                        }
                    }

                    Label {
                        text: controller.MusicList[index].artist + " ("+controller.MusicList[index].size+")"
                        color: "white"
                        font.pointSize: 18
                        font.family: "Helvetica"
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        btAdd.visible = true
                        btNew.visible = false
                        btRemove.visible = false
                        filesFrame.headerTitle = "Artist: "+controller.MusicList[index].artist
                        filesFrame.selectListDataIndex = index
                        filesFrame.selectListIndex = 6
                    }
                }
            }
        }

        Component {
            id: delegateList
            ItemDelegate {
                width: parent.width
                height: 45

                Rectangle {
                    anchors.fill: parent
                    color: mainList.currentIndex === index && filesFrame.selectListMode? "#ff6666" : "#D0D0D0"
                }

                RowLayout {
                    Item{
                        width: 40
                        height: 40
                        Image {
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                            source: controller.MusicList[filesFrame.selectListDataIndex].list[index].cover!==""?galleryPath+"music/cache/"+controller.MusicList[filesFrame.selectListDataIndex].list[index].cover:""
                        }
                    }

                    Label {
                        text: controller.MusicList[filesFrame.selectListDataIndex].list[index].title
                        color: "white"
                        font.pointSize: 18
                        font.family: "Helvetica"
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("AAAAAAA");
                        mainList.currentIndex = index
                        if(filesFrame.selectListMode === false){
                            controller.MusicController.playlist().clear();
                            for(var x=0; x<controller.MusicList[filesFrame.selectListDataIndex].size; x++){
                                controller.MusicController.playlist().addMedia(controller.MusicList[filesFrame.selectListDataIndex].list[x].source)
                            }

                            controller.MusicController.stop()
                            //musicplayer.playlist = musicplaylist;
                            //musicplayer.source = controller.MusicList[filesFrame.selectListDataIndex].list[index].source;
                            controller.MusicController.playSource(controller.MusicList[filesFrame.selectListDataIndex].list[index].source);
                            controller.MusicController.play();
                            musicSwipeView.currentIndex = 0
                        }else{
                            //playlist
                            filesFrame.selectListModeSource = controller.MusicList[filesFrame.selectListDataIndex].list[index].source;
                        }
                    }
                }
            }
        }


        FolderListModel {
            id: playlist
            folder: galleryPath+"/playlist/"
            nameFilters: ["*.m3u"] //["*.mp3", "*.oga", "*.ogg", "*.flac", " *.opus", "*.m4a", "*.aac"]
        }

        Component {
            id: delegatePlaylist
            Item {
                width: parent.width
                height: 45
                RowLayout {
                    Item{
                        width: 40
                        height: 40
                        Image {
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                            source: "/files/icon/playlist.png"
                        }
                    }
                    Label {
                        text: SimpleJsTools.basename(fileName)
                        color: "white"
                        font.pointSize: 18
                        font.family: "Helvetica"
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("Test");
                        if(filesFrame.selectListMode === false){
                            btAdd.visible = false
                            btNew.visible = false
                            btRemove.visible = true
                            console.log("BBBBBBBB playlist",galleryPath+"playlist/"+fileName);
                            musicfiles.load(galleryPath+"playlist/"+fileName)
                            filesFrame.selectListDataPlaylist = "playlist/"+fileName
                            filesFrame.headerTitle = "Playlist - "+SimpleJsTools.basename(fileName)
                            filesFrame.selectListIndex = 4
                        }
                    }
                }
            }
        }

        Playlist {
            id: musicfiles
        }

        Component {
            id: delegateMusicfiles

            Item {
                width: parent.width
                height: 45
                property string fileName: source
                RowLayout {
                    Item{
                        width: 40
                        height: 40
                        Image {
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                            source: "image://imageprovider/mp3/"+fileName
                        }
                    }
                    Label {
                        text: SimpleJsTools.filename(source)
                        color: "white"
                        font.pointSize: 18
                        font.family: "Helvetica"
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(filesFrame.selectListMode === false){
                            console.debug("1111",index,'Debug: again '+source)
                            console.debug("1111",galleryDir,filesFrame.selectListDataPlaylist)

                            controller.MusicController.playlistSource(galleryDir+filesFrame.selectListDataPlaylist)
                            controller.MusicController.playlistIndex(index)
                            controller.MusicController.play()

                            musicSwipeView.currentIndex = 0
                        }
                    }
                }
            }
        }

        Component {
            id: delegateInternetRadio
            Item {
                id: internetRadio
                property string name: controller.internetRadio[index].name

                width: parent.width
                height: 45
                RowLayout {
                    spacing: 10
                    Item{
                        width: 40
                        height: 40
                        Image {
                            width: 40
                            height: 40
                            fillMode: Image.PreserveAspectCrop
                            source: controller.internetRadio[index].icon
                        }
                    }

                    Label {
                        text: controller.internetRadio[index].name
                        color: "white"
                        font.pointSize: 18
                        font.family: "Helvetica"
                    }

                    Text {
                        z: 3
                        id: btAddtoFav
                        color: "white"
                        visible: controller.internetRadio[index].link === "station"&&filesFrame.selectListMode?true:false
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_plus_square

                    }

                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(controller.internetRadio[index].link !== "station"){
                            controller.changeInternetRadioList(controller.internetRadio[index].link, controller.internetRadio[index].params)
                        }else{
                            if(filesFrame.selectListMode){
                                controller.addInternetRadioFav(
                                    controller.internetRadio[index].name,
                                    controller.internetRadio[index].icon,
                                    controller.internetRadio[index].tags,
                                    controller.internetRadio[index].params
                                )
                                filesFrame.selectListMode = false
                            }else{
                                //we got a station send it to main player
                                controller.MusicController.stop()
                                controller.MusicController.playSource(controller.internetRadio[index].params);
                                controller.MusicController.play();
    /*TODO                             musicplayer.lastMetaData = {
                                    source: controller.internetRadio[index].params,
                                    title: controller.internetRadio[index].name,
                                    icon: controller.internetRadio[index].icon,
                                    artist: controller.internetRadio[index].tags,
                                }*/
                                musicSwipeView.currentIndex = 0
                            }
                        }
                    }
                }
            }
        }

        ListView {
            id: mainList
            x: 0
            y: 0
            height: imageHeight - 30
            width: 730

            property variant jumpIndex: []

            function getModel(index){
                if(index === 1) return controller.internetRadio
                if(index === 2) return controller.MusicList
                if(index === 3) return playlist
                if(index === 4) return musicfiles
                if(index === 5) return controller.MusicList
                if(index === 6) return controller.MusicList[filesFrame.selectListDataIndex].list
                return controller.MusicList //allfiles
            }
            function getDelegate(index){
                if(index === 1) return delegateInternetRadio
                if(index === 2) return delegateAlbums
                if(index === 3) return delegatePlaylist
                if(index === 4) return delegateMusicfiles
                if(index === 5) return delegateArtist
                if(index === 6) return delegateList
                return delegateAllfiles
            }

            model: mainList.getModel(filesFrame.selectListIndex)
            delegate: mainList.getDelegate(filesFrame.selectListIndex)

            ScrollBar.vertical: ScrollBar {
                parent: filesFrame
                policy: ScrollBar.AlwaysOn
                anchors.top: parent.top
                anchors.topMargin: filesFrame.topPadding
                anchors.right: parent.right
                anchors.rightMargin: 1
                anchors.bottom: parent.bottom
                anchors.bottomMargin: filesFrame.bottomPadding
            }

            onModelChanged: {
                mainList.jumpIndex = [];
                if(filesFrame.selectListIndex === 0 || filesFrame.selectListIndex === 2 || filesFrame.selectListIndex === 5){
                    mainList.jumpIndex = controller.MusicListIndex
                }else if(filesFrame.selectListIndex === 1){
                    mainList.jumpIndex = controller.internetRadioIndex
                }
            }
        }
    }

    Item {
        id: sideScroll
        x: 700
        y: 0
        width: 40
        height: imageHeight
        z: 2

        property bool scrollVisible: false
        property int scrollSelected: -1

        Rectangle {
            width: 40
            height: 480
            color: sideScroll.scrollVisible?"#E0E0E0":"transparent"
        }

        Item {
            y: 5
            Repeater {
                model: aplhabet.length
                delegate: Item{
                    Rectangle {
                        y: (imageHeight-10)/26*index
                        width:  35
                        height: (imageHeight-10)/26
                        z: sideScroll.scrollSelected === index?3:2
                        color: "transparent"

                        Text {
                            anchors.centerIn: parent
                            color: sideScroll.scrollSelected === index?"white":"#989898"
                            font.pointSize: sideScroll.scrollSelected === index?36:18
                            font.family: "Helvetica"
                            text: aplhabet[index]
                            visible: sideScroll.scrollVisible
                        }
                    }
                }
            }
        }

        Timer {
            id: sideScrollTimer
            interval: 2000;
            running: false;
            repeat: false
            onTriggered: {
                sideScroll.scrollVisible = false
                sideScroll.scrollSelected = -1
            }
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                sideScroll.scrollVisible = true
                sideScrollTimer.restart();
            }
            onExited: {
                sideScroll.scrollVisible = false
                sideScroll.scrollSelected = -1
                sideScrollTimer.stop();
            }

            onPositionChanged:{
                var grid = (imageHeight-10)/26
                sideScroll.scrollSelected = Math.floor(mouseY/grid)
                //var letter = aplhabet[sideScroll.scrollSelected];
                /*if(filesFrame.selectListIndex === 0 || filesFrame.selectListIndex === 1){
                    var v = mainList.jumpIndex[sideScroll.scrollSelected];
                    if(v !== -1)
                        mainList.positionViewAtIndex(v,ListView.Beginning);
                }*/

                sideScrollTimer.restart();
            }
            onReleased: {
                //var letter = aplhabet[sideScroll.scrollSelected];
                if(filesFrame.selectListIndex === 0 || filesFrame.selectListIndex === 1 || filesFrame.selectListIndex === 2 || filesFrame.selectListIndex === 5){
                    var v = mainList.jumpIndex[sideScroll.scrollSelected];
                    if(v !== -1)
                        mainList.positionViewAtIndex(v,ListView.Beginning);
                }

                sideScroll.scrollVisible = false
                sideScroll.scrollSelected = -1
                sideScrollTimer.stop();
            }
        }
    }

    Item {
        id: playListAdd
        x: 0
        y: 0
        z: 2
        width: imageWidth
        height: imageHeight
        visible: false

        Rectangle {
            anchors.fill: parent;
            color: Qt.hsla(0,0,0,0.5)
        }

        Item {
            x: 250
            y: 100
            height: 220
            width: 300

            Rectangle{
                anchors.fill: parent
                color:"white"
            }

            ColumnLayout{
                x: 10
                y: 10
                height: 180
                width: parent.width
                spacing: 10

                ListView {
                    id: playlistView
                    x: 0
                    y: 0
                    height: parent.height - 30
                    width: parent.width

                    model: playlist
                    delegate: Component {
                        id: delegateAddPlaylist
                        Item {
                            width: parent.width
                            height: 45

                            RowLayout {
                                Label {
                                    text: SimpleJsTools.basename(fileName)
                                    color: "black"
                                    font.pointSize: 18
                                    font.family: "Helvetica"
                                }
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    musicfiles.load(galleryPath+"/playlist/"+fileName)
                                    musicfiles.addItem(filesFrame.selectListModeSource)
                                    musicfiles.save(galleryPath+"/playlist/"+fileName)
                                    filesFrame.selectListModeSource = null
                                    playListAdd.visible = false
                                }
                            }
                        }
                    }


                    ScrollBar.vertical: ScrollBar {
                        parent: filesFrame
                        policy: ScrollBar.AlwaysOn
                        anchors.top: parent.top
                        anchors.topMargin: filesFrame.topPadding
                        anchors.right: parent.right
                        anchors.rightMargin: 1
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: filesFrame.bottomPadding
                    }
                }

                RowLayout {
                    spacing: 10

                    Button {
                        height: 25
                        width: 25
                        text: "Cancel"
                        onClicked: {
                            playListAdd.visible = false
                        }
                    }
                }
            }
        }
    }

    Item {
        id: playListNew
        x: 0
        y: 0
        z: 2
        width: imageWidth
        height: imageHeight
        visible: false

        Rectangle {
            anchors.fill: parent;
            color: Qt.hsla(0,0,0,0.5)
        }

        Item {
            x: 250
            y: 100
            height: 200
            width: 300

            Rectangle{
                anchors.fill: parent
                color:"white"
            }

            ColumnLayout{
                x: 10
                y: 10
                height: 180
                width: parent.width
                spacing: 10

                Label {
                    text: "PlayList"
                    color: "#BEBEBE"
                    font.pointSize: 18
                    font.family: "Helvetica"
                }

                TextField {
                    id: textNewPlaylist
                    width: parent.width
                    height: 25
                    placeholderText: qsTr("Enter name")
                }

                RowLayout {
                    spacing: 10

                    Button {
                        height: 25
                        width: 25
                        text: "Ok"
                        onClicked: {
                            console.log(textNewPlaylist.text);
                            musicfiles.clear()
                            musicfiles.save(galleryPath+"/playlist/"+textNewPlaylist.text+".m3u")
                            playListNew.visible = false
                        }
                    }

                    Button {
                        height: 25
                        width: 25
                        text: "Cancel"
                        onClicked: {
                            playListNew.visible = false
                        }
                    }
                }
            }
        }
    }

    Item {
        x: 740
        y: 0

        Rectangle {
            width: 60
            height: 480
            color: "#A0A0A0"
        }

        Item {
            x: 5
            y: 10
            ColumnLayout {
                spacing: 16
                Rectangle {
                    id: btSongsList
                    width: 50
                    height: 50
                    color: "#BEBEBE"
                    Text {
                        color: filesFrame.selectListIndex === 0? "red" : "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_file
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.setMusicList("all");
                            filesFrame.selectListIndex = 0
                            filesFrame.selectListMode = false
                            filesFrame.selectListModeSource = null
                            filesFrame.headerTitle = "All Files"
                            btAdd.visible = true
                            btNew.visible = false
                            btRemove.visible = false
                        }
                    }
                }
                Rectangle {
                    id: btArtist
                    width: 50
                    height: 50
                    color: "#BEBEBE"
                    Text {
                        color: filesFrame.selectListIndex === 5? "red" : "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_list
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.setMusicList("artist");
                            filesFrame.selectListIndex = 5
                            filesFrame.selectListMode = false
                            filesFrame.selectListModeSource = null
                            filesFrame.headerTitle = "Artist"
                            btAdd.visible = false
                            btNew.visible = false
                            btRemove.visible = false
                        }
                    }
                }
                Rectangle {
                    id: btAlbums
                    width: 50
                    height: 50
                    color: "#BEBEBE"
                    Text {
                        color: filesFrame.selectListIndex === 2? "red" : "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_bookmark
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            controller.setMusicList("albums");
                            filesFrame.selectListIndex = 2
                            filesFrame.selectListMode = false
                            filesFrame.selectListModeSource = null
                            filesFrame.headerTitle = "Albums"
                            btAdd.visible = false
                            btNew.visible = false
                            btRemove.visible = false
                        }
                    }
                }
                Rectangle {
                    id: btPlaylist
                    width: 50
                    height: 50
                    color: "#BEBEBE"
                    Text {
                        color: filesFrame.selectListIndex === 3? "red" : "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_book
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            filesFrame.selectListIndex = 3
                            filesFrame.selectListMode = false
                            filesFrame.selectListModeSource = null
                            filesFrame.headerTitle = "Playlist"
                            btAdd.visible = false
                            btNew.visible = true
                            btRemove.visible = true
                        }
                    }
                }
                Rectangle {
                    id: btInternetRadio
                    width: 50
                    height: 50
                    color: "#BEBEBE"
                    Text {
                        color: filesFrame.selectListIndex === 1? "red" : "white"
                        anchors.centerIn: parent
                        font.pointSize: 25
                        font.family: fontAwesome.name
                        text: FontAwesome.icons.fa_signal
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            filesFrame.selectListIndex = 1
                            filesFrame.selectListMode = false
                            filesFrame.selectListModeSource = null
                            filesFrame.headerTitle = "Internet Radio"
                            btAdd.visible = true
                            btNew.visible = false
                            btRemove.visible = false
                        }
                    }
                }
            }
        }
    }
}

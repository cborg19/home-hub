import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 800
    height: 480

    background: Rectangle{
        color:"transparent"
    }

    header: Label {
        text: qsTr("System Controls")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Button {
        id: btShutdown
        x: 40
        y: 50
        width: 100
        height: 35
        text: qsTr("Shutdown")
    }

    Connections {
        target: btShutdown
        onClicked: {
            controller.shutdown()
        }
    }

    Button {
        id: btReboot
        x: 40
        y: 100
        width: 100
        height: 35
        text: qsTr("Reboot")
    }

    Connections {
        target: btReboot
        onClicked: {
            controller.reboot()
        }
    }

    Button {
        id: btClose
        x: 40
        y: 150
        width: 100
        height: 35
        text: qsTr("Close")

    }

    Connections {
        target: btClose
        onClicked: {
            Qt.callLater(Qt.quit)
        }
    }
}

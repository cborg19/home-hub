import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 800
    height: 480

    background: Rectangle{
        color:"transparent"
    }

    header: Label {
        text: qsTr("Voice Assistant")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    CheckBox {
        id: cbAssistantEnable
        x: 38
        y: 12
        width: 328
        height: 40
        text: qsTr("Enable Voice Connection")
        font.pointSize: 16
    }

    GroupBox {
        id: gbPlayBackSettings
        x: 20
        y: 73
        width: 760
        height: 70
        font.pointSize: 12
        title: qsTr("Assistant Settings")

        CheckBox {
            id: cbCloudPlayEnable
            x: 0
            y: -5
            width: 328
            height: 32
            text: qsTr("Use Cloud")
            font.pointSize: 16
        }

        Label {
            //id: label
            x: 130
            y: 0
            height: 32
            text: qsTr("Voices")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        ComboBox {
            //id: bmComboWeather
            x: 200
            y: -5
            width: 300
            height: 32
            editable: false
            font.pointSize: 12
            //model: settingWeather.intervalList
        }

        Label {
            //id: label
            x: 530
            y: 0
            text: qsTr("Volume")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        ComboBox {
            //id: bmComboWeather
            x: 600
            y: -5
            width: 114
            height: 32
            editable: false
            font.pointSize: 12
            //model: settingWeather.intervalList
        }
    }

    GroupBox {
        id: gbVoiceSettings
        x: 20
        y: 150
        width: 760
        height: 230
        font.pointSize: 12
        title: qsTr("Voice Recognition Settings")

        CheckBox {
            id: cbCloudRecEnable
            x: 0
            y: 0
            width: 328
            height: 40
            text: qsTr("Use Cloud")
            font.pointSize: 16
        }

        Label {
            //id: label
            x: 0
            y: 50
            text: qsTr("WakeUp Word")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        ComboBox {
            //id: bmComboWeather
            x: 110
            y: 45
            width: 250
            height: 32
            editable: false
            font.pointSize: 12
            //model: settingWeather.intervalList
        }

        Label {
            //id: label4
            x: 400
            y: 50
            text: qsTr("Sensitivity")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        TextField {
            //id: tfIpAddress
            x: 500
            y: 45
            width: 50
            text: qsTr("")
            placeholderText: "V"
        }

        TextField {
            //id: tfIpAddress
            x: 600
            y: 45
            width: 50
            text: qsTr("")
            placeholderText: "1"
        }

        Label {
            //id: label4
            x: 0
            y: 100
            text: qsTr("Audio Gain")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        TextField {
            //id: tfIpAddress
            x: 100
            y: 95
            width: 45
            text: qsTr("")
            placeholderText: "V"
        }

        Label {
            //id: label
            x: 200
            y: 100
            text: qsTr("Play Sound")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        ComboBox {
            //id: bmComboWeather
            x: 300
            y: 95
            width: 200
            height: 32
            editable: false
            font.pointSize: 12
            //model: settingWeather.intervalList
        }

        Label {
            //id: label4
            x: 0
            y: 150
            text: qsTr("Silent Count Threshold")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        TextField {
            //id: tfIpAddress
            x: 200
            y: 145
            width: 50
            text: qsTr("")
            placeholderText: "V"
        }

        Label {
            //id: label4
            x: 350
            y: 150
            text: qsTr("Recording Timeout")
            horizontalAlignment: Text.AlignRight
            font.pointSize: 16
        }

        TextField {
            //id: tfIpAddress
            x: 500
            y: 145
            width: 50
            text: qsTr("")
            placeholderText: "V"
        }
    }
}


import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle {
        color: "white"
    }

    Connections {
        target: trainsDisplay
        onVisibleChanged:
            if(visible){
                clockTimer.start();
            }else{
                clockTimer.stop();
            }

        ignoreUnknownSignals: true
    }

    Timer {
        id: clockTimer
        interval: 1000
        running: false
        repeat: true

        property int fireTrigger: 60
        onTriggered:{
            trainBoard.displayTime();
            fireTrigger--;
            if(fireTrigger < 0){
                clockTimer.stop();
                window.switchToPhoto(true)
            }
        }
    }

    Item {
        width: imageWidth
        height: imageHeight
        visible: controller.sydneyTrainList.length > 0? false: true
        BusyIndicator {
           id: busyIndication
           anchors.centerIn: parent
           running: controller.sydneyTrainList.length > 0? false: true
        }

        Image {
            width: 100
            height: 100
            x: 10
            y: 10
            source: "files/icon/trains.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Item {
        id: trainBoard
        visible: controller.sydneyTrainList.length > 0? true: false

        property string currentTime: ""
        property var currentTimeInt: ""

        function displayTime(){
            var currentDate = new Date();
            trainBoard.currentTimeInt = currentDate.getTime();
            if(time24hr){
                trainBoard.currentTime = Qt.formatDateTime(currentDate, "HH:mm:ss")
            }else{
                trainBoard.currentTime = Qt.formatDateTime(currentDate, "h:mm:ss")
            }
        }

        function maxNextTrains(v){
            if(v < 1) return 0;
            if((v-1) > 3) return 3;
            return (v-1)
        }

        function timeLeftInMins(v){
            var adjust = (trainBoard.currentTimeInt/1000);
            if(adjust > v) return "Now";
            var d = (v - adjust);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            if(h > 0) return h+" hour";
            return m+" min"
        }

        function timeETA(v){
            var currentDate = new Date(v*1000);
            if(time24hr){
                return Qt.formatDateTime(currentDate, "HH:mm")
            }
            return Qt.formatDateTime(currentDate, "h:mm A")
        }

        function getTitle(){
            if(controller.sydneyTrainList.length === 0) return;
            return "Next Train to "+controller.sydneyTrainList[0].legs[0].destination.disassembledName
        }

        ColumnLayout {
            spacing: 0
            Item {
                width: imageWidth
                height: 30
                Rectangle {
                    anchors.fill: parent
                    color: "#f78c3b"
                }
                Label {
                    x: 100
                    y: 2
                    text: trainBoard.getTitle()
                    color: "white"
                    font.pointSize: 18
                    font.family: "Helvetica"
                    font.bold: true
                }

                Label {
                    id: timeView
                    x: 550
                    y: 2
                    text: "Time now "+trainBoard.currentTime
                    color: "white"
                    font.pointSize: 18
                    font.family: "Helvetica"
                    font.bold: true
                }
            }
        }

        Item {
            x: 10
            y: 40
            width: imageWidth/2-10
            height: imageHeight-40

            Column {
                Repeater {
                    model: trainBoard.maxNextTrains(controller.sydneyTrainList.length)
                    Item {
                        width: imageWidth/2-10
                        height: 120
                        RowLayout {
                            Rectangle {
                                width: 60
                                height: 60
                                radius: 5
                                color: controller.sydneyTrainList[index+1].legs[0].transportation.lineColour
                                Label {
                                    anchors.centerIn: parent
                                    text: controller.sydneyTrainList[index+1].legs[0].transportation.disassembledName
                                    color: "white"
                                    font.pointSize: 26
                                    font.family: "Helvetica"
                                    font.bold: true
                                }
                            }

                            Item{
                                width: 260
                                height: 60
                                ColumnLayout {
                                    spacing: 3
                                    Label {
                                        text: controller.sydneyTrainList[index+1].legs[0].transportation.lineName
                                        color: "black"
                                        font.pointSize: 22
                                        font.family: "Helvetica"
                                        font.bold: true
                                    }

                                    Label {
                                        text: controller.sydneyTrainList.length > 0?controller.sydneyTrainList[index+1].legs[0].transportation.lineVia:""
                                        color: "black"
                                        font.pointSize: 14
                                        font.family: "Helvetica"
                                        font.bold: false
                                    }

                                    Label {
                                        text: trainBoard.timeETA(controller.sydneyTrainList[index+1].legs[controller.sydneyTrainList[index+1].legs.length-1].destination.arrivalTime)
                                        color: "black"
                                        font.pointSize: 14
                                        font.family: "Helvetica"
                                        font.bold: true
                                    }

                                    Rectangle {
                                        color: "grey"
                                        width: 80
                                        height: 20
                                        visible: controller.sydneyTrainList[index+1].legs[0].limited
                                        Label {
                                            anchors.centerIn: parent
                                            text: "Limited Stop"
                                            color: "white"
                                            font.pointSize: 10
                                            font.family: "Helvetica"
                                        }
                                    }
                                }
                            }

                            ColumnLayout {
                                Label {
                                    text: "Platform"
                                    color: "black"
                                    font.pointSize: 10
                                    font.family: "Helvetica"
                                    font.bold: false
                                }

                                Label {
                                    text: controller.sydneyTrainList[index+1].legs[0].origin.platform
                                    color: "#f78c3b"
                                    font.pointSize: 16
                                    font.family: "Helvetica"
                                    font.bold: true
                                }

                                Label {
                                    text: "Departs"
                                    color: "black"
                                    font.pointSize: 10
                                    font.family: "Helvetica"
                                    font.bold: false
                                }

                                Label {
                                    text: trainBoard.timeLeftInMins(controller.sydneyTrainList[index+1].legs[0].origin.departureTime)
                                    color: "#f78c3b"
                                    font.pointSize: 16
                                    font.family: "Helvetica"
                                    font.bold: true
                                }
                            }
                        }
                    }
                }
            }
        }

        Item {
            id: nextTrain
            x: imageWidth/2
            y: 30
            width: imageWidth/2
            height: imageHeight-30

            property int interchanges: 0

            function displayTotalTime(){
                if(controller.sydneyTrainList.length === 0) return "";
                if(controller.sydneyTrainList[0].legs.length === 1 || nextTrain.interchanges === controller.sydneyTrainList[0].legs.length-1)
                    return trainBoard.timeETA(controller.sydneyTrainList[0].legs[0].destination.arrivalTime);
                var s = trainBoard.timeETA(controller.sydneyTrainList[0].legs[nextTrain.interchanges].destination.arrivalTime)
                s+= " > " + trainBoard.timeETA(controller.sydneyTrainList[0].legs[controller.sydneyTrainList[0].legs.length-1].destination.arrivalTime)
                return s
            }

            Item {
                x: 10
                y: 10
                width: imageWidth/2
                height: 100
                RowLayout {
                    Rectangle {
                        width: 60
                        height: 60
                        radius: 5
                        color: controller.sydneyTrainList.length > 0?controller.sydneyTrainList[0].legs[nextTrain.interchanges].transportation.lineColour:""
                        Label {
                            anchors.centerIn: parent
                            text: controller.sydneyTrainList.length > 0?controller.sydneyTrainList[0].legs[nextTrain.interchanges].transportation.disassembledName:""
                            color: "white"
                            font.pointSize: 26
                            font.family: "Helvetica"
                            font.bold: true
                        }
                    }

                    Item{
                        width: 250
                        height: 70
                        ColumnLayout {
                            spacing: 3
                            Label {
                                text: controller.sydneyTrainList.length > 0?controller.sydneyTrainList[0].legs[nextTrain.interchanges].transportation.lineName:""
                                color: "black"
                                font.pointSize: 22
                                font.family: "Helvetica"
                                font.bold: true
                            }

                            Label {
                                text: controller.sydneyTrainList.length > 0?controller.sydneyTrainList[0].legs[nextTrain.interchanges].transportation.lineVia:""
                                color: "black"
                                font.pointSize: 14
                                font.family: "Helvetica"
                                font.bold: false
                            }

                            Label {
                                text: nextTrain.displayTotalTime()
                                color: "black"
                                font.pointSize: 14
                                font.family: "Helvetica"
                                font.bold: true
                            }
                        }
                    }
                    ColumnLayout {
                        Label {
                            text: "Platform"
                            color: "black"
                            font.pointSize: 14
                            font.family: "Helvetica"
                            font.bold: false
                        }

                        Label {
                            text: controller.sydneyTrainList.length > 0?controller.sydneyTrainList[0].legs[nextTrain.interchanges].origin.platform:""
                            color: "#f78c3b"
                            font.pointSize: 20
                            font.family: "Helvetica"
                            font.bold: true
                        }
                    }
                }
            }

            Rectangle {
                id: btInterchange
                x: 320
                y: 60
                z: 2
                width: 50
                height: 50
                color: "transparent"
                visible: controller.sydneyTrainList[0].interchanges > 0?true:false
                Text {
                    color: "black"
                    anchors.centerIn: parent
                    font.pointSize: 25
                    font.family: fontAwesome.name
                    text: FontAwesome.icons.fa_random
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        var v = nextTrain.interchanges;
                        v++;
                        if(v > controller.sydneyTrainList[0].interchanges)
                            v = 0;
                        nextTrain.interchanges = v;
                    }
                }
            }

            Component {
                id: trainStation
                Item {
                    width: imageWidth/2
                    height: 26
                    Column {
                        Text {
                            text: controller.sydneyTrainList[0].legs[nextTrain.interchanges].stops[index].name
                            font.pointSize: 20
                            font.family: "Helvetica"
                            font.bold: true
                        }
                    }
                }
            }

            ListView {
                x: 30
                y: 100
                width: imageWidth/2
                height: 380
                model: controller.sydneyTrainList.length > 0?controller.sydneyTrainList[0].legs[nextTrain.interchanges].stops:null
                delegate: trainStation
            }

            Item {
                x: 300
                y: 380
                ColumnLayout {
                    Label {
                        text: "Departs"
                        color: "black"
                        font.pointSize: 16
                        font.family: "Helvetica"
                        font.bold: false
                    }

                    Label {
                        text: trainBoard.timeLeftInMins(controller.sydneyTrainList.length > 0?controller.sydneyTrainList[0].legs[nextTrain.interchanges].origin.departureTime:0)
                        color: "#f78c3b"
                        font.pointSize: 24
                        font.family: "Helvetica"
                        font.bold: true
                    }
                }
            }
        }
    }

    Button {
        id: btMenu
        x:0
        y:0
        height: 100
        width: 100
        z: 2
        focusPolicy: Qt.NoFocus
        background: Rectangle {
            color: "transparent"
        }
    }

    Connections {
        target: btMenu
        onClicked: {
            console.log("TrainsPage switchToMenu");
            window.switchToMenu(true)
        }
    }
}

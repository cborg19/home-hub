import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import "./js/fontawesome.js" as FontAwesome
import "./js/simpletools.js" as SimpleJsTools

Page {
    width: imageWidth
    height: imageHeight

    background: Rectangle {
        color: "transparent"
    }

    Item {
        id: mainFrame
        x: 0
        y: 0
        width: imageWidth-60
        height: imageHeight

        function durationToMin(v){
            return (v/60).toString()+" min | ";
        }

        Component {
            id: delegateTvShows
            ItemDelegate {
                width: parent.width
                height: 85
                RowLayout {
                    spacing: 0
                    Item{
                        width: 85
                        height: 80
                        Image {
                            width: 80
                            height: 80
                            fillMode: Image.PreserveAspectCrop
                            source: controller.KodiList[index].thumbnail !== undefined?"image://imageprovider/"+controller.KodiList[index].thumbnail:""
                        }
                    }
                    ColumnLayout {
                        spacing: 0
                        Label {
                            text: controller.KodiList[index].title
                            color: "white"
                            font.pointSize: 18
                            font.family: "Helvetica"
                            font.bold: true
                        }

                        Slider {
                            id: doneSlider
                            value: 50//controller.KodiList[index].watchedepisodes
                            to: 100//controller.KodiList[index].episode

                            width: 220
                            height: 15

                            handle: Rectangle {
                                x: doneSlider.visualPosition * (doneSlider.width - width)
                                y: (doneSlider.height - height) / 2
                                width: 12
                                height: 12

                                radius: 6
                                color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
                                border.color: "#9c27b0"
                            }

                            background: Rectangle {
                                y: (doneSlider.height - height) / 2
                                height: 8
                                radius: 4
                                color: "#686868" //background slider

                                Rectangle {
                                    width: doneSlider.visualPosition * parent.width
                                    height: parent.height
                                    color: "#b92ed1" //done part
                                    radius: 4
                                }
                            }
                        }
                        Label {
                            text: controller.KodiList[index].episode+" episodes | "+(controller.KodiList[index].episode-controller.KodiList[index].watchedepisodes)+" unwatched"
                            color: "white"
                            font.pointSize: 12
                            font.family: "Helvetica"
                        }
                        Label {
                            text: "Premiered "+controller.KodiList[index].year
                            color: "white"
                            font.pointSize: 12
                            font.family: "Helvetica"
                        }
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        controller.getKodiTvShow(controller.KodiList[index].tvshowid)
                    }
                }
            }
        }

        Component {
            id: delegateSeason
            ItemDelegate {
                width: parent.width
                height: 75
                RowLayout {
                    spacing: 0
                    Item{
                        width: 70
                        height: 70
                        Image {
                            width: 70
                            height: 70
                            fillMode: Image.PreserveAspectCrop
                            source: controller.KodiList[index].thumbnail !== undefined?"image://imageprovider/"+controller.KodiList[index].thumbnail:""
                        }
                    }
                    ColumnLayout {
                        spacing: 0
                        Label {
                            text: controller.KodiList[index].label
                            color: "white"
                            font.pointSize: 18
                            font.family: "Helvetica"
                            font.bold: true
                        }

                        Slider {
                            id: doneSlider
                            value: 50//controller.KodiList[index].watchedepisodes
                            to: 100//controller.KodiList[index].episode

                            width: 220
                            height: 15

                            handle: Rectangle {
                                x: doneSlider.visualPosition * (doneSlider.width - width)
                                y: (doneSlider.height - height) / 2
                                width: 12
                                height: 12

                                radius: 6
                                color: "#f0f0f0" //seekSlider.pressed ? "#cc0099" : "#E91E63" //"#f0f0f0" : "#f6f6f6"
                                border.color: "#9c27b0"
                            }

                            background: Rectangle {
                                y: (doneSlider.height - height) / 2
                                height: 8
                                radius: 4
                                color: "#686868" //background slider

                                Rectangle {
                                    width: doneSlider.visualPosition * parent.width
                                    height: parent.height
                                    color: "#b92ed1" //done part
                                    radius: 4
                                }
                            }
                        }
                        Label {
                            text: controller.KodiList[index].episode+" episodes | "+(controller.KodiList[index].episode-controller.KodiList[index].watchedepisodes)+" unwatched"
                            color: "white"
                            font.pointSize: 12
                            font.family: "Helvetica"
                        }
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        controller.getKodiSeason(controller.KodiList[index].tvshowid, index+1)
                    }
                }
            }
        }

        Component {
            id: delegateTvSeason
            ItemDelegate {
                width: parent.width
                height: 70
                RowLayout {
                    spacing: 0
                    Item{
                        width: 70
                        height: 70
                        Image {
                            width: 70
                            height: 70
                            fillMode: Image.PreserveAspectCrop
                            source: controller.KodiList[index].thumbnail !== undefined?"image://imageprovider/"+controller.KodiList[index].thumbnail:""
                        }
                    }
                    ColumnLayout {
                        spacing: 0
                        Label {
                            text: controller.KodiList[index].label
                            color: "white"
                            font.pointSize: 26
                            font.family: "Helvetica"
                            font.bold: true
                        }
                        Label {
                            text: controller.KodiList[index].duration+" min | "+controller.KodiList[index].firstaired
                            color: "white"
                            font.pointSize: 18
                            font.family: "Helvetica"
                        }
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        controller.getKodiShowDetails(controller.KodiList[index].episodeid)
                    }
                }
            }
        }

        Component {
            id: delegateMovies
            ItemDelegate {
                width: parent.width
                height: 100
                RowLayout {
                    spacing: 0
                    Item{
                        width: 70
                        height: 100
                        Image {
                            width: 70
                            height: 70
                            fillMode: Image.PreserveAspectCrop
                            source: controller.KodiList[index].fanImage !== undefined?"image://imageprovider/"+controller.KodiList[index].fanImage:""
                        }
                    }
                    ColumnLayout {
                        spacing: 0
                        Label {
                            text: controller.KodiList[index].title
                            color: "white"
                            font.pointSize: 18
                            font.family: "Helvetica"
                            font.bold: true
                        }
                        Label {
                            text: controller.KodiList[index].tagline
                            color: "white"
                            font.pointSize: 18
                            font.family: "Helvetica"
                        }
                        Label {
                            text: "152 min | "+controller.KodiList[index].year
                            color: "white"
                            font.pointSize: 18
                            font.family: "Helvetica"
                        }
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        controller.getKodiVideo(controller.KodiList[index].movieid)
                    }
                }
            }
        }

        Component {
            id: delegateMovieDetail
            ItemDelegate {
                width: parent.width
                height: parent.Height

                Image {
                    width: parent.width
                    height: parent.Height
                    fillMode: Image.PreserveAspectCrop
                    source: controller.KodiList[index].fanImage !== undefined?"image://imageprovider/"+controller.KodiList[index].fanImage:""

                    Rectangle {
                        anchors.fill: parent;
                        color: Qt.hsla(0,0,0,0.6)
                    }
                }

                ColumnLayout {
                    RowLayout {
                        Item{
                            width: 100
                            height: 200
                            Image {
                                width: 100
                                height: 100
                                fillMode: Image.PreserveAspectCrop
                                source: controller.KodiList[index].bgImage !== undefined?"image://imageprovider/"+controller.KodiList[index].bgImage:""
                            }
                        }
                        ColumnLayout {
                            Label {
                                text: controller.KodiList[index].title
                                color: "white"
                                font.pointSize: 18
                                font.family: "Helvetica"
                                font.bold: true
                            }
                            Label {
                                text: ""//mainFrame.durationToMin(controller.KodiList[index].stream.video.duration)+controller.KodiList[index].year
                                color: "white"
                                font.pointSize: 18
                                font.family: "Helvetica"
                            }

                            RowLayout {
                                spacing: 20

                                Rectangle {
                                    id: btWatchOnKodi
                                    width: 50
                                    height: 50
                                    radius: 25
                                    color: "transparent"
                                    Text {
                                        color: "white"
                                        anchors.centerIn: parent
                                        font.pointSize: 25
                                        font.family: fontAwesome.name
                                        text: FontAwesome.icons.fa_desktop
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            controller.setKodiPlayer(controller.KodiList[index].file)
                                            kodiSwipeView.currentIndex = 1
                                        }
                                    }
                                }

                                Rectangle {
                                    id: btWatchOnKodiPlaylist
                                    width: 50
                                    height: 50
                                    radius: 25
                                    color: "transparent"
                                    Text {
                                        color: "white"
                                        anchors.centerIn: parent
                                        font.pointSize: 25
                                        font.family: fontAwesome.name
                                        text: FontAwesome.icons.fa_plus_circle
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            controller.addKodiToPlayList(controller.KodiList[index].file)
                                        }
                                    }
                                }
                                Rectangle {
                                    id: btWatchOnFrame
                                    width: 50
                                    height: 50
                                    radius: 25
                                    color: "transparent"
                                    Text {
                                        color: "white"
                                        anchors.centerIn: parent
                                        font.pointSize: 25
                                        font.family: fontAwesome.name
                                        text: FontAwesome.icons.fa_external_link
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {

                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                Text {
                    x: 20
                    y: 150
                    width: 700
                    height: 150
                    wrapMode: Text.WordWrap
                    text: controller.KodiList[index].plot
                    color: "white"
                    font.pointSize: 18
                    font.family: "Helvetica"
                }

                Item {
                    x: 60
                    y: 360
                    width: 520
                    height: 100

                    RowLayout {
                        Rectangle {
                            id: btleftCast
                            width: 40
                            height: 40
                            radius: 20
                            color: "#ce6ddf"
                            Text {
                                color: "white"
                                anchors.centerIn: parent
                                font.pointSize: 25
                                font.family: fontAwesome.name
                                text: FontAwesome.icons.fa_arrow_circle_left
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    cast.castback()
                                }
                            }
                        }

                        Rectangle {
                            id: cast
                            width: 430
                            height: 100

                            property int posCast: 0
                            function castLength(){
                                if(controller.KodiList[0].cast !== undefined)
                                    if(controller.KodiList[0].cast.length > 4)
                                        return 4
                                    else return controller.KodiList[0].cast.length
                                return 0
                            }

                            function castback(){
                                if(controller.KodiList[0].cast === undefined) return
                                cast.posCast--;
                                if(cast.posCast < 0) cast.posCast = 0;
                            }

                            function castNext(){
                                if(controller.KodiList[0].cast === undefined) return
                                cast.posCast++;
                                if(cast.posCast >= (controller.KodiList[0].cast.length - 4)) cast.posCast = controller.KodiList[0].cast.length-5;
                            }

                            RowLayout {
                                spacing: 10
                                Repeater {
                                    id: castDisplay
                                    model: controller.KodiList[0].cast !== undefined? cast.castLength() : 0
                                    Rectangle {
                                        height: 100
                                        width: 100
                                        color: "black"

                                        Image {
                                            anchors.fill: parent
                                            fillMode: Image.PreserveAspectCrop
                                            source: controller.KodiList[0].cast !== undefined?"image://imageprovider/"+controller.KodiList[0].cast[index+ cast.posCast].image : ""
                                        }

                                        Label {
                                            anchors.bottom: parent.bottom
                                            color: "white"
                                            font.pointSize: 10
                                            font.family: "Helvetica"
                                            text: controller.KodiList[0].cast[index + cast.posCast].role
                                            Label {
                                                anchors.bottom: parent.top
                                                color: "white"
                                                font.pointSize: 10
                                                font.family: "Helvetica"
                                                font.bold: true
                                                text: controller.KodiList[0].cast[index + cast.posCast].name
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        Rectangle {
                            id: btrightCast
                            width: 40
                            height: 40
                            radius: 20
                            color: "#ce6ddf"
                            Text {
                                color: "white"
                                anchors.centerIn: parent
                                font.pointSize: 25
                                font.family: fontAwesome.name
                                text: FontAwesome.icons.fa_arrow_circle_right
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    cast.castNext()
                                }
                            }
                        }
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        controller.getKodiVideo(controller.KodiList[index].movieid)
                    }
                }
            }
        }

        Component {
            id: delegateEpisodeDetail
            ItemDelegate {
                width: parent.width
                height: parent.Height

                Image {
                    width: parent.width
                    height: parent.Height
                    fillMode: Image.PreserveAspectCrop
                    source: controller.KodiList[index].fanImage !== undefined?"image://imageprovider/"+controller.KodiList[index].fanImage:""

                    Rectangle {
                        anchors.fill: parent;
                        color: Qt.hsla(0,0,0,0.6)
                    }
                }

                ColumnLayout {
                    RowLayout {
                        Item{
                            width: 100
                            height: 200
                            Image {
                                width: 100
                                height: 100
                                fillMode: Image.PreserveAspectCrop
                                source: controller.KodiList[index].bgImage !== undefined?"image://imageprovider/"+controller.KodiList[index].bgImage:""
                            }
                        }
                        ColumnLayout {
                            Label {
                                text: controller.KodiList[index].title
                                color: "white"
                                font.pointSize: 18
                                font.family: "Helvetica"
                                font.bold: true
                            }
                            Label {
                                text: controller.KodiList[index].firstaired
                                color: "white"
                                font.pointSize: 18
                                font.family: "Helvetica"
                            }

                            RowLayout {
                                spacing: 20

                                Rectangle {
                                    id: btWatchOnKodi
                                    width: 50
                                    height: 50
                                    radius: 25
                                    color: "transparent"
                                    Text {
                                        color: "white"
                                        anchors.centerIn: parent
                                        font.pointSize: 25
                                        font.family: fontAwesome.name
                                        text: FontAwesome.icons.fa_desktop
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            controller.setKodiPlayer(controller.KodiList[index].file)
                                            kodiSwipeView.currentIndex = 1
                                        }
                                    }
                                }

                                Rectangle {
                                    id: btWatchOnKodiPlaylist
                                    width: 50
                                    height: 50
                                    radius: 25
                                    color: "transparent"
                                    Text {
                                        color: "white"
                                        anchors.centerIn: parent
                                        font.pointSize: 25
                                        font.family: fontAwesome.name
                                        text: FontAwesome.icons.fa_plus_circle
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {
                                            controller.addKodiToPlayList(controller.KodiList[index].file)
                                        }
                                    }
                                }
                                Rectangle {
                                    id: btWatchOnFrame
                                    width: 50
                                    height: 50
                                    radius: 25
                                    color: "transparent"
                                    Text {
                                        color: "white"
                                        anchors.centerIn: parent
                                        font.pointSize: 25
                                        font.family: fontAwesome.name
                                        text: FontAwesome.icons.fa_external_link
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: {

                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                Text {
                    x: 20
                    y: 150
                    width: 700
                    height: 150
                    wrapMode: Text.WordWrap
                    text: controller.KodiList[index].plot
                    color: "white"
                    font.pointSize: 18
                    font.family: "Helvetica"
                }

                Item {
                    x: 60
                    y: 360
                    width: 520
                    height: 100

                    RowLayout {
                        Rectangle {
                            id: btleftCast
                            width: 40
                            height: 40
                            radius: 20
                            color: "#ce6ddf"
                            Text {
                                color: "white"
                                anchors.centerIn: parent
                                font.pointSize: 25
                                font.family: fontAwesome.name
                                text: FontAwesome.icons.fa_arrow_circle_left
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    cast.castback()
                                }
                            }
                        }

                        Rectangle {
                            id: cast
                            width: 430
                            height: 100

                            property int posCast: 0
                            function castLength(){
                                if(controller.KodiList[0].cast !== undefined)
                                    if(controller.KodiList[0].cast.length > 4)
                                        return 4
                                    else return controller.KodiList[0].cast.length
                                return 0
                            }

                            function castback(){
                                if(controller.KodiList[0].cast === undefined) return
                                cast.posCast--;
                                if(cast.posCast < 0) cast.posCast = 0;
                            }

                            function castNext(){
                                if(controller.KodiList[0].cast === undefined) return
                                cast.posCast++;
                                if(cast.posCast >= (controller.KodiList[0].cast.length - 4)) cast.posCast = controller.KodiList[0].cast.length-5;
                            }

                            RowLayout {
                                spacing: 10
                                Repeater {
                                    id: castDisplay
                                    model: controller.KodiList[0].cast !== undefined? cast.castLength() : 0
                                    Rectangle {
                                        height: 100
                                        width: 100
                                        color: "black"

                                        Image {
                                            anchors.fill: parent
                                            fillMode: Image.PreserveAspectCrop
                                            source: controller.KodiList[0].cast !== undefined?"image://imageprovider/"+controller.KodiList[0].cast[index+ cast.posCast].image : ""
                                        }

                                        Label {
                                            anchors.bottom: parent.bottom
                                            color: "white"
                                            font.pointSize: 10
                                            font.family: "Helvetica"
                                            text: controller.KodiList[0].cast[index + cast.posCast].role
                                            Label {
                                                anchors.bottom: parent.top
                                                color: "white"
                                                font.pointSize: 10
                                                font.family: "Helvetica"
                                                font.bold: true
                                                text: controller.KodiList[0].cast[index + cast.posCast].name
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        Rectangle {
                            id: btrightCast
                            width: 40
                            height: 40
                            radius: 20
                            color: "#ce6ddf"
                            Text {
                                color: "white"
                                anchors.centerIn: parent
                                font.pointSize: 25
                                font.family: fontAwesome.name
                                text: FontAwesome.icons.fa_arrow_circle_right
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    cast.castNext()
                                }
                            }
                        }
                    }
                }
            }
        }

        Component {
            id: delegatePlaylist
            ItemDelegate {
                width: parent.width
                height: 85
                RowLayout {
                    spacing: 0
                    Item{
                        width: 85
                        height: 80
                        Image {
                            width: 80
                            height: 80
                            fillMode: Image.PreserveAspectCrop
                            source: controller.KodiList[index].thumbnail !== undefined?"image://imageprovider/"+controller.KodiList[index].thumbnail:""
                        }
                    }
                    ColumnLayout {
                        spacing: 0
                        Label {
                            text: controller.KodiList[index].title
                            color: "white"
                            font.pointSize: 18
                            font.family: "Helvetica"
                            font.bold: true
                        }

                        Label {
                            visible: controller.KodiList[index].type === "episodes"?true:false
                            text: "Season "+ controller.KodiList[index].season + " episodes " + controller.KodiList[index].episode
                            color: "white"
                            font.pointSize: 12
                            font.family: "Helvetica"
                        }

                        Label {
                            visible: controller.KodiList[index].type === "movie"?true:false
                            text: controller.KodiList[index].originaltitle
                            color: "white"
                            font.pointSize: 12
                            font.family: "Helvetica"
                        }

                        Label {
                            text: "Premiered "+controller.KodiList[index].year
                            color: "white"
                            font.pointSize: 12
                            font.family: "Helvetica"
                        }
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        controller.getKodiTvShow(controller.KodiList[index].tvshowid)
                    }
                }
            }
        }

        ListView {
            id: mainList
            x: 0
            y: 0
            width: imageWidth-60
            height: imageHeight

            property var jumpIndex: ({})
            property int scrollSize: 0
            property int lastScrollPoisition: 0
            property int oldCount: 0

            function getDelegate(index){
                if(index === "playlist") return delegatePlaylist
                if(index === "moviedetail") return delegateMovieDetail
                if(index === "season") return delegateSeason
                if(index === "tvSeason") return delegateTvSeason
                if(index === "movies") return delegateMovies
                if(index === "episodedetail") return delegateEpisodeDetail
                return delegateTvShows
            }

            model: controller.KodiList
            delegate: mainList.getDelegate(controller.KodiMenu)

            ScrollBar.vertical: ScrollBar {
                parent: mainFrame
                policy: ScrollBar.AlwaysOn
                anchors.top: parent.top
                anchors.topMargin: mainFrame.topPadding
                anchors.right: parent.right
                anchors.rightMargin: 1
                anchors.bottom: parent.bottom
                anchors.bottomMargin: mainFrame.bottomPadding
            }

            onModelChanged: {
                mainList.jumpIndex = [];
                if(controller.KodiMenu === "movies" || controller.KodiMenu === "tv"){
                    mainList.jumpIndex = controller.KodiListIndex
                }else{
                    mainList.jumpIndex = []
                }
            }

            onContentYChanged: {
                if (contentY > (contentHeight - height) * 0.8) {
                    if(scrollSize !== mainList.count){
                        scrollSize = mainList.count
                        if(controller.KodiMenu === "movies"){
                            controller.getKodiMovies(mainList.count, 200)
                        }
                    }
                }
                if(contentY === 0 && lastScrollPoisition !== 0){
                    console.log("MOVED BACK", scrollSize);
                    moveTo.start()
                }else
                    lastScrollPoisition = contentY;
            }

            Timer {
                id: moveTo
                interval: 100;
                running: false;
                repeat: false
                onTriggered: {
                    mainList.positionViewAtIndex(mainList.scrollSize,ListView.End);
                }
            }
        }
    }

    Item {
        id: sideScroll
        x: 700
        y: 0
        width: 40
        height: imageHeight
        z: 2
        visible: false

        property bool scrollVisible: false
        property int scrollSelected: -1

        Rectangle {
            width: 40
            height: 480
            color: sideScroll.scrollVisible?"#E0E0E0":"transparent"
        }

        Item {
            y: 5
            Repeater {
                model: aplhabet.length
                delegate: Item{
                    Rectangle {
                        y: (imageHeight-10)/26*index
                        width:  35
                        height: (imageHeight-10)/26
                        z: sideScroll.scrollSelected === index?3:2
                        color: "transparent"

                        Text {
                            anchors.centerIn: parent
                            color: sideScroll.scrollSelected === index?"white":"#989898"
                            font.pointSize: sideScroll.scrollSelected === index?36:18
                            font.family: "Helvetica"
                            text: aplhabet[index]
                            visible: sideScroll.scrollVisible
                        }
                    }
                }
            }
        }
        Timer {
            id: sideScrollTimer
            interval: 2000;
            running: false;
            repeat: false
            onTriggered: {
                sideScroll.scrollVisible = false
                sideScroll.scrollSelected = -1
            }
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                sideScroll.scrollVisible = true
                sideScrollTimer.restart();
            }
            onExited: {
                sideScroll.scrollVisible = false
                sideScroll.scrollSelected = -1
                sideScrollTimer.stop();
            }
            onPositionChanged:{
                var grid = (imageHeight-10)/26
                sideScroll.scrollSelected = Math.floor(mouseY/grid)
                //var letter = aplhabet[sideScroll.scrollSelected];
                var v = mainList.jumpIndex[sideScroll.scrollSelected];
                if(v !== -1){
                    mainList.positionViewAtIndex(v,ListView.Beginning);
                }else{
                    if(mainList.scrollSize !== mainList.count){
                        mainList.scrollSize = mainList.count
                        controller.getKodiMovies(mainList.count, mainList.jumpIndex[26]-200)
                    }
                }

                sideScrollTimer.restart();
            }
            onReleased: {
                sideScroll.scrollVisible = false
                sideScroll.scrollSelected = -1
                sideScrollTimer.stop();
            }
        }
    }

    Rectangle {
       id: sideMenu
       x: imageWidth-60
       y: 0
       width: 60
       height: imageHeight

       color: Qt.hsla(0,0,0,0.4)

       property int menu: 0

       ColumnLayout {
           spacing: 10
           Rectangle {
               id: btPlayList
               width: 50
               height: 50
               radius: 25
               color: "transparent"
               Text {
                   color: sideMenu.menu === 0?"red":"white"
                   anchors.centerIn: parent
                   font.pointSize: 25
                   font.family: fontAwesome.name
                   text: FontAwesome.icons.fa_list
               }
               MouseArea {
                   anchors.fill: parent
                   onClicked: {
                       controller.getKodiPlayList()
                       sideMenu.menu = 0
                   }
               }
           }
           Rectangle {
               id: btFilm
               width: 50
               height: 50
               radius: 25
               color: "transparent"
               Text {
                   color: sideMenu.menu === 1?"red":"white"
                   anchors.centerIn: parent
                   font.pointSize: 25
                   font.family: fontAwesome.name
                   text: FontAwesome.icons.fa_film
               }
               MouseArea {
                   anchors.fill: parent
                   onClicked: {
                       controller.getKodiMovies(0)
                       sideScroll.visible = true
                       sideMenu.menu = 1
                   }
               }
           }
           Rectangle {
               id: btTV
               width: 50
               height: 50
               radius: 25
               color: "transparent"
               Text {
                   color: sideMenu.menu === 2?"red":"white"
                   anchors.centerIn: parent
                   font.pointSize: 25
                   font.family: fontAwesome.name
                   text: FontAwesome.icons.fa_desktop
               }
               MouseArea {
                   anchors.fill: parent
                   onClicked: {
                       controller.getKodiTvShows(0)
                       sideScroll.visible = true
                       sideMenu.menu = 2
                   }
               }
           }
           Rectangle {
               id: btMusic
               width: 50
               height: 50
               radius: 25
               color: "transparent"
               Text {
                   color: sideMenu.menu === 3?"red":"white"
                   anchors.centerIn: parent
                   font.pointSize: 25
                   font.family: fontAwesome.name
                   text: FontAwesome.icons.fa_headphones
               }
               MouseArea {
                   anchors.fill: parent
                   onClicked: {
                        sideScroll.visible = false
                        sideMenu.menu = 3
                   }
               }
           }
           Rectangle {
               id: btImage
               width: 50
               height: 50
               radius: 25
               color: "transparent"
               Text {
                   color: sideMenu.menu === 4?"red":"white"
                   anchors.centerIn: parent
                   font.pointSize: 25
                   font.family: fontAwesome.name
                   text: FontAwesome.icons.fa_image
               }
               MouseArea {
                   anchors.fill: parent
                   onClicked: {
                       sideScroll.visible = false
                       sideMenu.menu = 4
                   }
               }
           }
           Rectangle {
               id: btFileList
               width: 50
               height: 50
               radius: 25
               color: "transparent"
               Text {
                   color: sideMenu.menu === 5?"red":"white"
                   anchors.centerIn: parent
                   font.pointSize: 25
                   font.family: fontAwesome.name
                   text: FontAwesome.icons.fa_file_video_o
               }
               MouseArea {
                   anchors.fill: parent
                   onClicked: {
                        sideScroll.visible = false
                        sideMenu.menu = 5
                   }
               }
           }
       }
    }

}

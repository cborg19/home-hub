#ifndef FOODCONTROLLER_H
#define FOODCONTROLLER_H

#include <QObject>
#include <QQmlListProperty>
#include <QThreadPool>
#include <QJsonDocument>
#include <QMutex>
#include <QTimer>

class AsyncRecipeResponse : public QObject, public QRunnable
{
    Q_OBJECT
public:
    AsyncRecipeResponse(const QString &path, const QString &basic, const QString &type)
        : m_type(type), m_path(path), m_basic(basic)
    {

    }

    void run() override;
signals:
    void listChanged(QVariantList, QString type);
private:
    QString m_type, m_path, m_basic;
    QVariantList m_list;

    void parseCategory(QJsonDocument);
    void parseList(QJsonDocument);
    void parseRecipe(QJsonDocument);
};

class FoodController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList CategoryList READ getCategoryList NOTIFY categoryListChanged)
    Q_PROPERTY(QVariantList RecipeList READ getRecipeList NOTIFY recipeListChanged)

    Q_PROPERTY(QVariantList CurrentRecipe READ getCurrentRecipe NOTIFY currentRecipeChanged)
public:
    explicit FoodController(QObject *parent = nullptr);

    void setSettings(bool enabled, QString ip, QString user, QString password, int port);

    QVariantList getCategoryList(){ return m_category; }
    QVariantList getRecipeList(){ return m_recipes; }
    QVariantList getCurrentRecipe(){ return m_recipe; }

    void setEnabled(bool);
    bool getEnabled();

    QStringList getSettings();
    void setSettings(QStringList);

    void getAllCategory();
    void getCategory(QString id);
    void getByIngredient(QString id);
    void getRecipe(int path);

    bool processVoiceCmd(QString cmd, QString &response);
signals:
    void categoryListChanged();
    void recipeListChanged();
    void currentRecipeChanged();
    void pageTrigger(QString page, int index);
public slots:
private slots:
    void listChanged(QVariantList, QString);
private:
    QString m_basic;
    QString m_ip;
    int m_port;
    QString m_uname;
    QString m_password;
    QString m_token;

    bool m_enabled;

    QVariantList m_category, m_recipes;
    QVariantList m_recipe;

    int currentStep;

    QThreadPool pool;

    void login();

    bool wordInterger(QString word, int & value);
};

#endif // FOODCONTROLLER_H
